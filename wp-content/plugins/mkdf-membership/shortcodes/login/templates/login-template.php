<div class="mkdf-social-login-holder">
    <div class="mkdf-social-login-holder-outer">
        <div class="mkdf-social-login-holder-inner">
            <form method="post" class="mkdf-login-form">
                <?php
                $redirect = '';
                if ( isset( $_GET['redirect_uri'] ) ) {
                    $redirect = $_GET['redirect_uri'];
                } ?>
                <fieldset>
                    <div>
                        <label class="mkdf-username-label"><?php esc_html_e( 'Username', 'mkdf-membership' ) ?></label>
                        <input type="text" name="user_login_name" id="user_login_name"  value="" required pattern=".{3,}" title="<?php esc_attr_e( 'Three or more characters', 'mkdf-membership' ); ?>"/>
                    </div>
                    <div>
                        <label class="mkdf-password-label"><?php esc_html_e( 'Password', 'mkdf-membership' ) ?></label>
                        <input type="password" name="user_login_password" id="user_login_password" value="" required/>
                    </div>
                    <div class="mkdf-lost-pass-remember-holder clearfix">
                        <div class="mkdf-remember-holder">
                            <span class="mkdf-login-remember">
                                <input name="rememberme" value="forever" id="rememberme" type="checkbox"/>
                                <label for="rememberme" class="mkdf-checbox-label"><?php esc_html_e( 'Remember me', 'mkdf-membership' ) ?></label>
                            </span>
                        </div>
                        <div class="mkdf-lost-pass-holder">
                            <a href="#" class="mkdf-modal-opener" data-modal="password"><?php esc_html_e( 'Lost your password?', 'mkdf-membership' ); ?></a>
                        </div>
                    </div>
                    <input type="hidden" name="redirect" id="redirect" value="<?php echo esc_url( $redirect ); ?>">
                    <div class="mkdf-login-button-holder">
                        <?php
                        if ( mkdf_membership_theme_installed() ) {
                            echo iacademy_mikado_get_button_html( array(
                                'html_type' => 'button',
                                'text'      => esc_html__( 'Login', 'mkdf-membership' ),
                                'type'      => 'solid',
                                'size'      => 'large'
                            ) );
                        } else {
                            echo '<button type="submit">' . esc_html__( 'Login', 'mkdf-membership' ) . '</button>';
                        }
                        ?>
                        <?php wp_nonce_field( 'mkdf-ajax-login-nonce', 'mkdf-login-security' ); ?>
                    </div>
                    <div class="mkdf-register-link-holder">
                        <span class="mkdf-register-label">
                            <?php esc_html_e( 'Not a member yet?', 'mkdf-membership' ); ?>
                        </span>
                        <a href="#" class="mkdf-modal-opener" data-modal="register"><?php esc_html_e( 'Register Now', 'mkdf-membership' ); ?></a>
                    </div>
                </fieldset>
            </form>
        </div>
        <?php
        if(mkdf_membership_theme_installed()) {
            //if social login enabled add social networks login
            $social_login_enabled = iacademy_mikado_options()->getOptionValue('enable_social_login') == 'yes' ? true : false;
            if($social_login_enabled) { ?>
                <div class="mkdf-login-form-social-login">
                    <div class="mkdf-login-social-title">
                        <span><?php esc_html_e('Recommended', 'mkdf-membership'); ?></span>
                    </div>
                    <div class="mkdf-login-social-networks">
                        <?php do_action('mkdf_membership_social_network_login'); ?>
                    </div>
                    <div class="mkdf-login-social-info">
                        <?php esc_html_e('Connect with Social Networks', 'mkdf-membership'); ?>
                    </div>
                </div>
            <?php }
        }
        ?>
    </div>
    <?php
    do_action( 'mkdf_membership_action_login_ajax_response' );
    ?>
</div>