<div class="mkdf-register-notice">
	<h5 class="mkdf-register-notice-title"><?php echo esc_html($message); ?></h5>
	<a href="#" class="mkdf-login-action-btn mkdf-modal-opener" data-modal="login" data-title="<?php esc_attr_e('LOGIN', 'mkdf-membership'); ?>"><?php esc_html_e('LOGIN', 'mkdf-membership'); ?></a>
</div>