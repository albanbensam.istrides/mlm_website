<div class="mkdf-register-holder mkdf-modal-holder" data-modal="register">
    <div class="mkdf-register-content mkdf-modal-content">
        <div class="mkdf-register-content-inner mkdf-modal-content-inner" id="mkdf-register-content">
            <h3><?php esc_html_e("User registration", "mikado-membership") ?></h3>
            <div class="mkdf-wp-register-holder">
                <?php echo mkdf_membership_execute_shortcode( 'mkdf_user_register', array() ) ?>
            </div>
        </div>
    </div>
</div>