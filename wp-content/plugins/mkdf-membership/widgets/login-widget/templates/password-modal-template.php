<div class="mkdf-password-holder mkdf-modal-holder" data-modal="password">
    <div class="mkdf-password-content mkdf-modal-content">
        <div class="mkdf-reset-pass-content-inner mkdf-modal-content-inner" id="mkdf-reset-pass-content">
            <h3><?php esc_html_e("Reset password", "mikado-membership") ?></h3>
            <div class="mkdf-wp-reset-pass-holder">
                <?php echo mkdf_membership_execute_shortcode( 'mkdf_user_reset_password', array() ) ?>
            </div>
        </div>
    </div>
</div>