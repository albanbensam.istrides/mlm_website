<div class="mkdf-login-holder mkdf-modal-holder" data-modal="login">
	<div class="mkdf-login-content mkdf-modal-content">
		<div class="mkdf-login-content-inner mkdf-modal-content-inner">
            <h3><?php esc_html_e("User login", "mikado-membership") ?></h3>
			<div class="mkdf-wp-login-holder">
                <?php echo mkdf_membership_execute_shortcode( 'mkdf_user_login', array() ); ?>
            </div>
		</div>
	</div>
</div>