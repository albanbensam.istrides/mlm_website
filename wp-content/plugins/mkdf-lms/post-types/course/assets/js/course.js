(function($) {
    'use strict';

    var course = {};
    mkdf.modules.course = course;

	course.mkdfOnDocumentReady = mkdfOnDocumentReady;
	course.mkdfOnWindowLoad = mkdfOnWindowLoad;
	course.mkdfOnWindowResize = mkdfOnWindowResize;
	course.mkdfOnWindowScroll = mkdfOnWindowScroll;

    $(document).ready(mkdfOnDocumentReady);
    $(window).load(mkdfOnWindowLoad);
    $(window).resize(mkdfOnWindowResize);
    $(window).scroll(mkdfOnWindowScroll);
    
    /* 
     All functions to be called on $(document).ready() should be in this function
     */
    function mkdfOnDocumentReady() {
	    mkdfInitCoursePopup();
	    mkdfInitCoursePopupClose();
	    mkdfCompleteItem();
	    mkdfCourseAddToWishlist();
	    mkdfRetakeCourse();
	    mkdfSearchCourses();
	    mkdfInitCourseList();
	    mkdfInitAdvancedCourseSearch();
    }

    /*
     All functions to be called on $(window).load() should be in this function
     */
    function mkdfOnWindowLoad() {
        mkdfInitCourseListAnimation();
        mkdfInitCoursePagination().init();
    }

    /*
     All functions to be called on $(window).resize() should be in this function
     */
    function mkdfOnWindowResize() {

    }

    /*
     All functions to be called on $(window).scroll() should be in this function
     */
    function mkdfOnWindowScroll() {
        mkdfInitCoursePagination().scroll();
    }


    function mkdfInitCoursePopup(){
	    var elements = $('.mkdf-element-link-open');
	    var popup = $('.mkdf-course-popup');
	    var popupContent = $('.mkdf-popup-content');

        if(elements.length){
	        elements.each(function(){
				var element = $(this);
		        element.on('click', function(e){
			        e.preventDefault();
			        if(!popup.hasClass('mkdf-course-popup-opened')){
				        popup.addClass('mkdf-course-popup-opened');
				        mkdf.modules.common.mkdfDisableScroll();
			        }
			        var courseId = 0;
			        if(typeof element.data('course-id') !== 'undefined' && element.data('course-id') !== false) {
				        courseId = element.data('course-id');
			        }
                    mkdfPopupScroll();
			        mkdfLoadElementItem(element.data('item-id'),courseId, popupContent);
		        });
	        });
        }
    }
	function mkdfInitCourseItemsNavigation(){
		var elements = $('.mkdf-course-popup-navigation .mkdf-element-link-open');
		var popupContent = $('.mkdf-popup-content');

		if(elements.length){
			elements.each(function(){
				var element = $(this);
				element.on('click', function(e){
					e.preventDefault();
					var courseId = 0;
					if(typeof element.data('course-id') !== 'undefined' && element.data('course-id') !== false) {
						courseId = element.data('course-id');
					}
					mkdfLoadElementItem(element.data('item-id'),courseId, popupContent);
				});
			});
		}
	}

	function mkdfInitCoursePopupClose(){
		var closeButton = $('.mkdf-course-popup-close');
		var popup = $('.mkdf-course-popup');
		if(closeButton.length){
			closeButton.on('click', function(e){
				e.preventDefault();
				popup.removeClass('mkdf-course-popup-opened');
				location.reload();
			});
		}
	}

	function mkdfLoadElementItem(id ,courseId, container){
        var preloader = container.prevAll('.mkdf-course-item-preloader');
        preloader.removeClass('mkdf-hide');
		var ajaxData = {
			action: 'mkdf_lms_load_element_item',
			item_id : id,
			course_id : courseId
		};
		$.ajax({
			type: 'POST',
			data: ajaxData,
			url: mkdfGlobalVars.vars.mkdfAjaxUrl,
			success: function (data) {
				var response = JSON.parse(data);
				if(response.status == 'success'){
					container.html(response.data.html);
					mkdfInitCourseItemsNavigation();
					mkdfCompleteItem();
					mkdfSearchCourses();
                    mkdf.modules.quiz.mkdfStartQuiz();
                    mkdf.modules.common.mkdfFluidVideo();
                    preloader.addClass('mkdf-hide');
				} else {
				    alert("An error occurred");
                    preloader.addClass('mkdf-hide');
                }

			}
		});

	}

	function mkdfCompleteItem(){

		$('.mkdf-lms-complete-item-form').on('submit',function(e) {

			e.preventDefault();
			var form = $(this);
			var itemID = $(this).find( "input[name$='mkdf_lms_item_id']").val();
			var formData = form.serialize();
			var ajaxData = {
				action: 'mkdf_lms_complete_item',
				post: formData
			};

			$.ajax({
				type: 'POST',
				data: ajaxData,
				url: mkdfGlobalVars.vars.mkdfAjaxUrl,
				success: function (data) {
					var response = JSON.parse(data);
					if(response.status == 'success'){

						form.replaceWith(response.data['content_message']);
						var elements =  $('.mkdf-section-element.mkdf-section-lesson');
						elements.each(function () {
							if($(this).data('section-element-id') == itemID){
								$(this).addClass('mkdf-item-completed')
							}
						})
					}
				}
			});
		});

	}

	function mkdfRetakeCourse(){

		$('.mkdf-lms-retake-course-form').on('submit',function(e) {

			e.preventDefault();
			var form = $(this);
			var formData = form.serialize();
			var ajaxData = {
				action: 'mkdf_lms_retake_course',
				post: formData
			};

			$.ajax({
				type: 'POST',
				data: ajaxData,
				url: mkdfGlobalVars.vars.mkdfAjaxUrl,
				success: function (data) {
					var response = JSON.parse(data);
					if(response.status == 'success'){
						alert(response.message);
                        location.reload();
					}
				}
			});
		});

	}

	function mkdfPopupScroll(){


        var mainHolder = $('.mkdf-course-popup');

        /* Content items */
		var content = $('.mkdf-popup-content');
		var contentHolder = $('.mkdf-course-popup-inner');
		var contentHeading = $('.mkdf-popup-heading');

		/* Navigation items */
        var navigationHolder = $('.mkdf-course-popup-items');
        var navigationWrapper = $('.mkdf-popup-info-wrapper');
        var searchHolder = $('.mkdf-lms-search-holder');

        if(mkdf.windowWidth > 1024) {
            if (content.length) {
                content.height(mainHolder.height() - contentHeading.outerHeight());
                content.perfectScrollbar({
                    wheelSpeed: 0.6,
                    suppressScrollX: true
                });
            }

            if (navigationHolder.length) {
                navigationHolder.height(mainHolder.height() - parseInt(navigationWrapper.css('padding-top')) - parseInt(navigationWrapper.css('padding-bottom')) - searchHolder.outerHeight(true));
                navigationHolder.perfectScrollbar({
                    wheelSpeed: 0.6,
                    suppressScrollX: true
                });
            }
        } else {
            contentHolder.find('.mkdf-grid-row').height(mainHolder.height());
            contentHolder.find('.mkdf-grid-row').perfectScrollbar({
                wheelSpeed: 0.6,
                suppressScrollX: true
            });
        }

		return true

	}

	function mkdfCourseAddToWishlist(){

		$('.mkdf-course-wishlist').on('click',function(e) {
			e.preventDefault();
			var course = $(this),
				courseId;

			if(typeof course.data('course-id') !== 'undefined') {
				courseId = course.data('course-id');
			}

            mkdfCoursewishlistAdding(course, courseId);

		});

	}

	function mkdfCoursewishlistAdding(course, courseId){

		var ajaxData = {
			action: 'mkdf_lms_add_course_to_wishlist',
			course_id : courseId
		};

		$.ajax({
			type: 'POST',
			data: ajaxData,
			url: mkdfGlobalVars.vars.mkdfAjaxUrl,
			success: function (data) {
				var response = JSON.parse(data);
				if(response.status == 'success'){
                    if(!course.hasClass('mkdf-icon-only')) {
                        course.find('span').text(response.data.message);
                    }
                    course.find('i').removeClass('icon_heart_alt icon_heart').addClass(response.data.icon);
				}
			}
		});

		return false;

	}

	function mkdfSearchCourses(){

        var courseSearchHolder = $('.mkdf-lms-search-holder');

        if (courseSearchHolder.length) {
            courseSearchHolder.each(function () {
                var thisSearch = $(this),
                    searchField = thisSearch.find('.mkdf-lms-search-field'),
                    resultsHolder = thisSearch.find('.mkdf-lms-search-results'),
                    searchLoading = thisSearch.find('.mkdf-search-loading'),
                    searchIcon = thisSearch.find('.mkdf-search-icon');

                searchLoading.addClass('mkdf-hidden');

                var keyPressTimeout;

                searchField.on('keyup paste', function(e) {
                    var field = $(this);
                    field.attr('autocomplete','off');
                    searchLoading.removeClass('mkdf-hidden');
                    searchIcon.addClass('mkdf-hidden');
                    clearTimeout(keyPressTimeout);

                    keyPressTimeout = setTimeout( function() {
                        var searchTerm = field.val();
                        if(searchTerm.length < 3) {
                            resultsHolder.html('');
                            resultsHolder.fadeOut();
                            searchLoading.addClass('mkdf-hidden');
                            searchIcon.removeClass('mkdf-hidden');
                        } else {
                            var ajaxData = {
                                action: 'mkdf_lms_search_courses',
                                term: searchTerm
                            };

                            $.ajax({
                                type: 'POST',
                                data: ajaxData,
                                url: mkdfGlobalVars.vars.mkdfAjaxUrl,
                                success: function (data) {
                                    var response = JSON.parse(data);
                                    if (response.status == 'success') {
                                        searchLoading.addClass('mkdf-hidden');
                                        searchIcon.removeClass('mkdf-hidden');
                                        resultsHolder.html(response.data.html);
                                        resultsHolder.fadeIn();
                                    }
                                },
                                error: function(XMLHttpRequest, textStatus, errorThrown) {
                                    console.log("Status: " + textStatus);
                                    console.log("Error: " + errorThrown);
                                    searchLoading.addClass('mkdf-hidden');
                                    searchIcon.removeClass('mkdf-hidden');
                                    resultsHolder.fadeOut();
                                }
                            });
                        }
                    }, 500);
                });

                searchField.on('focusout', function () {
                    searchLoading.addClass('mkdf-hidden');
                    searchIcon.removeClass('mkdf-hidden');
                    resultsHolder.fadeOut();
                });
            });
        }

	}

    /**
     * Initializes course pagination functions
     */
    function mkdfInitCoursePagination(){
        var courseList = $('.mkdf-course-list-holder');

        var initStandardPagination = function(thisCourseList) {
            var standardLink = thisCourseList.find('.mkdf-cl-standard-pagination li');

            if(standardLink.length) {
                standardLink.each(function(){
                    var thisLink = $(this).children('a'),
                        pagedLink = 1;

                    thisLink.on('click', function(e) {
                        e.preventDefault();
                        e.stopPropagation();

                        if (typeof thisLink.data('paged') !== 'undefined' && thisLink.data('paged') !== false) {
                            pagedLink = thisLink.data('paged');
                        }

                        initMainPagFunctionality(thisCourseList, pagedLink);
                    });
                });
            }
        };

        var initLoadMorePagination = function(thisCourseList) {
            var loadMoreButton = thisCourseList.find('.mkdf-cl-load-more a');

            loadMoreButton.on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();

                initMainPagFunctionality(thisCourseList);
            });
        };

        var initInifiteScrollPagination = function(thisCourseList) {
            var courseListHeight = thisCourseList.outerHeight(),
                courseListTopOffest = thisCourseList.offset().top,
                courseListPosition = courseListHeight + courseListTopOffest - mkdfGlobalVars.vars.mkdfAddForAdminBar;

            if(!thisCourseList.hasClass('mkdf-cl-infinite-scroll-started') && mkdf.scroll + mkdf.windowHeight > courseListPosition) {
                initMainPagFunctionality(thisCourseList);
            }
        };

        var initMainPagFunctionality = function(thisCourseList, pagedLink) {
            var thisCourseListInner = thisCourseList.find('.mkdf-cl-inner'),
                nextPage,
                maxNumPages;

            if (typeof thisCourseList.data('max-num-pages') !== 'undefined' && thisCourseList.data('max-num-pages') !== false) {
                maxNumPages = thisCourseList.data('max-num-pages');
            }

            if(thisCourseList.hasClass('mkdf-cl-pag-standard')) {
                thisCourseList.data('next-page', pagedLink);
            }

            if(thisCourseList.hasClass('mkdf-cl-pag-infinite-scroll')) {
                thisCourseList.addClass('mkdf-cl-infinite-scroll-started');
            }

            var loadMoreData = mkdf.modules.common.getLoadMoreData(thisCourseList),
                loadingItem = thisCourseList.find('.mkdf-cl-loading');

            nextPage = loadMoreData.nextPage;

            if(nextPage <= maxNumPages || maxNumPages == 0){
                if(thisCourseList.hasClass('mkdf-cl-pag-standard')) {
                    loadingItem.addClass('mkdf-showing mkdf-standard-pag-trigger');
                    thisCourseList.addClass('mkdf-cl-pag-standard-animate');
                } else {
                    loadingItem.addClass('mkdf-showing');
                }

                var ajaxData = mkdf.modules.common.setLoadMoreAjaxData(loadMoreData, 'mkdf_lms_course_ajax_load_more');

                $.ajax({
                    type: 'POST',
                    data: ajaxData,
                    url: mkdfGlobalVars.vars.mkdfAjaxUrl,
                    success: function (data) {
                        if(!thisCourseList.hasClass('mkdf-cl-pag-standard')) {
                            nextPage++;
                        }

                        thisCourseList.data('next-page', nextPage);

                        var response = $.parseJSON(data),
                            responseHtml =  response.html,
                            minValue = response.minValue,
                            maxValue = response.maxValue;

                        if(thisCourseList.hasClass('mkdf-cl-pag-standard') || pagedLink == 1) {
                            mkdfInitStandardPaginationLinkChanges(thisCourseList, maxNumPages, nextPage);
                            mkdfInitHtmlGalleryNewContent(thisCourseList, thisCourseListInner, loadingItem, responseHtml);
                            mkdfInitPostsCounterChanges(thisCourseList, minValue, maxValue);
                        } else {
                            mkdfInitAppendGalleryNewContent(thisCourseListInner, loadingItem, responseHtml);
                            mkdfInitPostsCounterChanges(thisCourseList, 1, maxValue);
                        }

                        if(thisCourseList.hasClass('mkdf-cl-infinite-scroll-started')) {
                            thisCourseList.removeClass('mkdf-cl-infinite-scroll-started');
                        }
                    }
                });
            }

            if(pagedLink == 1) {
                thisCourseList.find('.mkdf-cl-load-more-holder').show();
            }

            if(nextPage === maxNumPages){
                thisCourseList.find('.mkdf-cl-load-more-holder').hide();
            }
        };

        var mkdfInitStandardPaginationLinkChanges = function(thisCourseList, maxNumPages, nextPage) {
            var standardPagHolder = thisCourseList.find('.mkdf-cl-standard-pagination'),
                standardPagNumericItem = standardPagHolder.find('li.mkdf-cl-pag-number'),
                standardPagPrevItem = standardPagHolder.find('li.mkdf-cl-pag-prev a'),
                standardPagNextItem = standardPagHolder.find('li.mkdf-cl-pag-next a');

            standardPagNumericItem.removeClass('mkdf-cl-pag-active');
            standardPagNumericItem.eq(nextPage-1).addClass('mkdf-cl-pag-active');

            standardPagPrevItem.data('paged', nextPage-1);
            standardPagNextItem.data('paged', nextPage+1);

            if(nextPage > 1) {
                standardPagPrevItem.css({'opacity': '1'});
            } else {
                standardPagPrevItem.css({'opacity': '0'});
            }

            if(nextPage === maxNumPages) {
                standardPagNextItem.css({'opacity': '0'});
            } else {
                standardPagNextItem.css({'opacity': '1'});
            }
        };

        var mkdfInitPostsCounterChanges = function(thisCourseList, minValue, maxValue) {
            var postsCounterHolder = thisCourseList.find('.mkdf-course-items-counter');
            var minValueHolder = postsCounterHolder.find('.counter-min-value');
            var maxValueHolder = postsCounterHolder.find('.counter-max-value');
            minValueHolder.text(minValue);
            maxValueHolder.text(maxValue);
        };

        var mkdfInitHtmlGalleryNewContent = function(thisCourseList, thisCourseListInner, loadingItem, responseHtml) {
            loadingItem.removeClass('mkdf-showing mkdf-standard-pag-trigger');
            thisCourseListInner.waitForImages(function() {
                thisCourseList.removeClass('mkdf-cl-pag-standard-animate');
                thisCourseListInner.html(responseHtml);
                mkdfInitCourseListAnimation();
                mkdf.modules.common.mkdfInitParallax();
            });
        };

        var mkdfInitAppendGalleryNewContent = function(thisCourseListInner, loadingItem, responseHtml) {
            loadingItem.removeClass('mkdf-showing');
            thisCourseListInner.waitForImages(function() {
                thisCourseListInner.append(responseHtml);
                mkdfInitCourseListAnimation();
                mkdf.modules.common.mkdfInitParallax();
            });
        };

        return {
            init: function() {
                if(courseList.length) {
                    courseList.each(function() {
                        var thisCourseList = $(this);

                        if(thisCourseList.hasClass('mkdf-cl-pag-standard')) {
                            initStandardPagination(thisCourseList);
                        }

                        if(thisCourseList.hasClass('mkdf-cl-pag-load-more')) {
                            initLoadMorePagination(thisCourseList);
                        }

                        if(thisCourseList.hasClass('mkdf-cl-pag-infinite-scroll')) {
                            initInifiteScrollPagination(thisCourseList);
                        }
                    });
                }
            },
            scroll: function() {
                if(courseList.length) {
                    courseList.each(function() {
                        var thisCourseList = $(this);

                        if(thisCourseList.hasClass('mkdf-cl-pag-infinite-scroll')) {
                            initInifiteScrollPagination(thisCourseList);
                        }
                    });
                }
            },
            getMainPagFunction: function(thisCourseList, paged) {
                initMainPagFunctionality(thisCourseList, paged);
            }
        };
    }

    /**
     * Initializes portfolio list article animation
     */
    function mkdfInitCourseListAnimation(){
        var courseList = $('.mkdf-course-list-holder.mkdf-cl-has-animation');

        if(courseList.length){
            courseList.each(function(){
                var thisCourseList = $(this).children('.mkdf-cl-inner');

                thisCourseList.children('article').each(function(l) {
                    var thisArticle = $(this);

                    thisArticle.appear(function() {
                        thisArticle.addClass('mkdf-item-show');

                        setTimeout(function(){
                            thisArticle.addClass('mkdf-item-shown');
                        }, 1000);
                    },{accX: 0, accY: 0});
                });
            });
        }
    }

    function mkdfInitCourseList() {
        var courseLists = $('.mkdf-course-list-holder');
        if (courseLists.length) {
            courseLists.each(function () {
                var thisList = $(this);
                if (thisList.hasClass('mkdf-cl-has-filter')) {
                    mkdfInitCourseLayoutChange(thisList);
                    mkdfInitCourseLayoutOrdering(thisList);
                }
            })
        }
    }

    function mkdfInitCourseLayoutOrdering(thisList) {
        var filter = thisList.find('.mkdf-cl-filter-holder .mkdf-course-order-filter');
        filter.select2({
            minimumResultsForSearch: -1
        }).on('select2:select', function (evt) {
            var dataAtts = evt.params.data.element.dataset;
            var type = dataAtts.type;
            var order = dataAtts.order;
            thisList.data('order-by', type);
            thisList.data('order', order);
            thisList.data('next-page', 1);
            mkdfInitCoursePagination().getMainPagFunction(thisList, 1);
        });
    }

    function mkdfInitCourseLayoutChange(thisList) {
        var filter = thisList.find('.mkdf-cl-filter-holder .mkdf-course-layout-filter');
        var filterElements = filter.find('span');
        if (filter.length > 0) {
            filterElements.on('click',function() {
                filterElements.removeClass('mkdf-active');
                var thisFilter = $(this);
                thisFilter.addClass('mkdf-active');
                var type = thisFilter.data('type');
                thisList.removeClass('mkdf-cl-gallery mkdf-cl-simple');
                thisList.addClass('mkdf-cl-' + type);
            });
        }
    }

    function mkdfInitAdvancedCourseSearch() {
        var advancedCoursSearches = $('.mkdf-advanced-course-search');
        if (advancedCoursSearches.length) {
            advancedCoursSearches.each(function () {
                var thisSearch = $(this);
                var select = thisSearch.find('select');
                if(select.length) {
                    select.each(function() {
                        var thisSelect = $(this);
                        thisSelect.select2({
                            minimumResultsForSearch: -1
                        });
                        thisSelect.next().addClass(thisSelect.attr('name'));
                    });
                }
            })
        }
    }

})(jQuery);