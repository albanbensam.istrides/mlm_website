<?php
$tabs = apply_filters( 'iacademy_mikado_single_course_tabs', array() );
if ( ! empty( $tabs ) ) :
?>

<div class="mkdf-tabs mkdf-tabs-standard">
    <ul class="mkdf-tabs-nav clearfix">
        <?php foreach ( $tabs as $key => $tab ) : ?>
            <?php if(isset($tab['link'])) { ?>
            <li class="mkdf-custom-tab-link">
                <a class="mkdf-external-link" href="<?php echo esc_url( $tab['link'] ); ?>">
            <?php } else { ?>
            <li class="<?php echo esc_attr( $key ); ?>_tab">
                <a href="#tab-<?php echo esc_attr( $key ); ?>">
                <?php } ?>
                    <span class="mkdf-tab-icon">
                        <?php echo iacademy_mikado_get_module_part($tab['icon']); ?>
                    </span>
                    <span class="mkdf-tab-title">
                        <?php echo apply_filters( 'iacademy_mikado_sc_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?>
                    </span>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
    <?php foreach ( $tabs as $key => $tab ) : ?>
        <?php if(!isset($tab['link'])) { ?>
        <div class="mkdf-tab-container" id="tab-<?php echo sanitize_title($key); ?>">
            <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/' . $tab['template'], 'course', '', $params); ?>
        </div>
        <?php } ?>
    <?php endforeach; ?>
</div>

<?php endif; ?>