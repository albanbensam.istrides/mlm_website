<form action="" method="post" class="mkdf-lms-retake-course-form">
	<input type="hidden" name="mkdf_lms_course_id" value="<?php echo get_the_ID(); ?>" />
	<?php if(mkdf_lms_core_plugin_installed()) { ?>
		<?php echo iacademy_mikado_get_button_html(array(
			'html_type' 	=> 'input',
			'text'			=> esc_html__('Retake', 'mkdf-lms'),
			'input_name'	=> 'submit'
		)); ?>
	<?php } else { ?>
		<input name="submit" type="submit" value="<?php echo esc_html__('Retake', 'mkdf-lms'); ?>" />
	<?php } ?>
</form>
