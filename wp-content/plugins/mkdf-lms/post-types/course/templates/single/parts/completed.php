<?php if(mkdf_lms_core_plugin_installed()) { ?>
	<?php echo iacademy_mikado_get_button_html(array(
		'text'			=> esc_html__('Completed', 'mkdf-lms'),
		'link'			=> 'javascript:void(0)'
	)); ?>
<?php } else { ?>
	<a href="javascript:void(0)" class="mkdf-btn mkdf-btn-medium mkdf-btn-solid"><?php echo esc_html__('Completed', 'mkdf-lms'); ?></a>
<?php } ?>