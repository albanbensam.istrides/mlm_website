<?php if (isset($instructor) & !empty($instructor)) { ?>
<div class="mkdf-grid-col-4 mkdf-course-info-wrapper">
    <div class="mkdf-course-instructor">
        <div class="mkdf-instructor-image">
            <?php echo get_the_post_thumbnail($instructor, array(80,80)); ?>
        </div>
        <div class="mkdf-instructor-info">
            <span class="mkdf-instructor-label">
                <?php esc_html_e('Instructor:', 'mkdf-lms') ?>
            </span>
            <a itemprop="url" href="<?php echo get_permalink($instructor); ?>" target="_self">
                <span class="mkdf-instructor-name">
                    <?php echo get_the_title($instructor); ?>
                </span>
            </a>
        </div>
    </div>
</div>
<?php } ?>