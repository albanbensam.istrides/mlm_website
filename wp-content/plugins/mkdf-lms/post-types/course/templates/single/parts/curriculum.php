<?php
$course_sections = get_post_meta(get_the_ID(), 'mkdf_course_curriculum', true);
$course_curriculum_desc = get_post_meta(get_the_ID(), 'mkdf_course_curriculum_desc_meta', true);
if(!empty($course_sections)) { ?>
    <div class="mkdf-course-curriculum">
        <div class="mkdf-curriculum-description">
            <h4 class="mkdf-curriculum-title"><?php esc_html_e('Syllabus', 'mkdf-lms'); ?></h4>
            <p><?php echo esc_attr( $course_curriculum_desc); ?></p>
        </div>
        <div class="mkdf-course-curriculum-list">
            <?php foreach($course_sections as $course_section) { ?>
                <div class="mkdf-curriculum-section">
                    <h5 class="mkdf-section-name">
                        <?php echo esc_html($course_section['section_name']) ?>
                    </h5>
                    <div class="mkdf-section-content">
                        <h6 class="mkdf-section-title">
                            <?php echo esc_html($course_section['section_title']) ?>
                        </h6>
                        <p class="mkdf-section-description">
                            <?php echo esc_html($course_section['section_description']) ?>
                        </p>
                        <?php
                        if (isset($course_section['section_elements']) && $course_section['section_elements'] !== ''){
                            $section_elements = $course_section['section_elements'];
                            if(!empty($section_elements)) {
                                $list = mkdf_lms_get_course_curriculum_list($section_elements);
                                $elements = $list['elements'];
                                $lessons_summary = $list['lessons_summary'];
                                ?>
                                <div class="mkdf-section-elements">
                                    <?php if(!empty($lessons_summary)) {
                                        $lesson_info = implode(', ', $lessons_summary);
                                    ?>
                                        <div class="mkdf-section-elements-summary">
                                            <i class="lnr lnr-book" aria-hidden="true"></i> <span class="mkdf-summary-value"><?php echo esc_html($lesson_info); ?></span>
                                        </div>
                                    <?php } ?>
                                    <?php foreach ($elements as $key => $element) {	?>
                                        <div class="mkdf-section-element <?php echo esc_attr($element['class']); ?> clearfix <?php echo mkdf_lms_get_course_item_completed_class($element['id']); ?>" data-section-element-id="<?php echo esc_attr($element['id']); ?>">
                                            <div class="mkdf-element-title">
                                                <span class="mkdf-element-icon">
                                                    <?php echo iacademy_mikado_get_module_part($element['icon']); ?>
                                                </span>
                                                <span class="mkdf-element-label">
                                                    <?php echo esc_attr($element['label']); ?>
                                                </span>
                                                <?php if(mkdf_lms_course_is_preview_available($element['id'])) { ?>
                                                <a class="mkdf-element-name mkdf-element-link-open" itemprop="url" href="<?php echo esc_url($element['url']); ?>" title="<?php echo esc_attr($element['title']); ?>" data-item-id="<?php echo esc_attr($element['id']); ?>" data-course-id="<?php echo get_the_ID(); ?>"  >
                                                    <?php echo esc_html($element['title']); ?><?php if(!mkdf_lms_user_has_course() || !mkdf_lms_user_completed_prerequired_course()) { ?> <span class="mkdf-element-preview-holder"><?php esc_html_e('preview', 'mkdf-lms'); ?></span> <?php } ?>
                                                </a>
                                                <?php } else { ?>
                                                    <?php echo esc_html($element['title']); ?>
                                                <?php } ?>
                                            </div>
                                            <div class="mkdf-element-info">
                                                <?php if($element['class'] !== 'mkdf-section-quiz') {?>
                                                <span class="mkdf-element-clock-icon lnr lnr-clock"></span>
                                                <?php } ?>
                                                <span class="mkdf-element-extra-info-value">
                                                    <?php echo esc_html($element['extra_info_value']); ?>
                                                </span>
                                                <span class="mkdf-element-extra-info-unit">
                                                    <?php echo esc_html($element['extra_info_unit']); ?>
                                                </span>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>
