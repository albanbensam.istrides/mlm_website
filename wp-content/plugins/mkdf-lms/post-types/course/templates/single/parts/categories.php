<?php
$categories   = wp_get_post_terms(get_the_ID(), 'course-category');
if(is_array($categories) && count($categories)) :
?>
<div class="mkdf-grid-col-4 mkdf-course-info-wrapper">
    <div class="mkdf-course-categories">
        <div class="mkdf-course-category-label">
            <?php esc_html_e('Categories:', 'mkdf-lms') ?>
        </div>
        <div class="mkdf-course-category-items">
            <?php foreach($categories as $cat) { ?>
                <a itemprop="url" class="mkdf-course-category" href="<?php echo esc_url(get_term_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
            <?php } ?>
        </div>
       
    </div>
</div>
<?php endif;