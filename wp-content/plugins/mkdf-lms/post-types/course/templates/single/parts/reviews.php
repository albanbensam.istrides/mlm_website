<?php if(comments_open()) { ?>
<div class="mkdf-grid-col-4 mkdf-course-info-wrapper">
    <div class="mkdf-course-reviews">
        <div class="mkdf-course-reviews-label">
            <?php esc_html_e('Reviews:', 'mkdf-lms') ?>
        </div>
        <span class="mkdf-course-stars">
            <?php
                $review_rating = mkdf_lms_course_average_rating();
                for($i=1; $i<=$review_rating; $i++){ ?>
                    <i class="fa fa-star" aria-hidden="true"></i>
            <?php } ?>
		</span>
        <!-- This should change to open tab -->
        <a itemprop="url" class="mkdf-post-info-comments" href="<?php comments_link(); ?>" target="_self">
            <?php comments_number('0 ' . esc_html__('Reviews','mkdf-lms'), '1 '.esc_html__('Review','mkdf-lms'), '% '.esc_html__('Reviews','mkdf-lms') ); ?>
        </a>
    </div>
</div>
<?php } ?>