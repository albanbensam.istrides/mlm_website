<?php if(mkdf_lms_core_plugin_installed()) {
	global $woocommerce;
	$cart_url = wc_get_cart_url();
	?>
	<?php echo iacademy_mikado_get_button_html(array(
		'text'			=> esc_html__('View Cart', 'mkdf-lms'),
		'link'			=> $cart_url
	)); ?>
<?php } else { ?>
	<a href="<?php echo esc_url($cart_url); ?>" class="mkdf-btn mkdf-btn-medium mkdf-btn-solid"><?php echo esc_html__('View Cart', 'mkdf-lms'); ?></a>
<?php } ?>