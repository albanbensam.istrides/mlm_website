
<div class="mkdf-course-reviews-main-title">
	<h4><?php esc_html_e('Ratings and Reviews' , 'mkdf-lms') ?></h4>
</div>

<div class="mkdf-course-reviews-list-top">
	<div class="mkdf-grid-row">
		<div class="mkdf-grid-col-4">
			<div class="mkdf-course-reviews-number-wrapper">
				<span class="mkdf-course-reviews-number"><?php echo mkdf_lms_course_average_rating(); ?></span>
				<span class="mkdf-course-stars-wrapper">
					<span class="mkdf-course-stars">
						<?php
						$review_rating = mkdf_lms_course_average_rating();
						for($i=1; $i<=$review_rating; $i++){ ?>
							<i class="fa fa-star" aria-hidden="true"></i>
						<?php } ?>
					</span>
					<span class="mkdf-course-reviews-count"><?php echo esc_html__('Rated', 'mkdf-lms') . ' ' . mkdf_lms_course_average_rating() . ' ' . esc_html__('out of', 'mkdf-lms') . ' '; comments_number('0 ' . esc_html__('Ratings','mkdf-lms'), '1 '.esc_html__('Rating','mkdf-lms'), '% '.esc_html__('Ratings','mkdf-lms') ); ?></span>
				</span>
			</div>
		</div>
		<div class="mkdf-grid-col-8">
			<div class="mkdf-course-rating-percente-wrapper">
				<?php $ratings_array = mkdf_lms_course_ratings();
				$number = mkdf_lms_course_number_of_ratings();
				foreach ($ratings_array as $item => $value) {
					$percentage = $number == 0 ? 0 : round(($value /$number)*100);
					echo do_shortcode('[mkdf_progress_bar percent="'.$percentage.'" title="'.$item . esc_attr__(' stars', 'mkdf-lms').'"]');
				}
				?>
			</div>
		</div>
	</div>
</div>
<div class="mkdf-course-reviews-list">
	<?php comments_template('', true); ?>
</div>