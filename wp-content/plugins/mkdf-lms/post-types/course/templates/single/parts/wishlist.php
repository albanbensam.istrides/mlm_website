<span class="mkdf-course-wishlist-wrapper">
	<a href="javascript:void(0)" class="mkdf-course-wishlist" data-course-id="<?php echo get_the_ID(); ?>">
        <i class="<?php echo esc_attr($wishlist_icon); ?>"></i>
        <span class="mkdf-course-wishlist-text">
            <?php echo esc_attr($wishlist_text); ?>
        </span>
    </a>
</span>


