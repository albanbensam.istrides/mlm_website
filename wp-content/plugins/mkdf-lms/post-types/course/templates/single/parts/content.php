<div class="mkdf-course-content">
    <h4><?php esc_html_e('About this course', 'mkdf-lms') ?></h4>
    <?php the_content(); ?>
</div>