<?php if(isset($prerequired) && !empty($prerequired)) { ?>
    <div class="mkdf-course-prerequired-info">
        <a itemprop="url" target="_self" href="<?php the_permalink($prerequired); ?>"><?php echo esc_html__('Course', 'mkdf-lms') . ' ' . get_the_title($prerequired) . ' ' . esc_html__('must be completed first', 'mkdf-lms'); ?></a>
    </div>
<?php } ?>