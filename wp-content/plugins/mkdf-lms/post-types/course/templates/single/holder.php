<div class="mkdf-container">
    <div class="mkdf-container-inner clearfix">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="mkdf-course-single-holder">
                <?php if(post_password_required()) {
                    echo get_the_password_form();
                } else {

                } ?>
                <div class="mkdf-grid-row">
                    <div <?php echo iacademy_mikado_get_content_sidebar_class(); ?>>
                        <div class="mkdf-course-single-outer">
                            <?php
                            do_action('iacademy_mikado_course_page_before_content');

                            mkdf_lms_get_cpt_single_module_template_part('templates/single/layout-collections/default', 'course', '', $params);

                            do_action('iacademy_mikado_course_page_after_content');
                            ?>
                        </div>
                    </div>
                    <?php if($sidebar_layout !== 'no-sidebar') { ?>
                        <div <?php echo iacademy_mikado_get_sidebar_holder_class(); ?>>
                            <?php get_sidebar(); ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php endwhile; endif; ?>
    </div>
</div>
<?php do_action('mkdf_lms_course_popup'); ?>