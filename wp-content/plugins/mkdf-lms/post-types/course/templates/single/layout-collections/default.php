<div class="mkdf-course-single-wrapper">
    <div class="mkdf-course-title-wrapper">
        <div class="mkdf-course-left-section">
            <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/title', 'course', '', $params); ?>
            <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/course-type', 'course', '', $params); ?>
        </div>
        <div class="mkdf-course-right-section">
            <?php mkdf_lms_get_wishlist_button(); ?>
        </div>
    </div>
    <div class="mkdf-course-basic-info-wrapper">
        <div class="mkdf-grid-row">
            <div class="mkdf-grid-col-9">
                <div class="mkdf-grid-row">
                    <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/instructor', 'course', '', $params); ?>
                    <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/categories', 'course', '', $params); ?>
                    <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/reviews', 'course', '', $params); ?>
                </div>
            </div>
            <div class="mkdf-grid-col-3">
                <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/action', 'course', '', $params); ?>
            </div>
        </div>
    </div>
    <div class="mkdf-course-image-wrapper">
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/image', 'course', '', $params); ?>
    </div>
    <div class="mkdf-course-tabs-wrapper">
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/tabs', 'course', '', $params); ?>
    </div>
</div>