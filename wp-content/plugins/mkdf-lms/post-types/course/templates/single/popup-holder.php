<div class="mkdf-course-popup">
	<div class="mkdf-course-popup-inner">
		<div class="mkdf-grid-row">
            <div class="mkdf-grid-col-8">
                <div class="mkdf-course-item-preloader mkdf-hide">
                    <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
                </div>
                <div class="mkdf-popup-heading">
                    <h5 class="mkdf-course-popup-title"><?php the_title(); ?></h5>
                    <span class="mkdf-course-popup-close"><i class="icon_close"></i></span>
                </div>
                <div class="mkdf-popup-content">

                </div>
            </div>
			<div class="mkdf-grid-col-4">
				<div class="mkdf-popup-info-wrapper">
                    <div class="mkdf-lms-search-holder">
                        <div class="mkdf-lms-search-field-wrapper">
                            <input class="mkdf-lms-search-field" value="" placeholder="<?php esc_attr_e('Search Courses', 'mkdf-lms') ?>">
                            <i class="mkdf-search-icon fa fa-search" aria-hidden="true"></i>
                            <i class="mkdf-search-loading fa fa-spinner fa-spin mkdf-hidden" aria-hidden="true"></i>
                        </div>
                        <div class="mkdf-lms-search-results"></div>
                    </div>
					<?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/popup-items-list', 'course') ?>
				</div>
			</div>
		</div>
	</div>
</div>
