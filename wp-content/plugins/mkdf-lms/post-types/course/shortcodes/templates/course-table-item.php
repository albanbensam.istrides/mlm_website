<tr class="mkdf-ct-item">
    <td class="mkdf-tc-course-field">
        <a href="<?php echo get_the_permalink(); ?>" class="mkdf-cli-title-holder"><?php echo esc_attr(get_the_title()); ?></a>
        <?php echo mkdf_lms_get_cpt_shortcode_module_template_part('course', 'parts/price', '', $params); ?>
    </td>
    <?php if($enable_category == 'yes') { ?>
    <td class="mkdf-tc-category-field">
        <?php echo mkdf_lms_get_cpt_shortcode_module_template_part('course', 'parts/category', '', $params); ?>
    </td>
    <?php } ?>
    <?php if($enable_instructor == 'yes') { ?>
    <td class="mkdf-tc-instructor-field">
        <?php echo mkdf_lms_get_cpt_shortcode_module_template_part('course', 'parts/instructor', '', $params); ?>
    </td>
    <?php } ?>
    <?php if($enable_students == 'yes') { ?>
    <td class="mkdf-tc-students-field">
        <?php echo mkdf_lms_get_cpt_shortcode_module_template_part('course', 'parts/students', '', $params); ?>
    </td>
    <?php } ?>
    <?php if($enable_price == 'yes') { ?>
    <td class="mkdf-tc-price-field">
        <?php echo mkdf_lms_get_cpt_shortcode_module_template_part('course', 'parts/price', '', $params); ?>
    </td>
    <?php } ?>
    <td class="mkdf-tc-button-field">
        <?php echo mkdf_lms_get_cpt_shortcode_module_template_part('course', 'parts/button', '', $params); ?>
    </td>
</tr>