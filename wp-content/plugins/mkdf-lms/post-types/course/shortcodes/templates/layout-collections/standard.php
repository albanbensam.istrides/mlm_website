<div class="mkdf-cli-image-wrapper">
<?php echo mkdf_lms_get_cpt_shortcode_module_template_part('course', 'parts/image', $item_layout, $params); ?>
<?php echo mkdf_lms_get_cpt_shortcode_module_template_part('course', 'parts/wishlist', $item_layout, $params);?>
</div>
<div class="mkdf-cli-text-holder">
	<div class="mkdf-cli-text-wrapper">
		<div class="mkdf-cli-text">
			<div class="mkdf-cli-top-info">
				<?php echo mkdf_lms_get_cpt_shortcode_module_template_part('course', 'parts/title', $item_layout, $params); ?>

				<?php if($enable_price == 'yes') {
                    echo mkdf_lms_get_cpt_shortcode_module_template_part('course', 'parts/price', $item_layout, $params);
                } ?>
				<?php if($enable_instructor == 'yes') {
                    echo mkdf_lms_get_cpt_shortcode_module_template_part('course', 'parts/instructor-simple', $item_layout, $params);
                } ?>
			</div>
			<?php echo mkdf_lms_get_cpt_shortcode_module_template_part('course', 'parts/excerpt', $item_layout, $params); ?>
			<div class="mkdf-cli-bottom-info">
				<?php if($enable_students == 'yes') {
                    echo mkdf_lms_get_cpt_shortcode_module_template_part('course', 'parts/students', $item_layout, $params);
                } ?>
				<?php echo mkdf_lms_get_cpt_shortcode_module_template_part('course', 'parts/category', $item_layout, $params);?>
			</div>
		</div>
	</div>
</div>