<?php if($query_results->max_num_pages > 1) {
	$holder_styles = $this_object->getLoadMoreStyles($params);
	?>
	<div class="mkdf-cl-loading">
		<div class="mkdf-cl-loading-bounce1"></div>
		<div class="mkdf-cl-loading-bounce2"></div>
		<div class="mkdf-cl-loading-bounce3"></div>
	</div>
	<div class="mkdf-cl-load-more-holder">
		<div class="mkdf-cl-load-more" <?php iacademy_mikado_inline_style($holder_styles); ?>>
			<?php 
				echo iacademy_mikado_get_button_html(array(
					'link' => 'javascript: void(0)',
					'size' => 'large',
					'text' => esc_html__('Load more', 'mkdf-lms')
				));
			?>
		</div>
	</div>
<?php }