<thead>
    <tr>
        <td>
            <?php esc_html_e("Program", "'mkdf-lms'"); ?>
        </td>
        <?php if($enable_category == 'yes') { ?>
        <td class="mkdf-ct-category-title">
            <?php esc_html_e("Category", "'mkdf-lms'"); ?>
        </td>
        <?php } ?>
        <?php if($enable_instructor == 'yes') { ?>
            <td class="mkdf-ct-instructor-title">
                <?php esc_html_e("Instructor", "'mkdf-lms'"); ?>
            </td>
        <?php } ?>
        <?php if($enable_students == 'yes') { ?>
            <td class="mkdf-ct-students-title">
                <?php esc_html_e("Students", "'mkdf-lms'"); ?>
            </td>
        <?php } ?>
        <?php if($enable_price == 'yes') { ?>
            <td class="mkdf-ct-price-title">
                <?php esc_html_e("Price", "'mkdf-lms'"); ?>
            </td>
        <?php } ?>
        <td>
            &nbsp;
        </td>
    </tr>
</thead>