<tr class="mkdf-ct-not-found">
    <td colspan="<?php echo esc_attr($col_number); ?>">
        <?php esc_html_e( 'Sorry, no posts matched your criteria.', 'mkdf-lms' ); ?>
    </td>
</tr>