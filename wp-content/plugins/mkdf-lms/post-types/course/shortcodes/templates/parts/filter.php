<?php if($filter == 'yes') { ?>
<div class="mkdf-cl-filter-holder">
    <div class="mkdf-course-layout-filter">
        <span class="mkdf-active" data-type="gallery"><i class=" icon_grid-2x2" aria-hidden="true"></i></span>
        <span data-type="simple"><i class="icon_ul" aria-hidden="true"></i></span>
    </div>
    <div class="mkdf-course-items-counter">
        <span class="counter-label"><?php esc_html_e('Showing', 'mkdf-lms');?></span>
        <span class="counter-min-value"><?php echo esc_html($pagination_values['min_value']) ?></span>
        <span class="counter-dash">&ndash;</span>
        <span class="counter-max-value"><?php echo esc_html($pagination_values['max_value']) ?></span>
        <span class="counter-label"><?php esc_html_e('of', 'mkdf-lms');?></span>
        <span class="counter-total"><?php echo esc_html($pagination_values['total_items']) ?></span>
    </div>
    <div class="mkdf-course-items-order">
        <select class="mkdf-course-order-filter">
            <option data-type="date" data-order="DESC"><?php esc_html_e('Newly Publised', 'mkdf-lms'); ?></option>
            <option data-type="name" data-order="ASC"><?php esc_html_e('A-Z', 'mkdf-lms'); ?></option>
            <option data-type="name" data-order="DESC"><?php esc_html_e('Z-A', 'mkdf-lms'); ?></option>
        </select>
    </div>
</div>
<?php } ?>