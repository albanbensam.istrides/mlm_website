<?php
$icon = mkdf_lms_is_course_in_wishlist() ? 'icon_heart' : 'icon_heart_alt';
?>
<a href="javascript:void(0)" class="mkdf-course-wishlist mkdf-icon-only" data-course-id="<?php echo esc_attr(get_the_ID()); ?>">
    <i class="<?php echo esc_attr($icon); ?>" aria-hidden="true"></i>
</a>