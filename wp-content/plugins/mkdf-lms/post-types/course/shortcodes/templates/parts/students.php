<?php
$students = get_post_meta(get_the_ID(), 'mkdf_course_users_attended', true);
$students_number = is_array( $students ) ? count($students) : 0;
?>

<div class="mkdf-students-number-holder">
    <span aria-hidden="true" class="icon_profile mkdf-student-icon"></span>
    <span>
    <?php echo esc_html($students_number); ?>
    </span>
</div>
