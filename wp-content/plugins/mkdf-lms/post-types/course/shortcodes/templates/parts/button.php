<?php
    if(mkdf_lms_user_has_course()) {
        $user_current_course_status = mkdf_lms_user_current_course_status();
        if ($user_current_course_status == 'completed') {
            $button_text = esc_html__('Retake', 'mkdf-lms');
        } else if ($user_current_course_status == 'in-progress') {
            $button_text = esc_html__('Resume', 'mkdf-lms');
        } else {
            $button_text = esc_html__('Start ', 'mkdf-lms');
        }
    } else {
        $button_text = esc_html__('Enroll', 'mkdf-lms');
    }
?>
<?php if(mkdf_lms_core_plugin_installed()) {
    ?>
    <?php echo iacademy_mikado_get_button_html(array(
        'text'			=> $button_text,
        'link'			=> get_the_permalink(),
        'size'          => 'small',
        'type'          => 'outline',
    )); ?>
<?php } else { ?>
    <a href="<?php echo get_the_permalink(); ?>" class="mkdf-btn mkdf-btn-small mkdf-btn-outline"><?php echo esc_html($button_text); ?></a>
<?php } ?>