<?php

if ( ! function_exists('mkdf_lms_course_options_map') ) {
	function mkdf_lms_course_options_map() {

		iacademy_mikado_add_admin_page(array(
			'slug'  => '_course',
			'title' => esc_html__('Course', 'mkdf-lms'),
			'icon'  => 'fa fa-book'
		));

		$panel_archive = iacademy_mikado_add_admin_panel(array(
			'title' => esc_html__('Course Archive', 'mkdf-lms'),
			'name'  => 'panel_course_archive',
			'page'  => '_course'
		));

		iacademy_mikado_add_admin_field(array(
			'name'        => 'course_archive_number_of_items',
			'type'        => 'text',
			'label'       => esc_html__('Number of Items', 'mkdf-lms'),
			'description' => esc_html__('Set number of items for your course list on archive pages. Default value is 12', 'mkdf-lms'),
			'parent'      => $panel_archive,
			'args'        => array(
				'col_width' => 3
			)
		));

		iacademy_mikado_add_admin_field(array(
			'name'        => 'course_archive_number_of_columns',
			'type'        => 'select',
			'label'       => esc_html__('Number of Columns', 'mkdf-lms'),
			'default_value' => '4',
			'description' => esc_html__('Set number of columns for your course list on archive pages. Default value is 4 columns', 'mkdf-lms'),
			'parent'      => $panel_archive,
			'options'     => array(
				'2' => esc_html__('2 Columns', 'mkdf-lms'),
				'3' => esc_html__('3 Columns', 'mkdf-lms'),
				'4' => esc_html__('4 Columns', 'mkdf-lms'),
				'5' => esc_html__('5 Columns', 'mkdf-lms')
			)
		));

		iacademy_mikado_add_admin_field(array(
			'name'        => 'course_archive_space_between_items',
			'type'        => 'select',
			'label'       => esc_html__('Space Between Items', 'mkdf-lms'),
			'default_value' => 'normal',
			'description' => esc_html__('Set space size between course items for your course list on archive pages. Default value is normal', 'mkdf-lms'),
			'parent'      => $panel_archive,
			'options'     => array(
				'normal'    => esc_html__('Normal', 'mkdf-lms'),
				'small'     => esc_html__('Small', 'mkdf-lms'),
				'tiny'      => esc_html__('Tiny', 'mkdf-lms'),
				'no'        => esc_html__('No Space', 'mkdf-lms')
			)
		));

		iacademy_mikado_add_admin_field(array(
			'name'        => 'course_archive_image_size',
			'type'        => 'select',
			'label'       => esc_html__('Image Proportions', 'mkdf-lms'),
			'default_value' => 'landscape',
			'description' => esc_html__('Set image proportions for your course list on archive pages. Default value is landscape', 'mkdf-lms'),
			'parent'      => $panel_archive,
			'options'     => array(
				'full'      => esc_html__('Original', 'mkdf-lms'),
				'landscape' => esc_html__('Landscape', 'mkdf-lms'),
				'portrait'  => esc_html__('Portrait', 'mkdf-lms'),
				'square'    => esc_html__('Square', 'mkdf-lms')
			)
		));

		$panel = iacademy_mikado_add_admin_panel(array(
			'title' => esc_html__('Course Single', 'mkdf-lms'),
			'name'  => 'panel_course_single',
			'page'  => '_course'
		));
		
		iacademy_mikado_add_admin_field(
			array(
				'type' => 'select',
				'name' => 'show_title_area_course_single',
				'default_value' => '',
				'label'       => esc_html__('Show Title Area', 'mkdf-lms'),
				'description' => esc_html__('Enabling this option will show title area on single courses', 'mkdf-lms'),
				'parent'      => $panel,
                'options' => array(
                    '' => esc_html__('Default', 'mkdf-lms'),
                    'yes' => esc_html__('Yes', 'mkdf-lms'),
                    'no' => esc_html__('No', 'mkdf-lms')
                ),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		iacademy_mikado_add_admin_field(array(
			'name'          => 'course_single_comments',
			'type'          => 'yesno',
			'label'         => esc_html__('Show Comments', 'mkdf-lms'),
			'description'   => esc_html__('Enabling this option will show comments on your page', 'mkdf-lms'),
			'parent'        => $panel,
			'default_value' => 'yes'
		));

		iacademy_mikado_add_admin_field(array(
			'name'        => 'course_single_slug',
			'type'        => 'text',
			'label'       => esc_html__('Course Single Slug', 'mkdf-lms'),
			'description' => esc_html__('Enter if you wish to use a different Single Course slug (Note: After entering slug, navigate to Settings -> Permalinks and click "Save" in order for changes to take effect)', 'mkdf-lms'),
			'parent'      => $panel,
			'args'        => array(
				'col_width' => 3
			)
		));
	}

	add_action( 'iacademy_mikado_options_map', 'mkdf_lms_course_options_map', 14);
}