<?php

if (!function_exists('mkdf_lms_map_course_meta')) {
    function mkdf_lms_map_course_meta() {

        //Get list of courses;
        $mkd_courses = array();
        $courses = get_posts(
            array(
                'numberposts' => -1,
                'post_type' => 'course',
                'post_status'    => 'publish'
            )
        );
        foreach ($courses as $course) {
            $mkd_courses[$course->ID] = $course->post_title;
        }

        //Get list of instructors;
        $mkd_instructors = array();
        $instructors = get_posts(
            array(
                'numberposts' => -1,
                'post_type' => 'instructor',
                'post_status'    => 'publish'
            )
        );
        foreach ($instructors as $instructor) {
            $mkd_instructors[$instructor->ID] = $instructor->post_title;
        }

        if(mkdf_lms_bbpress_plugin_installed()) {
            //Get list of forums;
            $mkd_forums = array();
            $forums = get_posts(
                array(
                    'numberposts' => -1,
                    'post_type' => 'forum',
                    'post_status' => 'publish',
                    'posts_per_page' => get_option('_bbp_forums_per_page', 50),
                    'ignore_sticky_posts' => true,
                    'orderby' => 'menu_order title',
                    'order' => 'ASC'
                )
            );
            foreach ($forums as $forum) {
                $mkd_forums[$forum->ID] = $forum->post_title;
            }
        }

        $meta_box = iacademy_mikado_create_meta_box(array(
            'scope' => 'course',
            'title' => esc_html__('Course Settings', 'mkdf-lms'),
            'name'  => 'course_settings_meta_box'
        ));

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_show_title_area_course_single_meta',
                'type' => 'select',
                'default_value' => '',
                'label'       => esc_html__('Show Title Area', 'mkdf-lms'),
                'description' => esc_html__('Enabling this option will show title area on your single course page', 'mkdf-lms'),
                'parent'      => $meta_box,
                'options' => array(
                    '' => esc_html__('Default', 'mkdf-lms'),
                    'yes' => esc_html__('Yes', 'mkdf-lms'),
                    'no' => esc_html__('No', 'mkdf-lms')
                )
            )
        );

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_course_instructor_meta',
            'type'        => 'selectblank',
            'label'       => esc_html__('Course Instructor', 'mkdf-lms'),
            'description' => esc_html__('Select instructor for this course', 'mkdf-lms'),
            'parent'      => $meta_box,
            'options'     => $mkd_instructors,
            'args'        => array(
                'select2' => true
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_course_duration_meta',
            'type'        => 'text',
            'label'       => esc_html__('Course Duration', 'mkdf-lms'),
            'description' => esc_html__('Set duration for course', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_course_curriculum_desc_meta',
            'type'        => 'textarea',
            'label'       => esc_html__('General Curriculum Description', 'mkdf-lms'),
            'description' => esc_html__('Set general description of course curriculum', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name' => 'mkdf_course_duration_parameter_meta',
            'type' => 'select',
            'label' => esc_html__('Course Duration Parameter', 'mkdf-lms'),
            'description' => esc_html__('Choose parameter for course duration', 'mkdf-lms'),
            'default_value' => 'minutes',
            'parent' => $meta_box,
            'options' => array(
                '' => esc_html__('Default', 'mkdf-lms'),
                'minutes' => esc_html__('Minutes', 'mkdf-lms'),
                'hours' => esc_html__('Hours', 'mkdf-lms'),
                'days' => esc_html__('Days', 'mkdf-lms'),
                'weeks' => esc_html__('Weeks', 'mkdf-lms'),
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_course_maximum_students_meta',
            'type'        => 'text',
            'label'       => esc_html__('Maximum Students', 'mkdf-lms'),
            'description' => esc_html__('Set maximal number of students', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_course_retake_number_meta',
            'type'        => 'text',
            'label'       => esc_html__('Number of Re-Takes', 'mkdf-lms'),
            'description' => esc_html__('Set maximal number of retakes', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

        iacademy_mikado_create_meta_box_field(
            array(
                'name'          => 'mkdf_course_featured_meta',
                'type'          => 'yesno',
                'default_value' => 'no',
                'label'         => esc_html__( 'Featured Course', 'mkdf-lms' ),
                'description'   => esc_html__( 'Enable this option to set course featured', 'mkdf-lms' ),
                'parent'        => $meta_box
            )
        );

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_course_prerequired_meta',
            'type'        => 'selectblank',
            'label'       => esc_html__('Pre-Required Course', 'mkdf-lms'),
            'description' => esc_html__('Select course that needs to be completed before attending', 'mkdf-lms'),
            'parent'      => $meta_box,
            'options'     => $mkd_courses,
            'args'        => array(
                'select2' => true
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_course_passing_percentage_meta',
            'type'        => 'text',
            'label'       => esc_html__('Passing Percentage', 'mkdf-lms'),
            'description' => esc_html__('Set value required to pass the course', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_course_free_meta',
                'type' => 'select',
                'default_value' => '',
                'label'       => esc_html__('Free Course', 'mkdf-lms'),
                'description' => esc_html__('Enabling this option will set course to be free', 'mkdf-lms'),
                'parent'      => $meta_box,
                'options' => array(
                    'no' => esc_html__('No', 'mkdf-lms'),
                    'yes' => esc_html__('Yes', 'mkdf-lms')
                ),
                'args' => array(
                    'dependence' => true,
                    'hide' => array(
                        'no'  => '',
                        'yes' => '#mkdf_course_price_container'
                    ),
                    'show' => array(
                        'no'  => '#mkdf_course_price_container',
                        'yes' => ''
                    )
                )
            )
        );

        $course_price_container = iacademy_mikado_add_admin_container(array(
            'type'            => 'container',
            'name'            => 'course_price_container',
            'parent'          => $meta_box,
            'hidden_property' => 'mkdf_course_free_meta',
            'hidden_values'   => array('yes')
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_course_price_meta',
            'type'        => 'text',
            'label'       => esc_html__('Price', 'mkdf-lms'),
            'description' => esc_html__('Set price for course', 'mkdf-lms'),
            'parent'      => $course_price_container,
            'args'        => array(
                'col_width' => 3
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_course_price_discount_meta',
            'type'        => 'text',
            'label'       => esc_html__('Discount', 'mkdf-lms'),
            'description' => esc_html__('Enter discount value for course', 'mkdf-lms'),
            'parent'      => $course_price_container,
            'args'        => array(
                'col_width' => 3
            )
        ));

        if(mkdf_lms_bbpress_plugin_installed()) {
            iacademy_mikado_create_meta_box_field(array(
                'name'        => 'mkdf_course_forum_meta',
                'type'        => 'selectblank',
                'label'       => esc_html__('Course Forum', 'mkdf-lms'),
                'description' => esc_html__('Select forum for this course', 'mkdf-lms'),
                'parent'      => $meta_box,
                'options'     => $mkd_forums,
                'args'        => array(
                    'select2' => true
                )
            ));
        }

        $meta_box_curriculum = iacademy_mikado_create_meta_box(array(
            'scope' => 'course',
            'title' => esc_html__('Course Curriculum', 'mkdf-lms'),
            'name'  => 'course_curriculum_meta_box'
        ));

        mkdf_lms_create_meta_box_course_items_field(array(
            'name'        => 'mkdf_course_curriculum',
            'label'       => esc_html__('Curriculum', 'mkdf-lms'),
            'description' => esc_html__('Organize lessons and quizzes into sections.', 'mkdf-lms'),
            'parent'      => $meta_box_curriculum
        ));

    }

    add_action('iacademy_mikado_meta_boxes_map', 'mkdf_lms_map_course_meta', 5);
}