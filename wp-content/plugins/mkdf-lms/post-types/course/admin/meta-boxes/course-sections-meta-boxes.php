<?php

/*
   Class: IacademyMikadoMultipleImages
   A class that initializes Mikado LMS Course Sections
*/
class MikadofLMSCourseSectionsMetaBox implements iIacademyMikadoRender {
    private $name;
    private $label;
    private $description;

    function __construct($name, $label="", $description="") {
        global $iacademy_mikado_Framework;
        $this->name = $name;
        $this->label = $label;
        $this->description = $description;
        $iacademy_mikado_Framework->mkdMetaBoxes->addOption($this->name,"");
    }

    public function render($factory) {

        global $post;
        $rows = empty($post->ID) ? array() : get_post_meta($post->ID, 'mkdf_course_curriculum', true);

        //Get list of lessons;
        $mkd_lessons = array();
        $lessons = get_posts(
            array(
                'numberposts' => -1,
                'post_type' => 'lesson',
                'post_status' => 'publish',
            )
        );
        foreach ($lessons as $lesson) {
            $mkd_lessons[$lesson->ID] = $lesson->post_title;
        }

        //Get list of quizzes;
        $mkd_quizzes = array();
        $quizzes = get_posts(
            array(
                'numberposts' => -1,
                'post_type' => 'quiz',
                'post_status' => 'publish'
            )
        );
        foreach ($quizzes as $quiz) {
            $mkd_quizzes[$quiz->ID] = $quiz->post_title;
        }

        ?>
        <input type="hidden" id="course_id" name="course_id" value="<?php echo esc_attr($post->ID); ?>">
        <div id="mkdf-course-section-content" class="mkdf-repeater-fields-holder mkdf-enable-pc mkdf-sortable-holder clearfix">
            <?php if(is_array($rows) && count($rows)) :
            $i = 0;
            ?>
            <?php foreach($rows as $key=>$value) : ?>
            <div class="mkdf-course-section mkdf-repeater-fields-row mkdf-sort-parent first-level" data-index="<?php echo esc_attr($i); ?>">
                <div class="mkdf-repeater-fields-row-inner">
                    <div class="mkdf-repeater-sort">
                        <i class="fa fa-sort"></i>
                    </div>
                    <div class="mkdf-repeater-field-item">
                        <div class="mkdf-page-form-section mkdf-repeater-field mkdf-no-description">
                            <div class="mkdf-section-content">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h4><?php esc_html_e('Section Name', 'mkdf-lms'); ?></h4>
                                            <div class="form-group">
                                                <input type="text" class="form-control mkdf-input mkdf-form-element" placeholder="" value="<?php echo esc_attr($value['section_name']); ?>" name="mkdf_course_curriculum[<?php echo esc_attr($i); ?>][section_name]">
                                            </div>
                                            <h4><?php esc_html_e('Section Title', 'mkdf-lms'); ?></h4>
                                            <div class="form-group">
                                                <input type="text" class="form-control mkdf-input mkdf-form-element" placeholder="" value="<?php echo esc_attr($value['section_title']); ?>" name="mkdf_course_curriculum[<?php echo esc_attr($i); ?>][section_title]">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4><?php esc_html_e('Section Description', 'mkdf-lms'); ?></h4>
                                            <div class="form-group">
                                                <textarea type="text" rows="6" class="form-control mkdf-input mkdf-form-element" placeholder="" name="mkdf_course_curriculum[<?php echo esc_attr($i); ?>][section_description]"><?php echo esc_attr($value['section_description']); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mkdf-sortable-holder" id="mkdf-course-section-elements-<?php echo esc_attr($i); ?>">
                            <?php if(!empty($value['section_elements']) && is_array($value['section_elements']) && count($value['section_elements'])) : ?>
                                <?php $j = 0; ?>
                                <?php foreach($value['section_elements'] as $element) : ?>
                                    <?php if($element['type'] == 'lesson'): ?>
                                    <div class="mkdf-course-element mkdf-repeater-fields-row mkdf-sort-child second-level" data-index="<?php echo esc_attr($j); ?>">
                                        <div class="mkdf-repeater-fields-row-inner">
                                            <div class="mkdf-repeater-sort">
                                                <i class="fa fa-sort"></i>
                                            </div>
                                            <div class="mkdf-repeater-field-item">
                                                <div class="mkdf-page-form-section mkdf-repeater-field mkdf-no-description">
                                                    <div class="mkdf-section-content">
                                                        <div class="container-fluid">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="mkdf-inner-field-holder">
                                                                        <input type="hidden" value="lesson" name="mkdf_course_curriculum[<?php echo esc_attr($i); ?>][section_elements][<?php echo esc_attr($j); ?>][type]">
                                                                        <select class="mkdf-select2 form-control mkdf-form-element" name="mkdf_course_curriculum[<?php echo esc_attr($i); ?>][section_elements][<?php echo esc_attr($j); ?>][value]">
                                                                            <?php foreach($mkd_lessons as $key=>$value) { if ($key == "-1") $key = ""; ?>
                                                                                <option <?php if ($element['value'] == $key) { echo "selected='selected'"; } ?>  value="<?php echo esc_attr($key); ?>"><?php echo esc_html($value); ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mkdf-repeater-remove">
                                                <a href="#" class="mkdf-course-lesson-remove-item" data-toggle="tooltip" data-placement="left" title="<?php esc_attr_e('Remove Section', 'mkdf-lms'); ?>"><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php elseif($element['type'] == 'quiz'): ?>
                                    <div class="mkdf-course-element mkdf-repeater-fields-row mkdf-sort-child second-level" data-index="<?php echo esc_attr($j); ?>">
                                        <div class="mkdf-repeater-fields-row-inner">
                                            <div class="mkdf-repeater-sort">
                                                <i class="fa fa-sort"></i>
                                            </div>
                                            <div class="mkdf-repeater-field-item">
                                                <div class="mkdf-page-form-section mkdf-repeater-field mkdf-no-description">
                                                    <div class="mkdf-section-content">
                                                        <div class="container-fluid">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="mkdf-inner-field-holder">
                                                                        <input type="hidden" value="quiz" name="mkdf_course_curriculum[<?php echo esc_attr($i); ?>][section_elements][<?php echo esc_attr($j); ?>][type]">
                                                                        <select class="mkdf-select2 form-control mkdf-form-element" name="mkdf_course_curriculum[<?php echo esc_attr($i); ?>][section_elements][<?php echo esc_attr($j); ?>][value]">
                                                                            <?php foreach($mkd_quizzes as $key=>$value) { if ($key == "-1") $key = ""; ?>
                                                                                <option <?php if ($element['value'] == $key) { echo "selected='selected'"; } ?>  value="<?php echo esc_attr($key); ?>"><?php echo esc_html($value); ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mkdf-repeater-remove">
                                                <a href="#" class="mkdf-course-quiz-remove-item" data-toggle="tooltip" data-placement="left" title="<?php esc_attr_e('Remove Section', 'mkdf-lms'); ?>"><i class="fa fa-times"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <?php $j++; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <div class="mkdf-course-section-controls">
                            <div class="mkdf-repeater-add">
                                <a id="mkdf-course-lesson-add" href="#" class="btn btn-primary"><?php esc_html_e('Add New Lesson', 'mkdf-lms'); ?></a>
                                <a id="mkdf-course-quiz-add" href="#" class="btn btn-primary"><?php esc_html_e('Add New Quiz', 'mkdf-lms'); ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="mkdf-repeater-remove">
                        <a href="#" class="mkdf-course-section-remove-item" data-toggle="tooltip" data-placement="left" title="<?php esc_attr_e('Remove Section', 'mkdf-lms'); ?>"><i class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
            <?php
            $i++;
            endforeach;
                ?>
            <?php endif; ?>
        </div>

        <div class="mkdf-course-section-controls">
            <div class="mkdf-repeater-add">
                <a id="mkdf-course-section-add" href="#" class="btn btn-primary"><?php esc_html_e('Add New Section', 'mkdf-lms'); ?></a>
            </div>
        </div>

        <script type="text/html" id="tmpl-mkdf-course-section-template">
            <div class="mkdf-course-section mkdf-repeater-fields-row mkdf-sort-parent first-level" data-index="{{{ data.rowIndex }}}">
                <div class="mkdf-repeater-fields-row-inner">
                    <div class="mkdf-repeater-sort">
                        <i class="fa fa-sort"></i>
                    </div>
                    <div class="mkdf-repeater-field-item">
                        <div class="mkdf-page-form-section mkdf-repeater-field mkdf-no-description">
                            <div class="mkdf-section-content">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h4><?php esc_html_e('Section Name', 'mkdf-lms'); ?></h4>
                                            <div class="form-group">
                                                <input type="text" class="form-control mkdf-input mkdf-form-element" placeholder="" name="mkdf_course_curriculum[{{{ data.rowIndex }}}][section_name]">
                                            </div>
                                            <h4><?php esc_html_e('Section Title', 'mkdf-lms'); ?></h4>
                                            <div class="form-group">
                                                <input type="text" class="form-control mkdf-input mkdf-form-element" placeholder="" name="mkdf_course_curriculum[{{{ data.rowIndex }}}][section_title]">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h4><?php esc_html_e('Section Description', 'mkdf-lms'); ?></h4>
                                            <div class="form-group">
                                                <textarea type="text" rows="6" class="form-control mkdf-input mkdf-form-element" placeholder="" name="mkdf_course_curriculum[{{{ data.rowIndex }}}][section_description]"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mkdf-sortable-holder" id="mkdf-course-section-elements-{{{ data.rowIndex }}}">

                        </div>
                        <div class="mkdf-course-section-controls">
                            <div class="mkdf-repeater-add">
                                <a id="mkdf-course-lesson-add" href="#" class="btn btn-primary"><?php esc_html_e('Add New Lesson', 'mkdf-lms'); ?></a>
                                <a id="mkdf-course-quiz-add" href="#" class="btn btn-primary"><?php esc_html_e('Add New Quiz', 'mkdf-lms'); ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="mkdf-repeater-remove">
                        <a href="#" class="mkdf-course-section-remove-item" data-toggle="tooltip" data-placement="left" title="<?php esc_attr_e('Remove Section', 'mkdf-lms'); ?>"><i class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
        </script>

        <script type="text/html" id="tmpl-mkdf-section-lesson-template">
            <div class="mkdf-course-element mkdf-repeater-fields-row mkdf-sort-child second-level" data-index="{{{ data.lessonIndex }}}">
                <div class="mkdf-repeater-fields-row-inner">
                    <div class="mkdf-repeater-sort">
                        <i class="fa fa-sort"></i>
                    </div>
                    <div class="mkdf-repeater-field-item">
                        <div class="mkdf-page-form-section mkdf-repeater-field mkdf-no-description">
                            <div class="mkdf-section-content">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="mkdf-inner-field-holder">
                                                <input type="hidden" value="lesson" name="mkdf_course_curriculum[{{{ data.rowIndex }}}][section_elements][{{{ data.lessonIndex }}}][type]">
                                                <select class="mkdf-select2 form-control mkdf-form-element" name="mkdf_course_curriculum[{{{ data.rowIndex }}}][section_elements][{{{ data.lessonIndex }}}][value]">
                                                    <?php foreach($mkd_lessons as $key=>$value) { if ($key == "-1") $key = ""; ?>
                                                        <option value="<?php echo esc_attr($key); ?>"><?php echo esc_html($value); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mkdf-repeater-remove">
                        <a href="#" class="mkdf-course-lesson-remove-item" data-toggle="tooltip" data-placement="left" title="<?php esc_attr_e('Remove Lesson', 'mkdf-lms'); ?>"><i class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
        </script>

        <script type="text/html" id="tmpl-mkdf-section-quiz-template">
            <div class="mkdf-course-element mkdf-repeater-fields-row mkdf-sort-child second-level" data-index="{{{ data.quizIndex }}}">
                <div class="mkdf-repeater-fields-row-inner">
                    <div class="mkdf-repeater-sort">
                        <i class="fa fa-sort"></i>
                    </div>
                    <div class="mkdf-repeater-field-item">
                        <div class="mkdf-page-form-section mkdf-repeater-field mkdf-no-description">
                            <div class="mkdf-section-content">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="mkdf-inner-field-holder">
                                                <input type="hidden" value="quiz" name="mkdf_course_curriculum[{{{ data.rowIndex }}}][section_elements][{{{ data.quizIndex }}}][type]">
                                                <select class="mkdf-select2 form-control mkdf-form-element" name="mkdf_course_curriculum[{{{ data.rowIndex }}}][section_elements][{{{ data.quizIndex }}}][value]">
                                                    <?php foreach($mkd_quizzes as $key=>$value) { if ($key == "-1") $key = ""; ?>
                                                        <option value="<?php echo esc_attr($key); ?>"><?php echo esc_html($value); ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mkdf-repeater-remove">
                        <a href="#" class="mkdf-course-lesson-remove-item" data-toggle="tooltip" data-placement="left" title="<?php esc_attr_e('Remove Quiz', 'mkdf-lms'); ?>"><i class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
        </script>

        <?php
    }
}