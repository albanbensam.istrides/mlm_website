<?php
$customer_orders = mkdf_lms_get_user_profile_course_items();
?>
<div class="mkdf-lms-profile-courses-holder">
    <?php if(!empty($customer_orders)) { ?>
        <?php
        foreach($customer_orders as $customer_order) {
            $id = $customer_order['id'];
            $order_status = $customer_order['order_status'];
            ?>
            <div class="mkdf-lms-profile-course-item">
                <div class="mkdf-lms-profile-course-item-image">
                    <?php
                    if (has_post_thumbnail($id)) {
                        $image = get_the_post_thumbnail_url($id, 'thumbnail');
                    } else {
                        $image = MIKADO_LMS_CPT_URL_PATH . '/course/assets/img/course_featured_image.jpg';
                    }
                    ?>
                    <img src="<?php echo esc_url($image); ?>" alt="<?php echo esc_attr__('Course thumbnail', 'mkdf-lms') ?>"/>
                </div>
                <div class="mkdf-lms-profile-course-item-title">
                    <h5>
                        <span class="mkdf-profile-course-title">
                            <?php echo get_the_title($id); ?>
                        </span>
                        <span class="mkdf-profile-course-status">
                        <?php
                        echo esc_html('(');
                        if($order_status !== 'completed') {
                            echo wc_get_order_status_name($order_status);
                        } else { ?>
                            <a href="<?php echo get_the_permalink($id); ?>">
                            <?php
                            $user_current_course_status = mkdf_lms_user_current_course_status($id);
                            if ($user_current_course_status == 'completed') {
                                esc_html_e('Retake', 'mkdf-lms');
                            } else if ($user_current_course_status == 'in-progress') {
                                esc_html_e('Resume', 'mkdf-lms');
                            } else {
                                esc_html_e('Start ', 'mkdf-lms');
                            } ?>
                            </a>
                        <?php
                            }
                            echo esc_html(')');
                        ?>
                        </span>
                    </h5>
                </div>
            </div>
            <?php
        }
    } else { ?>
        <h3><?php esc_html_e("Your courses list is empty.", 'mkdf-lms') ?> </h3>
    <?php } ?>
</div>