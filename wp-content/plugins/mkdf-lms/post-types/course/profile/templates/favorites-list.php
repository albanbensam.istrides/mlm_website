<?php
$user_favorites = get_user_meta(get_current_user_id(), 'mkdf_course_wishlist', true);
?>
<div class="mkdf-lms-profile-favorites-holder">
<?php if(!empty($user_favorites)) { ?>
    <?php
	foreach($user_favorites as $user_favorite){	?>

		<div class="mkdf-lms-profile-favorite-item">
			<div class="mkdf-lms-profile-favorite-item-image">
                <?php
                    if(has_post_thumbnail($user_favorite)) {
                        $image = get_the_post_thumbnail_url($user_favorite, 'thumbnail');
                    }
                    else {
                        $image = MIKADO_LMS_CPT_URL_PATH.'/course/assets/img/course_featured_image.jpg';
                    }
                ?>
            <img src="<?php echo esc_url($image); ?>" alt="<?php echo esc_attr__('Course thumbnail','mkdf-lms') ?>" />
			</div>
            <div class="mkdf-lms-profile-favorite-item-title">
			    <h5>
                    <a href="<?php echo get_the_permalink($user_favorite); ?>">
                        <?php echo get_the_title($user_favorite); ?>
                    </a>
                    <?php
                        $icon = mkdf_lms_is_course_in_wishlist($user_favorite) ? 'icon_heart' : 'icon_heart_alt';
                    ?>
                    <a href="javascript:void(0)" class="mkdf-course-wishlist mkdf-icon-only" data-course-id="<?php echo esc_attr($user_favorite); ?>">
                        <i class="<?php echo esc_attr($icon); ?>" aria-hidden="true"></i>
                    </a>
                </h5>
            </div>
		</div>
    <?php
	}
 } else { ?>
    <h3><?php esc_html_e("Your favorites list is empty.", "'mkdf-lms'") ?> </h3>
<?php } ?>
</div>