<?php

if (!function_exists('mkdf_lms_map_lesson_meta')) {
    function mkdf_lms_map_lesson_meta() {

        $meta_box = iacademy_mikado_create_meta_box(array(
            'scope' => 'lesson',
            'title' => esc_html__('Lesson Settings', 'mkdf-lms'),
            'name'  => 'lesson_settings_meta_box'
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_lesson_description_meta',
            'type'        => 'textarea',
            'label'       => esc_html__('Lesson Description', 'mkdf-lms'),
            'description' => esc_html__('Add lesson description', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));


        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_lesson_duration_meta',
            'type'        => 'text',
            'label'       => esc_html__('Lesson Duration', 'mkdf-lms'),
            'description' => esc_html__('Set duration for lesson', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name' => 'mkdf_lesson_duration_parameter_meta',
            'type' => 'select',
            'label' => esc_html__('Lesson Duration Parameter', 'mkdf-lms'),
            'description' => esc_html__('Choose parameter for lesson duration', 'mkdf-lms'),
            'default_value' => 'minutes',
            'parent' => $meta_box,
            'options' => array(
                '' => esc_html__('Default', 'mkdf-lms'),
                'minutes' => esc_html__('Minutes', 'mkdf-lms'),
                'hours' => esc_html__('Hours', 'mkdf-lms'),
                'days' => esc_html__('Days', 'mkdf-lms'),
                'weeks' => esc_html__('Weeks', 'mkdf-lms'),
            )
        ));

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_lesson_free_meta',
                'type' => 'select',
                'default_value' => '',
                'label'       => esc_html__('Free Lesson', 'mkdf-lms'),
                'description' => esc_html__('Enabling this option will set lesson to be free', 'mkdf-lms'),
                'parent'      => $meta_box,
                'options' => array(
                    '' => esc_html__('Default', 'mkdf-lms'),
                    'yes' => esc_html__('Yes', 'mkdf-lms'),
                    'no' => esc_html__('No', 'mkdf-lms')
                )
            )
        );

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_lesson_post_message_meta',
            'type'        => 'textarea',
            'label'       => esc_html__('Lesson Post Message', 'mkdf-lms'),
            'description' => esc_html__('Set message that will be displayed after the lesson is completed', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_lesson_type_meta',
                'type' => 'select',
                'default_value' => '',
                'label'       => esc_html__('Lesson Type', 'mkdf-lms'),
                'description' => esc_html__('Choose desired lesson type', 'mkdf-lms'),
                'parent'      => $meta_box,
                'options' => array(
                    'reading'  => esc_html__('Reading', 'mkdf-lms'),
                    'video' => esc_html__('Video', 'mkdf-lms'),
                    'audio' => esc_html__('Audio', 'mkdf-lms')
                ),
                'args' => array(
                    'dependence' => true,
                    'hide' => array(
                        'reading' => '#mkdf_mkdf_video_container, #mkdf_mkdf_audio_container',
                        'video' => '#mkdf_mkdf_audio_container',
                        'audio' => '#mkdf_mkdf_video_container'
                    ),
                    'show' => array(
                        'reading' => '',
                        'video' => '#mkdf_mkdf_video_container',
                        'audio' => '#mkdf_mkdf_audio_container'
                    )
                )
            )
        );

        //VIDEO TYPE
        $mkdf_video_container = iacademy_mikado_add_admin_container(
            array(
                'parent'          => $meta_box,
                'name'            => 'mkdf_video_container',
                'hidden_property' => 'mkdf_lesson_type_meta',
                'hidden_value'    => array('reading, audio')
            )
        );

        iacademy_mikado_create_meta_box_field(
            array(
                'name'          => 'mkdf_lesson_video_type_meta',
                'type'          => 'select',
                'label'         => esc_html__( 'Video Type', 'mkdf-lms' ),
                'description'   => esc_html__( 'Choose video type', 'mkdf-lms' ),
                'parent'        => $mkdf_video_container,
                'default_value' => 'social_networks',
                'options'       => array(
                    'social_networks' => esc_html__( 'Video Service', 'mkdf-lms' ),
                    'self'            => esc_html__( 'Self Hosted', 'mkdf-lms' )
                ),
                'args'          => array(
                    'dependence' => true,
                    'hide'       => array(
                        'social_networks' => '#mkdf_mkdf_video_self_hosted_container',
                        'self'            => '#mkdf_mkdf_video_embedded_container'
                    ),
                    'show'       => array(
                        'social_networks' => '#mkdf_mkdf_video_embedded_container',
                        'self'            => '#mkdf_mkdf_video_self_hosted_container'
                    )
                )
            )
        );

        $mkdf_video_embedded_container = iacademy_mikado_add_admin_container(
            array(
                'parent'          => $mkdf_video_container,
                'name'            => 'mkdf_video_embedded_container',
                'hidden_property' => 'mkdf_lesson_video_type_meta',
                'hidden_value'    => 'self'
            )
        );

        $mkdf_video_self_hosted_container = iacademy_mikado_add_admin_container(
            array(
                'parent'          => $mkdf_video_container,
                'name'            => 'mkdf_video_self_hosted_container',
                'hidden_property' => 'mkdf_lesson_video_type_meta',
                'hidden_value'    => 'social_networks'
            )
        );

        iacademy_mikado_create_meta_box_field(
            array(
                'name'        => 'mkdf_lesson_video_link_meta',
                'type'        => 'text',
                'label'       => esc_html__( 'Video URL', 'mkdf-lms' ),
                'description' => esc_html__( 'Enter Video URL', 'mkdf-lms' ),
                'parent'      => $mkdf_video_embedded_container,
            )
        );

        iacademy_mikado_create_meta_box_field(
            array(
                'name'        => 'mkdf_lesson_video_custom_meta',
                'type'        => 'text',
                'label'       => esc_html__( 'Video MP4', 'mkdf-lms' ),
                'description' => esc_html__( 'Enter video URL for MP4 format', 'mkdf-lms' ),
                'parent'      => $mkdf_video_self_hosted_container,
            )
        );

        //AUDIO TYPE
        $mkdf_audio_container = iacademy_mikado_add_admin_container(
            array(
                'parent'          => $meta_box,
                'name'            => 'mkdf_audio_container',
                'hidden_property' => 'mkdf_lesson_type_meta',
                'hidden_value'    => array('reading, video')
            )
        );

        iacademy_mikado_create_meta_box_field(
            array(
                'name'          => 'mkdf_lesson_audio_type_meta',
                'type'          => 'select',
                'label'         => esc_html__( 'Audio Type', 'mkdf-lms' ),
                'description'   => esc_html__( 'Choose audio type', 'mkdf-lms' ),
                'parent'        => $mkdf_audio_container,
                'default_value' => 'social_networks',
                'options'       => array(
                    'social_networks' => esc_html__( 'Audio Service', 'mkdf-lms' ),
                    'self'            => esc_html__( 'Self Hosted', 'mkdf-lms' )
                ),
                'args'          => array(
                    'dependence' => true,
                    'hide'       => array(
                        'social_networks' => '#mkdf_mkdf_audio_self_hosted_container',
                        'self'            => '#mkdf_mkdf_audio_embedded_container'
                    ),
                    'show'       => array(
                        'social_networks' => '#mkdf_mkdf_audio_embedded_container',
                        'self'            => '#mkdf_mkdf_audio_self_hosted_container'
                    )
                )
            )
        );

        $mkdf_audio_embedded_container = iacademy_mikado_add_admin_container(
            array(
                'parent'          => $mkdf_audio_container,
                'name'            => 'mkdf_audio_embedded_container',
                'hidden_property' => 'mkdf_lesson_audio_type_meta',
                'hidden_value'    => 'self'
            )
        );

        $mkdf_audio_self_hosted_container = iacademy_mikado_add_admin_container(
            array(
                'parent'          => $mkdf_audio_container,
                'name'            => 'mkdf_audio_self_hosted_container',
                'hidden_property' => 'mkdf_lesson_audio_type_meta',
                'hidden_value'    => 'social_networks'
            )
        );

        iacademy_mikado_create_meta_box_field(
            array(
                'name'        => 'mkdf_lesson_audio_link_meta',
                'type'        => 'text',
                'label'       => esc_html__( 'Audio URL', 'mkdf-lms' ),
                'description' => esc_html__( 'Enter audio URL', 'mkdf-lms' ),
                'parent'      => $mkdf_audio_embedded_container,
            )
        );

        iacademy_mikado_create_meta_box_field(
            array(
                'name'        => 'mkdf_lesson_audio_custom_meta',
                'type'        => 'text',
                'label'       => esc_html__( 'Audio Link', 'mkdf-lms' ),
                'description' => esc_html__( 'Enter audio link', 'mkdf-lms' ),
                'parent'      => $mkdf_audio_self_hosted_container,
            )
        );

    }

    add_action('iacademy_mikado_meta_boxes_map', 'mkdf_lms_map_lesson_meta', 5);
}