<?php
//Register meta boxes
if(!function_exists('mkdf_lms_lesson_meta_box_functions')) {
	function mkdf_lms_lesson_meta_box_functions($post_types) {
		$post_types[] = 'lesson';
		
		return $post_types;
	}
	
	add_filter('iacademy_mikado_meta_box_post_types_save', 'mkdf_lms_lesson_meta_box_functions');
	add_filter('iacademy_mikado_meta_box_post_types_remove', 'mkdf_lms_lesson_meta_box_functions');
}

//Register meta boxes scope
if(!function_exists('mkdf_lms_lesson_scope_meta_box_functions')) {
	function mkdf_lms_lesson_scope_meta_box_functions($post_types) {
		$post_types[] = 'lesson';
		
		return $post_types;
	}
	
	add_filter('iacademy_mikado_set_scope_for_meta_boxes', 'mkdf_lms_lesson_scope_meta_box_functions');
}

//Register lesson post type
if(!function_exists('mkdf_lms_register_lesson_cpt')) {
	function mkdf_lms_register_lesson_cpt($cpt_class_name) {
		$cpt_class = array(
			'MikadofLMS\CPT\Lesson\LessonRegister'
		);
		
		$cpt_class_name = array_merge($cpt_class_name, $cpt_class);
		
		return $cpt_class_name;
	}
	
	add_filter('mkdf_lms_filter_register_custom_post_types', 'mkdf_lms_register_lesson_cpt');
}

//Lesson single functions
if(!function_exists('mkdf_lms_get_single_lesson')) {
    function mkdf_lms_get_single_lesson() {

        $params = array();
        $params['item_id'] = get_the_ID();
        $params['lesson_type'] = get_post_meta(get_the_ID(), 'mkdf_lesson_type_meta', true);

        mkdf_lms_get_cpt_single_module_template_part('templates/single/holder', 'lesson', '', $params);

    }
}