<div class="mkdf-lms-lesson-media">
    <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/video', 'lesson', '', $params); ?>
</div>
<div class="mkdf-lms-lesson-content-wrapper">
    <div class="mkdf-lms-lesson-title">
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/title', 'lesson', '', $params); ?>
    </div>
    <div class="mkdf-lms-lesson-content">
        <?php the_content(); ?>
    </div>
    <div class="mkdf-lms-lesson-complete">
        <?php echo mkdf_lms_complete_button($params); ?>
    </div>
</div>
