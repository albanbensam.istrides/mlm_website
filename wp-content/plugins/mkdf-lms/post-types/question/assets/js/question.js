(function($) {
    'use strict';

    var question = {};
    mkdf.modules.question = question;

    question.mkdfQuestionHint = mkdfQuestionHint;
    question.mkdfQuestionCheck = mkdfQuestionCheck;
    question.mkdfQuestionChange = mkdfQuestionChange;
    question.mkdfQuestionAnswerChange = mkdfQuestionAnswerChange;
    question.mkdfValidateAnswer = mkdfValidateAnswer;
    question.mkdfQuestionSave = mkdfQuestionSave;

    question.mkdfOnDocumentReady = mkdfOnDocumentReady;
    question.mkdfOnWindowLoad = mkdfOnWindowLoad;
    question.mkdfOnWindowResize = mkdfOnWindowResize;
    question.mkdfOnWindowScroll = mkdfOnWindowScroll;

    $(document).ready(mkdfOnDocumentReady);
    $(window).load(mkdfOnWindowLoad);
    $(window).resize(mkdfOnWindowResize);
    $(window).scroll(mkdfOnWindowScroll);

    /*
     All functions to be called on $(document).ready() should be in this function
     */
    function mkdfOnDocumentReady() {
        mkdfQuestionHint();
        mkdfQuestionCheck();
        mkdfQuestionChange();
        mkdfQuestionAnswerChange();
    }

    /*
     All functions to be called on $(window).load() should be in this function
     */
    function mkdfOnWindowLoad() {

    }

    /*
     All functions to be called on $(window).resize() should be in this function
     */
    function mkdfOnWindowResize() {

    }

    /*
     All functions to be called on $(window).scroll() should be in this function
     */
    function mkdfOnWindowScroll() {

    }

    function mkdfQuestionAnswerChange() {
        var answersHolder = $('.mkdf-question-answers');
        var radios = answersHolder.find('input[type=radio]');
        var checkboxes = answersHolder.find('input[type=checkbox]');
        var textbox = answersHolder.find('input[type=text]');
        var checkForm = $('.mkdf-lms-question-actions-check-form');
        var nextForm = $('.mkdf-lms-question-next-form');
        var prevForm = $('.mkdf-lms-question-prev-form');
        var finishForm = $('.mkdf-lms-finish-quiz-form');

        radios.change(function() {
            checkForm.find('input[name=mkdf_lms_question_answer]').val(this.value);
            nextForm.find('input[name=mkdf_lms_question_answer]').val(this.value);
            prevForm.find('input[name=mkdf_lms_question_answer]').val(this.value);
            finishForm.find('input[name=mkdf_lms_question_answer]').val(this.value);
        });

        checkboxes.on('change', function() {
            var values = $('input[type=checkbox]:checked').map(function() {
                return this.value;
            }).get().join(',');
            checkForm.find('input[name=mkdf_lms_question_answer]').val(values);
            nextForm.find('input[name=mkdf_lms_question_answer]').val(values);
            prevForm.find('input[name=mkdf_lms_question_answer]').val(values);
            finishForm.find('input[name=mkdf_lms_question_answer]').val(values);
        }).change();

        textbox.on("change paste keyup", function() {
            checkForm.find('input[name=mkdf_lms_question_answer]').val($(this).val());
            nextForm.find('input[name=mkdf_lms_question_answer]').val($(this).val());
            prevForm.find('input[name=mkdf_lms_question_answer]').val($(this).val());
            finishForm.find('input[name=mkdf_lms_question_answer]').val($(this).val());
        });
    }

    function mkdfUpdateQuestionPosition(questionPosition) {
        var positionHolder = $('.mkdf-question-number-completed');
        positionHolder.text(questionPosition);
    }

    function mkdfUpdateQuestionId(questionId) {
        var finishForm = $('.mkdf-lms-finish-quiz-form');
        finishForm.find('input[name=mkdf_lms_question_id]').val(questionId);
    }

    function mkdfValidateAnswer(answersHolder, result, originalResult, answerChecked) {
        var radios = answersHolder.find('input[type=radio]');
        var checkboxes = answersHolder.find('input[type=checkbox]');
        var textbox = answersHolder.find('input[type=text]');

        if(answerChecked == 'yes') {
            answersHolder.find('input').prop("disabled", true);
            if (radios.length) {
                $.each(result, function (key, val) {
                    var input = answersHolder.find('input[type=radio][value=' + key + ']');
                    if (val == true) {
                        input.parent().addClass('mkdf-true');
                    } else {
                        input.parent().addClass('mkdf-false');
                    }
                });
                $.each(originalResult, function (key, val) {
                    var input = answersHolder.find('input[type=radio][value=' + key + ']');
                    if (val == true) {
                        input.parent().addClass('mkdf-base-true');
                    }
                });
            }

            if (checkboxes.length) {
                $.each(result, function (key, val) {
                    var input = answersHolder.find('input[type=checkbox][value=' + key + ']');
                    if (val == true) {
                        input.parent().addClass('mkdf-true');
                    } else {
                        input.parent().addClass('mkdf-false');
                    }
                });
                $.each(originalResult, function (key, val) {
                    var input = answersHolder.find('input[type=checkbox][value=' + key + ']');
                    if (val == true) {
                        input.parent().addClass('mkdf-base-true');
                    }
                });
            }

            if (textbox.length) {
                if (result) {
                    textbox.parent().addClass('mkdf-true');
                } else {
                    textbox.parent().addClass('mkdf-false');
                    textbox.parent().append('<p class="mkdf-base-answer">' + originalResult + '</p>');
                }
            }
        }
    }

    function mkdfQuestionHint() {
        var answersHolder = $('.mkdf-question-answer-wrapper');
        $('.mkdf-lms-question-actions-hint-form').on('submit',function(e) {
            e.preventDefault();
            var form = $(this);
            var formData = form.serialize();
            var timeRemaining = $('input[name=mkdf_lms_time_remaining]');
            formData += '&mkdf_lms_time_remaining=' + timeRemaining.val();
            var ajaxData = {
                action: 'mkdf_lms_check_question_hint',
                post: formData
            };
            form.find('input').prop("disabled", true);
            $.ajax({
                type: 'POST',
                data: ajaxData,
                url: mkdfGlobalVars.vars.mkdfAjaxUrl,
                success: function (data) {
                    var response = JSON.parse(data);
                    if(response.status == 'success'){
                        answersHolder.append(response.data.html);
                    }
                }
            });
        });
    }

    function mkdfQuestionCheck() {
        var answersHolder = $('.mkdf-question-answer-wrapper');
        $('.mkdf-lms-question-actions-check-form').on('submit',function(e) {
            e.preventDefault();
            var form = $(this);
            var formData = form.serialize();
            var timeRemaining = $('input[name=mkdf_lms_time_remaining]');
            formData += '&mkdf_lms_time_remaining=' + timeRemaining.val();
            var ajaxData = {
                action: 'mkdf_lms_check_question_answer',
                post: formData
            };
            form.find('input').prop("disabled", true);
            $.ajax({
                type: 'POST',
                data: ajaxData,
                url: mkdfGlobalVars.vars.mkdfAjaxUrl,
                success: function (data) {
                    var response = JSON.parse(data);
                    if(response.status == 'success'){
                        var result = response.data.result;
                        var originalResult = response.data.original_result;
                        var answerChecked = response.data.answer_checked;
                        mkdfValidateAnswer(answersHolder, result, originalResult, answerChecked);
                    }
                }
            });
        });
    }

    function mkdfQuestionChange() {
        var questionHolder = $('.mkdf-quiz-question-wrapper');
        $('.mkdf-lms-question-prev-form, .mkdf-lms-question-next-form').on('submit',function(e) {
            e.preventDefault();
            var form = $(this);
            var formData = form.serialize();
            var timeRemaining = $('input[name=mkdf_lms_time_remaining]');
            var retakeId = $('input[name=mkdf_lms_retake_id]');
            formData += '&mkdf_lms_time_remaining=' + timeRemaining.val();
            formData += '&mkdf_lms_retake_id=' + retakeId.val();
            var ajaxData = {
                action: 'mkdf_lms_change_question',
                post: formData
            };

            $.ajax({
                type: 'POST',
                data: ajaxData,
                url: mkdfGlobalVars.vars.mkdfAjaxUrl,
                success: function (data) {
                    var response = JSON.parse(data);
                    if(response.status == 'success'){
                        questionHolder.html(response.data.html);
                        var answersHolder = $('.mkdf-question-answer-wrapper');
                        var result = response.data.result;
                        var originalResult = response.data.original_result;
                        var answerChecked = response.data.answer_checked;
                        mkdfQuestionHint();
                        mkdfQuestionCheck();
                        mkdfQuestionChange();
                        mkdfQuestionAnswerChange();
                        mkdfUpdateQuestionPosition(response.data.question_position);
                        mkdfUpdateQuestionId(response.data.question_id);
                        mkdfValidateAnswer(answersHolder, result, originalResult, answerChecked);
                        mkdf.modules.quiz.mkdfFinishQuiz();
                    }
                }
            });
        });
    }

    function mkdfQuestionSave() {
        $(window).unload(function() {
            var form = $('.mkdf-lms-question-next-form');
            if(!form.length) {
                form = $('mkdf-lms-question-prev-form');
            }
            var formData = form.serialize();
            var timeRemaining = $('input[name=mkdf_lms_time_remaining]');
            var retakeId = $('input[name=mkdf_lms_retake_id]');
            formData += '&mkdf_lms_time_remaining=' + timeRemaining.val();
            formData += '&mkdf_lms_retake_id=' + retakeId.val();
            console.log(formData);
            var ajaxData = {
                action: 'mkdf_lms_save_question',
                post: formData
            };

            $.ajax({
                type: 'POST',
                data: ajaxData,
                async: false,
                url: mkdfGlobalVars.vars.mkdfAjaxUrl
            });
        });
    }

})(jQuery);