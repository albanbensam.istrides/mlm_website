<?php

if (!function_exists('mkdf_lms_map_question_meta')) {
    function mkdf_lms_map_question_meta() {

        $meta_box = iacademy_mikado_create_meta_box(array(
            'scope' => 'question',
            'title' => esc_html__('Question Settings', 'mkdf-lms'),
            'name'  => 'question_settings_meta_box'
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_question_description_meta',
            'type'        => 'textarea',
            'label'       => esc_html__('Question Description', 'mkdf-lms'),
            'description' => esc_html__('Set duration for question', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name' => 'mkdf_question_type_meta',
            'type' => 'select',
            'label' => esc_html__('Question Type', 'mkdf-lms'),
            'description' => esc_html__('Choose type for question', 'mkdf-lms'),
            'default_value' => 'multi_choice',
            'parent' => $meta_box,
            'options' => array(
                'multi_choice' => esc_html__('Multi Choice', 'mkdf-lms'),
                'single_choice' => esc_html__('Single Choice', 'mkdf-lms'),
                'text' => esc_html__('Text', 'mkdf-lms'),
            ),
            'args' => array(
                'dependence' => true,
                'hide' => array(
                    'multi_choice'  => '#mkdf_answers_holder_text_section_container',
                    'single_choice' => '#mkdf_answers_holder_text_section_container',
                    'text'          => '#mkdf_answers_holder_choices_section_container'
                ),
                'show' => array(
                    'multi_choice'  => '#mkdf_answers_holder_choices_section_container',
                    'single_choice' => '#mkdf_answers_holder_choices_section_container',
                    'text'          => '#mkdf_answers_holder_text_section_container'
                ),
                'use_as_switcher' => true,
                'switch_type'     => 'single_yesno',
                'switch_property' => 'mkdf_question_answer_true_meta',
                'switch_enabled'  => 'single_choice'
            )
        ));


        //Choice Type
        $question_answers_single_container = iacademy_mikado_add_admin_container(array(
            'type'            => 'container',
            'name'            => 'answers_holder_choices_section_container',
            'parent'          => $meta_box,
            'hidden_property' => 'mkdf_question_type_meta',
            'hidden_values'   => array('text')
        ));

        iacademy_mikado_add_table_repeater_field(array(
                'name'        => 'mkdf_answers_list_meta',
                'parent'      => $question_answers_single_container,
                'button_text' => '',
                'fields'      => array(
                    array(
                        'type'        => 'text',
                        'name'        => 'mkdf_question_answer_title_meta',
                        'label'       => '',
                        'description' => '',
                        'th'          => esc_html__('Answer text', 'mkdf-lms')
                    ),
                    array(
                        'type'          => 'yesno',
                        'name'          => 'mkdf_question_answer_true_meta',
                        'default_value' => 'no',
                        'label'         => '',
                        'description'   => '',
                        'th'            => esc_html__('Correct?', 'mkdf-lms')
                    )
                )
            )
        );

        //Text Type
        $question_answers_text_container = iacademy_mikado_add_admin_container(array(
            'type'            => 'container',
            'name'            => 'answers_holder_text_section_container',
            'parent'          => $meta_box,
            'hidden_property' => 'mkdf_question_type_meta',
            'hidden_values'   => array('single_choice', 'multi_choice')
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_answers_text_meta',
            'type'        => 'textarea',
            'label'       => esc_html__('Answer', 'mkdf-lms'),
            'description' => '',
            'parent'      => $question_answers_text_container,
            'args'        => array(
                'col_width' => 3
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_question_mark_meta',
            'type'        => 'text',
            'label'       => esc_html__('Question Mark', 'mkdf-lms'),
            'description' => esc_html__('Set mark that is given for correct answer', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_question_hint_meta',
            'type'        => 'textarea',
            'label'       => esc_html__('Question Hint', 'mkdf-lms'),
            'description' => esc_html__('Set Hint that can be displayed to student', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

    }

    add_action('iacademy_mikado_meta_boxes_map', 'mkdf_lms_map_question_meta', 5);
}