<?php
$question_slug = str_replace('_', '-', $question_type);
?>
<div class="mkdf-question-single-wrapper">
    <div class="mkdf-question-title-wrapper">
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/title', 'question', '', $params); ?>
    </div>
    <div class="mkdf-question-text-wrapper">
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/text', 'question', '', $params); ?>
    </div>
    <div class="mkdf-question-answer-wrapper">
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/answers', 'question', $question_slug, $params); ?>
        <?php if($question_params['show_hint'] == 'yes') { ?>
            <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/hint', 'question', '', $params); ?>
        <?php } ?>
    </div>
    <div class="mkdf-question-actions-wrapper">
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/actions-prev-form', 'question', '', $params); ?>
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/actions-hint-form', 'question', '', $params); ?>
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/actions-check-form', 'question', '', $params); ?>
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/actions-next-form', 'question', '', $params); ?>
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/actions-finish', 'quiz', '', $params); ?>
    </div>
</div>