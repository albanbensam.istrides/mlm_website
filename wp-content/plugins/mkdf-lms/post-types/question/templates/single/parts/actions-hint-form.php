<?php if($question_params['show_hint'] != 'yes') { ?>
<form action='' method='post' class="mkdf-lms-question-actions-hint-form">
    <input type='hidden' name='mkdf_lms_questions' value='<?php echo esc_attr($questions); ?>' />
    <input type='hidden' name='mkdf_lms_question_id' value='<?php echo esc_attr($question_id); ?>' />
    <input type='hidden' name='mkdf_lms_course_id' value='<?php echo esc_attr($course_id); ?>' />
    <input type='hidden' name='mkdf_lms_quiz_id' value='<?php echo esc_attr($quiz_id); ?>' />
    <div class="mkdf-question-actions">
        <?php
        echo iacademy_mikado_get_button_html(
            array(
                'custom_class' => 'mkdf-hint-question',
                'html_type' => 'input',
                'input_name' => 'submit',
                'size' => 'medium',
                'type' => 'outline',
                'text' => esc_html__('Hint', 'mkdf-lms')
            )
        );
        ?>
    </div>
</form>
<?php } ?>