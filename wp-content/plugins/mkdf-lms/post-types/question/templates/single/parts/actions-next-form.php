<?php if($next_question !== -1) {
    $value = isset($question_params['answers']) && $question_params['answers'] != '' ? $question_params['answers'] : '';
?>
<form action='' method='post' class="mkdf-lms-question-next-form">
    <input type='hidden' name='mkdf_lms_questions' value='<?php echo esc_attr($questions); ?>' />
    <input type='hidden' name='mkdf_lms_question_id' value='<?php echo esc_attr($question_id); ?>' />
    <input type='hidden' name='mkdf_lms_course_id' value='<?php echo esc_attr($course_id); ?>' />
    <input type='hidden' name='mkdf_lms_quiz_id' value='<?php echo esc_attr($quiz_id); ?>' />
    <input type='hidden' name='mkdf_lms_change_question' value='<?php echo esc_attr($next_question); ?>' />
    <input type='hidden' name='mkdf_lms_question_answer' value='<?php echo esc_attr($value); ?>' />
    <div class="mkdf-question-actions">
        <?php
        echo iacademy_mikado_get_button_html(
            array(
                'custom_class' => 'mkdf-next-question',
                'html_type' => 'input',
                'input_name' => 'submit',
                'size' => 'medium',
                'text' => esc_html__('Next >', 'mkdf-lms')
            )
        );
        ?>
    </div>
</form>
<?php } ?>