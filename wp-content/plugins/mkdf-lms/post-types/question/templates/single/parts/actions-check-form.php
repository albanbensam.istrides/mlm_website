<?php $value = isset($question_params['answers']) && $question_params['answers'] != '' ? $question_params['answers'] : ''; ?>
<?php if($question_params['answer_checked'] != 'yes') { ?>
<form action='' method='post' class="mkdf-lms-question-actions-check-form">
    <input type='hidden' name='mkdf_lms_questions' value='<?php echo esc_attr($questions); ?>' />
    <input type='hidden' name='mkdf_lms_question_id' value='<?php echo esc_attr($question_id); ?>' />
    <input type='hidden' name='mkdf_lms_course_id' value='<?php echo esc_attr($course_id); ?>' />
    <input type='hidden' name='mkdf_lms_quiz_id' value='<?php echo esc_attr($quiz_id); ?>' />
    <input type='hidden' name='mkdf_lms_question_answer' value='<?php echo esc_attr($value); ?>' />
    <div class="mkdf-question-actions">
        <?php
        echo iacademy_mikado_get_button_html(
            array(
                'custom_class' => 'mkdf-check-question',
                'html_type' => 'input',
                'input_name' => 'submit',
                'size' => 'medium',
                'type' => 'outline',
                'text' => esc_html__('Check Answer', 'mkdf-lms')
            )
        );
        ?>
    </div>
</form>
<?php } ?>