<?php
$value = isset($question_params['answers']) && $question_params['answers'] != '' ? $question_params['answers'] : '';
?>
<div class="mkdf-question-answers">
    <div class="mkdf-answer-wrapper mkdf-answer-text">
        <input type="text" title="question_answer" name="question_answer" value="<?php echo esc_attr($value); ?>"/>
    </div>
</div>
