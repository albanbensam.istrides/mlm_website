<?php if(!empty($quiz_results) && !$empty) { ?>
    <div class="mkdf-quiz-retakes">
        <div class="mkdf-results-caption">
            <?php echo esc_html__('Other results', 'mkdf-lms'); ?>
        </div>
        <table>
            <thead>
                <tr>
                    <th>
                        <?php echo esc_html__('#', 'mkdf-lms'); ?>
                    </th>
                    <th>
                        <?php echo esc_html__('Date', 'mkdf-lms'); ?>
                    </th>
                    <th>
                        <?php echo esc_html__('Time', 'mkdf-lms'); ?>
                    </th>
                    <th>
                        <?php echo esc_html__('Result', 'mkdf-lms'); ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($quiz_results as $key => $quiz_result) { ?>
                    <?php if($quiz_result['status'] == 'completed') { ?>
                        <tr>
                            <td>
                                <?php echo esc_html($key + 1); ?>
                            </td>
                            <td>
                                <?php echo esc_html($quiz_result['timestamp']); ?>
                            </td>
                            <td>
                                <?php echo esc_html($quiz_result['time']); ?>
                            </td>
                            <td>
                                <?php echo esc_html($quiz_result['result']) . '%'; ?>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
            </tbody>
        </table>
    </div>
<?php }