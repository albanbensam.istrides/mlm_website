<div class="mkdf-quiz-info-top">
    <div class="mkdf-quiz-questions-number">
        <i class="icon_folder-alt" aria-hidden="true"></i>
        <span class="mkdf-question-number-completed"><?php echo esc_html($question_position); ?></span> /
        <span class="mkdf-question-number-total"><?php echo esc_html($questions_number); ?></span>
    </div>
    <?php if($time_remaining != "") { ?>
    <div class="mkdf-quiz-duration">
        <i class=" icon_clock_alt" aria-hidden="true"></i>
        <span class="mkdf-duration-value" id="mkdf-quiz-timer" data-duration="<?php echo esc_attr($time_remaining) ?>"><?php echo esc_html($time_remaining_formatted); ?></span>
        <span class="mkdf-duration-parameter"><?php esc_html_e('(mm:ss)', 'mkdf-lms'); ?></span>
    </div>
    <input type='hidden' name='mkdf_lms_time_remaining' value='' />
    <?php } ?>
</div>