<?php if (!$first_attempt) { ?>
    <div class="mkdf-quiz-results">
        <?php if(isset($post_message) && !empty($post_message)) { ?>
        <div class="mkdf-results-message">
            <?php echo esc_html($post_message); ?>
        </div>
        <?php } ?>
        <div class="mkdf-results-caption">
            <?php echo esc_html__('You have reached ', 'mkdf-lms') . $points .  esc_html__(' of ', 'mkdf-lms') . $points_t .  esc_html__(' points ', 'mkdf-lms') . '(' . $points_p . '%)'; ?>
        </div>
        <div class="mkdf-results-values">
            <div class="mkdf-results-correct"><?php echo esc_html__('Correct', 'mkdf-lms') . ' ' . $correct ?></div>
            <div class="mkdf-results-wrong"><?php echo esc_html__('Wrong', 'mkdf-lms') . ' ' . $wrong ?></div>
            <div class="mkdf-results-empty"><?php echo esc_html__('Empty', 'mkdf-lms') . ' ' . $empty ?></div>
            <div class="mkdf-results-points"><?php echo esc_html__('Points', 'mkdf-lms') . ' ' . $points . '/' . $points_t ?></div>
            <div class="mkdf-results-time"><?php echo esc_html__('Time', 'mkdf-lms') . ' ' . $time ?></div>
        </div>
    </div>
    <div class="mkdf-quiz-message">
        <?php if($points_p < $required_p) { ?>
            <div class="mkdf-message-error">
                <?php echo esc_html__('Your quiz grade - failed. Quiz requirement', 'mkdf-lms') . ' ' . esc_attr($required_p) . '%'; ?>
            </div>
        <?php } else { ?>
            <div class="mkdf-message-error">
                <?php echo esc_html__('Your quiz grade - success.', 'mkdf-lms'); ?>
            </div>
        <?php } ?>
    </div>
<?php } ?>
