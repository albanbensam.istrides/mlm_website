<form action='' method='post' class="mkdf-lms-finish-quiz-form">
    <input type='hidden' name='mkdf_lms_questions' value='<?php echo esc_attr($questions); ?>' />
    <input type='hidden' name='mkdf_lms_question_id' value='<?php echo esc_attr($question_id); ?>' />
    <input type='hidden' name='mkdf_lms_quiz_id' value='<?php echo esc_attr($quiz_id); ?>' />
    <input type='hidden' name='mkdf_lms_course_id' value='<?php echo esc_attr($course_id); ?>' />
    <input type='hidden' name='mkdf_lms_retake_id' value='<?php echo esc_attr($retake); ?>' />
    <input type='hidden' name='mkdf_lms_question_answer' value='<?php echo esc_attr($value); ?>' />
    <div class="mkdf-question-actions">
        <?php
        echo iacademy_mikado_get_button_html(
            array(
                'custom_class' => 'mkdf-quiz-finish',
                'html_type' => 'input',
                'input_name' => 'submit',
                'size' => 'medium',
                'text' => esc_html__('Finish', 'mkdf-lms')
            )
        );
        ?>
    </div>
</form>