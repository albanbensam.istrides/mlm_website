<div class="mkdf-quiz-info-top">
    <div class="mkdf-quiz-questions-number">
        <i class="icon_folder-alt" aria-hidden="true"></i>
        <span class="mkdf-question-number"><?php echo esc_html($questions_number); ?></span>
        <span class="mkdf-question-label"><?php echo esc_html($questions_label); ?></span>
    </div>
    <?php if($quiz_duration_value != "") { ?>
    <div class="mkdf-quiz-duration">
        <i class=" icon_clock_alt" aria-hidden="true"></i>
        <span class="mkdf-duration-value"><?php echo esc_html($quiz_duration_value); ?></span>
        <span class="mkdf-duration-parameter"><?php echo esc_html($quiz_duration_parameter); ?></span>
    </div>
    <?php } ?>
</div>