<div class="mkdf-quiz-active-wrapper">
    <div class="mkdf-quiz-title-wrapper">
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/title', 'quiz', '', $params); ?>
    </div>
    <div class="mkdf-quiz-info-top-wrapper">
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/info-top', 'quiz', 'active', $params); ?>
    </div>
    <div class="mkdf-quiz-question-wrapper">
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/layout-collections/default', 'question', '', $params); ?>
    </div>
</div>