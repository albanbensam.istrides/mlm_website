<div class="mkdf-quiz-single-wrapper">
    <div class="mkdf-quiz-title-wrapper">
        <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/title', 'quiz'); ?>
    </div>
    <div class="mkdf-quiz-info-top-wrapper">
        <?php mkdf_lms_template_quiz_info_top($params); ?>
        <?php mkdf_lms_template_start_quiz_button($params); ?>
    </div>
    <div class="mkdf-quiz-result-wrapper">
        <?php mkdf_lms_template_quiz_status($params); ?>
    </div>
    <div class="mkdf-quiz-old-results-wrapper">
        <?php mkdf_lms_template_quiz_results($params); ?>
    </div>
</div>