<?php

if (!function_exists('mkdf_lms_map_quiz_meta')) {
    function mkdf_lms_map_quiz_meta() {

        $meta_box = iacademy_mikado_create_meta_box(array(
            'scope' => 'quiz',
            'title' => esc_html__('Quiz Settings', 'mkdf-lms'),
            'name'  => 'quiz_settings_meta_box'
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_quiz_description_meta',
            'type'        => 'textarea',
            'label'       => esc_html__('Quiz Description', 'mkdf-lms'),
            'description' => esc_html__('Set duration for quiz', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));


        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_quiz_duration_meta',
            'type'        => 'text',
            'label'       => esc_html__('Quiz Duration', 'mkdf-lms'),
            'description' => esc_html__('Set duration for quiz', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name' => 'mkdf_quiz_duration_parameter_meta',
            'type' => 'select',
            'label' => esc_html__('Quiz Duration Parameter', 'mkdf-lms'),
            'description' => esc_html__('Choose parameter for quiz duration', 'mkdf-lms'),
            'default_value' => 'minutes',
            'parent' => $meta_box,
            'options' => array(
                'seconds' => esc_html__('Seconds', 'mkdf-lms'),
                'minutes' => esc_html__('Minutes', 'mkdf-lms'),
                'hours' => esc_html__('Hours', 'mkdf-lms')
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_quiz_number_retakes_meta',
            'type'        => 'text',
            'label'       => esc_html__('Number of Retakes', 'mkdf-lms'),
            'description' => esc_html__('Set allowed number of quiz retakes.', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_quiz_passing_percentage_meta',
            'type'        => 'text',
            'label'       => esc_html__('Passing Percentage', 'mkdf-lms'),
            'description' => esc_html__('Set value required to pass the quiz', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_quiz_post_message_meta',
            'type'        => 'textarea',
            'label'       => esc_html__('Quiz Post Message', 'mkdf-lms'),
            'description' => esc_html__('Set message that will be displayed after the quiz is completed', 'mkdf-lms'),
            'parent'      => $meta_box,
            'args'        => array(
                'col_width' => 3
            )
        ));

    }

    add_action('iacademy_mikado_meta_boxes_map', 'mkdf_lms_map_quiz_meta', 5);
}