<?php

if (!function_exists('mkdf_lms_map_quiz_questions_meta')) {
    function mkdf_lms_map_quiz_questions_meta() {

        $mkd_questions = array();
        $questions = get_posts(
            array(
                'numberposts' => -1,
                'post_type' => 'question',
                'post_status' => 'publish'
            )
        );
        foreach ($questions as $question) {
            $mkd_questions[$question->ID] = $question->post_title;
        }

        $meta_box = iacademy_mikado_create_meta_box(array(
            'scope' => 'quiz',
            'title' => esc_html__('Quiz Questions', 'mkdf-lms'),
            'name'  => 'quiz_questions_meta_box'
        ));

        iacademy_mikado_add_table_repeater_field(array(
                'name'        => 'mkdf_quiz_question_list_meta',
                'parent'      => $meta_box,
                'button_text' => esc_html__('Add Question', 'mkdf-lms'),
                'fields'      => array(
                    array(
                        'name'        => 'mkdf_quiz_question_meta',
                        'type'        => 'select',
                        'label'       => '',
                        'description' => '',
                        'parent'      => $meta_box,
                        'options'     => $mkd_questions,
                        'args' => array(
                            'select2' => true,
                            'colWidth'=> 12
                        ),
                        'th'          => esc_html__('Question', 'mkdf-lms')
                    )
                )
            )
        );
    }

    add_action('iacademy_mikado_meta_boxes_map', 'mkdf_lms_map_quiz_questions_meta', 4);
}