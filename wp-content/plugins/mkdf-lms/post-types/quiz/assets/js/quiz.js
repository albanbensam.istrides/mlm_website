(function($) {
    'use strict';

    var quiz = {};
    mkdf.modules.quiz = quiz;

    quiz.mkdfStartQuiz = mkdfStartQuiz;
    quiz.mkdfFinishQuiz = mkdfFinishQuiz;

    quiz.mkdfOnDocumentReady = mkdfOnDocumentReady;
    quiz.mkdfOnWindowLoad = mkdfOnWindowLoad;
    quiz.mkdfOnWindowResize = mkdfOnWindowResize;
    quiz.mkdfOnWindowScroll = mkdfOnWindowScroll;

    $(document).ready(mkdfOnDocumentReady);
    $(window).load(mkdfOnWindowLoad);
    $(window).resize(mkdfOnWindowResize);
    $(window).scroll(mkdfOnWindowScroll);

    /*
     All functions to be called on $(document).ready() should be in this function
     */
    function mkdfOnDocumentReady() {
        mkdfStartQuiz();
        mkdfFinishQuiz();
    }

    /*
     All functions to be called on $(window).load() should be in this function
     */
    function mkdfOnWindowLoad() {
        
    }

    /*
     All functions to be called on $(window).resize() should be in this function
     */
    function mkdfOnWindowResize() {

    }

    /*
     All functions to be called on $(window).scroll() should be in this function
     */
    function mkdfOnWindowScroll() {

    }

    function mkdfStartQuiz(){
        var popupContent = $('.mkdf-quiz-single-holder');
        var preloader = $('.mkdf-course-item-preloader');
        $('.mkdf-lms-start-quiz-form').on('submit',function(e) {
            e.preventDefault();
            preloader.removeClass('mkdf-hide');
            var form = $(this);
            var formData = form.serialize();
            var ajaxData = {
                action: 'mkdf_lms_start_quiz',
                post: formData
            };

            $.ajax({
                type: 'POST',
                data: ajaxData,
                url: mkdfGlobalVars.vars.mkdfAjaxUrl,
                success: function (data) {
                    var response = JSON.parse(data);
                    if(response.status == 'success'){
                        var questionId = response.data.question_id;
                        var quizId = response.data.quiz_id;
                        var courseId = response.data.course_id;
                        var retake = response.data.retake;
                        mkdfLoadQuizQuestion(questionId, quizId, courseId, retake, popupContent);
                        mkdf.modules.question.mkdfQuestionSave();
                    } else {
                        alert("An error occurred");
                        preloader.addClass('mkdf-hide');
                    }
                }
            });
        });
    }

    function mkdfLoadQuizQuestion(questionId ,quizId, courseId, retake, container){
        var preloader = $('.mkdf-course-item-preloader');
        var ajaxData = {
            action: 'mkdf_lms_load_first_question',
            question_id : questionId,
            quiz_id : quizId,
            course_id : courseId,
            retake : retake
        };
        $.ajax({
            type: 'POST',
            data: ajaxData,
            url: mkdfGlobalVars.vars.mkdfAjaxUrl,
            success: function (data) {
                var response = JSON.parse(data);
                if(response.status == 'success'){
                    container.html(response.data.html);
                    mkdf.modules.question.mkdfQuestionHint();
                    mkdf.modules.question.mkdfQuestionCheck();
                    mkdf.modules.question.mkdfQuestionChange();
                    mkdf.modules.question.mkdfQuestionAnswerChange();
                    mkdfFinishQuiz();

                    var answersHolder = $('.mkdf-question-answer-wrapper');
                    var result = response.data.result;
                    var originalResult = response.data.original_result;
                    var answerChecked = response.data.answer_checked;
                    mkdf.modules.question.mkdfValidateAnswer(answersHolder, result, originalResult, answerChecked);

                    var timerHolder = $('#mkdf-quiz-timer');
                    var duration = timerHolder.data('duration');
                    var timeRemaining = $('input[name=mkdf_lms_time_remaining]');
                    timerHolder.vTimer('start', {duration: duration})
                        .on('update', function (e, remaining) {
                            // total seconds
                            var seconds = remaining;
                            // calculate seconds
                            var s = seconds % 60;
                            // add leading zero to seconds if needed
                            s = s < 10 ? "0" + s : s;
                            // calculate minutes
                            var m = Math.floor(seconds / 60) % 60;
                            // add leading zero to minutes if needed
                            m = m < 10 ? "0" + m : m;
                            // calculate hours
                            var h = Math.floor(seconds / 60 / 60);
                            h = h < 10 ? "0" + h : h;
                            var time = h + ":" + m + ":" + s;
                            timerHolder.text(time);
                            timeRemaining.val(remaining);
                        })
                        .on('complete', function () {
                            $('.mkdf-lms-finish-quiz-form').submit();
                        });
                    preloader.addClass('mkdf-hide');
                } else {
                    alert("An error occurred");
                    preloader.addClass('mkdf-hide');
                }
            }
        });

    }

    function mkdfFinishQuiz(){
        var popupContent = $('.mkdf-quiz-single-holder');
        var preloader = $('.mkdf-course-item-preloader');
        $('.mkdf-lms-finish-quiz-form').on('submit',function(e) {
            e.preventDefault();
            preloader.removeClass('mkdf-hide');
            var form = $(this);
            var formData = form.serialize();
            var timeRemaining = $('input[name=mkdf_lms_time_remaining]');
            formData += '&mkdf_lms_time_remaining=' + timeRemaining.val();
            var ajaxData = {
                action: 'mkdf_lms_finish_quiz',
                post: formData
            };

            $.ajax({
                type: 'POST',
                data: ajaxData,
                url: mkdfGlobalVars.vars.mkdfAjaxUrl,
                success: function (data) {
                    var response = JSON.parse(data);
                    if(response.status == 'success'){
                        popupContent.replaceWith(response.data.html);
                        mkdfStartQuiz();
                        preloader.addClass('mkdf-hide');
                    } else {
                        alert("An error occurred");
                        preloader.addClass('mkdf-hide');
                    }
                }
            });
        });
    }

})(jQuery);