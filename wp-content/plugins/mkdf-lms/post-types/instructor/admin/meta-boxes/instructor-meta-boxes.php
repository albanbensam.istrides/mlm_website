<?php

if(!function_exists('mkdf_lms_map_instructor_single_meta')) {
    function mkdf_lms_map_instructor_single_meta() {

        $meta_box = iacademy_mikado_create_meta_box(array(
            'scope' => 'instructor',
            'title' => esc_html__('Instructor Info', 'mkdf-lms'),
            'name'  => 'instructor_meta'
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_instructor_title',
            'type'        => 'text',
            'label'       => esc_html__('Title', 'mkdf-lms'),
            'description' => esc_html__('The members\'s title', 'mkdf-lms'),
            'parent'      => $meta_box
        ));

        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_instructor_vita',
            'type'        => 'textarea',
            'label'       => esc_html__('Brief Vita', 'mkdf-lms'),
            'description' => esc_html__('The members\'s brief vita', 'mkdf-lms'),
            'parent'      => $meta_box
        ));
        
        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_instructor_email',
            'type'        => 'text',
            'label'       => esc_html__('Email', 'mkdf-lms'),
            'description' => esc_html__('The members\'s email', 'mkdf-lms'),
            'parent'      => $meta_box
        ));


        iacademy_mikado_create_meta_box_field(array(
            'name'        => 'mkdf_instructor_resume',
            'type'        => 'file',
            'label'       => esc_html__('Resume', 'mkdf-lms'),
            'description' => esc_html__('Upload members\'s resume', 'mkdf-lms'),
            'parent'      => $meta_box
        ));

        for($x = 1; $x < 6; $x++) {

            $social_icon_group = iacademy_mikado_add_admin_group(array(
                'name'   => 'mkdf_instructor_social_icon_group'.$x,
                'title'  => esc_html__('Social Link ', 'mkdf-lms').$x,
                'parent' => $meta_box
            ));

                $social_row1 = iacademy_mikado_add_admin_row(array(
                    'name'   => 'mkdf_instructor_social_icon_row1'.$x,
                    'parent' => $social_icon_group
                ));

                    IacademyMikadoIconCollections::get_instance()->getSocialIconsMetaBoxOrOption(array(
                        'label' => esc_html__('Icon ', 'mkdf-lms').$x,
                        'parent' => $social_row1,
                        'name' => 'mkdf_instructor_social_icon_pack_'.$x,
                        'defaul_icon_pack' => '',
                        'type' => 'meta-box',
                        'field_type' => 'simple'
                    ));

                $social_row2 = iacademy_mikado_add_admin_row(array(
                    'name'   => 'mkdf_instructor_social_icon_row2'.$x,
                    'parent' => $social_icon_group
                ));

                    iacademy_mikado_create_meta_box_field(array(
                        'type'            => 'textsimple',
                        'label'           => esc_html__('Link', 'mkdf-lms'),
                        'name'            => 'mkdf_instructor_social_icon_'.$x.'_link',
                        'hidden_property' => 'mkdf_instructor_social_icon_pack_'.$x,
                        'hidden_value'    => '',
                        'parent'          => $social_row2
                    ));
	
			        iacademy_mikado_create_meta_box_field(array(
				        'type'          => 'selectsimple',
				        'label'         => esc_html__('Target', 'mkdf-lms'),
				        'name'          => 'mkdf_instructor_social_icon_'.$x.'_target',
				        'options'       => iacademy_mikado_get_link_target_array(),
				        'hidden_property' => 'mkdf_instructor_social_icon_'.$x.'_link',
				        'hidden_value'    => '',
				        'parent'          => $social_row2
			        ));
        }
    }

    add_action('iacademy_mikado_meta_boxes_map', 'mkdf_lms_map_instructor_single_meta', 46);
}