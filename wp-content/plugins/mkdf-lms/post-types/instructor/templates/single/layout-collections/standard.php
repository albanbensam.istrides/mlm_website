<div class="mkdf-instructor-single-info-holder">
    <div class="mkdf-grid-row">
        <div class="mkdf-ts-info-holder mkdf-grid-col-3">
            <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/image', 'instructor', '', $params); ?>
            <div class="mkdf-ts-insturctor-bio-holder">
                <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/name', 'instructor', '', $params); ?>
                <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/title', 'instructor', '', $params); ?>
                <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/vita', 'instructor', '', $params); ?>
                <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/social', 'instructor', '', $params); ?>
                <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/email', 'instructor', '', $params); ?>
                <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/resume', 'instructor', '', $params); ?>
            </div>
        </div>
        <div class="mkdf-ts-content-holder mkdf-grid-col-9">
            <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/tabs', 'instructor', '', $params); ?>
        </div>
    </div>
</div>