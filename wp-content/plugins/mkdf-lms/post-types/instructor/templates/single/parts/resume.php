<?php if(!empty($resume)) { ?>
    <div class="mkdf-ts-info-row">
        <span aria-hidden="true" class="icon_document_alt mkdf-ts-bio-icon"></span>
        <a href="<?php echo esc_url($resume); ?>" download target="_blank">
                            <span class="mkdf-ts-bio-info">
                                <?php echo esc_html__('Download Resume', 'mkdf-lms'); ?>
                            </span>
        </a>
    </div>
<?php } ?>