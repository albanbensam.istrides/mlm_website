<?php if(!empty($email)) { ?>
    <div class="mkdf-ts-info-row">
        <span aria-hidden="true" class="icon_mail_alt mkdf-ts-bio-icon"></span>
        <span itemprop="email" class="mkdf-ts-bio-info"><?php echo esc_html__('email: ', 'mkdf-lms').sanitize_email(esc_html($email)); ?></span>
    </div>
<?php } ?>