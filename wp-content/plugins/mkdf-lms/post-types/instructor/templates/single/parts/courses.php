<?php if ( ! empty( $courses ) ) {
	echo iacademy_mikado_execute_shortcode('mkdf_course_list', $courses);
} else {
	esc_html_e( 'Sorry, no posts matched your criteria.', 'mkdf-lms' );
}
