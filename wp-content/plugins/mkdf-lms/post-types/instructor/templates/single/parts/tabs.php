<?php
$tabs = apply_filters( 'iacademy_mikado_single_instructor_tabs', array() );
if ( ! empty( $tabs ) ) :
    ?>

    <div class="mkdf-tabs mkdf-tabs-standard">
        <ul class="mkdf-tabs-nav clearfix">
            <?php foreach ( $tabs as $key => $tab ) : ?>
                <li class="<?php echo esc_attr( $key ); ?>_tab">
                    <a href="#tab-<?php echo esc_attr( $key ); ?>">
                        <span class="mkdf-tab-icon">
                            <?php echo iacademy_mikado_get_module_part($tab['icon']); ?>
                        </span>
                        <span class="mkdf-tab-title">
                            <?php echo apply_filters( 'iacademy_mikado_instructor_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?>
                        </span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        <?php foreach ( $tabs as $key => $tab ) : ?>
            <div class="mkdf-tab-container" id="tab-<?php echo sanitize_title($key); ?>">
                <?php mkdf_lms_get_cpt_single_module_template_part('templates/single/parts/' . $tab['template'], 'instructor', '', $params); ?>
            </div>
        <?php endforeach; ?>
    </div>

<?php endif; ?>