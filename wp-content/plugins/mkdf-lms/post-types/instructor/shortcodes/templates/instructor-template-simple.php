<div class="mkdf-instructor <?php echo esc_attr($instructor_layout) ?>">
	<div class="mkdf-instructor-inner" <?php echo iacademy_mikado_inline_style($background);?>>
		<?php if (get_the_post_thumbnail($instructor_id) !== '') { ?>
			<div class="mkdf-instructor-image">
                <a itemprop="url" href="<?php echo esc_url(get_the_permalink($instructor_id)) ?>">
                    <?php echo get_the_post_thumbnail($instructor_id, 'full'); ?>
                </a>
			</div>
		<?php } ?>
		<div class="mkdf-instructor-info">
            <div class="mkdf-instructor-title-holder">
                <h5 itemprop="name" class="mkdf-instructor-name entry-title">
                    <a itemprop="url" href="<?php echo esc_url(get_the_permalink($instructor_id)) ?>"><?php echo esc_html($title) ?></a>
                </h5>
                <?php if (!empty($position)) { ?>
                    <span class="mkdf-instructor-position"><?php echo esc_html($position); ?></span>
                <?php } ?>
            </div>
		</div>
        <a itemprop="url" href="<?php echo esc_url(get_the_permalink($instructor_id)) ?>" class="mkdf-instructor-link"></a>
	</div>
</div>