<div class="mkdf-instructor <?php echo esc_attr($instructor_layout) ?>">
    <div class="mkdf-instructor-inner">
        <?php if (get_the_post_thumbnail($instructor_id) !== '') { ?>
            <div class="mkdf-instructor-image">
                <?php echo get_the_post_thumbnail($instructor_id); ?>
                <div class="mkdf-instructor-info-tb">
                    <div class="mkdf-instructor-info-tc">
                        <div class="mkdf-instructor-title-holder">
                            <h4 itemprop="name" class="mkdf-instructor-name entry-title">
                                <a itemprop="url" href="<?php echo esc_url(get_the_permalink($instructor_id)) ?>"><?php echo esc_html($title) ?></a>
                            </h4>
                            <?php if (!empty($position)) { ?>
                                <h6 class="mkdf-instructor-position"><?php echo esc_html($position); ?></h6>
                            <?php } ?>
                        </div>
                        <div class="mkdf-instructor-social-holder-between">
                            <div class="mkdf-instructor-social">
                                <div class="mkdf-instructor-social-inner">
                                    <div class="mkdf-instructor-social-wrapp">
                                        <?php foreach($instructor_social_icons as $instructor_social_icon) {
                                            echo wp_kses_post($instructor_social_icon);
                                        } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>