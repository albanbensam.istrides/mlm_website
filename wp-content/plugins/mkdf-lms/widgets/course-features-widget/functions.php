<?php

if(!function_exists('mkdf_lms_register_course_features_widget')) {
	/**
	 * Function that register course features widget
	 */
	function mkdf_lms_register_course_features_widget($widgets) {
		$widgets[] = 'IacademyMikadoCourseFeaturesWidget';
		
		return $widgets;
	}
	
	add_filter('iacademy_mikado_register_widgets', 'mkdf_lms_register_course_features_widget');
}