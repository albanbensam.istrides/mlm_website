<?php

if(!function_exists('mkdf_lms_register_course_list_widget')) {
	/**
	 * Function that register course list widget
	 */
	function mkdf_lms_register_course_list_widget($widgets) {
		$widgets[] = 'IacademyMikadoCourseListWidget';
		
		return $widgets;
	}
	
	add_filter('iacademy_mikado_register_widgets', 'mkdf_lms_register_course_list_widget');
}