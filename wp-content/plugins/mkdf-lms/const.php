<?php

define('MIKADO_LMS_VERSION', '1.1');
define('MIKADO_LMS_ABS_PATH', dirname(__FILE__));
define('MIKADO_LMS_REL_PATH', dirname(plugin_basename(__FILE__ )));
define('MIKADO_LMS_URL_PATH', plugin_dir_url( __FILE__ ));
define('MIKADO_LMS_ASSETS_PATH', MIKADO_LMS_ABS_PATH.'/assets');
define('MIKADO_LMS_ASSETS_URL_PATH', MIKADO_LMS_URL_PATH.'assets');
define('MIKADO_LMS_CPT_PATH', MIKADO_LMS_ABS_PATH.'/post-types');
define('MIKADO_LMS_CPT_URL_PATH', MIKADO_LMS_URL_PATH.'post-types');
define('MIKADO_LMS_SHORTCODES_PATH', MIKADO_LMS_ABS_PATH.'/shortcodes');
define('MIKADO_LMS_SHORTCODES_URL_PATH', MIKADO_LMS_URL_PATH.'shortcodes');