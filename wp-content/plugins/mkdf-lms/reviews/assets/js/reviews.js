(function($) {
    'use strict';

    var course = {};
    mkdf.modules.course = course;

	course.mkdfOnDocumentReady = mkdfOnDocumentReady;
	course.mkdfOnWindowLoad = mkdfOnWindowLoad;
	course.mkdfOnWindowResize = mkdfOnWindowResize;
	course.mkdfOnWindowScroll = mkdfOnWindowScroll;

    $(document).ready(mkdfOnDocumentReady);
    $(window).load(mkdfOnWindowLoad);
    $(window).resize(mkdfOnWindowResize);
    $(window).scroll(mkdfOnWindowScroll);
    
    /* 
     All functions to be called on $(document).ready() should be in this function
     */
    function mkdfOnDocumentReady() {

	    mkdfInitCommentRating();
    }

    /*
     All functions to be called on $(window).load() should be in this function
     */
    function mkdfOnWindowLoad() {

    }

    /*
     All functions to be called on $(window).resize() should be in this function
     */
    function mkdfOnWindowResize() {

    }

    /*
     All functions to be called on $(window).scroll() should be in this function
     */
    function mkdfOnWindowScroll() {
    }

	function mkdfInitCommentRating() {
		var ratingInput = $('#mkdf-rating'),
			ratingValue = ratingInput.val(),
			stars = $('.mkdf-star-rating');

		var addActive = function() {
			for ( var i = 0; i < stars.length; i++ ) {
				var star = stars[i];
				if ( i < ratingValue ) {
					$(star).addClass('active');
				} else {
					$(star).removeClass('active');
				}
			}
		};

		addActive();

		stars.on('click',function(){
			ratingInput.val( $(this).data('value')).trigger('change');
		});

		ratingInput.change(function(){
			ratingValue = ratingInput.val();
			addActive();
		});

	}


})(jQuery);