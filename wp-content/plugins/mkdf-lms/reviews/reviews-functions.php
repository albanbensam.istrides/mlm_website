<?php

if(!function_exists('mkdf_lms_rating_posts_types')) {

	function mkdf_lms_rating_posts_types() {

		$post_types = apply_filters( 'mkdf_lms_rating_post_types', array() );

		return $post_types;
	}

}

if(!function_exists('mkdf_lms_comment_additional_title_field')) {

	function mkdf_lms_comment_additional_title_field($textarea) {
		$post_types = mkdf_lms_rating_posts_types();
		if(is_array($post_types) && count($post_types) > 0) {
			foreach($post_types as $post_type) {
				if (is_singular($post_type)) {

                    $textarea = mkdf_lms_get_module_template_part('reviews/templates/title-field');
                    $textarea .= mkdf_lms_get_module_template_part('reviews/templates/text-field');
                    $textarea .= mkdf_lms_get_module_template_part('reviews/templates/stars-field');
				}
			}
		}

		return $textarea;
	}

	add_filter( 'iacademy_mikado_comment_form_textarea_field', 'mkdf_lms_comment_additional_title_field', 10, 1 );

}

if(!function_exists('mkdf_lms_extend_comment_edit_metafields')) {

	function mkdf_lms_extend_comment_edit_metafields($comment_id) {
		if ((!isset($_POST['extend_comment_update']) || !wp_verify_nonce($_POST['extend_comment_update'], 'extend_comment_update'))) return;

		if ((isset($_POST['mkdf_comment_title'])) && ($_POST['mkdf_comment_title'] != '')):
			$title = wp_filter_nohtml_kses($_POST['mkdf_comment_title']);
			update_comment_meta($comment_id, 'mkdf_comment_title', $title);
		else :
			delete_comment_meta($comment_id, 'mkdf_comment_title');
		endif;

		if ((isset($_POST['mkdf_rating'])) && ($_POST['mkdf_rating'] != '')):
			$rating = wp_filter_nohtml_kses($_POST['mkdf_rating']);
			update_comment_meta($comment_id, 'mkdf_rating', $rating);
		else :
			delete_comment_meta($comment_id, 'mkdf_rating');
		endif;
	}

	add_action('edit_comment', 'mkdf_lms_extend_comment_edit_metafields');
}

if(!function_exists('mkdf_lms_extend_comment_create_meta_box')) {

	function mkdf_lms_extend_comment_create_meta_box() {
		add_meta_box('title', esc_html__('Comment - Reviews', 'mkdf-lms'), 'mkdf_lms_extend_comment_meta_box', 'comment', 'normal', 'high');
	}

	add_action('add_meta_boxes_comment', 'mkdf_lms_extend_comment_create_meta_box');

}

if(!function_exists('mkdf_lms_extend_comment_meta_box')) {

	function mkdf_lms_extend_comment_meta_box($comment) {

		$post_types = mkdf_lms_rating_posts_types();
		if(is_array($post_types) && count($post_types) > 0) {
			foreach($post_types as $post_type) {
				if ($comment->post_type == $post_type) {
					$title = get_comment_meta($comment->comment_ID, 'mkdf_comment_title', true);
					$rating = get_comment_meta($comment->comment_ID, 'mkdf_rating', true);
					wp_nonce_field('extend_comment_update', 'extend_comment_update', false);
					?>
					<p>
						<label for="title"><?php esc_html_e('Comment Title', 'mkdf-lms'); ?></label>
						<input type="text" name="mkdf_comment_title" value="<?php echo esc_attr($title); ?>" class="widefat"/>
					</p>
					<p>
						<label for="rating"><?php esc_html_e('Rating', 'mkdf-lms'); ?>: </label>
						<span class="commentratingbox">
							<?php
							for ($i = 1; $i <= 5; $i++) {
								echo '<span class="commentrating"><input type="radio" name="mkdf_rating" id="rating" value="' . $i . '"';
								if ($rating == $i) echo ' checked="checked"';
								echo ' />' . $i . ' </span>';
							}
							?>
						</span>
					</p>
					<?php
				}
			}
		}
	}
}

if(!function_exists('mkdf_lms_save_comment_meta_data')) {

	function mkdf_lms_save_comment_meta_data($comment_id) {

		if ((isset($_POST['mkdf_comment_title'])) && ($_POST['mkdf_comment_title'] != '')) {
			$title = wp_filter_nohtml_kses($_POST['mkdf_comment_title']);
			add_comment_meta($comment_id, 'mkdf_comment_title', $title);
		}

		if ((isset($_POST['mkdf_rating'])) && ($_POST['mkdf_rating'] != '')) {
			$rating = wp_filter_nohtml_kses($_POST['mkdf_rating']);
			add_comment_meta($comment_id, 'mkdf_rating', $rating);
		}

	}

	add_action('comment_post', 'mkdf_lms_save_comment_meta_data');

}

if(!function_exists('mkdf_lms_verify_comment_meta_data')) {

	function mkdf_lms_verify_comment_meta_data($commentdata) {

		$post_types = mkdf_lms_rating_posts_types();

		if(is_array($post_types) && count($post_types) > 0) {
			foreach ($post_types as $post_type) {
				if (is_singular($post_type)) {
					if (!isset($_POST['mkdf_rating'])) {
						wp_die(esc_html__('Error: You did not add a rating. Hit the Back button on your Web browser and resubmit your comment with a rating.', 'mkdf-lms'));
					}
				}
			}
		}
		return $commentdata;
	}

	add_filter('preprocess_comment', 'mkdf_lms_verify_comment_meta_data');

}


if(!function_exists('mkdf_lms_override_comments_callback')) {

	function mkdf_lms_override_comments_callback($args) {
		$post_types = mkdf_lms_rating_posts_types();

		if(is_array($post_types) && count($post_types) > 0) {
			foreach ($post_types as $post_type) {
				if (is_singular($post_type)) {
					$args['callback'] = 'mkdf_lms_reviews';
				}
			}
		}
		return $args;
	}

	add_filter('iacademy_mikado_comments_callback', 'mkdf_lms_override_comments_callback');

}

if(!function_exists('mkdf_lms_reviews')) {

	function mkdf_lms_reviews($comment, $args, $depth) {

		$GLOBALS['comment'] = $comment;

		global $post;

		$is_pingback_comment = $comment->comment_type == 'pingback';
		$is_author_comment  = $post->post_author == $comment->user_id;

		$comment_class = 'mkdf-comment clearfix';

		if($is_author_comment) {
			$comment_class .= ' mkdf-post-author-comment';
		}
		$review_rating = get_comment_meta( $comment->comment_ID, 'mkdf_rating', true );
		$review_title = get_comment_meta( $comment->comment_ID, 'mkdf_comment_title', true );

		?>

		<li>
		<div class="<?php echo esc_attr($comment_class); ?>">
			<?php if(!$is_pingback_comment) { ?>
				<div class="mkdf-comment-image"> <?php echo iacademy_mikado_kses_img(get_avatar($comment, 'thumbnail')); ?> </div>
			<?php } ?>
			<div class="mkdf-comment-text">
				<div class="mkdf-comment-info">
					<h5 class="mkdf-comment-name vcard">
						<?php echo wp_kses_post(get_comment_author_link()); ?>
					</h5>
					<div class="mkdf-review-rating">
						<span class="mkdf-rating-inner">
							<?php for($i=1; $i<=$review_rating; $i++){ ?>
								<i class="fa fa-star" aria-hidden="true"></i>
							<?php } ?>
						</span>
					</div>
				</div>
				<?php if(!$is_pingback_comment) { ?>
					<div class="mkdf-text-holder" id="comment-<?php comment_ID(); ?>">
						<div class="mkdf-review-title">
							<span><?php echo esc_html( $review_title ); ?></span>
						</div>
						<?php comment_text(); ?>
					</div>
				<?php } ?>
			</div>
		</div>
		<?php //li tag will be closed by WordPress after looping through child elements ?>
		<?php
	}

}