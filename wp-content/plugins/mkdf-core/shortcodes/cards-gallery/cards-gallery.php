<?php
namespace MikadoCore\CPT\Shortcodes\CardsGallery;

use MikadoCore\Lib;

class CardsGallery implements Lib\ShortcodeInterface {
    private $base;

    public function __construct() {
        $this->base = 'mkdf_cards_gallery';

        add_action( 'vc_before_init', array( $this, 'vcMap' ) );
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        if ( function_exists( 'vc_map' ) ) {
            vc_map(
                array(
                    'name'                      => esc_html__( 'Mikado Cards Gallery', 'mkdf-core' ),
                    'base'                      => $this->getBase(),
                    'category'                  => esc_html__( 'by MIKADO', 'mkdf-core' ),
                    'icon'                      => 'icon-wpb-cards-gallery extended-custom-icon',
                    'allowed_container_element' => 'vc_row',
                    'params'                    => array(
                        array(
                            'type'        => 'textfield',
                            'param_name'  => 'custom_class',
                            'heading'     => esc_html__( 'Custom CSS Class', 'mkdf-core' ),
                            'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS', 'mkdf-core' )
                        ),
                        array(
                            'type'        => 'attach_images',
                            'param_name'  => 'images',
                            'heading'     => esc_html__( 'Images', 'mkdf-core' ),
                            'description' => esc_html__( 'Select images from media library', 'mkdf-core' )
                        ),
                        array(
                            'type'        => 'dropdown',
                            'param_name'  => 'layout',
                            'heading'     => esc_html__( 'Layout', 'mkdf-core' ),
                            'value'       => array(
                                esc_html__( 'Shuffled Left', 'mkdf-core' )  => 'shuffled-left',
                                esc_html__( 'Shuffled Right', 'mkdf-core' ) => 'shuffled-right'
                            ),
                            'save_always' => true,
                            'admin_label' => true
                        ),
                        array(
                            'type'        => 'dropdown',
                            'param_name'  => 'appear_effect',
                            'heading'     => esc_html__( 'Appear effect', 'mkdf-core' ),
                            'value'       => array(
                                esc_html__( 'Yes', 'mkdf-core' )  => 'yes',
                                esc_html__( 'No', 'mkdf-core' ) => 'no'
                            ),
                            'save_always' => true
                        )
                    )
                )
            );
        }
    }

    public function render( $atts, $content = null ) {
        $args   = array(
            'custom_class'    => '',
            'images'          => '',
            'layout'          => '',
            'appear_effect'   => 'yes'
        );

        $params                   = shortcode_atts($args, $atts);
        $params['images']         = $this->getGalleryImages($params);
        $params['holder_classes'] = $this->getHolderClasses($params);

        $html = mkdf_core_get_shortcode_module_template_part( 'templates/cards-gallery', 'cards-gallery', '', $params );

        return $html;
    }

    /**
     * Return images for slider
     *
     * @param $params
     *
     * @return array
     */
    private function getGalleryImages($params) {
        $image_ids = array();
        $images    = array();
        $i         = 0;

        if($params['images'] !== '') {
            $image_ids = explode(',', $params['images']);
        }

        foreach($image_ids as $id) {

            $image['image_id']     = $id;
            $image_original        = wp_get_attachment_image_src($id, 'full');
            $image['url']          = $image_original[0];
            $image['title']        = get_the_title($id);
            $image['image_link']   = get_post_meta($id, 'attachment_image_link', true);
            $image['image_target'] = get_post_meta($id, 'attachment_image_target', true);

            $image_dimensions = iacademy_mikado_get_image_dimensions($image['url']);
            if(is_array($image_dimensions) && array_key_exists('height', $image_dimensions)) {

                if(!empty($image_dimensions['height']) && $image_dimensions['width']) {
                    $image['height'] = $image_dimensions['height'];
                    $image['width']  = $image_dimensions['width'];
                }
            }

            $images[$i] = $image;
            $i++;
        }

        return $images;

    }

    private function getHolderClasses($params) {
        $classes = array('mkdf-cards-gallery-holder');

        $classes[] = 'mkdf-'.$params['layout'];

        $classes[] = 'mkdf-appear-effect-'.$params['appear_effect'];

        return $classes;
    }
}