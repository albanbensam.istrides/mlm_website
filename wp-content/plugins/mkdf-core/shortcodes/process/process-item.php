<?php
namespace MikadoCore\CPT\Shortcodes\Process;

use MikadoCore\Lib;

class ProcessItem implements Lib\ShortcodeInterface {
    private $base;

    function __construct() {
        $this->base = 'mkdf_process_item';
        add_action( 'vc_before_init', array( $this, 'vcMap' ) );
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        if ( function_exists( 'vc_map' ) ) {
            vc_map(array(
                'name' => 'Mikado Process Item',
                'base' => $this->getBase(),
                'as_child' => array('only' => 'mkdf_process_holder'),
                'category'=> esc_html__( 'by MIKADO', 'mkdf-core' ),
                'icon' => 'icon-wpb-process-item extended-custom-icon',
                'show_settings_on_create' => true,
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'heading' => esc_html__( 'Image', 'mkdf-core' ),
                        'param_name' => 'image'
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => esc_html__( 'Title', 'mkdf-core' ),
                        'param_name' => 'title',
                        'save_always' => true,
                        'admin_label' => true
                    ),
                    array(
                        'type' => 'textarea',
                        'heading' => esc_html__( 'Text', 'mkdf-core' ),
                        'param_name' => 'text',
                        'save_always' => true,
                        'admin_label' => true
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__( 'Highlight Item?', 'mkdf-core' ),
                        'param_name' => 'highlighted',
                        'value' => array(
                            'No' => 'no',
                            'Yes' => 'yes'
                        ),
                        'save_always' => true,
                        'admin_label' => true
                    )
                )
            ));
        }
    }

    public function render($atts, $content = null) {
        $default_atts = array(
            'image'     => '',
            'title'     => '',
            'text'      => '',
            'highlighted' => ''
        );

        $params = shortcode_atts($default_atts, $atts);

        $params['item_classes'] = array(
            'mkdf-process-item-holder'
        );

        if($params['highlighted'] === 'yes') {
            $params['item_classes'][] = 'mkdf-pi-highlighted';
        }

        return mkdf_core_get_shortcode_module_template_part('templates/process-item-template', 'process', '', $params);
    }

}