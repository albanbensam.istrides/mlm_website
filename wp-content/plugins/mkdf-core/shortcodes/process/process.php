<?php
namespace MikadoCore\CPT\Shortcodes\Process;

use MikadoCore\Lib;

class Process implements Lib\ShortcodeInterface {
    private $base;

    function __construct() {
        $this->base = 'mkdf_process';
        add_action( 'vc_before_init', array( $this, 'vcMap' ) );
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        if ( function_exists( 'vc_map' ) ) {
            vc_map(array(
                'name' => 'Mikado Process',
                'base' => $this->getBase(),
                'as_parent' => array('only' => 'mkdf_process_item'),
                'content_element' => true,
                'show_settings_on_create' => true,
                'category' => esc_html__( 'by MIKADO', 'mkdf-core' ),
                'icon' => 'icon-wpb-process extended-custom-icon',
                'js_view' => 'VcColumnView',
                'params' => array(
                    array(
                        'type' => 'dropdown',
                        'param_name' => 'number_of_items',
                        'heading' => esc_html__( 'Number of Process Items', 'mkdf-core' ),
                        'value' => array(
                            'Three' => 'three',
                            'Four' => 'four'
                        ),
                        'save_always' => true,
                        'admin_label' => true,
                        'description' => ''
                    )
                )
            ));
        }
    }

    public function render($atts, $content = null) {
        $default_atts = array(
            'number_of_items' => ''
        );

        $params            = shortcode_atts($default_atts, $atts);
        $params['content'] = $content;

        $params['holder_classes'] = array(
            'mkdf-process-holder',
            'mkdf-process-holder-items-'.$params['number_of_items']
        );

        return mkdf_core_get_shortcode_module_template_part('templates/process-holder-template', 'process', '', $params);
    }
}