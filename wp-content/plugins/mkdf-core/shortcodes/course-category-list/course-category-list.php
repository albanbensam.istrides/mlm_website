<?php
namespace MikadoCore\CPT\Shortcodes\CourseCategoryList;

use MikadoCore\Lib;

class CourseCategoryList implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'mkdf_course_category_list';
		
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map(
				array(
					'name'     => esc_html__( 'Mikado Linked Image List', 'mkdf-core' ),
					'base'     => $this->base,
					'icon'     => 'icon-wpb-linked-image-list extended-custom-icon',
					'category' => esc_html__( 'by MIKADO', 'mkdf-core' ),
					'params'   => array(
						array(
							'type'        => 'dropdown',
							'param_name'  => 'number_of_columns',
							'heading'     => esc_html__( 'Columns', 'mkdf-core' ),
							'value'       => array(
								esc_html__( '1 Column', 'mkdf-core' )  => 'one-column',
								esc_html__( '2 Columns', 'mkdf-core' ) => 'two-columns',
								esc_html__( '3 Columns', 'mkdf-core' ) => 'three-columns',
								esc_html__( '4 Columns', 'mkdf-core' ) => 'four-columns',
								esc_html__( '5 Columns', 'mkdf-core' ) => 'five-columns',
								esc_html__( '6 Columns', 'mkdf-core' ) => 'six-columns'
							),
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'target',
							'heading'     => esc_html__( 'Link Target', 'mkdf-core' ),
							'value'       => array_flip( iacademy_mikado_get_link_target_array() ),
							'save_always' => true
						),
						array(
							'type' => 'param_group',
							'heading' => esc_html__( 'Linked Image Items', 'mkdf-core' ),
							'param_name' => 'category_items',
							'value' => '',
							'params' => array(
								array(
									'type'        => 'textfield',
									'param_name'  => 'title',
									'heading'     => esc_html__( 'Title', 'mkdf-core' ),
									'save_always' => true,
									'admin_label' => true
								),
								array(
									'type'       => 'textfield',
									'param_name' => 'link',
									'heading'    => esc_html__( 'Link', 'mkdf-core' )
								),
								array(
									'type'        => 'attach_image',
									'param_name'  => 'image',
									'heading'     => esc_html__( 'Image', 'mkdf-core' ),
									'description' => esc_html__( 'Select image from media library', 'mkdf-core' )
								),
							)
						),
					)
				)
			);
		}
	}
	
	public function render( $atts, $content = null ) {
		$default_atts   = array(
			'number_of_columns'  => 'one-column',
			'target'   => '',
			'category_items'        => ''
		);
		$params       = shortcode_atts( $default_atts, $atts );

		$params['holder_class'] = $this->getHolderClasses( $params );
		$params['category_items'] = json_decode(urldecode($params['category_items']), true);
		
		$html = mkdf_core_get_shortcode_module_template_part( 'templates/course-category-list-template', 'course-category-list', '', $params );
		
		return $html;
	}


	private function getHolderClasses( $params ) {
		$holderClasses = array( 'mkdf-course-category-list' );

		$holderClasses[] = ! empty( $params['number_of_columns'] ) ? 'mkdf-' . $params['number_of_columns'] : '';

		return implode( ' ', $holderClasses );
	}
	

}