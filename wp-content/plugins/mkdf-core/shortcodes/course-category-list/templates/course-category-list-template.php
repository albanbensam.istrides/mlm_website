<div <?php iacademy_mikado_class_attribute( $holder_class ); ?> >
    <div class="mkdf-course-category-list-inner clearfix">
        <?php if(!empty($category_items)) { ?>
            <?php foreach($category_items as $category_item):?>
                <div class="mkdf-course-category-item">
                    <div class="mkdf-course-category-item-inner">
                        <?php if(isset($category_item['image'])) { ?>
                            <div class="mkdf-course-category-item-image">
                                <?php echo wp_get_attachment_image($category_item['image'], 'full'); ?>
                            </div>
                            <div class="mkdf-course-category-item-title">
                                <h5 class="mkdf-corse-category-title-text">  <?php echo esc_html($category_item['title']);?></h5>

                            </div>
                            <a class="mkdf-course-category-item-link" itemprop="url" target="<?php echo esc_attr($target); ?>" href="<?php echo esc_url($category_item['link']) ?>"></a>
                        <?php } ?>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php }?>
    </div>
</div>