(function($) {
	'use strict';
	
	var customFont = {};
	mkdf.modules.customFont = customFont;
	
	customFont.mkdfCustomFontResize = mkdfCustomFontResize;
	
	
	customFont.mkdfOnDocumentReady = mkdfOnDocumentReady;
	customFont.mkdfOnWindowResize = mkdfOnWindowResize;
	
	$(document).ready(mkdfOnDocumentReady);
	$(window).resize(mkdfOnWindowResize);
	
	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function mkdfOnDocumentReady() {
		mkdfCustomFontResize();
	}
	
	/* 
	 All functions to be called on $(window).resize() should be in this function
	 */
	function mkdfOnWindowResize() {
		mkdfCustomFontResize();
	}
	
	/*
	 **	Custom Font resizing style
	 */
	function mkdfCustomFontResize(){
		var holder = $('.mkdf-custom-font-holder');
		
		if(holder.length){
			holder.each(function() {
				var thisItem = $(this),
                    fontSize,
                    lineHeight;


                if(mkdf.windowWidth <= 680) {
                    if (typeof thisItem.data('font-size-680') !== 'undefined' && thisItem.data('font-size-680') !== false) {
                        fontSize = thisItem.data('font-size-680');
                    }
                    if (typeof thisItem.data('line-height-680') !== 'undefined' && thisItem.data('line-height-680') !== false) {
                        lineHeight = thisItem.data('line-height-680');
                    }
                }
                else if(mkdf.windowWidth <= 768) {
                    if (typeof thisItem.data('font-size-768') !== 'undefined' && thisItem.data('font-size-768') !== false) {
                        fontSize = thisItem.data('font-size-768');
                    }
                    if (typeof thisItem.data('line-height-768') !== 'undefined' && thisItem.data('line-height-768') !== false) {
                        lineHeight = thisItem.data('line-height-768');
                    }
                }
                else if(mkdf.windowWidth <= 1024) {
                    if (typeof thisItem.data('font-size-1024') !== 'undefined' && thisItem.data('font-size-1024') !== false) {
                        fontSize = thisItem.data('font-size-1024');
                    }
                    if (typeof thisItem.data('line-height-1024') !== 'undefined' && thisItem.data('line-height-1024') !== false) {
                        lineHeight = thisItem.data('line-height-1024');
                    }
                }
                else if(mkdf.windowWidth <= 1280) {
                    if (typeof thisItem.data('font-size-1280') !== 'undefined' && thisItem.data('font-size-1280') !== false) {
                        fontSize = thisItem.data('font-size-1280');
                    }
                    if (typeof thisItem.data('line-height-1280') !== 'undefined' && thisItem.data('line-height-1280') !== false) {
                        lineHeight = thisItem.data('line-height-1280');
                    }
                }
                else if(mkdf.windowWidth > 1280) {
                    if (typeof thisItem.data('font-size') !== 'undefined' && thisItem.data('font-size') !== false) {
                        fontSize = thisItem.data('font-size');
                    }
                    if (typeof thisItem.data('line-height') !== 'undefined' && thisItem.data('line-height') !== false) {
                        lineHeight = thisItem.data('line-height');
                    }
                }

                if(fontSize !== 'undefined' && fontSize !== false && fontSize !== '') {
				    thisItem.css('font-size', fontSize);
                }
                if(lineHeight !== 'undefined' && lineHeight !== false && lineHeight !== '') {
                    thisItem.css('line-height', lineHeight);
                }
			});
		}
	}
	
})(jQuery);