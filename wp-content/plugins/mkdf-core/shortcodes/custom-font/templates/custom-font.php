<<?php echo esc_attr($title_tag); ?> class="mkdf-custom-font-holder <?php echo esc_attr($holder_classes); ?>" <?php iacademy_mikado_inline_style($holder_styles); ?> <?php echo iacademy_mikado_get_inline_attrs($holder_data); ?>>
	<?php echo esc_html($title); ?>
</<?php echo esc_attr($title_tag); ?>>