<?php
/**
 * Team info on hover shortcode template
 */
?>
<div class="mkdf-team main-info-below-image <?php echo esc_attr($skin) ?>">
	<div class="mkdf-team-inner">
		<?php if ($team_image !== '') { ?>
			<div class="mkdf-team-image">
                <?php echo wp_get_attachment_image($team_image,'full');?>
			</div>
		<?php } ?>

		<?php if ($team_name !== '' || $team_position !== '' || $team_description != "") { ?>
			<div class="mkdf-team-info">
				<?php if ($team_name !== '' || $team_position !== '') { ?>
					<div class="mkdf-team-title-holder <?php echo esc_attr($team_social_icon_type) ?>">
						<?php if ($team_name !== '') { ?>
							<<?php echo esc_attr($team_name_tag); ?> class="mkdf-team-name">
								<?php echo esc_attr($team_name); ?>
							</<?php echo esc_attr($team_name_tag); ?>>
						<?php } ?>
						<?php if ($team_position !== "") { ?>
							<div class="mkdf-team-position"><?php echo esc_attr($team_position) ?></div>
						<?php } ?>
					</div>
				<?php } ?>

				<?php if ($team_description != "") { ?>
					<div class='mkdf-team-text'>
						<div class='mkdf-team-text-inner'>
							<div class='mkdf-team-description'>
								<p><?php echo esc_attr($team_description) ?></p>
							</div>
						</div>
					</div>
				<?php }
			} ?>

		<div class="mkdf-team-social-holder-between">
			<div class="mkdf-team-social <?php echo esc_attr($team_social_icon_type) ?>">
				<div class="mkdf-team-social-inner">
					<div class="mkdf-team-social-wrapp">

						<?php foreach( $team_social_icons as $team_social_icon ) {
							echo iacademy_mikado_get_module_part($team_social_icon);
						} ?>

					</div>
				</div>
			</div>
		</div>

		</div>
	</div>
</div>