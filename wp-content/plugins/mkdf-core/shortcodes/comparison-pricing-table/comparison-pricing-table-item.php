<?php

namespace MikadoCore\CPT\Shortcodes\ComparisonPricingTable;

use MikadoCore\Lib;

class ComparisonPricingTableItem implements Lib\ShortcodeInterface {
    private $base;

    /**
     * ComparisonPricingTable constructor.
     */
    public function __construct() {
        $this->base = 'mkdf_comparison_pricing_table_item';

        add_action('vc_before_init', array($this, 'vcMap'));
    }


    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        if ( function_exists( 'vc_map' ) ) {
            vc_map(
                array(
                    'name' => 'Mikado Comparison Pricing Table Item',
                    'base' => $this->base,
                    'icon' => 'icon-wpb-comparison-pricing-table-item extended-custom-icon',
                    'category' => esc_html__( 'by MIKADO', 'mkdf-core' ),
                    'allowed_container_element' => 'vc_row',
                    'as_child' => array('only' => 'mkdf_comparison_pricing_table'),
                    'params' => array(
                        array(
                            'type' => 'attach_image',
                            'heading' => esc_html__( 'Title Image', 'mkdf-core' ),
                            'param_name' => 'image'
                        ),
                        array(
                            'type' => 'textfield',
                            'admin_label' => true,
                            'heading' => esc_html__( 'Title', 'mkdf-core' ),
                            'param_name' => 'title',
                            'value' => 'Basic Plan',
                            'description' => ''
                        ),
                        array(
                            'type' => 'textfield',
                            'admin_label' => true,
                            'heading' => esc_html__( 'Title Size (px)', 'mkdf-core' ),
                            'param_name' => 'title_size',
                            'value' => '',
                            'description' => '',
                            'dependency' => array('element' => 'title', 'not_empty' => true),
                            'group' => esc_html__( 'Design Options', 'mkdf-core' )
                        ),
                        array(
                            'type' => 'textfield',
                            'admin_label' => true,
                            'heading' => esc_html__( 'Price', 'mkdf-core' ),
                            'param_name' => 'price',
                            'description' => esc_html__( 'Default value is 100', 'mkdf-core' )
                        ),
                        array(
                            'type' => 'textfield',
                            'admin_label' => true,
                            'heading' => esc_html__( 'Currency', 'mkdf-core' ),
                            'param_name' => 'currency',
                            'description' => esc_html__( 'Default mark is $', 'mkdf-core' )
                        ),
                        array(
                            'type' => 'dropdown',
                            'admin_label' => true,
                            'heading' => esc_html__( 'Featured Item', 'mkdf-core' ),
                            'param_name' => 'featured_item',
                            'value' => array(
                                'Default' => '',
                                'Yes' => 'yes',
                                'No' => 'no'
                            ),
                            'description' => ''
                        ),
                        array(
                            'type' => 'textfield',
                            'admin_label' => true,
                            'heading' => esc_html__( 'Price Period', 'mkdf-core' ),
                            'param_name' => 'price_period',
                            'description' => esc_html__( 'Default label is monthly', 'mkdf-core' )
                        ),
                        array(
                            'type' => 'dropdown',
                            'admin_label' => true,
                            'heading' => esc_html__( 'Show Button', 'mkdf-core' ),
                            'param_name' => 'show_button',
                            'value' => array(
                                'Default' => '',
                                'Yes' => 'yes',
                                'No' => 'no'
                            ),
                            'description' => ''
                        ),
                        array(
                            'type' => 'textfield',
                            'admin_label' => true,
                            'heading' => esc_html__( 'Button Text', 'mkdf-core' ),
                            'param_name' => 'button_text',
                            'dependency' => array('element' => 'show_button', 'value' => 'yes')
                        ),
                        array(
                            'type' => 'textfield',
                            'admin_label' => true,
                            'heading' => esc_html__( 'Button Link', 'mkdf-core' ),
                            'param_name' => 'link',
                            'dependency' => array('element' => 'show_button', 'value' => 'yes')
                        ),
                        array(
                            'type' => 'textarea_html',
                            'holder' => 'div',
                            'class' => '',
                            'heading' => esc_html__( 'Content', 'mkdf-core' ),
                            'param_name' => 'content',
                            'value' => '<li>content content content</li><li>content content content</li><li>content content content</li>',
                            'description' => '',
                            'admin_label' => false
                        ),
                        array(
                            'type' => 'colorpicker',
                            'holder' => 'div',
                            'class' => '',
                            'heading' => esc_html__( 'Border Top Color', 'mkdf-core' ),
                            'param_name' => 'border_top_color',
                            'value' => '',
                            'save_always' => true,
                            'group' => esc_html__( 'Design Options', 'mkdf-core' )
                        ),
                        array(
                            'type' => 'colorpicker',
                            'holder' => 'div',
                            'class' => '',
                            'heading' => esc_html__( 'Button Background Color', 'mkdf-core' ),
                            'param_name' => 'btn_background_color',
                            'value' => '',
                            'save_always' => true,
                            'group' => esc_html__( 'Design Options', 'mkdf-core' )
                        ),
                        array(
                            'type' => 'colorpicker',
                            'holder' => 'div',
                            'class' => '',
                            'heading' => esc_html__( 'Button Border Color', 'mkdf-core' ),
                            'param_name' => 'btn_border_color',
                            'value' => '',
                            'save_always' => true,
                            'group' => esc_html__( 'Design Options', 'mkdf-core' )
                        ),
                        array(
                            'type' => 'colorpicker',
                            'holder' => 'div',
                            'class' => '',
                            'heading' => esc_html__( 'Button Text Color', 'mkdf-core' ),
                            'param_name' => 'btn_text_color',
                            'value' => '',
                            'save_always' => true,
                            'group' => esc_html__( 'Design Options', 'mkdf-core' )
                        )
                    )
                ));
        }
    }

    public function render($atts, $content = null) {
        $args = array(
            'image'                => '',
            'title'                => 'Basic Plan',
            'title_size'           => '',
            'price'                => '100',
            'currency'             => '',
            'price_period'         => '',
            'featured_item'         => '',
            'show_button'          => 'yes',
            'link'                 => '',
            'button_text'          => 'button',
            'border_top_color'     => '',
            'btn_background_color' => '',
            'btn_border_color'     => '',
            'btn_text_color'     => ''
        );

        $params = shortcode_atts($args, $atts);

        $params['content']        = $content;
        $params['border_style']   = $this->getBorderStyles($params);
        $params['display_border'] = is_array($params['border_style']) && count($params['border_style']);
        $params['btn_styles']     = $this->getBtnStyles($params);
        $params['featured']       = $this->getFeaturedItem($params);

        return mkdf_core_get_shortcode_module_template_part('templates/cpt-table-template', 'comparison-pricing-table', '', $params);
    }

    private function getBorderStyles($params) {
        $styles = array();

        if($params['border_top_color'] !== '') {
            $styles[] = 'background-color: '.$params['border_top_color'];
        }

        return $styles;
    }

    private function getBtnStyles($params) {
        $styles = array();

        if($params['btn_background_color'] !== '') {
            $styles[] = 'background-color: '.$params['btn_background_color'];
        }

        if($params['btn_border_color'] !== '') {
            $styles[] = 'border-color: '.$params['btn_border_color'];
        }

        if($params['btn_text_color'] !== '') {
            $styles[] = 'color: '.$params['btn_text_color'];
        }

        return $styles;
    }


    private function getFeaturedItem ($params) {
        $styles = '';

        if($params['featured_item'] == 'yes') {
            $styles = 'mkdf-featured-item';
        }

        return $styles;
    }

}