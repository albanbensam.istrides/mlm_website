(function($) {
	'use strict';

	var timeline = {};
	mkdf.modules.timeline = timeline;

	timeline.mkdfTimeline = mkdfTimeline;


	timeline.mkdfOnDocumentReady = mkdfOnDocumentReady;

	$(document).ready(mkdfOnDocumentReady);

	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function mkdfOnDocumentReady() {
		mkdfTimeline();
	}

	/**
	 * Timeline animation
	 * @type {Function}
	 */
	function mkdfTimeline(){

		var itemTimeline = $('.mkdf-tml-item-holder');
		if(itemTimeline.length){


			itemTimeline.each(function(){

				var thisTimeline = $(this);


				setTimeout(function(){

					thisTimeline.appear(function(){
						thisTimeline.addClass('mkdf-appeared');
					},{accX: 0, accY: mkdfGlobalVars.vars.mkdfElementAppearAmount});
				},500*thisTimeline.index());

			});
		}
	}

})(jQuery);