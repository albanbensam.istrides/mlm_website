<?php
namespace MikadoCore\CPT\Shortcodes\EducationTimeline;

use MikadoCore\Lib;

class EducationTimelineHolder implements Lib\ShortcodeInterface {
    private $base;

    function __construct() {
        $this->base = 'mkdf_education_timeline_holder';
        add_action( 'vc_before_init', array( $this, 'vcMap' ) );
    }

    public function getBase() {
        return $this->base;
    }

    public function vcMap() {
        if ( function_exists( 'vc_map' ) ) {
            vc_map(
                array(
                    'name'      => esc_html__( 'Mikado Timeline Holder', 'mkdf-core' ),
                    'base'      => $this->base,
                    'icon'      => 'icon-wpb-education-timeline-holder extended-custom-icon',
                    'category'  => esc_html__( 'by MIKADO', 'mkdf-core' ),
                    'as_parent' => array( 'only' => 'mkdf_education_timeline_item' ),
                    'is_container'  => true,
                    'js_view'   => 'VcColumnView',
                    'params'    => array(
                        array(
                            'type'        => 'textfield',
                            'param_name'  => 'title',
                            'heading'     => esc_html__( 'Title', 'mkdf-core' ),
                            'description' => esc_html__( 'Add Timeline Title', 'mkdf-core' )
                        )
                    )
                )
            );
        }
    }

    public function render( $atts, $content = null ) {
        $args   = array(
            'title'         => '',
        );
        $params = shortcode_atts( $args, $atts );

        $params['content']        = $content;
        return mkdf_core_get_shortcode_module_template_part('templates/education-timeline-holder-template', 'education-timeline', '', $params);

    }
}
