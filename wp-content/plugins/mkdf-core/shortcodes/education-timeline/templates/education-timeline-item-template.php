<div class="mkdf-tml-item-holder">
    <span class="mkdf-tml-item-circle"></span>
    <div class="mkdf-tml-item-content">
        <div class="mkdf-tml-item-title"><?php echo esc_html($title);?></div>
        <div class="mkdf-tml-item-subtitle"><?php echo  esc_html($subtitle);?></div>
    </div>
</div>