<div class="mkdf-tml-holder">
    <?php if(!empty($title)) { ?>
        <h4 class="mkdf-tml-title"><?php echo esc_html($title);?></h4>
    <?php } ?>
    <div class="mkdf-timeline">
        <?php echo do_shortcode($content); ?>
    </div>
</div>