(function($) {
	'use strict';
	
	var elementsHolder = {};
	mkdf.modules.elementsHolder = elementsHolder;
	
	elementsHolder.mkdfInitElementsHolderResponsiveStyle = mkdfInitElementsHolderResponsiveStyle;
	
	
	elementsHolder.mkdfOnDocumentReady = mkdfOnDocumentReady;
    elementsHolder.mkdfOnWindowResize = mkdfOnWindowResize;
	
	$(document).ready(mkdfOnDocumentReady);
    $(window).resize(mkdfOnWindowResize);
	
	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function mkdfOnDocumentReady() {
		mkdfInitElementsHolderResponsiveStyle();
	}

    /*
     All functions to be called on $(window).resize() should be in this function
     */
    function mkdfOnWindowResize() {
        mkdfInitElementsHolderResponsiveStyle();
    }
	
	/*
	 **	Elements Holder responsive style
	 */
	function mkdfInitElementsHolderResponsiveStyle(){
		var elementsHolder = $('.mkdf-elements-holder');
		
		if(elementsHolder.length){
			elementsHolder.each(function() {
				var thisElementsHolder = $(this),
					elementsHolderItem = thisElementsHolder.children('.mkdf-eh-item');
				
				elementsHolderItem.each(function() {
					var thisItem = $(this),
                        padding = '';

                    if(mkdf.windowWidth < 681) {
                        if (typeof thisItem.data('680') !== 'undefined' && thisItem.data('680') !== false) {
                            padding = thisItem.data('680');
                        }
                    }
                    else if(mkdf.windowWidth > 680 && mkdf.windowWidth < 769) {
                        if (typeof thisItem.data('680-768') !== 'undefined' && thisItem.data('680-768') !== false) {
                            padding = thisItem.data('680-768');
                        }
                    }
                    else if(mkdf.windowWidth > 768 && mkdf.windowWidth < 1025) {
                        if (typeof thisItem.data('768-1024') !== 'undefined' && thisItem.data('768-1024') !== false) {
                            padding = thisItem.data('768-1024');
                        }
                    }
                    else if(mkdf.windowWidth > 1024 && mkdf.windowWidth < 1281) {
                        if (typeof thisItem.data('1024-1280') !== 'undefined' && thisItem.data('1024-1280') !== false) {
                            padding = thisItem.data('1024-1280');
                        }
                    }
                    else if(mkdf.windowWidth > 1280 && mkdf.windowWidth < 1601) {
                        if (typeof thisItem.data('1280-1600') !== 'undefined' && thisItem.data('1280-1600') !== false) {
                            padding = thisItem.data('1280-1600');
                        }
                    }
                    else if(mkdf.windowWidth > 1600) {
                        if (typeof thisItem.data('1600') !== 'undefined' && thisItem.data('1600') !== false) {
                            padding = thisItem.data('1600');
                        }
                    }

                    if(padding !== 'undefined' && padding !== false && padding !== '') {
                        thisItem.find('.mkdf-eh-item-content').css('padding', padding);
                    }
				});
				
				if (typeof mkdf.modules.common.mkdfOwlSlider === "function") {
					mkdf.modules.common.mkdfOwlSlider();
				}
			});
		}
	}
	
})(jQuery);