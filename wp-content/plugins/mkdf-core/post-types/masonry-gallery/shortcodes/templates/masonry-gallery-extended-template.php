<article class="<?php echo esc_attr($item_classes) ?>">
	<div class="mkdf-mg-content">
		<?php if (has_post_thumbnail()) { ?>
			<div class="mkdf-mg-image-overlay" <?php echo iacademy_mikado_inline_style($image_overlay_styles);?>></div>
			<div class="mkdf-mg-image">
				<?php the_post_thumbnail($image_size); ?>
			</div>
		<?php } ?>
		<div class="mkdf-mg-item-outer">
			<div class="mkdf-mg-item-inner">
				<div class="mkdf-mg-item-content">
					<?php if(!empty($item_image)) { ?>
						<img itemprop="image" class="mkdf-mg-item-icon" src="<?php echo esc_url($item_image['url'])?>" alt="<?php echo esc_attr($item_image['alt']); ?>" />
					<?php } ?>
					<?php if (!empty($item_subtitle)) { ?>
						<h4 class="mkdf-mg-item-subtitle" <?php echo iacademy_mikado_inline_style($item_subtitle_color);?>><?php echo esc_html($item_subtitle); ?></h4>
					<?php } ?>
					<?php if (!empty($item_title)) { ?>
						<<?php echo esc_attr($item_title_tag); ?> itemprop="name" class="mkdf-mg-item-title entry-title"><?php echo esc_html($item_title); ?></<?php echo esc_attr($item_title_tag); ?>>
					<?php } ?>
					<?php if (!empty($item_text)) { ?>
						<p class="mkdf-mg-item-text"><?php echo esc_html($item_text); ?></p>
					<?php } ?>
					<?php if (!empty($item_link)) { ?>
						<a itemprop="url" href="<?php echo esc_url($item_link); ?>" target="<?php echo esc_attr($item_link_target); ?>" class="mkdf-mg-link-over"></a>
					<?php } ?>
					<?php if(!empty($item_button_label) && !empty($item_link)) : ?>
						<a itemprop="url" href="<?php echo esc_url($item_link); ?>" class="mkdf-btn mkdf-btn-solid mkdf-btn-small mkdf-mg-item-button" target="<?php echo esc_attr($item_link_target); ?>">
							<?php echo iacademy_mikado_get_module_part($button_icon_html); ?><span class="mkdf-button-text"><?php echo esc_html($item_button_label); ?></span>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</article>
