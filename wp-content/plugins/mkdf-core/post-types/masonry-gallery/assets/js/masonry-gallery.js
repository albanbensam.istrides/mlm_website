(function($) {
    'use strict';
	
	var masonryGallery = {};
	mkdf.modules.masonryGallery = masonryGallery;
	
	masonryGallery.mkdfInitMasonryGallery = mkdfInitMasonryGallery;
	
	
	masonryGallery.mkdfOnDocumentReady = mkdfOnDocumentReady;
	
	$(document).ready(mkdfOnDocumentReady);
	
	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function mkdfOnDocumentReady() {
		mkdfInitMasonryGallery();
	}
	
	/**
	 * Masonry gallery, init masonry and resize pictures in grid
	 */
	function mkdfInitMasonryGallery(){
		var galleryHolder = $('.mkdf-masonry-gallery-holder'),
			gallery = galleryHolder.children('.mkdf-mg-inner'),
			gallerySizer = gallery.children('.mkdf-mg-grid-sizer');
		
		resizeMasonryGallery(gallerySizer.outerWidth(), gallery);
		
		if(galleryHolder.length){
			galleryHolder.each(function(){
				var holder = $(this),
					holderGallery = holder.children('.mkdf-mg-inner');
				
				holderGallery.waitForImages(function(){
					holderGallery.animate({opacity:1});
					
					holderGallery.isotope({
						layoutMode: 'packery',
						itemSelector: '.mkdf-mg-item',
						percentPosition: true,
						packery: {
							gutter: '.mkdf-mg-grid-gutter',
							columnWidth: '.mkdf-mg-grid-sizer'
						}
					});
				});
			});
			
			$(window).resize(function(){
				resizeMasonryGallery(gallerySizer.outerWidth(), gallery);
				
				gallery.isotope('reloadItems');
			});
		}
	}
	
	function resizeMasonryGallery(size, holder){
		var rectangle_portrait = holder.find('.mkdf-mg-rectangle-portrait'),
			rectangle_landscape = holder.find('.mkdf-mg-rectangle-landscape'),
			square_big = holder.find('.mkdf-mg-square-big'),
			square_small = holder.find('.mkdf-mg-square-small');
		
		rectangle_portrait.css('height', 2*size);
		
		if (window.innerWidth <= 680) {
			rectangle_landscape.css('height', size/2);
		} else {
			rectangle_landscape.css('height', size);
		}
		
		square_big.css('height', 2*size);
		
		if (window.innerWidth <= 680) {
			square_big.css('height', square_big.width());
		}
		
		square_small.css('height', size);
	}

})(jQuery);