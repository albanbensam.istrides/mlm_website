<?php

if(!function_exists('mkdf_core_map_masonry_gallery_meta')) {
    function mkdf_core_map_masonry_gallery_meta() {
        $masonry_gallery_meta_box = iacademy_mikado_create_meta_box(
            array(
                'scope' => array('masonry-gallery'),
                'title' => esc_html__('Masonry Gallery General', 'mkdf-core'),
                'name' => 'masonry_gallery_meta'
            )
        );
	    
        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_masonry_gallery_item_title_tag',
                'type' => 'select',
                'default_value' => 'h4',
                'label' => esc_html__('Title Tag', 'mkdf-core'),
                'parent' => $masonry_gallery_meta_box,
                'options' => iacademy_mikado_get_title_tag()
            )
        );

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_masonry_gallery_item_text',
                'type' => 'text',
                'label' => esc_html__('Text', 'mkdf-core'),
                'parent' => $masonry_gallery_meta_box
            )
        );

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_masonry_gallery_item_image',
                'type' => 'image',
                'label' => esc_html__('Custom Item Icon', 'mkdf-core'),
                'parent' => $masonry_gallery_meta_box
            )
        );


        $masonry_gallery_item_icon_container =  iacademy_mikado_add_admin_container_no_style(array(
            'name' => 'masonry_gallery_icon_container',
            'parent' => $masonry_gallery_meta_box
        ));

        IacademyMikadoIconCollections::get_instance()->getMetaBoxOrOptionParamsArray($masonry_gallery_item_icon_container, 'mkdf_masonry_gallery_item_icon', 'font_awesome', '', esc_html__('Icon', 'mkdf-core'), 'meta-box');

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_masonry_gallery_item_link',
                'type' => 'text',
                'label' => esc_html__('Link', 'mkdf-core'),
                'parent' => $masonry_gallery_meta_box
            )
        );

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_masonry_gallery_item_link_target',
                'type' => 'select',
                'default_value' => '_self',
                'label' => esc_html__('Link Target', 'mkdf-core'),
                'parent' => $masonry_gallery_meta_box,
                'options' => iacademy_mikado_get_link_target_array()
            )
        );

        iacademy_mikado_add_admin_section_title(array(
            'name'   => 'mkdf_section_style_title',
            'parent' => $masonry_gallery_meta_box,
            'title'  => esc_html__('Masonry Gallery Item Style', 'mkdf-core')
        ));

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_masonry_gallery_item_size',
                'type' => 'select',
                'default_value' => 'square-small',
                'label' => esc_html__('Size', 'mkdf-core'),
                'parent' => $masonry_gallery_meta_box,
                'options' => array(
                    'square-small'			=> esc_html__('Square Small', 'mkdf-core'),
                    'square-big'			=> esc_html__('Square Big', 'mkdf-core'),
                    'rectangle-portrait'	=> esc_html__('Rectangle Portrait', 'mkdf-core'),
                    'rectangle-landscape'	=> esc_html__('Rectangle Landscape', 'mkdf-core')
                )
            )
        );

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_masonry_gallery_item_type',
                'type' => 'select',
                'default_value' => 'standard',
                'label' => esc_html__('Type', 'mkdf-core'),
                'parent' => $masonry_gallery_meta_box,
                'options' => array(
                    'standard'		=> esc_html__('Standard', 'mkdf-core'),
                    'extended'	=> esc_html__('Extended', 'mkdf-core'),
                    'simple'		=> esc_html__('Simple', 'mkdf-core')
                ),
                'args' => array(
                    'dependence' => true,
                    'hide' => array(
                        'extended' => '#mkdf_masonry_gallery_item_simple_type_container',
                        'simple' => '#mkdf_masonry_gallery_item_button_type_container, #mkdf_masonry_gallery_item_standard_type_container',
                        'standard' => '#mkdf_masonry_gallery_item_button_type_container, #mkdf_masonry_gallery_item_simple_type_container'
                    ),
                    'show' => array(
                        'extended' => '#mkdf_masonry_gallery_item_button_type_container, #mkdf_masonry_gallery_item_standard_type_container',
                        'simple' => '#mkdf_masonry_gallery_item_simple_type_container',
                        'standard' => '#mkdf_masonry_gallery_item_standard_type_container'
                    )
                )
            )
        );

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_masonry_gallery_button_label',
                'type' => 'text',
                'label' => esc_html__('Button Label', 'mkdf-core'),
                'parent' => $masonry_gallery_meta_box
            )
        );

        $masonry_gallery_item_button_type_container = iacademy_mikado_add_admin_container_no_style(array(
            'name'				=> 'masonry_gallery_item_button_type_container',
            'parent'			=> $masonry_gallery_meta_box,
            'hidden_property'	=> 'mkdf_masonry_gallery_item_type',
            'hidden_values'		=> array('standard', 'simple')
        ));

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_masonry_gallery_subtitle_label',
                'type' => 'text',
                'label' => esc_html__('Subtitle', 'mkdf-core'),
                'parent' => $masonry_gallery_item_button_type_container
            )
        );

        IacademyMikadoIconCollections::get_instance()->getMetaBoxOrOptionParamsArray($masonry_gallery_item_button_type_container, 'mkdf_masonry_gallery_item_button_icon', 'font_awesome', '', esc_html__('Button Icon', 'mkdf-core'), 'meta-box');

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_masonry_gallery_subtitle_color',
                'type' => 'color',
                'label' => esc_html__('Subtitle Color', 'mkdf-core'),
                'parent' => $masonry_gallery_item_button_type_container
            )
        );

        $masonry_gallery_item_simple_type_container = iacademy_mikado_add_admin_container_no_style(array(
            'name'				=> 'masonry_gallery_item_simple_type_container',
            'parent'			=> $masonry_gallery_meta_box,
            'hidden_property'	=> 'mkdf_masonry_gallery_item_type',
            'hidden_values'		=> array('standard', 'extended')
        ));

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_masonry_gallery_simple_content_background_skin',
                'type' => 'select',
                'default_value' => '',
                'label' => esc_html__('Content Background Skin', 'mkdf-core'),
                'parent' => $masonry_gallery_item_simple_type_container,
                'options' => array(
                    'default' => esc_html__('Default', 'mkdf-core'),
                    'light'	=> esc_html__('Light', 'mkdf-core'),
                    'dark'	=> esc_html__('Dark', 'mkdf-core')
                )
            )
        );

        $masonry_gallery_item_standard_type_container = iacademy_mikado_add_admin_container_no_style(array(
            'name'				=> 'masonry_gallery_item_standard_type_container',
            'parent'			=> $masonry_gallery_meta_box,
            'hidden_property'	=> 'mkdf_masonry_gallery_item_type',
            'hidden_values'		=> array('simple')
        ));

        iacademy_mikado_create_meta_box_field(
            array(
                'name' => 'mkdf_masonry_gallery_standard_overlay_color',
                'type' => 'color',
                'default_value' => '',
                'label' => esc_html__('Image Overlay Color', 'mkdf-core'),
                'parent' => $masonry_gallery_item_standard_type_container
            )
        );
    }

    add_action('iacademy_mikado_meta_boxes_map', 'mkdf_core_map_masonry_gallery_meta', 45);
}