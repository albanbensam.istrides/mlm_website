<?php

if ( ! function_exists( 'iacademy_core_register_widgets' ) ) {
	function iacademy_core_register_widgets() {
		$widgets = apply_filters( 'iacademy_mikado_register_widgets', $widgets = array() );

		foreach ( $widgets as $widget ) {
			register_widget( $widget );
		}
	}

	add_action( 'widgets_init', 'iacademy_core_register_widgets' );
}