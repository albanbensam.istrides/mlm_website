<?php
//header get dev URL 
function ist_api_header_code($url='',$intialize=array(),$access_token='',$method='',$er_show=false) {
	if($url=="/usersearch"){
		$logurl = get_option('ist_api_dashboard_url').'/api/web/index.php/v1'.$url;
	}else{
		$logurl = get_option('ist_api_web_url').'/api/web/v1'.$url; 
	}
	$token=get_option('ist_api_web_token');
//echo $logurl;
//die;
//echo $logurl;

//$logurl = get_option('archer_web_url').$url;
	$headers=array();
	
	
	$args     = array(
    'method' => $method,
	);
	if($token!=""){ // for all service
		if($access_token!="" && $access_token!='true'){
			$args['headers']['Authorization']=$access_token; 
		}
		$time_zone_txt='';
		if(isset($_COOKIE['timezonejs'])){
			$time_zone_txt=$_COOKIE['timezonejs'];
		}
		//$token='638a02b2-0ebc-4996-9e05-2b52d5f1b157'; // set key
		$args['headers']['Content-Type']='application/json';
		$args['headers']['Authentication'] = $token;
		$args['headers']['timeout'] =1000000;
		$args['headers']['timezone'] =$time_zone_txt;
		$args['headers']['ip:'] =$_SERVER['REMOTE_ADDR'];
	}
	//print_r($args); die;
	if(isset($intialize) && count($intialize)>0){
	$args['body']  = json_encode($intialize);
	}
	
	//print_r($args); 
	$response = wp_remote_request( $logurl, $args );
	//$er_show=true;
	if($er_show){
	echo "<pre>";print_r($response); die;
	}

	//Check for success
	//$last_modified = wp_remote_retrieve_header( $response, 'last-modified' );
	//$http_code = wp_remote_retrieve_response_code( $response );
	//$body     = wp_remote_retrieve_body( $response );
  	if(!is_wp_error($response) && ($response['response']['code'] == 200 || $response['response']['code'] == 201)) {
    	$resp=$response['body'];
  	}else {
    	$resp= false;
 	}
 	//print_r($resp); die;

 	if($resp){
		$resp = json_decode($resp ,true);
	}

	return $resp;
}

// Get Registered and account_status status
function ist_get_user_status($id=""){
	$profile="";
	if($id==''){
		global $current_user;
		$current_user_id = $current_user->ID; // Get User ID
	}else{
		$current_user_id=$id;
	}
	$profile_array= get_user_meta($current_user_id,'ist_account_status'); 
	if($profile_array){
     $profile= $profile_array['0'];
      
	}
 return $profile;
}
add_shortcode( 'ist_get_user_status', 'ist_get_user_status' );

function ist_getCustomerInfo()
{
	global  $current_user;
    $current_user_id = $current_user->ID;
	$args = array(
            'name'    => '',
            'email'   => '',
            'contact' => '',
        );	
    	if ($current_user_id!="")
    	{
        $user_array_e= get_user_by( 'id', $current_user_id );
  		if($user_array_e){
  			$args['name']=$user_array_e->display_name;
  			$args['email']=$user_array_e->user_email;
  		}
  		$user_array_p= get_user_meta($current_user_id,'billing_phone');
  		if($user_array_p){
  			$args['contact']=$user_array_p['0'];
  		}
  		
    }

    return $args;
}
function ist_savepaymentInfo($attributes="",$amount="")
{
	global  $current_user;
    $user_id = $current_user->ID;
	
    	if ($user_id!="")
    	{
      		 update_user_meta( $user_id, 'raz_payment_info', json_encode(($attributes)));
             update_user_meta( $user_id, 'raz_payment_amount', sanitize_text_field($amount));
             update_user_meta( $user_id, 'ist_account_status', 'approved');
             $razorpay_payment_id=$razorpay_order_id="";
             if(isset($attributes) && array_key_exists('razorpay_payment_id', $attributes) ){
             	$razorpay_payment_id=$attributes['razorpay_payment_id'];
             }
             if(isset($attributes) && array_key_exists('razorpay_order_id', $attributes) ){
             	$razorpay_order_id=$attributes['razorpay_order_id'];
             }

  			 $access_token=$_SESSION['authorization']; 
			 $params_data=array('payment_status'=>'success','order_id'=>$razorpay_payment_id,'transaction_id'=>$razorpay_order_id);
			  $resp_sub=$ret_data=array();  
			//print_r($params_data); die;
			  if($access_token!=""){
			    $resp_sub = ist_api_header_code("/paymentvalidated",$params_data,$access_token,$method='post'); // Call API for Reg session 
			  }
    	}

    return true;
}
function ist_savepaymentstatus($attributes="",$amount="")
{
	global  $current_user;
    $user_id = $current_user->ID;
		$previous_status=ist_get_user_status();
    	if ($user_id!="" && $previous_status=='registered')
    	{	
             update_user_meta( $user_id, 'ist_account_status', 'cancelled');
             $razorpay_payment_id=$razorpay_order_id="";

    	}

    return true;
}
function ist_get_user_pay_info($id=""){
	$profile="";
	if($id==''){
		global $current_user;
		$current_user_id = $current_user->ID; // Get User ID
	}else{
		$current_user_id=$id;
	}
	$profile_array= get_user_meta($current_user_id,'raz_payment_info'); 
	if($profile_array){
     $profile= $profile_array['0'];
      
	}
 return $profile;
}
add_shortcode( 'ist_get_user_pay_info', 'ist_get_user_pay_info' );