<?PHP

// define Create Test Page
function create_test_shortcode($arg) { 
    ob_start();
    require_once ('create_test_page.php');
    return ob_get_clean();   
} 
add_shortcode( 'create_test_call', 'create_test_shortcode' );

// define my user page
function my_page_shortcode($arg) { 
    ob_start();
    require_once ('my_page.php');
    return ob_get_clean();   
} 
add_shortcode( 'my_page_call', 'my_page_shortcode' );

//School User Procile status check
function profile_form_shortcode() {
  ob_start();
  include ('user_html.php');
  return ob_get_clean();   
} 
add_shortcode( 'profile_form_call', 'profile_form_shortcode' );

//School User Procile status check
function ist_nav_menu_shortcode() {
  ob_start();
  include ('nav_menu.php');
  return ob_get_clean();   
} 
add_shortcode( 'ist_nav_menu', 'ist_nav_menu_shortcode' );

?>
