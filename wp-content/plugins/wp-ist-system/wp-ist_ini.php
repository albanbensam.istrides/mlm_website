<?php
/**
 * Plugin Name: ISTRIDES Core
 * Plugin URI: http://www.istrides.in/ist-core
 * Description: IStrides Core Plugin for Update.
 * Version: 1.2
 * Author: Vivek Developer
 * Author URI: http://www.istrides.in
 */
// Set timezone 
date_default_timezone_set('Asia/Kolkata'); 
define('IST_CORE_VERSION', '1.2.14');
define('IST_CORE_URL', plugin_dir_url( __FILE__ ));

// all includes
require_once 'includes/src/call_functions.php';

function add_roles_on_plugin_activation() {
 add_role( 'teacher', 'Teacher', array( 'read' => true, 'level_0' => true ) );
 add_role( 'parent', 'Parent', array( 'read' => true, 'level_0' => true ) );
}
register_activation_hook( __FILE__, 'add_roles_on_plugin_activation' );

function ist_installer(){
  require_once('core/installer.php');
}
function ist_installer_data(){
  require_once('core/installer_data.php');
}
register_activation_hook( __file__, 'ist_installer' );
register_activation_hook( __FILE__, 'ist_installer_data' );



//Session destroy while logout
add_action('wp_logout', 'session_logout');
function session_logout() {
  //session_destroy();
   //$sessions->destroy_all();//destroys all sessions
   //wp_clear_auth_cookie();//clears cookies regarding WP Auth
  return true;
}


// session Login Set and check
add_action('plugins_loaded', 'session_manager'); 
function session_manager() {

  if (!session_id()) {
    if (!headers_sent($filename, $linenum)) {
       session_start();

    // You would most likely trigger an error here.
    } else {

        echo "Headers already sent in $filename on line $linenum\n" .
              "Cannot redirect, for now please click this <a " .
              "href=\"https://eversmilelearning.com\">link</a> instead\n";
        exit;
    }
    
  }
  if( is_user_logged_in() && $_SESSION['user_id']==""){
    $id= get_current_user_id();
    if($id){
      $user = get_user_by('ID', $id);
      if($user){
     $_SESSION['user_id']=$user->id; //print_r(get_userdata($user->id)); die;
     $_SESSION['display_name']=$user->display_name;
     $_SESSION['caps']=$user->caps;
     $_SESSION['user_id']=$user->id;
     $user_arrayf= get_user_meta($user->ID,'first_name');
     if($user_arrayf){
      $_SESSION['first_name']=$user_arrayf['0']; 
    }else{
      $_SESSION['first_name']="error";
    } 
    $user_array= get_user_meta($user->ID,'authorization');
    if($user_array){
      $_SESSION['authorization']=$user_array['0']; 
    }else{
      $_SESSION['authorization']="error";
    } 

  }
  $user = new WP_User( $user->ID );
  if ( !empty( $user->roles ) && is_array( $user->roles ) ) { 
    foreach ( $user->roles as $role ) {     $_SESSION['role']= ucfirst($role); }  
  } 
}
}
}


require_once 'includes/custom_ini.php'; // after session set value
require_once 'includes/assets_ist.php'; // after session set value
require_once 'includes/actions_ist.php'; // after session set value
//Session Reset


/**
Name: Defind Custom Script.js
*/
function ist_scripts_method() {
   // register your script location, dependencies and version
 wp_register_script('wp-ist-system-script', IST_CORE_URL.'/js/js_ini.js', array('jquery'), IST_CORE_VERSION, false);
 wp_localize_script( 'wp-ist-system-script', 'OtpBut', array(
    // URL to wp-admin/admin-ajax.php to process the request
    'ajaxurl' => admin_url( 'admin-ajax.php' ),
    // generate a nonce with a unique ID "myajax-post-comment-nonce"
    // so that you can check it later when an AJAX request is sent
    'security' => wp_create_nonce( 'ref-string' )
  ));
   // enqueue the script
 wp_enqueue_script('wp-ist-system-script');
}
add_action('wp_enqueue_scripts', 'ist_scripts_method');


/**
Name: Defind Custom Style.css
*/
function ist_style_method() { 
   // register your Style location, dependencies and version
  wp_register_style('wp-ist-system-style', IST_CORE_URL.'/css/css_ini.css');

   // enqueue the style
  wp_enqueue_style('wp-ist-system-style');

}
add_action('wp_enqueue_scripts', 'ist_style_method');


// function that runs when shortcode is called
add_action( 'init', function() {
  add_shortcode( 'site_url', function( $atts = null, $content = null ) {
    return site_url();
  } );


} );



//Add Custom ext Link
add_action( 'init', 'acc_init_int' );
function acc_init_int()
{
  add_rewrite_rule( 'call_ext_links_auto.php$', 'index.php?acc_auto=ok', 'top' );
}

add_filter( 'query_vars', 'acc_query_vars' );
function acc_query_vars( $query_vars )
{
  $query_vars[] = 'acc_auto';
  return $query_vars;
}

add_action( 'parse_request', 'acc_parse_request' );
function acc_parse_request( &$wp )
{
  if ( array_key_exists( 'acc_auto', $wp->query_vars ) ) {
    include 'call_ext_links_auto.php';
    exit();
  }
  return;
} 


function add_query_vars_filter( $vars ){
  $vars[] = "t"; // for item
  $vars[] = "q"; // for ordr
  $vars[] = "testid";
  return $vars;
}

add_filter( 'query_vars', 'add_query_vars_filter' );



//$resp_f = ist_api_header_code("/setrefferalvalid",$params_data,$access_token,$method='POST'); // Call API for Register new USer 
//print_r($resp_f); die;



/* Omit closing PHP tag to avoid "Headers already sent" issues. */