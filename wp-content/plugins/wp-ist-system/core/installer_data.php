<?php global $wpdb;
	
	$welcome_name = 'IST_CORE';
	$welcome_text = 'Congratulations, you just completed the installation!';
	
	$table_name = $wpdb->prefix . 'ist_options';
	
	$wpdb->insert( 
		$table_name, 
		array( 
			'date_time' => current_time( 'mysql' ), 
			'option_name' => $welcome_name, 
			'option_value' => $welcome_text, 
		) 
	);
?>