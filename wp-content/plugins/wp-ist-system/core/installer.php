<?php 
global $wpdb; 
    $version = get_option( 'my_plugin_version', '1.0' );
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'ist_options';
    $sql = "CREATE TABLE $table_name ( 
        option_id mediumint(9) NOT NULL AUTO_INCREMENT, 
        date_time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL, 
        user_id  bigint(20) NOT NULL,
        option_name varchar(191) NOT NULL, 
        option_value longtext  NOT NULL, 
        UNIQUE KEY option_id (option_id) ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql ); 
    if ( version_compare( $version, '2.0' ) < 0 ) { 
        $sql = "CREATE TABLE $table_name ( 
        option_id mediumint(9) NOT NULL AUTO_INCREMENT, 
        date_time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL, 
        user_id  bigint(20) NOT NULL,
        option_name varchar(191) NOT NULL, 
        option_value longtext  NOT NULL, 
        UNIQUE KEY option_id (option_id) ) $charset_collate;";

        dbDelta( $sql ); update_option( 'my_plugin_version', '2.0' ); 
    }


?>