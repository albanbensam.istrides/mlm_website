<?php /* Template Name: User Subscription IST */  ?>
<?php


$mkdf_sidebar_layout  = iacademy_mikado_sidebar_layout();

$site_url=do_shortcode('[site_url]');
$ret_data= do_shortcode('[my_page_call]');
$subscripe_plan_array=json_decode($ret_data,true);

get_header();
//iacademy_mikado_get_title();
get_template_part('slider');

//foreach ($_SESSION as $key => $value) {
//	echo $key."=".$value."<br>";
//}
//Woocommerce content
?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="/wp-content/plugins/wp-ist-system/css/bootstrap4.css"  >
 
<style>
.bg-1{ background-color:#4682b4;color:#fff; }
.fw-400{font-weight:400;}
.fw-500{font-weight:500;}
.mt-1{
	margin-top:1em!important;
}
.mt-10{margin-top:2em;}
 .service-1,label {
  
  color: #3e4555;
   
}

.service-1 h1,
.service-1 h2,
.service-1 h3,
.service-1 h4,
.service-1 h5,
.service-1 h6 {
  color: #3e4555;
}

.service-1 .font-weight-medium {
  font-weight: 500;
}
.font-weight-bold{font-weight:800;}

.service-1 .bg-light {
  background-color: #f4f8fa !important;
}

.service-1 .subtitle {
  color: #8d97ad;
  line-height: 24px;
}

.service-1 .card.card-shadow {
  -webkit-box-shadow: 0px 0px 30px rgba(115, 128, 157, 0.1);
  box-shadow: 0px 0px 30px rgba(115, 128, 157, 0.1);
}

.service-1 .wrap-service1-box .card-body {
  padding: 20px;
  text-align:center;
  box-shadow: 3px 6px 10px #ddd;
}

.service-1 .btn-success-gradiant {
  background: #2cdd9b;
  background: -webkit-linear-gradient(legacy-direction(to right), #2cdd9b 0%, #1dc8cc 100%);
  background: -webkit-gradient(linear, left top, right top, from(#2cdd9b), to(#1dc8cc));
  background: -webkit-linear-gradient(left, #2cdd9b 0%, #1dc8cc 100%);
  background: -o-linear-gradient(left, #2cdd9b 0%, #1dc8cc 100%);
  background: linear-gradient(to right, #2cdd9b 0%, #1dc8cc 100%);
  border: 0px;
}

.service-1 .btn-success-gradiant:hover {
  background: #1dc8cc;
  background: -webkit-linear-gradient(legacy-direction(to right), #1dc8cc 0%, #2cdd9b 100%);
  background: -webkit-gradient(linear, left top, right top, from(#1dc8cc), to(#2cdd9b));
  background: -webkit-linear-gradient(left, #1dc8cc 0%, #2cdd9b 100%);
  background: -o-linear-gradient(left, #1dc8cc 0%, #2cdd9b 100%);
  background: linear-gradient(to right, #1dc8cc 0%, #2cdd9b 100%);
}

.service-1 .btn-md {
  padding: 15px 45px;
  font-size: 16px;
}
.text-left{text-align:left;}
.tooltip {
  position: relative;
  display: inline-block;
}


 


</style>

<div class="mkdf-title-holder mkdf-standard-with-breadcrumbs-type mkdf-has-bg-image  " style="height: 150px; background-color: rgb(255, 255, 255); background-image: url(&quot;/wp-content/uploads/2017/05/pages-title-parallax.jpg&quot;); background-position: center -200px;" data-height="150">
  <div class="mkdf-title-image">
    <img itemprop="image" src="/wp-content/uploads/2017/05/pages-title-parallax.jpg" alt="a">
  </div>
  <div class="mkdf-title-wrapper" style="height: 150px">
    <div class="mkdf-title-inner">
      <div class="mkdf-grid">
        <div class="mkdf-title-info">
          <h2 class="mkdf-page-title entry-title">Summary</h2>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="mkdf-container mkdf-custom-container">

	<div class="mkdf-container-inner clearfix " style="padding-top:50px;">
		<div class="mkdf-gr id-row">
		
		
		
		
		
				<nav>
		<ul class="horizontal" id="nav">
			<li><a  class="<?php echo $myproducts_active ?> active" href="<?php echo $site_url; ?>/user-dashboard">My Account</a></li>
			<li><a  class="<?php echo $taketest_active ?>" href="#<?php //echo $site_url.'/tree-structrue'; ?>">Tree Structure</a></li>
			<li><a  class="<?php echo $learnwithtutor_active ?>" href="#<?php //echo $site_url.'/commission-details'; ?>">Commission Details</a></li>
			<li class="float-right"><a  class="btn btn-back btn-sm " href="<?php echo $site_url; ?>/user-dashboard">Back</a></li>
		
		</ul>
		</nav>
				<div>
				<?php 
				$subscripe_id='';
				$random_subscribe_no="-";
				$user_id='';
				$subscription_master_id='';
				$commision_data_structure_id='';
				$registeration_date='';
				$activation_date='';
				$elimination_date='';
				$closing_date='';
				$rejoin_date='';
				$rejected_date='';
				$max_user_count='';
				$total_approved_user='';
				$pending_users_count='';
				$referal_code='';
				$refered_by_code='';
				$refered_by_user='';
				$current_status='';
				$reason='';
				$subscription_master_name='';
				//print_r($subscripe_plan_array);
				if(isset($subscripe_plan_array)){ foreach ($subscripe_plan_array as $key => $value) {
					$$key=$value;
					?>
					<!--<div class=""><span>$<?php //echo $key; ?>='';</span> <span class="c_val"><?php //echo $value; ?></span></div>-->
					<?PHP
				} } ?>
					</div>
		</div>
 
 
 
 
 
 
 
 
<div class="bg-l ight   service-1  ">
  
    <div class="card" style="    padding: 5px 0px 20px 0px;">
    <div class="row">
    <div class="col">
        <div class="  border-0    ">
          <div class="  text-center">              
             <h6 class="mt-3 font-weight-medium">Subscription ID </h6>
			 <h5 class="font-weight-bold"> <?php echo $random_subscribe_no; ?> </h5>
          </div>
        </div>
    </div>
    <div class="col">
        <div class=" border-0  ">
          <div class="  text-center">              
             <h6 class="mt-3 font-weight-medium">Your Referal Code </h6>
			 
			 <h5 class="font-weight-bold"><span id="textToCopy"> <?php echo $referal_code; ?></span> 
			     <span   class="btn btn-outline-default btn-sm" title="copy code" id="copyButton"><i class="fa fa-copy"></i></span> 
                  <span id="copyResult"></span>
			 </h5>
			
          </div>
        </div>
    </div>
       <div class="col">
        <div class=" border-0   ">
          <div class="  text-center">              
             <h6 class="mt-3 font-weight-medium">Status </h6>
			 <h5 class="font-weight-bold"> <?php echo $current_status; ?></h5>
          </div>
        </div>
    </div>
	<div class="col">
        <div class="  border-0  ">
          <div class="  text-center">              
             <h6 class="mt-3 font-weight-medium">Activation Date </h6>
			 <h5 class="font-weight-bold"> <?php if($activation_date!=""){ echo date('d-M-Y ', strtotime($activation_date)); }else { echo "-"; } ; ?></h5>
          </div>
        </div>
    </div>
	
    <div class="col">
        <div class="  border-0    ">
          <div class=" text-center">              
             <h6 class="mt-3 font-weight-medium">Refered By</h6>
			 <h5 class="font-weight-bold"><?php if(!empty($refered_by_user)){ echo $refered_by_user; }else { echo "-"; } ?>  </h5>
          </div>
        </div>
    </div>
    <div class="col">
        <div class="  border-0    ">
          <div class=" text-center">              
             <h6 class="mt-3 font-weight-medium">Package Type</h6>
			 <h5 class="font-weight-bold"><?php if(!empty($subscription_master_name)){ echo $subscription_master_name; }else { echo "-"; } ?>  </h5>
          </div>
        </div>
    </div>
  </div>
  </div>
  
    <div class="row">
	   <div class="col">
          <div class="card mt-10" style="min-height: 400px;">
	         <div class="card-header">Levels</div>
	         <div class="card-body">
                	<table class="table">
			    <thead>
				  <tr>
				    <th>Level </th>
				    <th>Total Members</th>
				    <th>Completed</th>
				     
				  </tr>
				</thead>
				<tbody>
				 <tr>
				  <td>Level 1</td>
				  <td>10</td>
				  <td>6</td>
				 </tr>
				 <tr>
				  <td>Level 1</td>
				  <td>10</td>
				  <td>6</td>
				 </tr>
				 <tr>
				  <td>Level 1</td>
				  <td>10</td>
				  <td>6</td>
				 </tr>
				 <tr>
				  <td>Level 1</td>
				  <td>10</td>
				  <td>6</td>
				 </tr>
				 <tr>
				  <td>Level 1</td>
				  <td>10</td>
				  <td>6</td>
				 </tr>
				 
				</body>
				
			  </table>		 
	         </div>
	      </div>
	   </div>
	   
	    <div class="col">
          <div class="card mt-10">
	         <div class="card-header">Counts</div>
	         <div class="card-body">
                <div class="list-group">
                <a   class="list-group-item visitor">
                    <h3 class="pull-right">
                        <i class="fa fa-users text-info"></i>
                    </h3>
                    <h4 class="list-group-item-heading count text-info">
                        <?php if($max_user_count!=""){ echo $max_user_count; }else { echo "0"; } ?></h4>
                    <p class="list-group-item-text">
                        Maximum User Count</p>
                </a>
				<a   class="list-group-item facebook-like">
                    <h3 class="pull-right">
                        <i class="fa fa-user-plus text-success"></i>
                    </h3>
                    <h4 class="list-group-item-heading count text-success">
                        <?php if($total_approved_user!=""){ echo $total_approved_user; }else { echo "0"; } ?></h4>
                    <p class="list-group-item-text">
                        Total Approved User</p>
                </a>
				<a  class="list-group-item google-plus">
                    <h3 class="pull-right">
                        <i class="fa fa-user-times text-warning"></i>
                    </h3>
                    <h4 class="list-group-item-heading count text-warning">
                        <?php if($pending_users_count!=""){ echo $pending_users_count; }else { echo "0"; } ?></h4>
                    <p class="list-group-item-text">
                        Pending Users</p>
                </a> 
            </div>		 
	         </div>
	      </div>
	   </div>
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	</div>
	
	   
	</div>
  
  
 

  <br><br>

  
  

	 
	  
	  
	  
  </div>
	  
	  
	  
	  
	  
	    
	    
	 
</div>
	
	</div>
	
  </div>
 
 <script>
   const answer = document.getElementById("copyResult");
const copy   = document.getElementById("copyButton");
const selection = window.getSelection();
const range = document.createRange();
const textToCopy = document.getElementById("textToCopy")

copy.addEventListener('click', function(e) {
    range.selectNodeContents(textToCopy);
    selection.removeAllRanges();
    selection.addRange(range);
    const successful = document.execCommand('copy');
    if(successful){
      answer.innerHTML = 'Copied!';
	  
    } else {
      answer.innerHTML = 'Unable to copy!';  
    }
	
    window.getSelection().removeAllRanges();
	
	 
	jQuery("#copyResult").fadeIn("3000");
    setTimeout(function(){ jQuery("#copyResult").fadeOut("slow"); }, 3000);
});
 
	
	
	
	
	 
 </script>







<?php  echo do_shortcode('[profile_form_call]'); ?>
<?php get_footer(); ?>