<?php /* Template Name: Payment Failed */  ?>
<?php


$mkdf_sidebar_layout  = iacademy_mikado_sidebar_layout();

$site_url=do_shortcode('[site_url]');
$ret_data= do_shortcode('[my_page_call]');
$subscripe_plan_array=json_decode($ret_data,true);

get_header();
//iacademy_mikado_get_title();
get_template_part('slider');

//foreach ($_SESSION as $key => $value) {
//	echo $key."=".$value."<br>";
//}
//Woocommerce content
?>
 

<div class="mkdf-title-holder mkdf-standard-with-breadcrumbs-type mkdf-has-bg-image  " style="height: 250px; background-color: rgb(255, 255, 255); background-image: url(&quot;/wp-content/uploads/2017/05/pages-title-parallax.jpg&quot;); background-position: center 0px;" data-height="250">
  <div class="mkdf-title-image">
    <img itemprop="image" src="/wp-content/uploads/2017/05/pages-title-parallax.jpg" alt="a">
  </div>
  <div class="mkdf-title-wrapper" style="height: 250px">
    <div class="mkdf-title-inner">
      <div class="mkdf-grid">
        <div class="mkdf-title-info">
          <h2 class="mkdf-page-title entry-title">Failed</h2>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="mkdf-container" style="padding-bottom:150px;">

	<div class="mkdf-container-inner clearfix " style="padding-top:50px;">
		<div class="mkdf-grid-row">
		
		   <div class="mkdf-grid-col-3"> </div>
		   <div class="mkdf-grid-col-6" style="text-align:center;">
		    <h4><i class="fa fa-times payment-failed"></i></h4>
		    <h3>Sorry!!! Your payment has been failed</h3>
		    <p>
		   <a href=""><button type="button" class="mkdf-btn mkdf-btn-medium mkdf-btn-solid mkdf-btn-custom-hover-bg">Go To </button></a>
		   </p>
		   </div>
				 
		</div>
 
    </div>
	
	</div>
	
 
 
 			
<?php // echo do_shortcode('[profile_form_call]'); ?>
<?php get_footer(); ?>