<?php /* Template Name: Profile Completion */  ?>
<?php
global  $current_user;
$current_user_id = $current_user->ID;

$mkdf_sidebar_layout  = iacademy_mikado_sidebar_layout();

$site_url=do_shortcode('[site_url]');
$ret_data= do_shortcode('[ist_get_user_status]'); 
$ret_scr= do_shortcode('[istsubscripeplan]');
$subscripe_plan_array=json_decode($ret_scr,true);
//$subscripe_plan_array=json_decode($ret_data,true);
//print_r($ret_data); die;
get_header();
//iacademy_mikado_get_title();
get_template_part('slider');
$package_id="";
$user_array_p= get_user_meta($current_user_id,'subscripe_plan');
if($user_array_p){
	$package_id=$user_array_p['0'];
}

$pay_amount='50000';
if($package_id!= "" && isset($subscripe_plan_array) && count($subscripe_plan_array)>0){ 
	foreach ($subscripe_plan_array as $ky=>$vl) {
	  if(array_key_exists('package_name',$vl) && $package_id==$vl['subscription_id']){
	    $pay_amount=$vl['package_amount'];
	    $pay_amount=(int)($pay_amount);
	    break;
	  }
	} 
} 

?>
<div class="mkdf-title-holder mkdf-standard-with-breadcrumbs-type mkdf-has-bg-image  " style="height: 250px; background-color: rgb(255, 255, 255); background-image: url(&quot;/wp-content/uploads/2017/05/pages-title-parallax.jpg&quot;); background-position: center 0px;" data-height="250">
  <div class="mkdf-title-image">
    <img itemprop="image" src="/wp-content/uploads/2017/05/pages-title-parallax.jpg" alt="a">
  </div>
  <div class="mkdf-title-wrapper" style="height: 250px">
    <div class="mkdf-title-inner">
      <div class="mkdf-grid">
        <div class="mkdf-title-info">
          <h2 class="mkdf-page-title entry-title">Account Status</h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="mkdf-container" style="padding-bottom:150px;">

	<div class="mkdf-container-inner clearfix " style="padding-top:0px;">
		<div class="mkdf-grid-row">
		
		   <div class="mkdf-grid-col-3"> </div>
		   <div class="mkdf-grid-col-6" style="text-align:center;">
		    <h4><i class="fa fa-ban payment-cancelled"></i></h4>
		    <h3>Your subscription has been successfully created. To activate your subscription, payment has to be processed successfully.</h3>
		    <p>
		    <center>
		    	<div class="mkdf-grid-col-6">
					<?php if($ret_data=='registered' || $ret_data=='cancelled'){ ?>
					<?php echo do_shortcode('[RZP]'); ?>
					<?php }else {
						header("location:".site_url()."/user-dashboard");  // To profile  
					}
					?>
				</div>
				<div class="mkdf-grid-col-5">
					<input type="hidden" name="payment" id="payment" value="<?php echo $pay_amount; ?>">
					<a href="/payment-cancelled" class="mkdf-btn mkdf-btn-small  payment-cancelled" >Cancel Payment </a> 
				</div>
			</center>
		   
		   </p>
		   </div>
				 
		</div>
 
	 
</div>
	
	</div>
 
<?php if($ret_data=='registered'){ ?>
<script type="text/javascript">
	 jQuery(document).ready(function(){ // vivek js code
	 	jQuery('#btn-razorpay').trigger('click');
	 });
</script>
<?php } ?>

 <?php get_footer(); ?>