<?php

if(!function_exists('iacademy_mikado_design_styles')) {
    /**
     * Generates general custom styles
     */
    function iacademy_mikado_design_styles() {
	    $font_family = iacademy_mikado_options()->getOptionValue( 'google_fonts' );
	    if ( ! empty( $font_family ) && iacademy_mikado_is_font_option_valid( $font_family ) ) {
		    $font_family_selector = array(
			    'body'
		    );
		    echo iacademy_mikado_dynamic_css( $font_family_selector, array( 'font-family' => iacademy_mikado_get_font_option_val( $font_family ) ) );
	    }

		$first_main_color = iacademy_mikado_options()->getOptionValue('first_color');
        if(!empty($first_main_color)) {
            $color_selector = array(
                'a:hover',
                'blockquote',
                'h1 a:hover',
                'h2 a:hover',
                'h3 a:hover',
                'h4 a:hover',
                'h5 a:hover',
                'p a:hover',
                'blockquote p:before',
                '.mkdf-comment-holder .mkdf-comment-text .comment-edit-link',
                '.mkdf-comment-holder .mkdf-comment-text .comment-reply-link',
                '.mkdf-comment-holder .mkdf-comment-text .replay',
                '.mkdf-comment-holder .mkdf-comment-text .comment-edit-link:before',
                '.mkdf-comment-holder .mkdf-comment-text .comment-reply-link:before',
                '.mkdf-comment-holder .mkdf-comment-text .replay:before',
                '.mkdf-comment-holder .mkdf-comment-text #cancel-comment-reply-link',
                '.mkdf-owl-slider .owl-nav .owl-next:hover .mkdf-next-icon',
                '.mkdf-owl-slider .owl-nav .owl-next:hover .mkdf-prev-icon',
                '.mkdf-owl-slider .owl-nav .owl-prev:hover .mkdf-next-icon',
                '.mkdf-owl-slider .owl-nav .owl-prev:hover .mkdf-prev-icon',
                'footer .widget ul li a:hover',
                'footer .widget #wp-calendar tfoot a:hover',
                'footer .widget.widget_search .input-holder button:hover',
                'footer .widget.widget_tag_cloud a:hover',
                'aside.mkdf-sidebar .widget.widget_text .mkdf-anchor-menu ul li:hover a',
                'aside.mkdf-sidebar .widget.widget_nav_menu ul>li ul.sub-menu li.current-menu-item>a',
                'aside.mkdf-sidebar .widget.widget_nav_menu ul>li ul.sub-menu li:hover>a',
                'aside.mkdf-sidebar .widget.widget_nav_menu ul>li.menu-item-has-children>a.mkdf-custom-menu-active',
                'aside.mkdf-sidebar .widget.widget_nav_menu ul>li.menu-item-has-children>a:hover',
                'aside.mkdf-sidebar .widget.widget_nav_menu ul>li.menu-item-has-children>a:before',
                '.wpb_widgetised_column .widget.widget_nav_menu ul>li.menu-item-has-children>a:before',
                '.wpb_widgetised_column .widget.widget_nav_menu ul>li ul.sub-menu li.current-menu-item a',
                '.wpb_widgetised_column .widget.widget_nav_menu ul>li ul.sub-menu li:hover a',
                '.widget.mkdf-blog-list-widget .mkdf-blog-list-holder.mkdf-bl-simple .mkdf-post-title a:hover',
                '.widget.mkdf-blog-list-widget .mkdf-post-info-date a:hover',
                '.wpb_widgetised_column.mkdf-course-features-widget .mkdf-course-features li .mkdf-item-icon',
                '.widget.widget_rss .mkdf-widget-title .rsswidget:hover',
                '.widget.widget_search button:hover',
                '.widget.mkdf-course-categories-widget .mkdf-course-categories-list li i',
                '.widget.mkdf-course-categories-widget .mkdf-course-categories-list li span',
                '.widget.mkdf-course-categories-widget .mkdf-course-categories-list li a:hover',
                '.widget.mkdf-blog-categories-widget .mkdf-blog-categories-list li i',
                '.widget.mkdf-blog-categories-widget .mkdf-blog-categories-list li span',
                '.widget.mkdf-blog-categories-widget .mkdf-blog-categories-list li a:hover',
                '.widget.mkdf-course-features-widget .mkdf-course-features li .mkdf-item-icon',
                '.mkdf-page-footer .widget a:hover',
                '.mkdf-side-menu .widget a:hover',
                '.mkdf-page-footer .widget.widget_rss .mkdf-footer-widget-title .rsswidget:hover',
                '.mkdf-side-menu .widget.widget_rss .mkdf-footer-widget-title .rsswidget:hover',
                '.mkdf-page-footer .widget.widget_search button:hover',
                '.mkdf-side-menu .widget.widget_search button:hover',
                '.mkdf-page-footer .widget.widget_tag_cloud a:hover',
                '.mkdf-side-menu .widget.widget_tag_cloud a:hover',
                '.mkdf-top-bar a:hover',
                '.mkdf-icon-widget-holder',
                '.mkdf-icon-widget-holder.mkdf-link-with-href:hover .mkdf-icon-text',
                '.widget.widget_mkdf_twitter_widget .mkdf-twitter-widget.mkdf-twitter-standard li .mkdf-twitter-icon',
                '.widget ul li a',
                '.widget #wp-calendar tfoot a',
                '.wpb_widgetised_column .widget.mkdf-search-post-type-widget .mkdf-post-type-search-results ul li a:hover',
                '.widget.widget_mkdf_twitter_widget .mkdf-twitter-widget.mkdf-twitter-slider li .mkdf-tweet-text a',
                '.widget.widget_mkdf_twitter_widget .mkdf-twitter-widget.mkdf-twitter-slider li .mkdf-tweet-text span',
                '.widget.widget_mkdf_twitter_widget .mkdf-twitter-widget.mkdf-twitter-standard li .mkdf-tweet-text a:hover',
                '.widget.widget_mkdf_twitter_widget .mkdf-twitter-widget.mkdf-twitter-slider li .mkdf-twitter-icon i',
                '#tribe-events-content-wrapper .tribe-bar-views-list li a:hover',
                '#tribe-events-content-wrapper .tribe-bar-views-list li.tribe-bar-active a',
                '#tribe-events-content-wrapper .tribe-events-sub-nav .tribe-events-nav-next a:hover',
                '#tribe-events-content-wrapper .tribe-events-sub-nav .tribe-events-nav-previous a:hover',
                '#tribe-events-content-wrapper .tribe-events-calendar td div[id*=tribe-events-daynum-] a:hover',
                '#tribe-events-content-wrapper .tribe-events-ical.tribe-events-button',
                '.mkdf-tribe-events-single .mkdf-events-single-meta .mkdf-events-single-meta-item span.mkdf-events-single-meta-value a',
                '.mkdf-tribe-events-single .mkdf-events-single-meta .mkdf-events-single-next-event a:hover',
                '.mkdf-tribe-events-single .mkdf-events-single-meta .mkdf-events-single-prev-event a:hover',
                '#bbpress-forums li.bbp-header>ul>li a:hover',
                '#bbpress-forums li.bbp-body .bbp-forum-freshness .bbp-author-name',
                'body.forum-archive #bbpress-forums li.bbp-body ul.forum li.bbp-forum-freshness>a:hover',
                'body.forum-archive #bbpress-forums li.bbp-body ul.forum li.bbp-topic-freshness>a:hover',
                'body.forum-archive #bbpress-forums li.bbp-body .bbp-topic-started-by .bbp-author-name',
                'body.forum #bbpress-forums .subscription-toggle',
                'body.forum #bbpress-forums li.bbp-body ul.topic li.bbp-forum-freshness>a:hover',
                'body.forum #bbpress-forums li.bbp-body ul.topic li.bbp-topic-freshness>a:hover',
                'body.forum #bbpress-forums li.bbp-body ul.topic .bbp-topic-freshness-author .bbp-author-name',
                'body.forum #bbpress-forums li.bbp-body ul.topic.sticky:after',
                'body.forum #bbpress-forums li.bbp-body .bbp-topic-started-by .bbp-author-name',
                '#bbpress-forums div.bbp-breadcrumb .bbp-breadcrumb-current',
                '#bbpress-forums div.bbp-breadcrumb .bbp-breadcrumb-home:hover',
                '#bbpress-forums #bbp-single-user-details #bbp-user-navigation li.current a',
                '#bbpress-forums #bbp-single-user-details #bbp-user-navigation li a:hover',
                '#bbpress-forums #bbp-user-body .bbp-topic-freshness-author .bbp-author-name',
                '#bbpress-forums #bbp-user-body .bbp-topic-started-by .bbp-author-name',
                'body.topic #bbpress-forums .bbp-replies li.bbp-body div.bbp-reply-author .bbp-author-name',
                '.mkdf-sidebar .widget.widget_display_replies ul li',
                '.mkdf-sidebar .widget.widget_display_topics ul li',
                '.mkdf-sidebar .widget_display_forums li a:after',
                '.mkdf-sidebar .widget_display_views li a:after',
                '.mkdf-sidebar .widget_display_forums li a:hover',
                '.mkdf-sidebar .widget_display_views li a:hover',
                '.mkdf-blog-holder article.sticky .mkdf-post-title a',
                '.mkdf-blog-holder.mkdf-blog-narrow article .mkdf-post-info.mkdf-section-bottom .mkdf-post-info-author a:hover',
                '.mkdf-blog-holder.mkdf-blog-narrow article .mkdf-post-info.mkdf-section-bottom .mkdf-blog-like:hover i:first-child',
                '.mkdf-blog-holder.mkdf-blog-narrow article .mkdf-post-info.mkdf-section-bottom .mkdf-blog-like:hover span:first-child',
                '.mkdf-blog-holder.mkdf-blog-narrow article .mkdf-post-info.mkdf-section-bottom .mkdf-post-info-comments-holder:hover span:first-child',
                '.mkdf-blog-holder.mkdf-blog-standard-date-on-side article .mkdf-post-date-inner .mkdf-post-date-day',
                '.mkdf-blog-holder.mkdf-blog-standard-date-on-side article .mkdf-post-date-inner .mkdf-post-date-month',
                '.mkdf-blog-holder.mkdf-blog-standard-date-on-side article .mkdf-post-title a:hover',
                '.mkdf-blog-holder.mkdf-blog-standard-date-on-side article .mkdf-post-info>div a:hover',
                '.mkdf-blog-holder.mkdf-blog-standard-date-on-side article.format-quote .mkdf-quote-author',
                '.mkdf-blog-holder.mkdf-blog-standard article .mkdf-post-info-bottom .mkdf-post-info-author a:hover',
                '.mkdf-blog-holder.mkdf-blog-standard article .mkdf-post-info-bottom .mkdf-blog-like:hover i:first-child',
                '.mkdf-blog-holder.mkdf-blog-standard article .mkdf-post-info-bottom .mkdf-blog-like:hover span:first-child',
                '.mkdf-blog-holder.mkdf-blog-standard article .mkdf-post-info-bottom .mkdf-post-info-comments-holder:hover span:first-child',
                '.mkdf-author-description .mkdf-author-description-text-holder .mkdf-author-name a:hover',
                '.mkdf-bl-standard-pagination ul li.mkdf-bl-pag-active a',
                '.mkdf-blog-pagination ul li a.mkdf-pag-active',
                '.mkdf-blog-pagination ul li a:hover',
                '.mkdf-blog-single-navigation .mkdf-blog-single-next:hover',
                '.mkdf-blog-single-navigation .mkdf-blog-single-prev:hover',
                '.mkdf-page-footer .mkdf-footer-top-holder.dark .widget a:hover',
                '.mkdf-main-menu ul li a:hover',
                '.mkdf-main-menu ul li a .mkdf-menu-featured-icon',
                '.mkdf-main-menu>ul>li.mkdf-active-item>a',
                '.mkdf-drop-down .second .inner ul li a:hover',
                '.mkdf-drop-down .second .inner ul li.current-menu-ancestor>a',
                '.mkdf-drop-down .second .inner ul li.current-menu-item>a',
                '.mkdf-drop-down .wide .second .inner ul li a:hover',
                '.mkdf-drop-down .wide .second .inner>ul>li.current-menu-ancestor>a',
                '.mkdf-drop-down .wide .second .inner>ul>li.current-menu-item>a',
                '.mkdf-drop-down .wide .second .inner>ul>li.uses-custom-sidebar .widget.mkdf-course-list-widget .mkdf-course-list-holder.mkdf-cl-minimal article .mkdf-cli-text .mkdf-cli-title a:hover',
                'nav.mkdf-fullscreen-menu ul li ul li.current-menu-ancestor>a',
                'nav.mkdf-fullscreen-menu ul li ul li.current-menu-item>a',
                'nav.mkdf-fullscreen-menu>ul>li.mkdf-active-item>a',
                '.mkdf-mobile-header .mkdf-mobile-nav ul li.current-menu-ancestor>a',
                '.mkdf-mobile-header .mkdf-mobile-nav ul li.current-menu-item>a',
                '.mkdf-mobile-header .mkdf-mobile-nav ul li a:hover',
                '.mkdf-mobile-header .mkdf-mobile-nav ul li h6:hover',
                '.mkdf-mobile-header .mkdf-mobile-nav ul ul li.current-menu-ancestor>a',
                '.mkdf-mobile-header .mkdf-mobile-nav ul ul li.current-menu-item>a',
                '.mkdf-mobile-header .mkdf-mobile-nav .mkdf-grid>ul>li>a:hover',
                '.mkdf-mobile-header .mkdf-mobile-nav .mkdf-grid>ul>li>h6:hover',
                '.mkdf-mobile-header .mkdf-mobile-nav .mkdf-grid>ul>li.mkdf-active-item>a',
                '.mkdf-mobile-header .mkdf-mobile-nav .mkdf-grid>ul>li.mkdf-active-item>.mobile_arrow>.mkdf-sub-arrow',
                '.mkdf-mobile-header .mkdf-mobile-nav li.current-menu-ancestor>.mobile_arrow',
                '.mkdf-mobile-header .mkdf-mobile-nav li.current-menu-item .mobile_arrow',
                '.mkdf-search-page-holder article.sticky .mkdf-post-title a',
                '.mkdf-side-menu-button-opener.opened',
                '.mkdf-side-menu-button-opener:hover',
                '.mkdf-side-menu a.mkdf-close-side-menu:hover',
                '.mkdf-masonry-gallery-holder .mkdf-mg-item .mkdf-mg-item-subtitle',
                '.mkdf-masonry-gallery-holder .mkdf-mg-item .mkdf-mg-item-link',
                '.mkdf-accordion-holder .mkdf-accordion-title .mkdf-accordion-mark',
                '.mkdf-banner-holder .mkdf-banner-link-text .mkdf-banner-link-hover span',
                '.mkdf-btn.mkdf-btn-simple',
                '.mkdf-countdown .countdown-row .countdown-section .countdown-amount',
                '.mkdf-countdown .countdown-row .countdown-section .countdown-period',
                '.mkdf-counter-holder .mkdf-counter',
                '.mkdf-counter-holder .mkdf-counter-title',
                '.mkdf-icon-list-holder .mkdf-il-icon-holder>*',
                '.mkdf-image-gallery.mkdf-ig-carousel-type .owl-nav .owl-next:hover .mkdf-next-icon',
                '.mkdf-image-gallery.mkdf-ig-carousel-type .owl-nav .owl-next:hover .mkdf-prev-icon',
                '.mkdf-image-gallery.mkdf-ig-carousel-type .owl-nav .owl-prev:hover .mkdf-next-icon',
                '.mkdf-image-gallery.mkdf-ig-carousel-type .owl-nav .owl-prev:hover .mkdf-prev-icon',
                '.mkdf-image-gallery.mkdf-ig-carousel-type .owl-nav .mkdf-next-icon',
                '.mkdf-image-gallery.mkdf-ig-carousel-type .owl-nav .mkdf-prev-icon',
                '.mkdf-social-share-holder.mkdf-dropdown .mkdf-social-share-dropdown-opener:hover',
                '.mkdf-tabs.mkdf-tabs-vertical .mkdf-tabs-nav li.ui-state-active a',
                '.mkdf-tabs.mkdf-tabs-vertical .mkdf-tabs-nav li.ui-state-hover a',
                '.mkdf-team .mkdf-team-social-wrapp .mkdf-icon-shortcode i:hover',
                '.mkdf-team .mkdf-team-social-wrapp .mkdf-icon-shortcode span:hover',
                '.mkdf-team.main-info-below-image.info-below-image-boxed .mkdf-team-social-wrapp .mkdf-icon-shortcode .flip-icon-holder .icon-normal span',
                '.mkdf-team.main-info-below-image.info-below-image-standard .mkdf-team-social-wrapp .mkdf-icon-shortcode .flip-icon-holder .icon-flip span',
                '.mkdf-membership-dashboard-content-holder .mkdf-lms-profile-favorites-holder .mkdf-lms-profile-favorite-item-title .mkdf-course-wishlist',
                '.mkdf-menu-area .mkdf-login-register-widget.mkdf-user-logged-in:hover .mkdf-logged-in-user .mkdf-logged-in-user-inner > span',
                '.mkdf-menu-area .mkdf-login-register-widget.mkdf-user-logged-in .mkdf-login-dropdown li a:hover',
                '.mkdf-cl-filter-holder .mkdf-course-layout-filter span.mkdf-active',
                '.mkdf-cl-filter-holder .mkdf-course-layout-filter span:hover',
                '.mkdf-cl-standard-pagination ul li.mkdf-cl-pag-active a',
                '.mkdf-advanced-course-search .select2.mkdf-course-category:before',
                '.mkdf-advanced-course-search .select2.mkdf-course-instructor:before',
                '.mkdf-advanced-course-search .select2.mkdf-course-price:before',
                '.mkdf-course-table-holder tbody .mkdf-ct-item .mkdf-tc-course-field .mkdf-cli-title-holder:hover',
                '.mkdf-course-table-holder tbody .mkdf-ct-item .mkdf-tc-instructor-field a:hover .mkdf-instructor-name',
                '.mkdf-course-table-holder tbody .mkdf-ct-item .mkdf-tc-category-field .mkdf-cli-category-holder a:hover',
                '.mkdf-course-features-holder .mkdf-course-features li .mkdf-item-icon',
                '.mkdf-course-list-holder.mkdf-cl-minimal article .mkdf-ci-price-holder',
                '.mkdf-course-single-holder .mkdf-course-basic-info-wrapper .mkdf-course-category-items a:hover',
                '.mkdf-course-single-holder .mkdf-course-basic-info-wrapper .mkdf-instructor-name:hover',
                '.mkdf-course-single-holder .mkdf-course-basic-info-wrapper .mkdf-post-info-comments:hover',
                '.mkdf-course-reviews-list .mkdf-comment-holder .mkdf-review-title',
                '.mkdf-course-single-holder .mkdf-course-tabs-wrapper .mkdf-course-curriculum .mkdf-section-element .mkdf-element-info .mkdf-element-clock-icon',
                '.mkdf-course-single-holder .mkdf-course-tabs-wrapper .mkdf-course-curriculum .mkdf-section-element .mkdf-element-preview-holder',
                '.mkdf-course-popup .mkdf-course-popup-items .mkdf-section-element .mkdf-element-name .mkdf-element-preview-holder',
                '.mkdf-course-popup .mkdf-course-popup-items .mkdf-section-element .mkdf-element-name:hover',
                '.mkdf-instructor.info-bellow .mkdf-instructor-name:hover',
                '.mkdf-lesson-single-holder .mkdf-lms-message',
                '.mkdf-twitter-list-holder .mkdf-twitter-icon',
                '.mkdf-twitter-list-holder .mkdf-tweet-text a:hover',
                '.mkdf-twitter-list-holder .mkdf-twitter-profile a:hover',
                '.mkdf-modal-holder .mkdf-modal-content .mkdf-lost-pass-holder a',
                '.mkdf-modal-holder .mkdf-modal-content .mkdf-register-link-holder .mkdf-modal-opener',
                '.mkdf-login-register-content .mkdf-lost-pass-remember-holder .mkdf-lost-pass-holder a',
                '.mkdf-login-register-content .mkdf-register-link-holder .mkdf-modal-opener',
                '.mkdf-menu-area .mkdf-login-register-widget.mkdf-user-not-logged-in .mkdf-modal-opener.mkdf-login-opener',
                '.mkdf-top-bar .mkdf-login-register-widget.mkdf-user-logged-in .mkdf-login-dropdown li a:hover'
            );

            $woo_color_selector = array();
            if(iacademy_mikado_is_woocommerce_installed()) {
                $woo_color_selector = array(
                    '.mkdf-woocommerce-page table.cart tr.cart_item td.product-name a:hover',
                    '.mkdf-woocommerce-page table.cart tr.cart_item td.product-subtotal .amount',
                    '.mkdf-woocommerce-page .cart-collaterals table td strong',
                    '.woocommerce-pagination .page-numbers li a.current',
                    '.woocommerce-pagination .page-numbers li a:hover',
                    '.woocommerce-pagination .page-numbers li span.current',
                    '.woocommerce-pagination .page-numbers li span:hover',
                    '.woocommerce-page .mkdf-content .mkdf-quantity-buttons .mkdf-quantity-minus:hover',
                    '.woocommerce-page .mkdf-content .mkdf-quantity-buttons .mkdf-quantity-plus:hover',
                    'div.woocommerce .mkdf-quantity-buttons .mkdf-quantity-minus:hover',
                    'div.woocommerce .mkdf-quantity-buttons .mkdf-quantity-plus:hover',
                    '.mkdf-woo-pl-info-below-image .mkdf-pl-main-holder ul.products>.product .added_to_cart',
                    '.mkdf-woo-pl-info-below-image .mkdf-pl-main-holder ul.products>.product .button',
                    'ul.products>.product:hover .added_to_cart',
                    'ul.products>.product:hover .button',
                    'ul.products>.product:hover .mkdf-pl-inner .added_to_cart',
                    'ul.products>.product:hover .mkdf-pl-inner .button',
                    'ul.products>.product .price',
                    'ul.products>.product .added_to_cart',
                    'ul.products>.product .button',
                    '.mkdf-woo-single-page .mkdf-single-product-summary .price',
                    '.mkdf-woo-single-page .mkdf-single-product-summary .product_meta>span a:hover',
                    '.mkdf-woo-single-page .mkdf-single-product-summary .mkdf-woo-social-share-holder .mkdf-social-share-title',
                    '.mkdf-woo-single-page .mkdf-single-product-summary .mkdf-woo-social-share-holder .social_share',
                    '.mkdf-woo-single-page .related.products .product .button',
                    '.mkdf-woo-single-page .upsells.products .product .button',
                    '.mkdf-shopping-cart-dropdown .mkdf-item-info-holder .remove:hover',
                    '.mkdf-shopping-cart-dropdown .mkdf-item-info-holder .amount',
                    '.mkdf-shopping-cart-dropdown .mkdf-item-info-holder .mkdf-quantity',
                    '.widget.woocommerce.widget_layered_nav ul li.chosen a',
                    '.widget.woocommerce.widget_price_filter .price_slider_amount .button',
                    '.widget.woocommerce.widget_product_categories ul li a:hover',
                    '.widget.woocommerce.widget_products ul li a:hover',
                    '.widget.woocommerce.widget_recently_viewed_products ul li a:hover',
                    '.widget.woocommerce.widget_top_rated_products ul li a:hover',
                    '.widget.woocommerce.widget_products ul li .amount',
                    '.widget.woocommerce.widget_recently_viewed_products ul li .amount',
                    '.widget.woocommerce.widget_top_rated_products ul li .amount'
                );
            }

            $color_selector = array_merge($color_selector, $woo_color_selector);

	        $color_important_selector = array(
                '.mkdf-top-bar-dark .mkdf-top-bar .mkdf-icon-widget-holder:hover',
                '.mkdf-top-bar-dark .mkdf-top-bar .widget a:hover .mkdf-btn-text',
                '.mkdf-top-bar-dark .mkdf-top-bar .widget a:not(.lang_sel_sel):hover',
                '.mkdf-btn.mkdf-btn-simple:not(.mkdf-btn-custom-hover-color):hover',
                '.mkdf-woocommerce-page .woocommerce-message a.button'
	        );

            $background_color_selector = array(
                '.mkdf-st-loader .pulse',
                '.mkdf-st-loader .double_pulse .double-bounce1',
                '.mkdf-st-loader .double_pulse .double-bounce2',
                '.mkdf-st-loader .cube',
                '.mkdf-st-loader .rotating_cubes .cube1',
                '.mkdf-st-loader .rotating_cubes .cube2',
                '.mkdf-st-loader .stripes>div',
                '.mkdf-st-loader .wave>div',
                '.mkdf-st-loader .two_rotating_circles .dot1',
                '.mkdf-st-loader .two_rotating_circles .dot2',
                '.mkdf-st-loader .five_rotating_circles .container1>div',
                '.mkdf-st-loader .five_rotating_circles .container2>div',
                '.mkdf-st-loader .five_rotating_circles .container3>div',
                '.mkdf-st-loader .atom .ball-1:before',
                '.mkdf-st-loader .atom .ball-2:before',
                '.mkdf-st-loader .atom .ball-3:before',
                '.mkdf-st-loader .atom .ball-4:before',
                '.mkdf-st-loader .clock .ball:before',
                '.mkdf-st-loader .mitosis .ball',
                '.mkdf-st-loader .lines .line1',
                '.mkdf-st-loader .lines .line2',
                '.mkdf-st-loader .lines .line3',
                '.mkdf-st-loader .lines .line4',
                '.mkdf-st-loader .fussion .ball',
                '.mkdf-st-loader .fussion .ball-1',
                '.mkdf-st-loader .fussion .ball-2',
                '.mkdf-st-loader .fussion .ball-3',
                '.mkdf-st-loader .fussion .ball-4',
                '.mkdf-st-loader .wave_circles .ball',
                '.mkdf-st-loader .pulse_circles .ball',
                '#submit_comment',
                '.post-password-form input[type=submit]',
                'input.wpcf7-form-control.wpcf7-submit',
                '.mkdf-owl-slider .owl-dots .owl-dot.active span',
                '.mkdf-owl-slider .owl-dots .owl-dot:hover span',
                '#mkdf-back-to-top',
                '.widget.mkdf-search-post-type-widget .mkdf-seach-icon-holder',
                '#tribe-events-content-wrapper .tribe-bar-filters .tribe-events-button',
                '#tribe-events-content-wrapper .tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-]',
                '.mkdf-tribe-events-single .mkdf-events-single-main-info .mkdf-events-single-title-holder .mkdf-events-single-cost',
                '#bbpress-forums button',
                '.mkdf-sidebar .bbp_widget_login button:hover',
                '.mkdf-blog-holder article.format-audio .mkdf-blog-audio-holder .mejs-container .mejs-controls>.mejs-time-rail .mejs-time-total .mejs-time-current',
                '.mkdf-blog-holder article.format-audio .mkdf-blog-audio-holder .mejs-container .mejs-controls>a.mejs-horizontal-volume-slider .mejs-horizontal-volume-current',
                '.mkdf-blog-holder.mkdf-blog-split-column article .mkdf-post-info-bottom .mkdf-post-info-bottom-left>div a',
                '.mkdf-blog-holder.mkdf-blog-standard article .mkdf-post-info-bottom .mkdf-post-info-bottom-left>div a',
                '.mkdf-blog-holder.mkdf-blog-single article .mkdf-post-info-bottom .mkdf-post-info-bottom-left>div a',
                '.mkdf-events-list-item-title-holder .mkdf-events-list-item-price',
                '.mkdf-drop-down .wide .second .inner>ul>li.uses-custom-sidebar .widget.mkdf-course-list-widget .mkdf-course-list-holder.mkdf-cl-minimal article .mkdf-cli-text .mkdf-ci-price-holder .mkdf-ci-price-value',
                '.mkdf-masonry-gallery-holder .mkdf-mg-item.mkdf-mg-simple.mkdf-mg-skin-default .mkdf-mg-item-inner',
                '.mkdf-btn.mkdf-btn-solid',
                '.mkdf-comparision-pricing-tables-holder .mkdf-cpt-table.mkdf-featured-item .mkdf-cpt-table-btn a',
                '.mkdf-comparision-pricing-tables-holder .mkdf-cpt-table .mkdf-cpt-table-btn a:hover',
                '.mkdf-tml-holder .mkdf-timeline .mkdf-tml-item-holder:not(:last-of-type)::after',
                '.mkdf-tml-holder .mkdf-timeline .mkdf-tml-item-holder .mkdf-tml-item-circle',
                '.mkdf-dark-header #fp-nav ul li a.active span',
                '.mkdf-dark-header #fp-nav ul li a:hover span',
                '.mkdf-icon-shortcode.mkdf-circle',
                '.mkdf-icon-shortcode.mkdf-dropcaps.mkdf-circle',
                '.mkdf-icon-shortcode.mkdf-square',
                '.mkdf-price-table.mkdf-price-table-active .mkdf-active-pt-label .mkdf-active-pt-label-inner',
                '.mkdf-progress-bar .mkdf-pb-content-holder .mkdf-pb-content',
                '.mkdf-course-table-holder tbody .mkdf-ct-item .mkdf-tc-course-field .mkdf-ci-price-holder .mkdf-ci-price-value',
                '.mkdf-course-list-holder.mkdf-cl-standard article .mkdf-cli-text-holder .mkdf-cli-top-info .mkdf-ci-price-holder .mkdf-ci-price-value',
                '.mkdf-course-single-holder .mkdf-course-single-type',
                '.mkdf-course-popup .mkdf-popup-heading',
                '.mkdf-menu-area .mkdf-login-register-widget.mkdf-user-not-logged-in .mkdf-modal-opener.mkdf-login-opener:hover',
                '.mkdf-menu-area .mkdf-login-register-widget.mkdf-user-not-logged-in .mkdf-modal-opener.mkdf-register-opener'
            );

            $woo_background_color_selector = array();
            if(iacademy_mikado_is_woocommerce_installed()) {
                $woo_background_color_selector = array(
                    '.woocommerce-page .mkdf-content .wc-forward:not(.added_to_cart):not(.checkout-button)',
                    '.woocommerce-page .mkdf-content a.added_to_cart',
                    '.woocommerce-page .mkdf-content a.button',
                    '.woocommerce-page .mkdf-content button[type=submit]:not(.mkdf-woo-search-widget-button)',
                    '.woocommerce-page .mkdf-content input[type=submit]',
                    'div.woocommerce .wc-forward:not(.added_to_cart):not(.checkout-button)',
                    'div.woocommerce a.added_to_cart',
                    'div.woocommerce a.button',
                    'div.woocommerce button[type=submit]:not(.mkdf-woo-search-widget-button)',
                    'div.woocommerce input[type=submit]',
                    '.woocommerce .mkdf-out-of-stock',
                    '.mkdf-shopping-cart-holder .mkdf-header-cart .mkdf-cart-count',
                    '.widget.woocommerce.widget_price_filter .price_slider_wrapper .ui-widget-content .ui-slider-handle',
                    '.widget.woocommerce.widget_price_filter .price_slider_wrapper .ui-widget-content .ui-slider-range',
                    '.mkdf-pl-holder .mkdf-pli-inner .mkdf-pli-text-inner .mkdf-pli-add-to-cart.mkdf-default-skin .added_to_cart',
                    '.mkdf-pl-holder .mkdf-pli-inner .mkdf-pli-text-inner .mkdf-pli-add-to-cart.mkdf-default-skin .button',
                    '.mkdf-pl-holder .mkdf-pli-inner .mkdf-pli-text-inner .mkdf-pli-add-to-cart.mkdf-light-skin .added_to_cart:hover',
                    '.mkdf-pl-holder .mkdf-pli-inner .mkdf-pli-text-inner .mkdf-pli-add-to-cart.mkdf-light-skin .button:hover',
                    '.mkdf-pl-holder .mkdf-pli-inner .mkdf-pli-text-inner .mkdf-pli-add-to-cart.mkdf-dark-skin .added_to_cart:hover',
                    '.mkdf-pl-holder .mkdf-pli-inner .mkdf-pli-text-inner .mkdf-pli-add-to-cart.mkdf-dark-skin .button:hover'
                );
            }

            $background_color_selector = array_merge($background_color_selector, $woo_background_color_selector);

            $background_color_important_selector = array(

                '.mkdf-tribe-events-single .tribe-events-cal-links .tribe-events-button:hover',
                '.mkdf-btn.mkdf-btn-outline:not(.mkdf-btn-custom-hover-bg):hover'
            );

            $border_color_selector = array(
                '.mkdf-st-loader .pulse_circles .ball',
                '#tribe-events-content-wrapper .tribe-bar-filters .tribe-events-button',
                '.mkdf-blog-holder.mkdf-blog-split-column article .mkdf-post-info-bottom .mkdf-post-info-bottom-left>div a',
                '.mkdf-blog-holder.mkdf-blog-standard article .mkdf-post-info-bottom .mkdf-post-info-bottom-left>div a',
                '.mkdf-blog-holder.mkdf-blog-single article .mkdf-post-info-bottom .mkdf-post-info-bottom-left>div a',
                '.mkdf-comparision-pricing-tables-holder .mkdf-cpt-table.mkdf-featured-item .mkdf-cpt-table-btn a',
                '.mkdf-comparision-pricing-tables-holder .mkdf-cpt-table .mkdf-cpt-table-btn a:hover',
                '.mkdf-woocommerce-page table.cart td.actions>input:hover',
                '.mkdf-menu-area .mkdf-login-register-widget.mkdf-user-not-logged-in .mkdf-modal-opener.mkdf-login-opener',
                '.mkdf-menu-area .mkdf-login-register-widget.mkdf-user-not-logged-in .mkdf-modal-opener.mkdf-login-opener:hover'
            );

            $border_color_important_selector = array(
                '.mkdf-tribe-events-single .tribe-events-cal-links .tribe-events-button:hover',
                '.mkdf-btn.mkdf-btn-outline:not(.mkdf-btn-custom-border-hover):hover'
            );

            $border_top_color_selector = array(
                '.mkdf-drop-down .narrow .second .inner ul',
                '.mkdf-drop-down .wide .second .inner',
                '.mkdf-slide-from-header-bottom-holder',
                '.mkdf-comparision-pricing-tables-holder .mkdf-cpt-table.mkdf-featured-item',
                '.mkdf-shopping-cart-dropdown',
                '.mkdf-top-bar .mkdf-login-register-widget.mkdf-user-logged-in .mkdf-login-dropdown',
                '.mkdf-menu-area .mkdf-login-register-widget.mkdf-user-logged-in .mkdf-login-dropdown'
            );

            $border_bottom_color_selector = array(
                '.mkdf-woo-single-page .woocommerce-tabs ul.tabs>li.active:after'
            );

            $border_left_color_selector = array(
                '.mkdf-tabs.mkdf-tabs-vertical .mkdf-tabs-nav li.ui-state-active a',
                '.mkdf-tabs.mkdf-tabs-vertical .mkdf-tabs-nav li.ui-state-hover a'
            );

            $background_opacity_slector = array(
                '.mkdf-title-holder'
            );

            $first_main_color_hex = str_replace( '#', '', $first_main_color );
            // Convert shorthand colors to full format, e.g. "FFF" -> "FFFFFF"
            $first_main_color_hex = preg_replace( '~^(.)(.)(.)$~', '$1$1$2$2$3$3', $first_main_color_hex );

            $rgb_first_main_color      = array();
            $rgb_first_main_color['R'] = hexdec( $first_main_color_hex{0} . $first_main_color_hex{1} );
            $rgb_first_main_color['G'] = hexdec( $first_main_color_hex{2} . $first_main_color_hex{3} );
            $rgb_first_main_color['B'] = hexdec( $first_main_color_hex{4} . $first_main_color_hex{5} );
            $dec_first_main_color = implode(',', $rgb_first_main_color);

            echo iacademy_mikado_dynamic_css($color_selector, array('color' => $first_main_color));
	        echo iacademy_mikado_dynamic_css($color_important_selector, array('color' => $first_main_color.'!important'));
	        echo iacademy_mikado_dynamic_css($background_color_selector, array('background-color' => $first_main_color));
            echo iacademy_mikado_dynamic_css($background_color_important_selector, array('background-color' => $first_main_color.'!important'));
	        echo iacademy_mikado_dynamic_css($border_color_selector, array('border-color' => $first_main_color));
            echo iacademy_mikado_dynamic_css($border_color_important_selector, array('border-color' => $first_main_color.'!important'));
            echo iacademy_mikado_dynamic_css($border_top_color_selector, array('border-top-color' => $first_main_color));
            echo iacademy_mikado_dynamic_css($border_bottom_color_selector, array('border-bottom-color' => $first_main_color));
            echo iacademy_mikado_dynamic_css($border_left_color_selector, array('border-left-color' => $first_main_color));

            echo iacademy_mikado_dynamic_css($background_opacity_slector, array('background-color' => 'rgba(' . $dec_first_main_color . ', 0.88)'));
        }

        $additional_fist_main_color = iacademy_mikado_options()->getOptionValue('first_color_additional');

        if(!empty($additional_fist_main_color)) {

            $additional_first_main_color_selector = array(
                '.widget.woocommerce.widget_price_filter .price_slider_amount .button:hover'
            );

            $additional_first_main_background_color_selector = array(
                '#submit_comment:hover',
                '.post-password-form input[type=submit]:hover',
                'input.wpcf7-form-control.wpcf7-submit:hover',
                '#tribe-events-content-wrapper .tribe-bar-filters .tribe-events-button:hover',
                '.mkdf-blog-holder.mkdf-blog-split-column article .mkdf-post-info-bottom .mkdf-post-info-bottom-left>div a:hover',
                '.mkdf-blog-holder.mkdf-blog-standard article .mkdf-post-info-bottom .mkdf-post-info-bottom-left>div a:hover',
                '.mkdf-blog-holder.mkdf-blog-single article .mkdf-post-info-bottom .mkdf-post-info-bottom-left>div a:hover',
                '.mkdf-comparision-pricing-tables-holder .mkdf-cpt-table.mkdf-featured-item .mkdf-cpt-table-btn a:hover',
                '.woocommerce-page .mkdf-content .wc-forward:not(.added_to_cart):not(.checkout-button):hover',
                '.woocommerce-page .mkdf-content a.added_to_cart:hover',
                '.woocommerce-page .mkdf-content a.button:hover',
                '.woocommerce-page .mkdf-content button[type=submit]:not(.mkdf-woo-search-widget-button):hover',
                '.woocommerce-page .mkdf-content input[type=submit]:hover',
                'div.woocommerce .wc-forward:not(.added_to_cart):not(.checkout-button):hover',
                'div.woocommerce a.added_to_cart:hover',
                'div.woocommerce a.button:hover',
                'div.woocommerce button[type=submit]:not(.mkdf-woo-search-widget-button):hover',
                'div.woocommerce input[type=submit]:hover',
                '.mkdf-pl-holder .mkdf-pli-inner .mkdf-pli-text-inner .mkdf-pli-add-to-cart.mkdf-default-skin .added_to_cart:hover',
                '.mkdf-pl-holder .mkdf-pli-inner .mkdf-pli-text-inner .mkdf-pli-add-to-cart.mkdf-default-skin .button:hover',
                '.mkdf-menu-area .mkdf-login-register-widget.mkdf-user-not-logged-in .mkdf-modal-opener.mkdf-register-opener:hover',
            );

            $additional_first_main_background_color_important_selector = array(
                '#bbpress-forums button:hover',
                '.mkdf-btn.mkdf-btn-solid:not(.mkdf-btn-custom-hover-bg):hover'
            );

            $additional_first_main_color_border_selector = array(

                '#tribe-events-content-wrapper .tribe-bar-filters .tribe-events-button:hover',
                '.mkdf-comparision-pricing-tables-holder .mkdf-cpt-table.mkdf-featured-item .mkdf-cpt-table-btn a:hover'
            );

            echo iacademy_mikado_dynamic_css($additional_first_main_color_selector, array('color' => $additional_fist_main_color));
            echo iacademy_mikado_dynamic_css($additional_first_main_background_color_selector, array('background-color' => $additional_fist_main_color));
            echo iacademy_mikado_dynamic_css($additional_first_main_background_color_important_selector, array('background-color' => $additional_fist_main_color.'!important'));
            echo iacademy_mikado_dynamic_css($additional_first_main_color_border_selector, array('border-color' => $additional_fist_main_color));
        }

        $second_main_color = iacademy_mikado_options()->getOptionValue('second_color');

        if(!empty($second_main_color)) {

            $second_color_selector = array(
                '.mkdf-comment-rating-box .mkdf-star-rating.active',
                '.mkdf-course-list-holder.mkdf-cl-minimal article .mkdf-ci-price-holder .mkdf-ci-price-free',
                '.mkdf-course-single-holder .mkdf-course-basic-info-wrapper .mkdf-course-stars i',
                '.mkdf-course-reviews-list-top .mkdf-course-reviews-number',
                '.mkdf-course-reviews-list-top .mkdf-course-stars-wrapper .mkdf-course-stars i',
                '.mkdf-course-reviews-list .mkdf-comment-holder .mkdf-review-rating',
                '.woocommerce .star-rating',
                '.woocommerce .star-rating span',
                '.mkdf-modal-holder .mkdf-modal-content .mkdf-username-label:after',
                ' .mkdf-modal-holder .mkdf-modal-content .mkdf-password-label:after'
            );

            $second_background_color_selector = array(
                '.mkdf-tribe-events-single .mkdf-events-single-main-info .mkdf-events-single-title-holder .mkdf-events-single-cost.mkdf-free',
                '.mkdf-events-list-item-title-holder .mkdf-events-list-item-price.mkdf-free',
                '.mkdf-course-table-holder tbody .mkdf-ct-item .mkdf-tc-course-field .mkdf-ci-price-holder .mkdf-ci-price-free',
                '.mkdf-course-list-holder.mkdf-cl-standard article .mkdf-cli-text-holder .mkdf-cli-top-info .mkdf-ci-price-holder .mkdf-ci-price-free',
                '.mkdf-course-single-holder .mkdf-course-single-type.mkdf-free-course',
                '.woocommerce .mkdf-onsale'
            );

            $second_background_color_important_selector = array(
                '.mkdf-drop-down .wide .second .inner>ul>li.uses-custom-sidebar .widget.mkdf-course-list-widget .mkdf-course-list-holder.mkdf-cl-minimal article .mkdf-cli-text .mkdf-ci-price-holder .mkdf-ci-price-free'
            );

            echo iacademy_mikado_dynamic_css($second_color_selector, array('color' => $second_main_color));
            echo iacademy_mikado_dynamic_css($second_background_color_selector, array('background-color' => $second_main_color));
            echo iacademy_mikado_dynamic_css($second_background_color_important_selector, array('background-color' => $second_main_color.'!important'));
        }
	
	    $page_background_color = iacademy_mikado_options()->getOptionValue( 'page_background_color' );
	    if ( ! empty( $page_background_color ) ) {
		    $background_color_selector = array(
			    '.mkdf-wrapper-inner',
			    '.mkdf-content',
			    '.mkdf-container'
		    );
		    echo iacademy_mikado_dynamic_css( $background_color_selector, array( 'background-color' => $page_background_color ) );
	    }

		$page_background_image = iacademy_mikado_options()->getOptionValue( 'page_background_image' );
	    if ( ! empty( $page_background_image ) ) {
		    $background_image_selector = array(
			    '.mkdf-wrapper'
		    );
			$background_color_selector = array(
				'.mkdf-content',
				'.mkdf-container'
			);

		    echo iacademy_mikado_dynamic_css( $background_image_selector, array( 'background-image' => 'url('. $page_background_image .');' ) );
		    echo iacademy_mikado_dynamic_css( $background_color_selector, array( 'background-color' => 'transparent' ) );

			
	    }
	
	    $selection_color = iacademy_mikado_options()->getOptionValue( 'selection_color' );
	    if ( ! empty( $selection_color ) ) {
		    echo iacademy_mikado_dynamic_css( '::selection', array( 'background' => $selection_color ) );
		    echo iacademy_mikado_dynamic_css( '::-moz-selection', array( 'background' => $selection_color ) );
	    }
	
	    $preload_background_styles = array();
	
	    if ( iacademy_mikado_options()->getOptionValue( 'preload_pattern_image' ) !== "" ) {
		    $preload_background_styles['background-image'] = 'url(' . iacademy_mikado_options()->getOptionValue( 'preload_pattern_image' ) . ') !important';
	    }
	
	    echo iacademy_mikado_dynamic_css( '.mkdf-preload-background', $preload_background_styles );
    }

    add_action('iacademy_mikado_style_dynamic', 'iacademy_mikado_design_styles');
}

if ( ! function_exists( 'iacademy_mikado_content_styles' ) ) {
	function iacademy_mikado_content_styles() {
		$content_style = array();
		
		$padding_top = iacademy_mikado_options()->getOptionValue( 'content_top_padding' );
		if ( $padding_top !== '' ) {
			$content_style['padding-top'] = iacademy_mikado_filter_px( $padding_top ) . 'px';
		}
		
		$content_selector = array(
			'.mkdf-content .mkdf-content-inner > .mkdf-full-width > .mkdf-full-width-inner',
		);
		
		echo iacademy_mikado_dynamic_css( $content_selector, $content_style );
		
		$content_style_in_grid = array();
		
		$padding_top_in_grid = iacademy_mikado_options()->getOptionValue( 'content_top_padding_in_grid' );
		if ( $padding_top_in_grid !== '' ) {
			$content_style_in_grid['padding-top'] = iacademy_mikado_filter_px( $padding_top_in_grid ) . 'px';
		}
		
		$content_selector_in_grid = array(
			'.mkdf-content .mkdf-content-inner > .mkdf-container > .mkdf-container-inner',
		);
		
		echo iacademy_mikado_dynamic_css( $content_selector_in_grid, $content_style_in_grid );
	}
	
	add_action( 'iacademy_mikado_style_dynamic', 'iacademy_mikado_content_styles' );
}

if ( ! function_exists( 'iacademy_mikado_h1_styles' ) ) {
	function iacademy_mikado_h1_styles() {
		$margin_top    = iacademy_mikado_options()->getOptionValue( 'h1_margin_top' );
		$margin_bottom = iacademy_mikado_options()->getOptionValue( 'h1_margin_bottom' );
		
		$item_styles = iacademy_mikado_get_typography_styles( 'h1' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = iacademy_mikado_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = iacademy_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h1'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo iacademy_mikado_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'iacademy_mikado_style_dynamic', 'iacademy_mikado_h1_styles' );
}

if ( ! function_exists( 'iacademy_mikado_h2_styles' ) ) {
	function iacademy_mikado_h2_styles() {
		$margin_top    = iacademy_mikado_options()->getOptionValue( 'h2_margin_top' );
		$margin_bottom = iacademy_mikado_options()->getOptionValue( 'h2_margin_bottom' );
		
		$item_styles = iacademy_mikado_get_typography_styles( 'h2' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = iacademy_mikado_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = iacademy_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h2'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo iacademy_mikado_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'iacademy_mikado_style_dynamic', 'iacademy_mikado_h2_styles' );
}

if ( ! function_exists( 'iacademy_mikado_h3_styles' ) ) {
	function iacademy_mikado_h3_styles() {
		$margin_top    = iacademy_mikado_options()->getOptionValue( 'h3_margin_top' );
		$margin_bottom = iacademy_mikado_options()->getOptionValue( 'h3_margin_bottom' );
		
		$item_styles = iacademy_mikado_get_typography_styles( 'h3' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = iacademy_mikado_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = iacademy_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h3'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo iacademy_mikado_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'iacademy_mikado_style_dynamic', 'iacademy_mikado_h3_styles' );
}

if ( ! function_exists( 'iacademy_mikado_h4_styles' ) ) {
	function iacademy_mikado_h4_styles() {
		$margin_top    = iacademy_mikado_options()->getOptionValue( 'h4_margin_top' );
		$margin_bottom = iacademy_mikado_options()->getOptionValue( 'h4_margin_bottom' );
		
		$item_styles = iacademy_mikado_get_typography_styles( 'h4' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = iacademy_mikado_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = iacademy_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h4'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo iacademy_mikado_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'iacademy_mikado_style_dynamic', 'iacademy_mikado_h4_styles' );
}

if ( ! function_exists( 'iacademy_mikado_h5_styles' ) ) {
	function iacademy_mikado_h5_styles() {
		$margin_top    = iacademy_mikado_options()->getOptionValue( 'h5_margin_top' );
		$margin_bottom = iacademy_mikado_options()->getOptionValue( 'h5_margin_bottom' );
		
		$item_styles = iacademy_mikado_get_typography_styles( 'h5' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = iacademy_mikado_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = iacademy_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h5'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo iacademy_mikado_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'iacademy_mikado_style_dynamic', 'iacademy_mikado_h5_styles' );
}

if ( ! function_exists( 'iacademy_mikado_h6_styles' ) ) {
	function iacademy_mikado_h6_styles() {
		$margin_top    = iacademy_mikado_options()->getOptionValue( 'h6_margin_top' );
		$margin_bottom = iacademy_mikado_options()->getOptionValue( 'h6_margin_bottom' );
		
		$item_styles = iacademy_mikado_get_typography_styles( 'h6' );
		
		if ( $margin_top !== '' ) {
			$item_styles['margin-top'] = iacademy_mikado_filter_px( $margin_top ) . 'px';
		}
		if ( $margin_bottom !== '' ) {
			$item_styles['margin-bottom'] = iacademy_mikado_filter_px( $margin_bottom ) . 'px';
		}
		
		$item_selector = array(
			'h6'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo iacademy_mikado_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'iacademy_mikado_style_dynamic', 'iacademy_mikado_h6_styles' );
}

if ( ! function_exists( 'iacademy_mikado_text_styles' ) ) {
	function iacademy_mikado_text_styles() {
		$item_styles = iacademy_mikado_get_typography_styles( 'text' );
		
		$item_selector = array(
			'p'
		);
		
		if ( ! empty( $item_styles ) ) {
			echo iacademy_mikado_dynamic_css( $item_selector, $item_styles );
		}
	}
	
	add_action( 'iacademy_mikado_style_dynamic', 'iacademy_mikado_text_styles' );
}

if ( ! function_exists( 'iacademy_mikado_link_styles' ) ) {
	function iacademy_mikado_link_styles() {
		$link_styles      = array();
		$link_color       = iacademy_mikado_options()->getOptionValue( 'link_color' );
		$link_font_style  = iacademy_mikado_options()->getOptionValue( 'link_fontstyle' );
		$link_font_weight = iacademy_mikado_options()->getOptionValue( 'link_fontweight' );
		$link_decoration  = iacademy_mikado_options()->getOptionValue( 'link_fontdecoration' );
		
		if ( ! empty( $link_color ) ) {
			$link_styles['color'] = $link_color;
		}
		if ( ! empty( $link_font_style ) ) {
			$link_styles['font-style'] = $link_font_style;
		}
		if ( ! empty( $link_font_weight ) ) {
			$link_styles['font-weight'] = $link_font_weight;
		}
		if ( ! empty( $link_decoration ) ) {
			$link_styles['text-decoration'] = $link_decoration;
		}
		
		$link_selector = array(
			'a',
			'p a'
		);
		
		if ( ! empty( $link_styles ) ) {
			echo iacademy_mikado_dynamic_css( $link_selector, $link_styles );
		}
	}
	
	add_action( 'iacademy_mikado_style_dynamic', 'iacademy_mikado_link_styles' );
}

if ( ! function_exists( 'iacademy_mikado_link_hover_styles' ) ) {
	function iacademy_mikado_link_hover_styles() {
		$link_hover_styles     = array();
		$link_hover_color      = iacademy_mikado_options()->getOptionValue( 'link_hovercolor' );
		$link_hover_decoration = iacademy_mikado_options()->getOptionValue( 'link_hover_fontdecoration' );
		
		if ( ! empty( $link_hover_color ) ) {
			$link_hover_styles['color'] = $link_hover_color;
		}
		if ( ! empty( $link_hover_decoration ) ) {
			$link_hover_styles['text-decoration'] = $link_hover_decoration;
		}
		
		$link_hover_selector = array(
			'a:hover',
			'p a:hover'
		);
		
		if ( ! empty( $link_hover_styles ) ) {
			echo iacademy_mikado_dynamic_css( $link_hover_selector, $link_hover_styles );
		}
		
		$link_heading_hover_styles = array();
		
		if ( ! empty( $link_hover_color ) ) {
			$link_heading_hover_styles['color'] = $link_hover_color;
		}
		
		$link_heading_hover_selector = array(
			'h1 a:hover',
			'h2 a:hover',
			'h3 a:hover',
			'h4 a:hover',
			'h5 a:hover',
			'h6 a:hover'
		);
		
		if ( ! empty( $link_heading_hover_styles ) ) {
			echo iacademy_mikado_dynamic_css( $link_heading_hover_selector, $link_heading_hover_styles );
		}
	}
	
	add_action( 'iacademy_mikado_style_dynamic', 'iacademy_mikado_link_hover_styles' );
}

if ( ! function_exists( 'iacademy_mikado_smooth_page_transition_styles' ) ) {
	function iacademy_mikado_smooth_page_transition_styles( $style ) {
		$id            = iacademy_mikado_get_page_id();
		$loader_style  = array();
		$current_style = '';
		
		$background_color = iacademy_mikado_get_meta_field_intersect( 'smooth_pt_bgnd_color', $id );
		if ( ! empty( $background_color ) ) {
			$loader_style['background-color'] = $background_color;
		}
		
		$loader_selector = array(
			'.mkdf-smooth-transition-loader'
		);
		
		if ( ! empty( $loader_style ) ) {
			$current_style .= iacademy_mikado_dynamic_css( $loader_selector, $loader_style );
		}
		
		$spinner_style = array();
		$spinner_color = iacademy_mikado_get_meta_field_intersect( 'smooth_pt_spinner_color', $id );
		if ( ! empty( $spinner_color ) ) {
			$spinner_style['background-color'] = $spinner_color;
		}
		
		$spinner_selectors = array(
			'.mkdf-st-loader .mkdf-rotate-circles > div',
			'.mkdf-st-loader .pulse',
			'.mkdf-st-loader .double_pulse .double-bounce1',
			'.mkdf-st-loader .double_pulse .double-bounce2',
			'.mkdf-st-loader .cube',
			'.mkdf-st-loader .rotating_cubes .cube1',
			'.mkdf-st-loader .rotating_cubes .cube2',
			'.mkdf-st-loader .stripes > div',
			'.mkdf-st-loader .wave > div',
			'.mkdf-st-loader .two_rotating_circles .dot1',
			'.mkdf-st-loader .two_rotating_circles .dot2',
			'.mkdf-st-loader .five_rotating_circles .container1 > div',
			'.mkdf-st-loader .five_rotating_circles .container2 > div',
			'.mkdf-st-loader .five_rotating_circles .container3 > div',
			'.mkdf-st-loader .atom .ball-1:before',
			'.mkdf-st-loader .atom .ball-2:before',
			'.mkdf-st-loader .atom .ball-3:before',
			'.mkdf-st-loader .atom .ball-4:before',
			'.mkdf-st-loader .clock .ball:before',
			'.mkdf-st-loader .mitosis .ball',
			'.mkdf-st-loader .lines .line1',
			'.mkdf-st-loader .lines .line2',
			'.mkdf-st-loader .lines .line3',
			'.mkdf-st-loader .lines .line4',
			'.mkdf-st-loader .fussion .ball',
			'.mkdf-st-loader .fussion .ball-1',
			'.mkdf-st-loader .fussion .ball-2',
			'.mkdf-st-loader .fussion .ball-3',
			'.mkdf-st-loader .fussion .ball-4',
			'.mkdf-st-loader .wave_circles .ball',
			'.mkdf-st-loader .pulse_circles .ball'
		);
		
		if ( ! empty( $spinner_style ) ) {
			$current_style .= iacademy_mikado_dynamic_css( $spinner_selectors, $spinner_style );
		}
		
		$current_style = $current_style . $style;
		
		return $current_style;
	}
	
	add_filter( 'iacademy_mikado_add_page_custom_style', 'iacademy_mikado_smooth_page_transition_styles' );
}