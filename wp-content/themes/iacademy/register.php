<?php /* Template Name: Register User */  ?>
<?php
  $mkdf_sidebar_layout  = iacademy_mikado_sidebar_layout();
  
  $site_url=do_shortcode('[site_url]');
  $ret_data= do_shortcode('[istsubscripeplan]');
  $subscripe_plan_array=json_decode($ret_data,true);
  //print_r($subscripe_plan_array); die;
  get_header();
  //iacademy_mikado_get_title();
  get_template_part('slider');
  
  //Woocommerce content
  ?>
<style type="text/css">
  .title_lb {
  font-weight: bold;
  }
  .mkdf-cf7-contact-wrapper .mkdf-cf7-inputs{padding:0!important;}
  .mkdf-cf7-contact-wrapper .mkdf-cf7-inputs label{font-size:unset; }
  .wpcf7-form-control.wpcf7-text,.mkdf-cf7-contact-wrapper.mkdf-button-small .wpcf7-form-control.wpcf7-text{margin:0 0 15px!important;}
  .mkdf-cf7-contact-wrapper{border:unset;background-color:unset;}
  .um-field-label label{color:#f ff;}
  .text-white{color:#fff;}
  #ret_div,#form_response_div{color: #e84a4a;}
  .no-referal-code,.have-referal-code{color:skyblue;cursor:pointer;}
  
  
  
  .input-field {
  border: none;
  height: 2.5rem;
  border-radius: 3px;
  font-size: 1.5rem;
  box-sizing: border-box;
  background: #555;
  color: #fff;
  }
  .btn {
  border: 1px solid transparent;
  background: #6f5499;
  color: #fff;
  height: 2.5rem;
  line-height: 2.5rem;
  display: inline-block;
  vertical-align: top;
  padding: 0 1rem;
  -webkit-appearance: none;
  appearance: none;
  border-radius: 3px;
  margin: 0 0 15px!important;
  }
  .btn:disabled {
  cursor: not-allowed;
  pointer-events: none;
  opacity: .6;
  }
   
  .input-group {
  position: relative;
  display: table;
  border-collapse: separate;
  box-sizing: border-box;
  }
  .input-group .input-field {
  display: table-cell;
  width: 100%;
  position: relative;
  float: left;
  z-index: 2;
  background-color: #fff;
  border: 1px solid #ddd;
  color:#000;
  }
  .input-group-btn {
  position: relative;
  white-space: nowrap;
  width: 1%;
  vertical-align: middle;
  }
  .input-group-btn {
  display: table-cell;
  }
  .input-group .input-field:not(:last-child),
  .input-group-btn:not(:last-child) .btn {
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
  }
  .input-group .input-field:not(:first-child),
  .input-group-btn:not(:first-child) .btn {
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
  }
  .um-form input[type=text] {
    margin: 0 0 15px!important;
}
.um-center{padding-top:15px;}
.um-center .um-button{
	width:100%!important;
	
}
</style>
<div class="mkdf-title-holder mkdf-standard-with-breadcrumbs-type mkdf-has-bg-image  " style="height: 250px; background-color: rgb(255, 255, 255); background-image: url(&quot;../wp-content/uploads/2017/05/pages-title-parallax.jpg&quot;); background-position: center 0px;" data-height="250">
  <div class="mkdf-title-image">
    <img itemprop="image" src="../wp-content/uploads/2017/05/pages-title-parallax.jpg" alt="a">
  </div>
  <div class="mkdf-title-wrapper" style="height: 250px">
    <div class="mkdf-title-inner">
      <div class="mkdf-grid">
        <div class="mkdf-title-info">
          <h2 class="mkdf-page-title entry-title">Login</h2>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="mkdf-container">
  <div class="mkdf-container-inner clearfix" style="padding-top: 20px;">
    <div class="mkdf-grid-row">
      <div class="mkdf-grid-col-5" style="margin-top:150px;margin-bottom:20px;box-shadow: 0px 1px 16px 1px #ddd;">
        <div class="login-block" >
          <div style=" padding:1px 0px;">
            <h4 style=" text-align:center;">Registered user? Login here</h4>
            <?php echo do_shortcode( '[ultimatemember form_id="4333"]' ); ?>
          </div>
        </div>
        <br></br>
      </div>
      <div class="mkdf-grid-col-1"></div>
      <div class="mkdf-grid-col-6">
        <div class=" " id="r_ div"  h idden>
          <div class=" " style="   background-color:#fff;padding-bottom:15px;margin-top:50px;box-shadow: 0px 1px 16px 1px #ddd;">
           
           
			
			 <div class="mkdf-cf7-inputs clearfix" style="margin-top:20px;border-bottom:1px solid #f2f1f1;">
					<div class="mkdf-grid-col-6">
					    <h4 class=" " style="text-align:left;"> New user? Register here</h4> 
					
					</div> 
					
					<div class="mkdf-grid-col-6">
					    <h6  style="text-align:right;"> <a href="return:javascript(0);" class="no-referal-code">Don't have referal code? Join directly</a></h6> 
					    <h6  style="text-align:right;"    > <a href="return:javascript(0);" class="have-referal-code" hidden>  Have referal code?  </a></h6> 
                         						
					</div>
					
					 
					 
					</div>
           
            <form class="" role="form" id="ses_form_w" name="ses_form_w" method="post" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>">
              <div class="um-form registration-block" data-mode=""  style="padding:10px 30px;">
                <div class="um-row _um_row_1">
				<div class="referal-block"  >
                  <div class="mkdf-cf7-contact-wrapper mkdf-button-small">
                     <div class="mkdf-cf7-inputs clearfix" >
                       <div class="mkdf-grid-col-6">
			             <label>Referal Code</label>
			             <input type="hidden" name="action" id="action" value="ist_user_register" />
                         <div class="input-group">                
				           <input type="text" name="ref_id" id="ref_id" value=""  size="40" required="required" class="wpcf7-form-control   input-field wpcf7-validates-as-required" aria-required="true" z-index="23" aria-invalid="false" />                
				           <span class="input-group-btn">
                             <button class="btn" type="button" id="c_but" name="c_but">Submit </button>
                           </span>				
                         </div>
			             <span class="spinner"></span>
			             <div id="ret_div"></div>
                        </div>
			            <div class="mkdf-grid-col-6">
                          <label class="title_lb"> Referred By </label>
                          <div class="wpcf7-form-control-wrap your-name">
                           <input type="text" name="ret_txt" id="ret_txt" value=""  readonly   class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                          </div>
                        </div>
			         </div>
					 </div>
					 </div>
                    <div class="mkdf-cf7-inputs clearfix">
                      <div class="mkdf-grid-col-6">
                        <label class="title_lb"> Name<span style="color: #ffd740;">*</span></label>
                        <div class="wpcf7-form-control-wrap your-name">
                          <input type="text" name="first_name" id="first_name" value="" size="40" required="required" class="set_dis wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                        </div>
                      </div>
                      <div class="mkdf-grid-col-6">
                        <label class="title_lb">  Mobile Number<span style="color: #ffd740;">*</span></label>
                        <div class="wpcf7-form-control-wrap your-name">
                          <input
                            type="text"
                            maxlength="10"
                            minlength="10"
                            name="mobile_number"
                            id="mobile_number"
                            value=""
                            size="40"
                            required="required"
                            class="set_dis wpcf7-form-control wpcf7-text wpcf7-validates-as-required decimal mobile_number"
                            aria-required="true"
                            aria-invalid="false"
                            />
                        </div>
                      </div>
                    </div>
                    <div class="mkdf-cf7-inputs clearfix">
                      <div class="mkdf-grid-col-6">
                        <label class="title_lb"> Email ID<span style="color: #ffd740;">*</span></label>
                        <div class="wpcf7-form-control-wrap your-name">
                          <input type="email" name="email_id" id="email_id" value="" size="40" required="required" class="set_dis wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                        </div>
                      </div>
                      <div class="mkdf-grid-col-6">
                        <label class="title_lb">  Password<span style="color: #ffd740;">*</span></label>
                        <div class="wpcf7-form-control-wrap your-name">
                          <input
                            type="Password"
                            minlength="8"
                            name="password_hash"
                            id="password_hash"
                            value=""
                            size="40"
                            required="required"
                            class="set_dis wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                            aria-required="true"
                            aria-invalid="false"
                            />
                        </div>
                      </div>
                    </div>
                    <div class="mkdf-cf7-inputs clearfix">
                      <div class="mkdf-grid-col-6">
                        <label class="title_lb">  Package Type<span style="color: #ffd740;">*</span></label>
                        <div class="wpcf7-form-control-wrap your-name">
                          <select name="subscripe_plan" id="subscripe_plan" class="set_dis wpcf7-form-control wpcf7-text wpcf7-validates-as-required" required="required">
                            <option value="">--Select--</option>
                            <?php 
                              if(isset($subscripe_plan_array) && count($subscripe_plan_array)>0){ 
                                foreach ($subscripe_plan_array as $ky=>$vl) { 
                                  if(array_key_exists('package_name',$vl)){
                                    $opv.='<option value="'.$vl['subscription_id'].'">'.$vl['package_name'].'</option>'; 
                                  }
                                  } echo $opv; } 
                              ?>
                          </select>
                        </div>
                      </div>
                      <div class="mkdf-grid-col-6">
                        <label class="title_lb"> Aadhar Number<span style="color: #ffd740;">*</span></label>
                        <div class="set_dis wpcf7-form-control-wrap your-name">
                          <input
                            type="text"
                            minlength="12"
                            data-type="adhaar-number"
                            maxlength="12"
                            name="aadhar_number"
                            id="aadhar_number"
                            value=""
                            size="40"
                            required="required"
                            class="set_dis wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                            aria-required="true"
                            aria-invalid="false"
                            />
                        </div>
                      </div>
                    </div>
                    <div class="mkdf-cf7-inputs clearfix">
                      <div class="mkdf-grid-col-12">
                        <label class="title_lb">  Address<span style="color: #ffd740;">*</span></label>
                        <div class="wpcf7-form-control-wrap your-name">
                          <textarea name="address" id="address" value="" size="40" rows="2" required="required" class="set_dis wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="mkdf-cf7-inputs clearfix">
                      <div class=" mkdf-grid-col-6">
                        <label class="title_lb">  Bank Name</label>
                        <div class="wpcf7-form-control-wrap your-name">
                          <input type="text" name="bank_name" id="bank_name" value="" size="40"  class="set_dis wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                        </div>
                      </div>
                      <div class=" mkdf-grid-col-6">
                        <label class="title_lb"> Bank Acc No</label>
                        <div class="wpcf7-form-control-wrap your-name">
                          <input type="text" name="bank_account_no" id="bank_account_no" value="" size="40"  class="set_dis wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                        </div>
                      </div>
                      </div>
					  <div class="mkdf-cf7-inputs clearfix">
                      <div class="mkdf-grid-col-6">
                        <label class="title_lb">  IFSC Number</label>
                        <div class="wpcf7-form-control-wrap your-name">
                          <input type="text" name="ifsc" id="ifsc" value="" size="40" class="set_dis wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                        </div>
                      </div>
                      <div class="mkdf-grid-col-6">
                        <label class="title_lb"> Branch Name</label>
                        <div class="wpcf7-form-control-wrap your-name">
                          <input type="text" name="branch" id="branch" value="" size="40" class="set_dis wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                        </div>
                      </div>
                    </div>
                  </div>
                 <div class="mkdf-cf7-inputs clearfix">
                  <div class="mkdf-grid-col-12">
                    <div id="form_response_div"></div>
                    <div class="um-col-alt">
                      <div class="um-full  ">
                        <button type="submit" name="r_submit" id="r_submit" value="Register" style="width:100%" class="mkdf-btn mkdf-btn-small mkdf-btn-solid pull-right" id="um-submit-btn">Register <span class="spinner"></span></button>
                      </div>
                      <div class="um-clear"></div>
                    </div>
                  </div>
                </div>
			   </div>
                
              </div>
          </div>
        </div>
      </div>
    </div>
    <br />
    <br />
    </form>
  </div>
</div>
</div>
</div>
<?php get_footer(); ?>