<?php

if ( ! function_exists( 'iacademy_mikado_map_general_meta' ) ) {
	function iacademy_mikado_map_general_meta() {
		
		$general_meta_box = iacademy_mikado_create_meta_box(
			array(
				'scope' => apply_filters( 'iacademy_mikado_set_scope_for_meta_boxes', array( 'page', 'post' ) ),
				'title' => esc_html__( 'General', 'iacademy' ),
				'name'  => 'general_meta'
			)
		);
		
		/***************** Slider Layout - begin **********************/
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_page_slider_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Slider Shortcode', 'iacademy' ),
				'description' => esc_html__( 'Paste your slider shortcode here', 'iacademy' ),
				'parent'      => $general_meta_box
			)
		);
		
		/***************** Slider Layout - begin **********************/
		
		/***************** Content Layout - begin **********************/
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'          => 'mkdf_page_content_behind_header_meta',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Always put content behind header', 'iacademy' ),
				'description'   => esc_html__( 'Enabling this option will put page content behind page header', 'iacademy' ),
				'parent'        => $general_meta_box,
				'args'          => array(
					'suffix' => 'px'
				)
			)
		);
		
		$mkdf_content_padding_group = iacademy_mikado_add_admin_group(
			array(
				'name'        => 'content_padding_group',
				'title'       => esc_html__( 'Content Style', 'iacademy' ),
				'description' => esc_html__( 'Define styles for Content area', 'iacademy' ),
				'parent'      => $general_meta_box
			)
		);
		
			$mkdf_content_padding_row = iacademy_mikado_add_admin_row(
				array(
					'name'   => 'mkdf_content_padding_row',
					'next'   => true,
					'parent' => $mkdf_content_padding_group
				)
			);
		
				iacademy_mikado_create_meta_box_field(
					array(
						'name'   => 'mkdf_page_content_top_padding',
						'type'   => 'textsimple',
						'label'  => esc_html__( 'Content Top Padding', 'iacademy' ),
						'parent' => $mkdf_content_padding_row,
						'args'   => array(
							'suffix' => 'px'
						)
					)
				);
				
				iacademy_mikado_create_meta_box_field(
					array(
						'name'    => 'mkdf_page_content_top_padding_mobile',
						'type'    => 'selectsimple',
						'label'   => esc_html__( 'Set this top padding for mobile header', 'iacademy' ),
						'parent'  => $mkdf_content_padding_row,
						'options' => iacademy_mikado_get_yes_no_select_array( false )
					)
				);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_page_background_color_meta',
				'type'        => 'color',
				'label'       => esc_html__( 'Page Background Color', 'iacademy' ),
				'description' => esc_html__( 'Choose background color for page content', 'iacademy' ),
				'parent'      => $general_meta_box
			)
		);

		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_page_background_image_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Page Background Image', 'iacademy' ),
				'description' => esc_html__( 'Choose background image for page content', 'iacademy' ),
				'parent'      => $general_meta_box
			)
		);
		
		/***************** Content Layout - end **********************/
		
		/***************** Boxed Layout - begin **********************/
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'    => 'mkdf_boxed_meta',
				'type'    => 'select',
				'label'   => esc_html__( 'Boxed Layout', 'iacademy' ),
				'parent'  => $general_meta_box,
				'options' => iacademy_mikado_get_yes_no_select_array(),
				'args'    => array(
					'dependence' => true,
					'hide'       => array(
						''    => '#mkdf_boxed_container_meta',
						'no'  => '#mkdf_boxed_container_meta',
						'yes' => ''
					),
					'show'       => array(
						''    => '',
						'no'  => '',
						'yes' => '#mkdf_boxed_container_meta'
					)
				)
			)
		);
		
			$boxed_container_meta = iacademy_mikado_add_admin_container(
				array(
					'parent'          => $general_meta_box,
					'name'            => 'boxed_container_meta',
					'hidden_property' => 'mkdf_boxed_meta',
					'hidden_values'   => array(
						'',
						'no'
					)
				)
			);
		
				iacademy_mikado_create_meta_box_field(
					array(
						'name'        => 'mkdf_page_background_color_in_box_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Page Background Color', 'iacademy' ),
						'description' => esc_html__( 'Choose the page background color outside box', 'iacademy' ),
						'parent'      => $boxed_container_meta
					)
				);
				
				iacademy_mikado_create_meta_box_field(
					array(
						'name'        => 'mkdf_boxed_background_image_meta',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Image', 'iacademy' ),
						'description' => esc_html__( 'Choose an image to be displayed in background', 'iacademy' ),
						'parent'      => $boxed_container_meta
					)
				);
				
				iacademy_mikado_create_meta_box_field(
					array(
						'name'        => 'mkdf_boxed_pattern_background_image_meta',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Pattern', 'iacademy' ),
						'description' => esc_html__( 'Choose an image to be used as background pattern', 'iacademy' ),
						'parent'      => $boxed_container_meta
					)
				);
				
				iacademy_mikado_create_meta_box_field(
					array(
						'name'          => 'mkdf_boxed_background_image_attachment_meta',
						'type'          => 'select',
						'default_value' => 'fixed',
						'label'         => esc_html__( 'Background Image Attachment', 'iacademy' ),
						'description'   => esc_html__( 'Choose background image attachment', 'iacademy' ),
						'parent'        => $boxed_container_meta,
						'options'       => array(
							''       => esc_html__( 'Default', 'iacademy' ),
							'fixed'  => esc_html__( 'Fixed', 'iacademy' ),
							'scroll' => esc_html__( 'Scroll', 'iacademy' )
						)
					)
				);
		
		/***************** Boxed Layout - end **********************/
		
		/***************** Passepartout Layout - begin **********************/
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'          => 'mkdf_paspartu_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Passepartout', 'iacademy' ),
				'description'   => esc_html__( 'Enabling this option will display passepartout around site content', 'iacademy' ),
				'parent'        => $general_meta_box,
				'options'       => iacademy_mikado_get_yes_no_select_array(),
				'args'    => array(
					'dependence'    => true,
					'hide'          => array(
						''    => '#mkdf_mkdf_paspartu_container_meta',
						'no'  => '#mkdf_mkdf_paspartu_container_meta',
						'yes' => ''
					),
					'show'          => array(
						''    => '',
						'no'  => '',
						'yes' => '#mkdf_mkdf_paspartu_container_meta'
					)
				)
			)
		);
		
			$paspartu_container_meta = iacademy_mikado_add_admin_container(
				array(
					'parent'          => $general_meta_box,
					'name'            => 'mkdf_paspartu_container_meta',
					'hidden_property' => 'mkdf_paspartu_meta',
					'hidden_values'   => array(
						'',
						'no'
					)
				)
			);
		
				iacademy_mikado_create_meta_box_field(
					array(
						'name'        => 'mkdf_paspartu_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Passepartout Color', 'iacademy' ),
						'description' => esc_html__( 'Choose passepartout color, default value is #ffffff', 'iacademy' ),
						'parent'      => $paspartu_container_meta
					)
				);
				
				iacademy_mikado_create_meta_box_field(
					array(
						'name'        => 'mkdf_paspartu_width_meta',
						'type'        => 'text',
						'label'       => esc_html__( 'Passepartout Size', 'iacademy' ),
						'description' => esc_html__( 'Enter size amount for passepartout', 'iacademy' ),
						'parent'      => $paspartu_container_meta,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px or %'
						)
					)
				);
				
				iacademy_mikado_create_meta_box_field(
					array(
						'parent'        => $paspartu_container_meta,
						'type'          => 'select',
						'default_value' => '',
						'name'          => 'mkdf_disable_top_paspartu_meta',
						'label'         => esc_html__( 'Disable Top Passepartout', 'iacademy' ),
						'options'       => iacademy_mikado_get_yes_no_select_array(),
					)
				);
		
		/***************** Passepartout Layout - end **********************/
		
		/***************** Content Width Layout - begin **********************/
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'          => 'mkdf_initial_content_width_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Initial Width of Content', 'iacademy' ),
				'description'   => esc_html__( 'Choose the initial width of content which is in grid (Applies to pages set to "Default Template" and rows set to "In Grid")', 'iacademy' ),
				'parent'        => $general_meta_box,
				'options'       => array(
					''                => esc_html__( 'Default', 'iacademy' ),
					'mkdf-grid-1100' => esc_html__( '1100px', 'iacademy' ),
					'mkdf-grid-1300' => esc_html__( '1300px', 'iacademy' ),
					'mkdf-grid-1200' => esc_html__( '1200px', 'iacademy' ),
					'mkdf-grid-1000' => esc_html__( '1000px', 'iacademy' ),
					'mkdf-grid-800'  => esc_html__( '800px', 'iacademy' )
				)
			)
		);
		
		/***************** Content Width Layout - end **********************/
		
		/***************** Smooth Page Transitions Layout - begin **********************/
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'          => 'mkdf_smooth_page_transitions_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Smooth Page Transitions', 'iacademy' ),
				'description'   => esc_html__( 'Enabling this option will perform a smooth transition between pages when clicking on links', 'iacademy' ),
				'parent'        => $general_meta_box,
				'options'       => iacademy_mikado_get_yes_no_select_array(),
				'args'          => array(
					'dependence' => true,
					'hide'       => array(
						''    => '#mkdf_page_transitions_container_meta',
						'no'  => '#mkdf_page_transitions_container_meta',
						'yes' => ''
					),
					'show'       => array(
						''    => '',
						'no'  => '',
						'yes' => '#mkdf_page_transitions_container_meta'
					)
				)
			)
		);
		
			$page_transitions_container_meta = iacademy_mikado_add_admin_container(
				array(
					'parent'          => $general_meta_box,
					'name'            => 'page_transitions_container_meta',
					'hidden_property' => 'mkdf_smooth_page_transitions_meta',
					'hidden_values'   => array(
						'',
						'no'
					)
				)
			);
		
				iacademy_mikado_create_meta_box_field(
					array(
						'name'        => 'mkdf_page_transition_preloader_meta',
						'type'        => 'select',
						'label'       => esc_html__( 'Enable Preloading Animation', 'iacademy' ),
						'description' => esc_html__( 'Enabling this option will display an animated preloader while the page content is loading', 'iacademy' ),
						'parent'      => $page_transitions_container_meta,
						'options'     => iacademy_mikado_get_yes_no_select_array(),
						'args'        => array(
							'dependence' => true,
							'hide'       => array(
								''    => '#mkdf_page_transition_preloader_container_meta',
								'no'  => '#mkdf_page_transition_preloader_container_meta',
								'yes' => ''
							),
							'show'       => array(
								''    => '',
								'no'  => '',
								'yes' => '#mkdf_page_transition_preloader_container_meta'
							)
						)
					)
				);
				
				$page_transition_preloader_container_meta = iacademy_mikado_add_admin_container(
					array(
						'parent'          => $page_transitions_container_meta,
						'name'            => 'page_transition_preloader_container_meta',
						'hidden_property' => 'mkdf_page_transition_preloader_meta',
						'hidden_values'   => array(
							'',
							'no'
						)
					)
				);
				
					iacademy_mikado_create_meta_box_field(
						array(
							'name'   => 'mkdf_smooth_pt_bgnd_color_meta',
							'type'   => 'color',
							'label'  => esc_html__( 'Page Loader Background Color', 'iacademy' ),
							'parent' => $page_transition_preloader_container_meta
						)
					);
					
					$group_pt_spinner_animation_meta = iacademy_mikado_add_admin_group(
						array(
							'name'        => 'group_pt_spinner_animation_meta',
							'title'       => esc_html__( 'Loader Style', 'iacademy' ),
							'description' => esc_html__( 'Define styles for loader spinner animation', 'iacademy' ),
							'parent'      => $page_transition_preloader_container_meta
						)
					);
					
					$row_pt_spinner_animation_meta = iacademy_mikado_add_admin_row(
						array(
							'name'   => 'row_pt_spinner_animation_meta',
							'parent' => $group_pt_spinner_animation_meta
						)
					);
					
					iacademy_mikado_create_meta_box_field(
						array(
							'type'    => 'selectsimple',
							'name'    => 'mkdf_smooth_pt_spinner_type_meta',
							'label'   => esc_html__( 'Spinner Type', 'iacademy' ),
							'parent'  => $row_pt_spinner_animation_meta,
							'options' => array(
								''                      => esc_html__( 'Default', 'iacademy' ),
								'rotate_circles'        => esc_html__( 'Rotate Circles', 'iacademy' ),
								'pulse'                 => esc_html__( 'Pulse', 'iacademy' ),
								'double_pulse'          => esc_html__( 'Double Pulse', 'iacademy' ),
								'cube'                  => esc_html__( 'Cube', 'iacademy' ),
								'rotating_cubes'        => esc_html__( 'Rotating Cubes', 'iacademy' ),
								'stripes'               => esc_html__( 'Stripes', 'iacademy' ),
								'wave'                  => esc_html__( 'Wave', 'iacademy' ),
								'two_rotating_circles'  => esc_html__( '2 Rotating Circles', 'iacademy' ),
								'five_rotating_circles' => esc_html__( '5 Rotating Circles', 'iacademy' ),
								'atom'                  => esc_html__( 'Atom', 'iacademy' ),
								'clock'                 => esc_html__( 'Clock', 'iacademy' ),
								'mitosis'               => esc_html__( 'Mitosis', 'iacademy' ),
								'lines'                 => esc_html__( 'Lines', 'iacademy' ),
								'fussion'               => esc_html__( 'Fussion', 'iacademy' ),
								'wave_circles'          => esc_html__( 'Wave Circles', 'iacademy' ),
								'pulse_circles'         => esc_html__( 'Pulse Circles', 'iacademy' )
							)
						)
					);
					
					iacademy_mikado_create_meta_box_field(
						array(
							'type'   => 'colorsimple',
							'name'   => 'mkdf_smooth_pt_spinner_color_meta',
							'label'  => esc_html__( 'Spinner Color', 'iacademy' ),
							'parent' => $row_pt_spinner_animation_meta
						)
					);
					
					iacademy_mikado_create_meta_box_field(
						array(
							'name'        => 'mkdf_page_transition_fadeout_meta',
							'type'        => 'select',
							'label'       => esc_html__( 'Enable Fade Out Animation', 'iacademy' ),
							'description' => esc_html__( 'Enabling this option will turn on fade out animation when leaving page', 'iacademy' ),
							'options'     => iacademy_mikado_get_yes_no_select_array(),
							'parent'      => $page_transitions_container_meta
						
						)
					);
		
		/***************** Smooth Page Transitions Layout - end **********************/
		
		/***************** Comments Layout - begin **********************/
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_page_comments_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Show Comments', 'iacademy' ),
				'description' => esc_html__( 'Enabling this option will show comments on your page', 'iacademy' ),
				'parent'      => $general_meta_box,
				'options'     => iacademy_mikado_get_yes_no_select_array()
			)
		);
		
		/***************** Comments Layout - end **********************/
	}
	
	add_action( 'iacademy_mikado_meta_boxes_map', 'iacademy_mikado_map_general_meta', 10 );
}