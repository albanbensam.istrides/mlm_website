<?php

if ( ! function_exists( 'iacademy_mikado_reset_options_map' ) ) {
	/**
	 * Reset options panel
	 */
	function iacademy_mikado_reset_options_map() {
		
		iacademy_mikado_add_admin_page(
			array(
				'slug'  => '_reset_page',
				'title' => esc_html__( 'Reset', 'iacademy' ),
				'icon'  => 'fa fa-retweet'
			)
		);
		
		$panel_reset = iacademy_mikado_add_admin_panel(
			array(
				'page'  => '_reset_page',
				'name'  => 'panel_reset',
				'title' => esc_html__( 'Reset', 'iacademy' )
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'reset_to_defaults',
				'default_value' => 'no',
				'label'         => esc_html__( 'Reset to Defaults', 'iacademy' ),
				'description'   => esc_html__( 'This option will reset all Select Options values to defaults', 'iacademy' ),
				'parent'        => $panel_reset
			)
		);
	}
	
	add_action( 'iacademy_mikado_options_map', 'iacademy_mikado_reset_options_map', 100 );
}