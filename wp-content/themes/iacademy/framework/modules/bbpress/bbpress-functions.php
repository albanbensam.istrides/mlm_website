<?php

if( ! function_exists( 'iacademy_mikado_override_bbpress_breadcrumbs_home_link' ) ) {
    function iacademy_mikado_override_bbpress_breadcrumbs_home_link($use_home_link) {
        if(function_exists(('is_bbpress')) && is_bbpress()) {
            $use_home_link = false;
        }

        return $use_home_link;
    }
    add_filter('iacademy_mikado_breadcrumbs_title_use_home_link', 'iacademy_mikado_override_bbpress_breadcrumbs_home_link');
}

if( ! function_exists( 'iacademy_mikado_override_bbpress_breadcrumbs' ) ) {

    function iacademy_mikado_override_bbpress_breadcrumbs($childContent, $delimiter, $before, $after) {
        if(function_exists(('is_bbpress')) && is_bbpress()) {

            $childContent = bbp_get_breadcrumb(
                array (
                    'sep' => '&nbsp; / &nbsp;'
                )
            );
        }

        return $childContent;
    }

    add_filter('iacademy_mikado_breadcrumbs_title_child_output', 'iacademy_mikado_override_bbpress_breadcrumbs', 10, 4);
}