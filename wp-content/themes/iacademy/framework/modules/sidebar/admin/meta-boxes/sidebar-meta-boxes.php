<?php

if ( ! function_exists( 'iacademy_mikado_map_sidebar_meta' ) ) {
	function iacademy_mikado_map_sidebar_meta() {
		$mkdf_sidebar_meta_box = iacademy_mikado_create_meta_box(
			array(
				'scope' => apply_filters( 'iacademy_mikado_set_scope_for_meta_boxes', array( 'page' ) ),
				'title' => esc_html__( 'Sidebar', 'iacademy' ),
				'name'  => 'sidebar_meta'
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_sidebar_layout_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Layout', 'iacademy' ),
				'description' => esc_html__( 'Choose the sidebar layout', 'iacademy' ),
				'parent'      => $mkdf_sidebar_meta_box,
				'options'     => array(
					''                 => esc_html__( 'Default', 'iacademy' ),
					'no-sidebar'       => esc_html__( 'No Sidebar', 'iacademy' ),
					'sidebar-33-right' => esc_html__( 'Sidebar 1/3 Right', 'iacademy' ),
					'sidebar-25-right' => esc_html__( 'Sidebar 1/4 Right', 'iacademy' ),
					'sidebar-33-left'  => esc_html__( 'Sidebar 1/3 Left', 'iacademy' ),
					'sidebar-25-left'  => esc_html__( 'Sidebar 1/4 Left', 'iacademy' )
				)
			)
		);
		
		$mkdf_custom_sidebars = iacademy_mikado_get_custom_sidebars();
		if ( count( $mkdf_custom_sidebars ) > 0 ) {
			iacademy_mikado_create_meta_box_field(
				array(
					'name'        => 'mkdf_custom_sidebar_area_meta',
					'type'        => 'selectblank',
					'label'       => esc_html__( 'Choose Widget Area in Sidebar', 'iacademy' ),
					'description' => esc_html__( 'Choose Custom Widget area to display in Sidebar"', 'iacademy' ),
					'parent'      => $mkdf_sidebar_meta_box,
					'options'     => $mkdf_custom_sidebars,
					'args'        => array(
						'select2'	=> true
					)
				)
			);
		}
	}
	
	add_action( 'iacademy_mikado_meta_boxes_map', 'iacademy_mikado_map_sidebar_meta', 31 );
}