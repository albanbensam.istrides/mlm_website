<?php

if ( ! function_exists('iacademy_mikado_sidebar_options_map') ) {

	function iacademy_mikado_sidebar_options_map() {


		$sidebar_panel = iacademy_mikado_add_admin_panel(
			array(
				'title' => esc_html__('Sidebar Area', 'iacademy'),
				'name' => 'sidebar',
				'page' => '_page_page'
			)
		);
		
		iacademy_mikado_add_admin_field(array(
			'name'          => 'sidebar_layout',
			'type'          => 'select',
			'label'         => esc_html__('Sidebar Layout', 'iacademy'),
			'description'   => esc_html__('Choose a sidebar layout for pages', 'iacademy'),
			'parent'        => $sidebar_panel,
			'default_value' => 'no-sidebar',
			'options'       => array(
				'no-sidebar'        => esc_html__('No Sidebar', 'iacademy'),
				'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'iacademy'),
				'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'iacademy'),
				'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'iacademy'),
				'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'iacademy')
			)
		));
		
		$iacademy_custom_sidebars = iacademy_mikado_get_custom_sidebars();
		if(count($iacademy_custom_sidebars) > 0) {
			iacademy_mikado_add_admin_field(array(
				'name' => 'custom_sidebar_area',
				'type' => 'selectblank',
				'label' => esc_html__('Sidebar to Display', 'iacademy'),
				'description' => esc_html__('Choose a sidebar to display on pages. Default sidebar is "Sidebar"', 'iacademy'),
				'parent' => $sidebar_panel,
				'options' => $iacademy_custom_sidebars,
				'args'        => array(
					'select2'	=> true
				)
			));
		}
	}

	add_action('iacademy_mikado_page_sidebar_options_map', 'iacademy_mikado_sidebar_options_map', 9);
}