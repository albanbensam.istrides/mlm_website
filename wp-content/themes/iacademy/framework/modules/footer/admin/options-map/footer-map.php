<?php

if ( ! function_exists( 'iacademy_mikado_footer_options_map' ) ) {
	function iacademy_mikado_footer_options_map() {
		
		iacademy_mikado_add_admin_page(
			array(
				'slug'  => '_footer_page',
				'title' => esc_html__( 'Footer', 'iacademy' ),
				'icon'  => 'fa fa-sort-amount-asc'
			)
		);
		
		$footer_panel = iacademy_mikado_add_admin_panel(
			array(
				'title' => esc_html__( 'Footer', 'iacademy' ),
				'name'  => 'footer',
				'page'  => '_footer_page'
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'footer_in_grid',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Footer in Grid', 'iacademy' ),
				'description'   => esc_html__( 'Enabling this option will place Footer content in grid', 'iacademy' ),
				'parent'        => $footer_panel,
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'show_footer_top',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Show Footer Top', 'iacademy' ),
				'description'   => esc_html__( 'Enabling this option will show Footer Top area', 'iacademy' ),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#mkdf_show_footer_top_container'
				),
				'parent'        => $footer_panel,
			)
		);
		
		$show_footer_top_container = iacademy_mikado_add_admin_container(
			array(
				'name'            => 'show_footer_top_container',
				'hidden_property' => 'show_footer_top',
				'hidden_value'    => 'no',
				'parent'          => $footer_panel
			)
		);

        iacademy_mikado_add_admin_field(
            array(
                'type'          => 'select',
                'name'          => 'footer_top_skin',
                'default_value' => 'dark',
                'label'         => esc_html__('Footer Top Skin', 'iacademy'),
                'description'   => esc_html__('Choose a footer top style to make footer top widgets in that predefined style', 'iacademy'),
                'options'       => array(
                    'standard' => esc_html__('Default', 'iacademy'),
                    'dark'     => esc_html__('Dark', 'iacademy'),
                    'light'    => esc_html__('Light', 'iacademy'),
                ),
                'parent'        => $show_footer_top_container,
            )
        );
		
		iacademy_mikado_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'footer_top_columns',
				'parent'        => $show_footer_top_container,
				'default_value' => '4',
				'label'         => esc_html__( 'Footer Top Columns', 'iacademy' ),
				'description'   => esc_html__( 'Choose number of columns for Footer Top area', 'iacademy' ),
				'options'       => array(
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4'
				)
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'footer_top_columns_alignment',
				'default_value' => 'left',
				'label'         => esc_html__( 'Footer Top Columns Alignment', 'iacademy' ),
				'description'   => esc_html__( 'Text Alignment in Footer Columns', 'iacademy' ),
				'options'       => array(
					''       => esc_html__( 'Default', 'iacademy' ),
					'left'   => esc_html__( 'Left', 'iacademy' ),
					'center' => esc_html__( 'Center', 'iacademy' ),
					'right'  => esc_html__( 'Right', 'iacademy' )
				),
				'parent'        => $show_footer_top_container,
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'name'        => 'footer_top_background_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Background Color', 'iacademy' ),
				'description' => esc_html__( 'Set background color for top footer area', 'iacademy' ),
				'parent'      => $show_footer_top_container
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'show_footer_bottom',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Show Footer Bottom', 'iacademy' ),
				'description'   => esc_html__( 'Enabling this option will show Footer Bottom area', 'iacademy' ),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#mkdf_show_footer_bottom_container'
				),
				'parent'        => $footer_panel,
			)
		);
		
		$show_footer_bottom_container = iacademy_mikado_add_admin_container(
			array(
				'name'            => 'show_footer_bottom_container',
				'hidden_property' => 'show_footer_bottom',
				'hidden_value'    => 'no',
				'parent'          => $footer_panel
			)
		);

        iacademy_mikado_add_admin_field(
            array(
                'type'          => 'select',
                'name'          => 'footer_bottom_skin',
                'default_value' => 'dark',
                'label'         => esc_html__('Footer Bottom Skin', 'iacademy'),
                'description'   => esc_html__('Choose a footer bottom style to make footer bottom widgets in that predefined style', 'iacademy'),
                'options'       => array(
                    'standard' => esc_html__('Default', 'iacademy'),
                    'dark'     => esc_html__('Dark', 'iacademy'),
                    'light'    => esc_html__('Light', 'iacademy'),
                ),
                'parent'        => $show_footer_bottom_container,
            )
        );
		
		iacademy_mikado_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'footer_bottom_columns',
				'default_value' => '4',
				'label'         => esc_html__( 'Footer Bottom Columns', 'iacademy' ),
				'description'   => esc_html__( 'Choose number of columns for Footer Bottom area', 'iacademy' ),
				'options'       => array(
					'1' => '1',
					'2' => '2',
					'3' => '3'
				),
				'parent'        => $show_footer_bottom_container,
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'name'        => 'footer_bottom_background_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Background Color', 'iacademy' ),
				'description' => esc_html__( 'Set background color for bottom footer area', 'iacademy' ),
				'parent'      => $show_footer_bottom_container
			)
		);

        iacademy_mikado_add_admin_field(
            array(
                'type'          => 'yesno',
                'name'          => 'footer_transparency',
                'default_value' => 'no',
                'label'         => esc_html__( 'Transparent Footer?', 'iacademy' ),
                'description'   => esc_html__( 'Enabling this option will make footer background transparent', 'iacademy' ),
                'parent'        => $footer_panel,
            )
        );
	}
	
	add_action( 'iacademy_mikado_options_map', 'iacademy_mikado_footer_options_map', 11 );
}