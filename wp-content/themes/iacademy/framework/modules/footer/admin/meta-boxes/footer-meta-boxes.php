<?php

if ( ! function_exists( 'iacademy_mikado_map_footer_meta' ) ) {
	function iacademy_mikado_map_footer_meta() {

		$mkdf_custom_widgets = iacademy_mikado_get_custom_sidebars();

		$footer_meta_box = iacademy_mikado_create_meta_box(
			array(
				'scope' => apply_filters( 'iacademy_mikado_set_scope_for_meta_boxes', array( 'page', 'post' ) ),
				'title' => esc_html__( 'Footer', 'iacademy' ),
				'name'  => 'footer_meta'
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'          => 'mkdf_disable_footer_meta',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Disable Footer for this Page', 'iacademy' ),
				'description'   => esc_html__( 'Enabling this option will hide footer on this page', 'iacademy' ),
				'parent'        => $footer_meta_box
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'          => 'mkdf_show_footer_top_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Footer Top', 'iacademy' ),
				'description'   => esc_html__( 'Enabling this option will show Footer Top area', 'iacademy' ),
				'parent'        => $footer_meta_box,
				'options'       => iacademy_mikado_get_yes_no_select_array()
			)
		);

        iacademy_mikado_create_meta_box_field(
            array(
                'type'          => 'select',
                'name'          => 'mkdf_footer_top_skin_meta',
                'default_value' => '',
                'label'         => esc_html__('Footer Top Skin', 'iacademy'),
                'description'   => esc_html__('Choose a footer top style to make footer top widgets in that predefined style', 'iacademy'),
                'options'       => array(
                    ''         => '',
                    'light'    => esc_html__('Light', 'iacademy'),
                    'dark'     => esc_html__('Dark', 'iacademy')
                ),
                'parent'        => $footer_meta_box,
            )
        );
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'          => 'mkdf_show_footer_bottom_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Footer Bottom', 'iacademy' ),
				'description'   => esc_html__( 'Enabling this option will show Footer Bottom area', 'iacademy' ),
				'parent'        => $footer_meta_box,
				'options'       => iacademy_mikado_get_yes_no_select_array()
			)
		);

        iacademy_mikado_create_meta_box_field(
            array(
                'type'          => 'select',
                'name'          => 'mkdf_footer_bottom_skin_meta',
                'default_value' => '',
                'label'         => esc_html__('Footer Bottom Skin', 'iacademy'),
                'description'   => esc_html__('Choose a footer bottom style to make footer top widgets in that predefined style', 'iacademy'),
                'options'       => array(
                    ''         => '',
                    'light'    => esc_html__('Light', 'iacademy'),
                    'dark'     => esc_html__('Dark', 'iacademy')
                ),
                'parent'        => $footer_meta_box,
            )
        );

        iacademy_mikado_create_meta_box_field(
            array(
                'type'          => 'select',
                'name'          => 'mkdf_footer_transparency_meta',
                'default_value' => '',
                'label'         => esc_html__( 'Transparent Footer?', 'iacademy' ),
                'description'   => esc_html__( 'Enabling this option will make footer background transparent', 'iacademy' ),
                'options'       => array(
                    ''         => '',
                    'yes'    => esc_html__('Yes', 'iacademy'),
                    'no'     => esc_html__('No', 'iacademy')
                ),
                'parent'        => $footer_meta_box,
            )
        );

		iacademy_mikado_create_meta_box_field(
			array(
				'type'          => 'yesno',
				'name'          => 'show_footer_custom_widget_areas',
				'default_value' => 'no',
				'label'         => esc_html__('Use Custom Widget Areas in Footer', 'iacademy'),
				'args'          => array(
					'dependence'             => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#mkdf_footer_custom_widget_areas'
				),
				'parent'        => $footer_meta_box,
			)
		);

		$show_footer_custom_widget_areas = iacademy_mikado_add_admin_container(
			array(
				'name'            => 'footer_custom_widget_areas',
				'hidden_property' => 'show_footer_custom_widget_areas',
				'hidden_value'    => 'no',
				'parent'          => $footer_meta_box
			)
		);

		$top_cols_num = 4;

		for ($i = 1; $i <= $top_cols_num; $i++) {

			iacademy_mikado_create_meta_box_field(array(
				'name'        => 'mkdf_footer_top_meta_' . $i,
				'type'        => 'selectblank',
				'label'       => esc_html__('Choose Widget Area in Footer Top Column ', 'iacademy') . $i,
				'description' => esc_html__('Choose Custom Widget area to display in Footer Top Column ', 'iacademy') . $i,
				'parent'      => $show_footer_custom_widget_areas,
				'options'     => $mkdf_custom_widgets
			));

		}
	}
	
	add_action( 'iacademy_mikado_meta_boxes_map', 'iacademy_mikado_map_footer_meta', 70 );
}