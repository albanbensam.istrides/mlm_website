<?php

if ( ! function_exists( 'iacademy_mikado_breadcrumbs_title_type_options_meta_boxes' ) ) {
	function iacademy_mikado_breadcrumbs_title_type_options_meta_boxes( $show_title_area_meta_container ) {
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_breadcrumbs_color_meta',
				'type'        => 'color',
				'label'       => esc_html__( 'Breadcrumbs Color', 'iacademy' ),
				'description' => esc_html__( 'Choose a color for breadcrumbs text', 'iacademy' ),
				'parent'      => $show_title_area_meta_container
			)
		);
	}
	
	add_action( 'iacademy_mikado_additional_title_area_meta_boxes', 'iacademy_mikado_breadcrumbs_title_type_options_meta_boxes' );
}