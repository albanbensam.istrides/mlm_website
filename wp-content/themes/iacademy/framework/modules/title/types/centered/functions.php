<?php

if ( ! function_exists( 'iacademy_mikado_set_title_centered_type_for_options' ) ) {
	/**
	 * This function set centered title type value for title options map and meta boxes
	 */
	function iacademy_mikado_set_title_centered_type_for_options( $type ) {
		$type['centered'] = esc_html__( 'Centered', 'iacademy' );
		
		return $type;
	}
	
	add_filter( 'iacademy_mikado_title_type_global_option', 'iacademy_mikado_set_title_centered_type_for_options' );
	add_filter( 'iacademy_mikado_title_type_meta_boxes', 'iacademy_mikado_set_title_centered_type_for_options' );
}