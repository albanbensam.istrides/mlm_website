<?php

if ( ! function_exists( 'iacademy_mikado_set_title_standard_type_for_options' ) ) {
	/**
	 * This function set standard title type value for title options map and meta boxes
	 */
	function iacademy_mikado_set_title_standard_type_for_options( $type ) {
		$type['standard'] = esc_html__( 'Standard', 'iacademy' );
		
		return $type;
	}
	
	add_filter( 'iacademy_mikado_title_type_global_option', 'iacademy_mikado_set_title_standard_type_for_options' );
	add_filter( 'iacademy_mikado_title_type_meta_boxes', 'iacademy_mikado_set_title_standard_type_for_options' );
}

if ( ! function_exists( 'iacademy_mikado_set_title_standard_type_for_options' ) ) {
	/**
	 * This function set default title type value for global title option map
	 */
	function iacademy_mikado_set_title_standard_type_for_options( $type ) {
		$type = 'standard';
		
		return $type;
	}
	
	add_filter( 'iacademy_mikado_default_title_type_global_option', 'iacademy_mikado_set_title_standard_type_for_options' );
}