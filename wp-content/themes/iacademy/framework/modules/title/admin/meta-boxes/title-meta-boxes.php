<?php

if ( ! function_exists( 'iacademy_mikado_get_title_types_meta_boxes' ) ) {
	function iacademy_mikado_get_title_types_meta_boxes() {
		$title_type_options = apply_filters( 'iacademy_mikado_title_type_meta_boxes', $title_type_options = array( '' => esc_html__( 'Default', 'iacademy' ) ) );
		
		return $title_type_options;
	}
}

foreach ( glob( MIKADO_FRAMEWORK_MODULES_ROOT_DIR . '/title/types/*/admin/meta-boxes/*.php' ) as $meta_box_load ) {
	include_once $meta_box_load;
}

if ( ! function_exists( 'iacademy_mikado_map_title_meta' ) ) {
	function iacademy_mikado_map_title_meta() {
		$title_type_meta_boxes = iacademy_mikado_get_title_types_meta_boxes();
		
		$title_meta_box = iacademy_mikado_create_meta_box(
			array(
				'scope' => apply_filters( 'iacademy_mikado_set_scope_for_meta_boxes', array( 'page', 'post' ) ),
				'title' => esc_html__( 'Title', 'iacademy' ),
				'name'  => 'title_meta'
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'          => 'mkdf_show_title_area_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Title Area', 'iacademy' ),
				'description'   => esc_html__( 'Disabling this option will turn off page title area', 'iacademy' ),
				'parent'        => $title_meta_box,
				'options'       => iacademy_mikado_get_yes_no_select_array(),
				'args'          => array(
					'dependence' => true,
					'hide'       => array(
						''    => '',
						'no'  => '#mkdf_mkdf_show_title_area_meta_container',
						'yes' => ''
					),
					'show'       => array(
						''    => '#mkdf_mkdf_show_title_area_meta_container',
						'no'  => '',
						'yes' => '#mkdf_mkdf_show_title_area_meta_container'
					)
				)
			)
		);
		
			$show_title_area_meta_container = iacademy_mikado_add_admin_container(
				array(
					'parent'          => $title_meta_box,
					'name'            => 'mkdf_show_title_area_meta_container',
					'hidden_property' => 'mkdf_show_title_area_meta',
					'hidden_value'    => 'no'
				)
			);
		
				iacademy_mikado_create_meta_box_field(
					array(
						'name'          => 'mkdf_title_area_type_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Title Area Type', 'iacademy' ),
						'description'   => esc_html__( 'Choose title type', 'iacademy' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => $title_type_meta_boxes
					)
				);
		
				iacademy_mikado_create_meta_box_field(
					array(
						'name'        => 'mkdf_title_area_height_meta',
						'type'        => 'text',
						'label'       => esc_html__( 'Height', 'iacademy' ),
						'description' => esc_html__( 'Set a height for Title Area', 'iacademy' ),
						'parent'      => $show_title_area_meta_container,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px'
						)
					)
				);
				
				iacademy_mikado_create_meta_box_field(
					array(
						'name'        => 'mkdf_title_area_background_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Background Color', 'iacademy' ),
						'description' => esc_html__( 'Choose a background color for title area', 'iacademy' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
				iacademy_mikado_create_meta_box_field(
					array(
						'name'        => 'mkdf_title_area_background_image_meta',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Image', 'iacademy' ),
						'description' => esc_html__( 'Choose an Image for title area', 'iacademy' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
				iacademy_mikado_create_meta_box_field(
					array(
						'name'          => 'mkdf_title_area_background_image_behavior_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Background Image Behavior', 'iacademy' ),
						'description'   => esc_html__( 'Choose title area background image behavior', 'iacademy' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => array(
							''                    => esc_html__( 'Default', 'iacademy' ),
							'hide'                => esc_html__( 'Hide Image', 'iacademy' ),
							'responsive'          => esc_html__( 'Enable Responsive Image', 'iacademy' ),
							'responsive-disabled' => esc_html__( 'Disable Responsive Image', 'iacademy' ),
							'parallax'            => esc_html__( 'Enable Parallax Image', 'iacademy' ),
							'parallax-zoom-out'   => esc_html__( 'Enable Parallax With Zoom Out Image', 'iacademy' ),
							'parallax-disabled'   => esc_html__( 'Disable Parallax Image', 'iacademy' )
						)
					)
				);
				
				iacademy_mikado_create_meta_box_field(
					array(
						'name'          => 'mkdf_title_area_vertical_alignment_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Vertical Alignment', 'iacademy' ),
						'description'   => esc_html__( 'Specify title area content vertical alignment', 'iacademy' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => array(
							''              => esc_html__( 'Default', 'iacademy' ),
							'header_bottom' => esc_html__( 'From Bottom of Header', 'iacademy' ),
							'window_top'    => esc_html__( 'From Window Top', 'iacademy' )
						)
					)
				);
				
				iacademy_mikado_create_meta_box_field(
					array(
						'name'          => 'mkdf_title_area_title_tag_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Title Tag', 'iacademy' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => iacademy_mikado_get_title_tag( true )
					)
				);
				
				iacademy_mikado_create_meta_box_field(
					array(
						'name'        => 'mkdf_title_text_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Title Color', 'iacademy' ),
						'description' => esc_html__( 'Choose a color for title text', 'iacademy' ),
						'parent'      => $show_title_area_meta_container
					)
				);
				
				iacademy_mikado_create_meta_box_field(
					array(
						'name'          => 'mkdf_title_area_subtitle_meta',
						'type'          => 'text',
						'default_value' => '',
						'label'         => esc_html__( 'Subtitle Text', 'iacademy' ),
						'description'   => esc_html__( 'Enter your subtitle text', 'iacademy' ),
						'parent'        => $show_title_area_meta_container,
						'args'          => array(
							'col_width' => 6
						)
					)
				);
				
				iacademy_mikado_create_meta_box_field(
					array(
						'name'        => 'mkdf_subtitle_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Subtitle Color', 'iacademy' ),
						'description' => esc_html__( 'Choose a color for subtitle text', 'iacademy' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
		/***************** Additional Title Area Layout - start *****************/
		
		do_action( 'iacademy_mikado_additional_title_area_meta_boxes', $show_title_area_meta_container );
		
		/***************** Additional Title Area Layout - end *****************/
		
	}
	
	add_action( 'iacademy_mikado_meta_boxes_map', 'iacademy_mikado_map_title_meta', 60 );
}