<?php

if(!function_exists('iacademy_mikado_disable_wpml_css')) {
    function iacademy_mikado_disable_wpml_css() {
	    define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
    }

	add_action('after_setup_theme', 'iacademy_mikado_disable_wpml_css');
}