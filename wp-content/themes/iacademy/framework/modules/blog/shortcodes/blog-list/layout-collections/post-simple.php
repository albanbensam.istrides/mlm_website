<li class="mkdf-bl-item clearfix">
	<div class="mkdf-bli-inner">
		<?php if ( $post_info_image == 'yes' ) {
			iacademy_mikado_get_module_template_part( 'templates/parts/image', 'blog', '', $params );
		} ?>
		<div class="mkdf-bli-content">
			<?php iacademy_mikado_get_module_template_part( 'templates/parts/title', 'blog', '', $params ); ?>
			<?php if ($post_info_section == 'yes') { ?>
				<div class="mkdf-bli-excerpt">
					<?php iacademy_mikado_get_module_template_part( 'templates/parts/excerpt', 'blog', '', $params ); ?>
				</div>
				<div class="mkdf-bli-info">
					<?php
					if ( $post_info_date == 'yes' ) {
						iacademy_mikado_get_module_template_part( 'templates/parts/post-info/date', 'blog', '', $params );
					}
					if ( $post_info_comments == 'yes' ) {
						iacademy_mikado_get_module_template_part( 'templates/parts/post-info/comments', 'blog', '', $params );
					}
					?>
				</div>
			<?php } ?>
		</div>
	</div>
</li>