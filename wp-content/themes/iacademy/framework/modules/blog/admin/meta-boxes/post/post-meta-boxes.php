<?php

/*** Post Settings ***/

if ( ! function_exists( 'iacademy_mikado_map_post_meta' ) ) {
	function iacademy_mikado_map_post_meta() {
		
		$post_meta_box = iacademy_mikado_create_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Post', 'iacademy' ),
				'name'  => 'post-meta'
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'          => 'mkdf_blog_single_sidebar_layout_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Sidebar Layout', 'iacademy' ),
				'description'   => esc_html__( 'Choose a sidebar layout for Blog single page', 'iacademy' ),
				'default_value' => '',
				'parent'        => $post_meta_box,
				'options'       => array(
					''                 => esc_html__( 'Default', 'iacademy' ),
					'no-sidebar'       => esc_html__( 'No Sidebar', 'iacademy' ),
					'sidebar-33-right' => esc_html__( 'Sidebar 1/3 Right', 'iacademy' ),
					'sidebar-25-right' => esc_html__( 'Sidebar 1/4 Right', 'iacademy' ),
					'sidebar-33-left'  => esc_html__( 'Sidebar 1/3 Left', 'iacademy' ),
					'sidebar-25-left'  => esc_html__( 'Sidebar 1/4 Left', 'iacademy' )
				)
			)
		);
		
		$iacademy_custom_sidebars = iacademy_mikado_get_custom_sidebars();
		if ( count( $iacademy_custom_sidebars ) > 0 ) {
			iacademy_mikado_create_meta_box_field( array(
				'name'        => 'mkdf_blog_single_custom_sidebar_area_meta',
				'type'        => 'selectblank',
				'label'       => esc_html__( 'Sidebar to Display', 'iacademy' ),
				'description' => esc_html__( 'Choose a sidebar to display on Blog single page. Default sidebar is "Sidebar"', 'iacademy' ),
				'parent'      => $post_meta_box,
				'options'     => iacademy_mikado_get_custom_sidebars(),
				'args' => array(
					'select2' => true
				)
			) );
		}
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_blog_list_featured_image_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Blog List Image', 'iacademy' ),
				'description' => esc_html__( 'Choose an Image for displaying in blog list. If not uploaded, featured image will be shown.', 'iacademy' ),
				'parent'      => $post_meta_box
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'          => 'mkdf_blog_masonry_gallery_fixed_dimensions_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Dimensions for Fixed Proportion', 'iacademy' ),
				'description'   => esc_html__( 'Choose image layout when it appears in Masonry lists in fixed proportion', 'iacademy' ),
				'default_value' => 'default',
				'parent'        => $post_meta_box,
				'options'       => array(
					'default'            => esc_html__( 'Default', 'iacademy' ),
					'large-width'        => esc_html__( 'Large Width', 'iacademy' ),
					'large-height'       => esc_html__( 'Large Height', 'iacademy' ),
					'large-width-height' => esc_html__( 'Large Width/Height', 'iacademy' )
				)
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'          => 'mkdf_blog_masonry_gallery_original_dimensions_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Dimensions for Original Proportion', 'iacademy' ),
				'description'   => esc_html__( 'Choose image layout when it appears in Masonry lists in original proportion', 'iacademy' ),
				'default_value' => 'default',
				'parent'        => $post_meta_box,
				'options'       => array(
					'default'     => esc_html__( 'Default', 'iacademy' ),
					'large-width' => esc_html__( 'Large Width', 'iacademy' )
				)
			)
		);

        iacademy_mikado_create_meta_box_field(
            array(
                'name'            => 'mkdf_standard_post_full_width_featured_image_meta',
                'type'            => 'yesno',
                'label'           => esc_html__( 'Fullwidth Featured Image?','iacademy'),
                'description'     => esc_html__( 'Enabling this option will make featured image fullwidth. This option is applied to standard, quote and link post formats.','iacademy'),
                'parent'          => $post_meta_box,
                'default_value'   => 'yes',
            )
        );
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'          => 'mkdf_show_title_area_blog_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Title Area', 'iacademy' ),
				'description'   => esc_html__( 'Enabling this option will show title area on your single post page', 'iacademy' ),
				'parent'        => $post_meta_box,
				'options'       => iacademy_mikado_get_yes_no_select_array(false, false)
			)
		);
	}
	
	add_action( 'iacademy_mikado_meta_boxes_map', 'iacademy_mikado_map_post_meta', 20 );
}
