<?php

foreach ( glob( MIKADO_FRAMEWORK_MODULES_ROOT_DIR . '/blog/admin/meta-boxes/*/*.php' ) as $meta_box_load ) {
	include_once $meta_box_load;
}

if ( ! function_exists( 'iacademy_mikado_map_blog_meta' ) ) {
	function iacademy_mikado_map_blog_meta() {
		$mkd_blog_categories = array();
		$categories           = get_categories();
		foreach ( $categories as $category ) {
			$mkd_blog_categories[ $category->slug ] = $category->name;
		}
		
		$blog_meta_box = iacademy_mikado_create_meta_box(
			array(
				'scope' => array( 'page' ),
				'title' => esc_html__( 'Blog', 'iacademy' ),
				'name'  => 'blog_meta'
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_blog_category_meta',
				'type'        => 'selectblank',
				'label'       => esc_html__( 'Blog Category', 'iacademy' ),
				'description' => esc_html__( 'Choose category of posts to display (leave empty to display all categories)', 'iacademy' ),
				'parent'      => $blog_meta_box,
				'options'     => $mkd_blog_categories
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_show_posts_per_page_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Number of Posts', 'iacademy' ),
				'description' => esc_html__( 'Enter the number of posts to display', 'iacademy' ),
				'parent'      => $blog_meta_box,
				'options'     => $mkd_blog_categories,
				'args'        => array( "col_width" => 3 )
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_blog_masonry_layout_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Masonry - Layout', 'iacademy' ),
				'description' => esc_html__( 'Set masonry layout. Default is in grid.', 'iacademy' ),
				'parent'      => $blog_meta_box,
				'options'     => array(
					''           => esc_html__( 'Default', 'iacademy' ),
					'in-grid'    => esc_html__( 'In Grid', 'iacademy' ),
					'full-width' => esc_html__( 'Full Width', 'iacademy' )
				)
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_blog_masonry_number_of_columns_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Masonry - Number of Columns', 'iacademy' ),
				'description' => esc_html__( 'Set number of columns for your masonry blog lists', 'iacademy' ),
				'parent'      => $blog_meta_box,
				'options'     => array(
					''      => esc_html__( 'Default', 'iacademy' ),
					'two'   => esc_html__( '2 Columns', 'iacademy' ),
					'three' => esc_html__( '3 Columns', 'iacademy' ),
					'four'  => esc_html__( '4 Columns', 'iacademy' ),
					'five'  => esc_html__( '5 Columns', 'iacademy' )
				)
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_blog_masonry_space_between_items_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Masonry - Space Between Items', 'iacademy' ),
				'description' => esc_html__( 'Set space size between posts for your masonry blog lists', 'iacademy' ),
				'parent'      => $blog_meta_box,
				'options'     => array(
					''       => esc_html__( 'Default', 'iacademy' ),
					'normal' => esc_html__( 'Normal', 'iacademy' ),
					'small'  => esc_html__( 'Small', 'iacademy' ),
					'tiny'   => esc_html__( 'Tiny', 'iacademy' ),
					'no'     => esc_html__( 'No Space', 'iacademy' )
				)
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'          => 'mkdf_blog_list_featured_image_proportion_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Masonry - Featured Image Proportion', 'iacademy' ),
				'description'   => esc_html__( 'Choose type of proportions you want to use for featured images on masonry blog lists', 'iacademy' ),
				'parent'        => $blog_meta_box,
				'default_value' => '',
				'options'       => array(
					''         => esc_html__( 'Default', 'iacademy' ),
					'fixed'    => esc_html__( 'Fixed', 'iacademy' ),
					'original' => esc_html__( 'Original', 'iacademy' )
				)
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'          => 'mkdf_blog_pagination_type_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Pagination Type', 'iacademy' ),
				'description'   => esc_html__( 'Choose a pagination layout for Blog Lists', 'iacademy' ),
				'parent'        => $blog_meta_box,
				'default_value' => '',
				'options'       => array(
					''                => esc_html__( 'Default', 'iacademy' ),
					'standard'        => esc_html__( 'Standard', 'iacademy' ),
					'load-more'       => esc_html__( 'Load More', 'iacademy' ),
					'infinite-scroll' => esc_html__( 'Infinite Scroll', 'iacademy' ),
					'no-pagination'   => esc_html__( 'No Pagination', 'iacademy' )
				)
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'type'          => 'text',
				'name'          => 'mkdf_number_of_chars_meta',
				'default_value' => '',
				'label'         => esc_html__( 'Number of Words in Excerpt', 'iacademy' ),
				'description'   => esc_html__( 'Enter a number of words in excerpt (article summary). Default value is 40', 'iacademy' ),
				'parent'        => $blog_meta_box,
				'args'          => array(
					'col_width' => 3
				)
			)
		);
	}
	
	add_action( 'iacademy_mikado_meta_boxes_map', 'iacademy_mikado_map_blog_meta', 30 );
}