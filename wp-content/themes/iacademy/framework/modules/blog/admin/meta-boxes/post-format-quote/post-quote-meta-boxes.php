<?php

if ( ! function_exists( 'iacademy_mikado_map_post_quote_meta' ) ) {
	function iacademy_mikado_map_post_quote_meta() {
		$quote_post_format_meta_box = iacademy_mikado_create_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Quote Post Format', 'iacademy' ),
				'name'  => 'post_format_quote_meta'
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_post_quote_text_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Quote Text', 'iacademy' ),
				'description' => esc_html__( 'Enter Quote text', 'iacademy' ),
				'parent'      => $quote_post_format_meta_box,
			
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_post_quote_author_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Quote Author', 'iacademy' ),
				'description' => esc_html__( 'Enter Quote author', 'iacademy' ),
				'parent'      => $quote_post_format_meta_box,
			)
		);
	}
	
	add_action( 'iacademy_mikado_meta_boxes_map', 'iacademy_mikado_map_post_quote_meta', 25 );
}