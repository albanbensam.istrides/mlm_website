<?php

if ( ! function_exists( 'iacademy_mikado_map_post_link_meta' ) ) {
	function iacademy_mikado_map_post_link_meta() {
		$link_post_format_meta_box = iacademy_mikado_create_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Link Post Format', 'iacademy' ),
				'name'  => 'post_format_link_meta'
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_post_link_link_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Link', 'iacademy' ),
				'description' => esc_html__( 'Enter link', 'iacademy' ),
				'parent'      => $link_post_format_meta_box,
			
			)
		);
	}
	
	add_action( 'iacademy_mikado_meta_boxes_map', 'iacademy_mikado_map_post_link_meta', 24 );
}