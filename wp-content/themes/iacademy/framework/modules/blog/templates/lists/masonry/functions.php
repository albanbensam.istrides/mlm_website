<?php

if ( ! function_exists( 'iacademy_mikado_register_blog_masonry_template_file' ) ) {
	/**
	 * Function that register blog masonry template
	 */
	function iacademy_mikado_register_blog_masonry_template_file( $templates ) {
		$templates['blog-masonry'] = esc_html__( 'Blog: Masonry', 'iacademy' );
		
		return $templates;
	}
	
	add_filter( 'iacademy_mikado_register_blog_templates', 'iacademy_mikado_register_blog_masonry_template_file' );
}

if ( ! function_exists( 'iacademy_mikado_set_blog_masonry_type_global_option' ) ) {
	/**
	 * Function that set blog list type value for global blog option map
	 */
	function iacademy_mikado_set_blog_masonry_type_global_option( $options ) {
		$options['masonry'] = esc_html__( 'Blog: Masonry', 'iacademy' );
		
		return $options;
	}
	
	add_filter( 'iacademy_mikado_blog_list_type_global_option', 'iacademy_mikado_set_blog_masonry_type_global_option' );
}