<?php

if ( ! function_exists( 'iacademy_mikado_register_blog_split_column_template_file' ) ) {
	/**
	 * Function that register blog split column template
	 */
	function iacademy_mikado_register_blog_split_column_template_file( $templates ) {
		$templates['blog-split-column'] = esc_html__( 'Blog: Split Column', 'iacademy' );
		
		return $templates;
	}
	
	add_filter( 'iacademy_mikado_register_blog_templates', 'iacademy_mikado_register_blog_split_column_template_file' );
}

if ( ! function_exists( 'iacademy_mikado_set_blog_split_column_type_global_option' ) ) {
	/**
	 * Function that set blog list type value for global blog option map
	 */
	function iacademy_mikado_set_blog_split_column_type_global_option( $options ) {
		$options['split-column'] = esc_html__( 'Blog: Split Column', 'iacademy' );
		
		return $options;
	}
	
	add_filter( 'iacademy_mikado_blog_list_type_global_option', 'iacademy_mikado_set_blog_split_column_type_global_option' );
}