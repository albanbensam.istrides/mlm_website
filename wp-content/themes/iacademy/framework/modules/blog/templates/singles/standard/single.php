<?php

iacademy_mikado_get_single_post_format_html($blog_single_type);

iacademy_mikado_get_module_template_part('templates/parts/single/author-info', 'blog');

iacademy_mikado_get_module_template_part('templates/parts/single/single-navigation', 'blog');

iacademy_mikado_get_module_template_part('templates/parts/single/related-posts', 'blog', '', $single_info_params);

iacademy_mikado_get_module_template_part('templates/parts/single/comments', 'blog');