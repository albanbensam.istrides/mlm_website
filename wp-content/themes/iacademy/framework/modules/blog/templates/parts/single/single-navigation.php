<?php
$blog_single_navigation = iacademy_mikado_options()->getOptionValue('blog_single_navigation') === 'no' ? false : true;
$blog_navigation_through_same_category = iacademy_mikado_options()->getOptionValue('blog_navigation_through_same_category') === 'no' ? false : true;
?>
<?php if($blog_single_navigation){ ?>
	<div class="mkdf-blog-single-navigation">
		<div class="mkdf-blog-single-navigation-inner clearfix">
			<?php
				/* Single navigation section - SETTING PARAMS */
				$post_navigation = array(
					'prev' => array(
						'title' => '',
						'image' => '',
						'label' => '<span class="mkdf-blog-single-nav-label">'.esc_html__('Previous', 'iacademy').'</span>'
					),
					'next' => array(
						'title' => '',
						'image' => '',
						'label' => '<span class="mkdf-blog-single-nav-label">'.esc_html__('Next', 'iacademy').'</span>'
					)
				);

			if(get_previous_post() !== ""){
				if($blog_navigation_through_same_category){
					if(get_previous_post(true) !== ""){
						$post_navigation['prev']['post'] = get_previous_post(true);
					}
				} else {
					if(get_previous_post() != ""){
						$post_navigation['prev']['post'] = get_previous_post();
					}
				}

				if (!empty($post_navigation['prev']['post'])) {
					if($post_navigation['prev']['post']->post_title != '') {
						$post_navigation['prev']['title'] = '<span class="mkdf-blog-single-nav-title-text">'.$post_navigation['prev']['post']->post_title.'</span>';
					}
					
					$prev_post_ID = $post_navigation['prev']['post']->ID;
					$prev_background_image_src = wp_get_attachment_image_src(get_post_thumbnail_id($prev_post_ID),'iacademy_mikado_square');
					$prev_post_thumbnail = $prev_background_image_src[0];
					
					if($prev_post_thumbnail != '') {
						$post_navigation['prev']['image'] = '<img class="mkdf-nav-image" src="'.esc_url($prev_post_thumbnail).'" alt="'.$post_navigation['prev']['post']->post_title.'">';
					}
				}
				
			}
			if(get_next_post() != ""){
				if($blog_navigation_through_same_category){
					if(get_next_post(true) !== ""){
						$post_navigation['next']['post'] = get_next_post(true);

					}
				} else {
					if(get_next_post() !== ""){
						$post_navigation['next']['post'] = get_next_post();
					}
				}
			
				if (!empty($post_navigation['next']['post'])) {
					if($post_navigation['next']['post']->post_title != '') {
						$post_navigation['next']['title'] = '<span class="mkdf-blog-single-nav-title-text">'.$post_navigation['next']['post']->post_title.'</span>';
					}
					
					$next_post_ID = $post_navigation['next']['post']->ID;
					$next_background_image_src = wp_get_attachment_image_src(get_post_thumbnail_id($next_post_ID),'iacademy_mikado_square');
					$next_post_thumbnail = $next_background_image_src[0];
					
					if($next_post_thumbnail != '') {
						$post_navigation['next']['image'] = '<img class="mkdf-nav-image" src="'.esc_url($next_post_thumbnail).'" alt="'.$post_navigation['next']['post']->post_title.'">';
					}
				}
				
			}

			if (isset($post_navigation['prev']['post']) || isset($post_navigation['next']['post'])) {
				foreach (array('prev', 'next') as $nav_type) {
					if (isset($post_navigation[$nav_type]['post'])) { ?>
                        <?php $mkdf_has_thumb = isset($post_navigation[$nav_type]['image']) && !empty($post_navigation[$nav_type]['image']); ?>
						<?php $mkdf_nav_class = !$mkdf_has_thumb ? ' mkdf-no-nav-image' : '';  ?>
						<a itemprop="url" class="mkdf-blog-single-<?php echo esc_attr($nav_type); ?> <?php echo esc_attr($mkdf_nav_class); ?>" href="<?php echo get_permalink($post_navigation[$nav_type]['post']->ID); ?>">
                            <?php if($mkdf_has_thumb) { ?>
							<div class="mkdf-nav-blog-post-image">
								<?php echo wp_kses($post_navigation[$nav_type]['image'], array('img' => array('class' => true, 'src' => true, 'alt' => true))); ?>
							</div>
                            <?php } ?>
							<div class="mkdf-nav-blog-post-label-wrapper">
								<?php echo wp_kses($post_navigation[$nav_type]['label'], array('span' => array('class' => true))); ?>
								<h5 class="mkdf-blog-single-nav-title">
									<?php echo wp_kses($post_navigation[$nav_type]['title'], array('span' => array('class' => true))); ?>
								</h5>
							</div>
						</a>
					<?php }
				}
			}
			?>
		</div>
	</div>
<?php } ?>
