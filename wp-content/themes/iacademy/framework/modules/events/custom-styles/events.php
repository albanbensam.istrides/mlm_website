<?php

if(!function_exists('iacademy_mikado_events_custom_styles')) {
	/**
	 * Outputs custom styles for events
	 */
	function iacademy_mikado_events_custom_styles() {
	    if(iacademy_mikado_options()->getOptionValue('first_color') !== "") {
		    $color_selector = array(
				'.mkdf-tribe-events-single .mkdf-events-single-meta .mkdf-events-single-next-event a:hover',
				'.mkdf-tribe-events-single .mkdf-events-single-meta .mkdf-events-single-prev-event a:hover',
				'#tribe-events-content-wrapper .tribe-bar-views-list li.tribe-bar-active a',
				'#tribe-events-content-wrapper .tribe-bar-views-list li a:hover',
		        '#tribe-events-content-wrapper .tribe-events-sub-nav .tribe-events-nav-previous a:hover',
				'#tribe-events-content-wrapper .tribe-events-sub-nav .tribe-events-nav-next a:hover',
				'#tribe-events-content-wrapper .tribe-events-calendar td div[id*=tribe-events-daynum-] a:hover'
		    );

		    $color_important_selector = array(

		    );

		    $background_color_selector = array(
				'.mkdf-tribe-events-single .mkdf-events-single-main-info .mkdf-events-single-date-holder'
		    );

		    $background_color_important_selector = array(

		    );

		    $border_color_selector = array(
				'#tribe-events-content-wrapper .tribe-bar-filters input[type=text]:focus'
		    );

		    echo iacademy_mikado_dynamic_css($color_selector, array('color' => iacademy_mikado_options()->getOptionValue('first_color')));
		    echo iacademy_mikado_dynamic_css($color_important_selector, array('color' => iacademy_mikado_options()->getOptionValue('first_color').'!important'));
		    echo iacademy_mikado_dynamic_css('::selection', array('background' => iacademy_mikado_options()->getOptionValue('first_color')));
		    echo iacademy_mikado_dynamic_css('::-moz-selection', array('background' => iacademy_mikado_options()->getOptionValue('first_color')));
		    echo iacademy_mikado_dynamic_css($background_color_selector, array('background-color' => iacademy_mikado_options()->getOptionValue('first_color')));
		    echo iacademy_mikado_dynamic_css($background_color_important_selector, array('background-color' => iacademy_mikado_options()->getOptionValue('first_color').'!important'));
		    echo iacademy_mikado_dynamic_css($border_color_selector, array('border-color' => iacademy_mikado_options()->getOptionValue('first_color')));
	    }
    }

	add_action('iacademy_mikado_style_dynamic', 'iacademy_mikado_events_custom_styles');
}