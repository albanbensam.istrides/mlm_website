<?php
namespace MikadoCore\CPT\Shortcodes\EventsList;

use MikadoCore\CPT\Shortcodes\EventsList\EventsQuery\EventsQuery;
use MikadoCore\Lib;

class EventsList implements Lib\ShortcodeInterface {
	private $base;

	function __construct() {
		$this->base = 'mkdf_events_list';

		add_action('vc_before_init', array($this, 'vcMap'));
	}

	public function getBase() {
		return $this->base;
	}

	public function vcMap() {
		vc_map(array(
				'name'                      => esc_html__( 'Mikado Events List', 'iacademy' ),
				'base'                      => $this->getBase(),
				'category'                  => esc_html__( 'by MIKADO', 'iacademy' ),
				'icon'                      => 'icon-wpb-events-list extended-custom-icon',
				'allowed_container_element' => 'vc_row',
				'params'                    => array_merge(
					array(
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Number of Columns', 'iacademy' ),
							'param_name'  => 'columns',
							'value'       => array(
								''      => '',
								esc_html__( 'One' , 'iacademy' )   => 'one',
								esc_html__( 'Two'  , 'iacademy' )  => 'two',
								esc_html__( 'Three' , 'iacademy' ) => 'three',
								esc_html__( 'Four' , 'iacademy' )  => 'four',
								esc_html__( 'Five' , 'iacademy' )  => 'five',
								esc_html__( 'Six'  , 'iacademy' )  => 'six'
							),
							'admin_label' => true,
							'description' => esc_html__( 'Default value is Three', 'iacademy' ),
							'group'       => esc_html__( 'Layout Options', 'iacademy' )
						),
						array(
							'type'        => 'dropdown',
							'heading'     => esc_html__( 'Image Proportions', 'iacademy' ),
							'param_name'  => 'image_size',
							'value'       => array(
								esc_html__( 'Original'  , 'iacademy' )  => 'full',
								esc_html__( 'Square'  , 'iacademy' )    => 'square',
								esc_html__( 'Landscape'  , 'iacademy' ) => 'landscape',
								esc_html__( 'Portrait'  , 'iacademy' )  => 'portrait'
							),
							'save_always' => true,
							'admin_label' => true,
							'description' => '',
							'group'       => esc_html__( 'Layout Options', 'iacademy' )
						),
					),
					EventsQuery::getInstance()->queryVCParams()
				)
			)
		);
	}

	public function render($atts, $content = null) {
		$default_atts = array(
			'columns'    => '',
			'image_size' => ''
		);

		$eventsQuery = EventsQuery::getInstance();
		
		$default_atts = array_merge($default_atts, $eventsQuery->getShortcodeAtts());
		$params       = shortcode_atts($default_atts, $atts);

		$queryResults = $eventsQuery->buildQueryObject($params);

		$params['query']  = $queryResults;
		$params['caller'] = $this;

		$itemClass[] = 'mkdf-events-list-item';

		switch($params['columns']) {
			case 'one':
				$itemClass[] = 'mkdf-grid-col-12';
				break;
			case 'two':
				$itemClass[] = 'mkdf-grid-col-6';
				break;
			case 'three':
				$itemClass[] = 'mkdf-grid-col-4';
				break;
			case 'four':
				$itemClass[] = 'mkdf-grid-col-3';
				$itemClass[] = 'mkdf-grid-col-ipad-landscape-6';
				$itemClass[] = 'mkdf-grid-col-ipad-portrait-12';
				break;
			default:
				$itemClass[] = 'mkdf-grid-col-4';
				break;
		}

		$params['item_class'] = implode(' ', $itemClass);

		$params['image_size'] = $this->getImageSize($params);

		return iacademy_mikado_get_events_shortcode_module_template_part('templates/events-list-holder', 'events-list', '', $params);
	}

	public function getEventItemTemplate($params) {
		echo iacademy_mikado_get_events_shortcode_module_template_part('templates/events-list-item', 'events-list', '', $params);
	}

	private function getImageSize($params) {

		if(!empty($params['image_size'])) {
			$image_size = $params['image_size'];

			switch($image_size) {
				case 'landscape':
					$thumb_size = 'iacademy_mikado_landscape';
					break;
				case 'portrait':
					$thumb_size = 'iacademy_mikado_portrait';
					break;
				case 'square':
					$thumb_size = 'iacademy_mikado_square';
					break;
				case 'full':
					$thumb_size = 'full';
					break;
				case 'custom':
					$thumb_size = 'custom';
					break;
				default:
					$thumb_size = 'full';
					break;
			}

			return $thumb_size;
		}
	}
}