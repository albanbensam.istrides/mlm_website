<?php
$price_class = '';
if(tribe_get_cost(null, true)== 'Free') {
	$price_class = 'mkdf-free';
}
?>

<div <?php post_class($item_class); ?>>
	<div class="mkdf-events-list-item-holder">
		<?php if(has_post_thumbnail()) : ?>
			<div class="mkdf-events-list-item-image-holder">
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail($image_size); ?>

					<div class="mkdf-events-list-item-date-holder">
						<div class="mkdf-events-list-item-date-inner">
							<span class="mkdf-events-list-item-date-day">
								<?php echo tribe_get_start_date(null, true, 'd'); ?>
							</span>

							<span class="mkdf-events-list-item-date-month">
								<?php echo tribe_get_start_date(null, true, 'M'); ?>
							</span>
						</div>
					</div>
				</a>
			</div>
		<?php endif; ?>
		<div class="mkdf-events-list-item-content">
			<div class="mkdf-events-list-item-title-holder">
				<h4 class="mkdf-events-list-item-title">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h4>
				<span class="mkdf-events-list-item-price <?php echo esc_attr($price_class);?>">
					<?php echo esc_html(tribe_get_cost(null, true)); ?>
				</span>
			</div>
			<div class="mkdf-events-list-item-info">
				<div class="mkdf-events-list-item-date">
					<span class="mkdf-events-item-info-icon">
						<?php echo iacademy_mikado_icon_collections()->renderIcon('icon-clock', 'simple_line_icons'); ?>
					</span>
					<?php echo tribe_events_event_schedule_details(); ?>
				</div>
				<div class="mkdf-events-list-item-location-holdere">
					<span class="mkdf-events-item-info-icon">
						<?php echo iacademy_mikado_icon_collections()->renderIcon('icon-location-pin', 'simple_line_icons'); ?>
					</span>
					<span class="qpdef-events-list-item-location"><?php echo esc_html(tribe_get_address()); ?></span>
				</div>
			</div>
		</div>
	</div>
</div>