<?php

if(!function_exists('iacademy_mikado_map_woocommerce_meta')) {
    function iacademy_mikado_map_woocommerce_meta() {
        $woocommerce_meta_box = iacademy_mikado_create_meta_box(
            array(
                'scope' => array('product'),
                'title' => esc_html__('Product Meta', 'iacademy'),
                'name' => 'woo_product_meta'
            )
        );

        iacademy_mikado_create_meta_box_field(
            array(
                'name'          => 'mkdf_show_title_area_woo_meta',
                'type'          => 'select',
                'default_value' => '',
                'label'         => esc_html__('Show Title Area', 'iacademy'),
                'description'   => esc_html__('Disabling this option will turn off page title area', 'iacademy'),
                'parent'        => $woocommerce_meta_box,
                'options'       => iacademy_mikado_get_yes_no_select_array()
            )
        );
    }
	
    add_action('iacademy_mikado_meta_boxes_map', 'iacademy_mikado_map_woocommerce_meta', 99);
}