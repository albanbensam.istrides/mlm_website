<?php

if ( ! function_exists('iacademy_mikado_woocommerce_options_map') ) {

	/**
	 * Add Woocommerce options page
	 */
	function iacademy_mikado_woocommerce_options_map() {

		iacademy_mikado_add_admin_page(
			array(
				'slug' => '_woocommerce_page',
				'title' => esc_html__('Woocommerce', 'iacademy'),
				'icon' => 'fa fa-shopping-cart'
			)
		);

		/**
		 * Product List Settings
		 */
		$panel_product_list = iacademy_mikado_add_admin_panel(
			array(
				'page' => '_woocommerce_page',
				'name' => 'panel_product_list',
				'title' => esc_html__('Product List', 'iacademy')
			)
		);

		iacademy_mikado_add_admin_field(array(
			'name'        	=> 'mkdf_woo_product_list_columns',
			'type'        	=> 'select',
			'label'       	=> esc_html__('Product List Columns', 'iacademy'),
			'default_value'	=> 'mkdf-woocommerce-columns-4',
			'description' 	=> esc_html__('Choose number of columns for product listing and related products on single product', 'iacademy'),
			'options'		=> array(
				'mkdf-woocommerce-columns-3' => esc_html__('3 Columns', 'iacademy'),
				'mkdf-woocommerce-columns-4' => esc_html__('4 Columns', 'iacademy')
			),
			'parent'      	=> $panel_product_list,
		));
		
		iacademy_mikado_add_admin_field(array(
			'name'        	=> 'mkdf_woo_product_list_columns_space',
			'type'        	=> 'select',
			'label'       	=> esc_html__('Space Between Products', 'iacademy'),
			'default_value'	=> 'mkdf-woo-normal-space',
			'description' 	=> esc_html__('Select space between products for product listing and related products on single product', 'iacademy'),
			'options'		=> array(
				'mkdf-woo-normal-space' => esc_html__('Normal', 'iacademy'),
				'mkdf-woo-small-space'  => esc_html__('Small', 'iacademy'),
				'mkdf-woo-no-space'     => esc_html__('No Space', 'iacademy')
			),
			'parent'      	=> $panel_product_list,
		));
		
		iacademy_mikado_add_admin_field(array(
			'name'        	=> 'mkdf_woo_product_list_info_position',
			'type'        	=> 'select',
			'label'       	=> esc_html__('Product Info Position', 'iacademy'),
			'default_value'	=> 'info_below_image',
			'description' 	=> esc_html__('Select product info position for product listing and related products on single product', 'iacademy'),
			'options'		=> array(
				'info_below_image'    => esc_html__('Info Below Image', 'iacademy'),
				'info_on_image_hover' => esc_html__('Info On Image Hover', 'iacademy')
			),
			'parent'      	=> $panel_product_list,
		));

		iacademy_mikado_add_admin_field(array(
			'name'        	=> 'mkdf_woo_products_per_page',
			'type'        	=> 'text',
			'label'       	=> esc_html__('Number of products per page', 'iacademy'),
			'default_value'	=> '',
			'description' 	=> esc_html__('Set number of products on shop page', 'iacademy'),
			'parent'      	=> $panel_product_list,
			'args' 			=> array(
				'col_width' => 3
			)
		));

		iacademy_mikado_add_admin_field(array(
			'name'        	=> 'mkdf_products_list_title_tag',
			'type'        	=> 'select',
			'label'       	=> esc_html__('Products Title Tag', 'iacademy'),
			'default_value'	=> 'h5',
			'description' 	=> '',
			'options'       => iacademy_mikado_get_title_tag(),
			'parent'      	=> $panel_product_list,
		));

		/**
		 * Single Product Settings
		 */
		$panel_single_product = iacademy_mikado_add_admin_panel(
			array(
				'page' => '_woocommerce_page',
				'name' => 'panel_single_product',
				'title' => esc_html__('Single Product', 'iacademy')
			)
		);
		
			iacademy_mikado_add_admin_field(
				array(
					'type'          => 'select',
					'name'          => 'show_title_area_woo',
					'default_value' => '',
					'label'         => esc_html__( 'Show Title Area', 'iacademy' ),
					'description'   => esc_html__( 'Enabling this option will show title area on single post pages', 'iacademy' ),
					'parent'        => $panel_single_product,
					'options'       => iacademy_mikado_get_yes_no_select_array(),
					'args'          => array(
						'col_width' => 3
					)
				)
			);
			
			iacademy_mikado_add_admin_field(
				array(
					'name'          => 'mkdf_single_product_title_tag',
					'type'          => 'select',
					'default_value' => 'h3',
					'label'         => esc_html__( 'Single Product Title Tag', 'iacademy' ),
					'options'       => iacademy_mikado_get_title_tag(),
					'parent'        => $panel_single_product,
				)
			);
		
			iacademy_mikado_add_admin_field(
				array(
					'name'          => 'woo_set_thumb_images_position',
					'type'          => 'select',
					'default_value' => 'below-image',
					'label'         => esc_html__( 'Set Thumbnail Images Position', 'iacademy' ),
					'options'       => array(
						'below-image'  => esc_html__( 'Below Featured Image', 'iacademy' ),
						'on-left-side' => esc_html__( 'On The Left Side Of Featured Image', 'iacademy' )
					),
					'parent'        => $panel_single_product
				)
			);
		
			iacademy_mikado_add_admin_field(
				array(
					'type'          => 'select',
					'name'          => 'woo_enable_single_product_zoom_image',
					'default_value' => 'no',
					'label'         => esc_html__( 'Enable Zoom Maginfier', 'iacademy' ),
					'description'   => esc_html__( 'Enabling this option will show magnifier image on featured image hover', 'iacademy' ),
					'parent'        => $panel_single_product,
					'options'       => iacademy_mikado_get_yes_no_select_array( false ),
					'args'          => array(
						'col_width' => 3
					)
				)
			);
		
			iacademy_mikado_add_admin_field(
				array(
					'name'          => 'woo_set_single_images_behavior',
					'type'          => 'select',
					'default_value' => '',
					'label'         => esc_html__( 'Set Images Behavior', 'iacademy' ),
					'options'       => array(
						''             => esc_html__( 'No Behavior', 'iacademy' ),
						'pretty-photo' => esc_html__( 'Pretty Photo Lightbox', 'iacademy' ),
						'photo-swipe'  => esc_html__( 'Photo Swipe Lightbox', 'iacademy' )
					),
					'parent'        => $panel_single_product
				)
			);
	}

	add_action( 'iacademy_mikado_options_map', 'iacademy_mikado_woocommerce_options_map', 21);
}