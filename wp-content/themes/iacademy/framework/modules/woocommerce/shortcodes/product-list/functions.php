<?php
if(!function_exists('iacademy_mikado_add_product_list_shortcode')) {
	function iacademy_mikado_add_product_list_shortcode($shortcodes_class_name) {
		$shortcodes = array(
			'MikadoCore\CPT\Shortcodes\ProductList\ProductList',
		);

		$shortcodes_class_name = array_merge($shortcodes_class_name, $shortcodes);

		return $shortcodes_class_name;
	}

	if(iacademy_mikado_core_plugin_installed()) {
		add_filter('mkdf_core_filter_add_vc_shortcode', 'iacademy_mikado_add_product_list_shortcode');
	}
}