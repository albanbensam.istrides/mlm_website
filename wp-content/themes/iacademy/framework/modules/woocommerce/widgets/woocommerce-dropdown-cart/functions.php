<?php

if ( ! function_exists( 'iacademy_mikado_register_woocommerce_dropdown_cart_widget' ) ) {
	/**
	 * Function that register image gallery widget
	 */
	function iacademy_mikado_register_woocommerce_dropdown_cart_widget( $widgets ) {
		$widgets[] = 'IacademyMikadoWoocommerceDropdownCart';
		
		return $widgets;
	}
	
	add_filter( 'iacademy_mikado_register_widgets', 'iacademy_mikado_register_woocommerce_dropdown_cart_widget' );
}