<?php

if ( ! function_exists( 'iacademy_mikado_logo_meta_box_map' ) ) {
	function iacademy_mikado_logo_meta_box_map() {
		
		$logo_meta_box = iacademy_mikado_create_meta_box(
			array(
				'scope' => apply_filters( 'iacademy_mikado_set_scope_for_meta_boxes', array( 'page', 'post' ) ),
				'title' => esc_html__( 'Logo', 'iacademy' ),
				'name'  => 'logo_meta'
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_logo_image_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Default', 'iacademy' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'iacademy' ),
				'parent'      => $logo_meta_box
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_logo_image_dark_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Dark', 'iacademy' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'iacademy' ),
				'parent'      => $logo_meta_box
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_logo_image_light_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Light', 'iacademy' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'iacademy' ),
				'parent'      => $logo_meta_box
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_logo_image_sticky_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Sticky', 'iacademy' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'iacademy' ),
				'parent'      => $logo_meta_box
			)
		);
		
		iacademy_mikado_create_meta_box_field(
			array(
				'name'        => 'mkdf_logo_image_mobile_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Mobile', 'iacademy' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'iacademy' ),
				'parent'      => $logo_meta_box
			)
		);
	}
	
	add_action( 'iacademy_mikado_meta_boxes_map', 'iacademy_mikado_logo_meta_box_map', 47 );
}