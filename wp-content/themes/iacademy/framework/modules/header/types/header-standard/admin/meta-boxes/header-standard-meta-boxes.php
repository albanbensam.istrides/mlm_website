<?php

if ( ! function_exists( 'iacademy_mikado_get_hide_dep_for_header_standard_meta_boxes' ) ) {
	function iacademy_mikado_get_hide_dep_for_header_standard_meta_boxes() {
		$hide_dep_options = apply_filters( 'iacademy_mikado_header_standard_hide_meta_boxes', $hide_dep_options = array() );
		
		return $hide_dep_options;
	}
}

if ( ! function_exists( 'iacademy_mikado_header_standard_meta_map' ) ) {
	function iacademy_mikado_header_standard_meta_map( $parent ) {
		$hide_dep_options = iacademy_mikado_get_hide_dep_for_header_standard_meta_boxes();
		
		iacademy_mikado_create_meta_box_field(
			array(
				'parent'          => $parent,
				'type'            => 'select',
				'name'            => 'mkdf_set_menu_area_position_meta',
				'default_value'   => '',
				'label'           => esc_html__( 'Choose Menu Area Position', 'iacademy' ),
				'description'     => esc_html__( 'Select menu area position in your header', 'iacademy' ),
				'options'         => array(
					''       => esc_html__( 'Default', 'iacademy' ),
					'left'   => esc_html__( 'Left', 'iacademy' ),
					'right'  => esc_html__( 'Right', 'iacademy' ),
					'center' => esc_html__( 'Center', 'iacademy' )
				),
				'hidden_property' => 'mkdf_header_type_meta',
				'hidden_values'   => $hide_dep_options
			)
		);
	}
	
	add_action( 'iacademy_mikado_additional_header_area_meta_boxes_map', 'iacademy_mikado_header_standard_meta_map' );
}