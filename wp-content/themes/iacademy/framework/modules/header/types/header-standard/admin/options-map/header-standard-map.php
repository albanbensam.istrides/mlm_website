<?php

if ( ! function_exists( 'iacademy_mikado_get_hide_dep_for_header_standard_options' ) ) {
	function iacademy_mikado_get_hide_dep_for_header_standard_options() {
		$hide_dep_options = apply_filters( 'iacademy_mikado_header_standard_hide_global_option', $hide_dep_options = array() );
		
		return $hide_dep_options;
	}
}

if ( ! function_exists( 'iacademy_mikado_header_standard_map' ) ) {
	function iacademy_mikado_header_standard_map( $parent ) {
		$hide_dep_options = iacademy_mikado_get_hide_dep_for_header_standard_options();
		
		iacademy_mikado_add_admin_field(
			array(
				'parent'          => $parent,
				'type'            => 'select',
				'name'            => 'set_menu_area_position',
				'default_value'   => 'right',
				'label'           => esc_html__( 'Choose Menu Area Position', 'iacademy' ),
				'description'     => esc_html__( 'Select menu area position in your header', 'iacademy' ),
				'options'         => array(
					'right'  => esc_html__( 'Right', 'iacademy' ),
					'left'   => esc_html__( 'Left', 'iacademy' ),
					'center' => esc_html__( 'Center', 'iacademy' )
				),
				'hidden_property' => 'header_type',
				'hidden_values'   => $hide_dep_options
			)
		);
	}
	
	add_action( 'iacademy_mikado_additional_header_menu_area_options_map', 'iacademy_mikado_header_standard_map' );
}