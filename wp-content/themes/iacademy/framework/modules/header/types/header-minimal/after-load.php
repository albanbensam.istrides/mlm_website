<?php

if ( ! function_exists( 'iacademy_mikado_header_minimal_full_screen_menu_body_class' ) ) {
	/**
	 * Function that adds body classes for different full screen menu types
	 *
	 * @param $classes array original array of body classes
	 *
	 * @return array modified array of classes
	 */
	function iacademy_mikado_header_minimal_full_screen_menu_body_class( $classes ) {
		$classes[] = 'mkdf-' . iacademy_mikado_options()->getOptionValue( 'fullscreen_menu_animation_style' );
		
		return $classes;
	}
	
	if ( iacademy_mikado_check_is_header_type_enabled( 'header-minimal', iacademy_mikado_get_page_id() ) ) {
		add_filter( 'body_class', 'iacademy_mikado_header_minimal_full_screen_menu_body_class' );
	}
}

if ( ! function_exists( 'iacademy_mikado_get_header_minimal_full_screen_menu' ) ) {
	/**
	 * Loads fullscreen menu HTML template
	 */
	function iacademy_mikado_get_header_minimal_full_screen_menu() {
		$parameters = array(
			'fullscreen_menu_in_grid' => iacademy_mikado_options()->getOptionValue( 'fullscreen_in_grid' ) === 'yes' ? true : false
		);
		
		iacademy_mikado_get_module_template_part( 'templates/full-screen-menu', 'header/types/header-minimal', '', $parameters );
	}
	
	if ( iacademy_mikado_check_is_header_type_enabled( 'header-minimal', iacademy_mikado_get_page_id() ) ) {
		add_action( 'iacademy_mikado_after_header_area', 'iacademy_mikado_get_header_minimal_full_screen_menu', 10 );
	}
}