<?php

if ( ! function_exists( 'iacademy_mikado_register_header_minimal_type' ) ) {
	/**
	 * This function is used to register header type class for header factory file
	 */
	function iacademy_mikado_register_header_minimal_type( $header_types ) {
		$header_type = array(
			'header-minimal' => 'IacademyMikado\Modules\Header\Types\HeaderMinimal'
		);
		
		$header_types = array_merge( $header_types, $header_type );
		
		return $header_types;
	}
}

if ( ! function_exists( 'iacademy_mikado_init_register_header_minimal_type' ) ) {
	/**
	 * This function is used to wait header-function.php file to init header object and then to init hook registration function above
	 */
	function iacademy_mikado_init_register_header_minimal_type() {
		add_filter( 'iacademy_mikado_register_header_type_class', 'iacademy_mikado_register_header_minimal_type' );
	}
	
	add_action( 'iacademy_mikado_before_header_function_init', 'iacademy_mikado_init_register_header_minimal_type' );
}

if ( ! function_exists( 'iacademy_mikado_include_header_minimal_full_screen_menu' ) ) {
	/**
	 * Registers additional menu navigation for theme
	 */
	function iacademy_mikado_include_header_minimal_full_screen_menu( $menus ) {
		$menus['popup-navigation'] = esc_html__( 'Full Screen Navigation', 'iacademy' );
		
		return $menus;
	}
	
	if ( iacademy_mikado_check_is_header_type_enabled( 'header-minimal' ) ) {
		add_filter( 'iacademy_mikado_register_headers_menu', 'iacademy_mikado_include_header_minimal_full_screen_menu' );
	}
}

if ( ! function_exists( 'iacademy_mikado_register_header_minimal_full_screen_menu_widgets' ) ) {
	/**
	 * Registers additional widget areas for this header type
	 */
	function iacademy_mikado_register_header_minimal_full_screen_menu_widgets() {
		register_sidebar(
			array(
				'id'            => 'fullscreen_menu_above',
				'name'          => esc_html__( 'Fullscreen Menu Top', 'iacademy' ),
				'description'   => esc_html__( 'This widget area is rendered above full screen menu', 'iacademy' ),
				'before_widget' => '<div class="%2$s mkdf-fullscreen-menu-above-widget">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 class="mkdf-widget-title">',
				'after_title'   => '</h5>'
			)
		);
		
		register_sidebar(
			array(
				'id'            => 'fullscreen_menu_below',
				'name'          => esc_html__( 'Fullscreen Menu Bottom', 'iacademy' ),
				'description'   => esc_html__( 'This widget area is rendered below full screen menu', 'iacademy' ),
				'before_widget' => '<div class="%2$s mkdf-fullscreen-menu-below-widget">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 class="mkdf-widget-title">',
				'after_title'   => '</h5>'
			)
		);
	}
	
	if ( iacademy_mikado_check_is_header_type_enabled( 'header-minimal' ) ) {
		add_action( 'widgets_init', 'iacademy_mikado_register_header_minimal_full_screen_menu_widgets' );
	}
}