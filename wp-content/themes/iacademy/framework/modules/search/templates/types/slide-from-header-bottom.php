<div class="mkdf-slide-from-header-bottom-holder">
	<form action="<?php echo esc_url(home_url('/')); ?>" method="get">
	    <div class="mkdf-form-holder">
	        <input type="text" placeholder="<?php esc_attr_e('Search', 'iacademy'); ?>" name="s" class="mkdf-search-field" autocomplete="off" />
	        <button type="submit" class="mkdf-search-submit"><span class="icon_search "></span></button>
	    </div>
	</form>
</div>