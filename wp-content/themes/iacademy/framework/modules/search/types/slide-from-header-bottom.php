<?php

if( !function_exists('iacademy_mikado_search_body_class') ) {
    /**
     * Function that adds body classes for different search types
     *
     * @param $classes array original array of body classes
     *
     * @return array modified array of classes
     */
    function iacademy_mikado_search_body_class($classes) {
        $classes[] = 'mkdf-slide-from-header-bottom';

        return $classes;
    }

    add_filter('body_class', 'iacademy_mikado_search_body_class');
}

if ( ! function_exists('iacademy_mikado_get_search') ) {
    /**
     * Loads search HTML based on search type option.
     */
    function iacademy_mikado_get_search() {
        iacademy_mikado_load_search_template();
    }

    add_action( 'iacademy_mikado_before_page_header_html_close', 'iacademy_mikado_get_search');
	add_action( 'iacademy_mikado_before_mobile_header_html_close', 'iacademy_mikado_get_search');
}

if ( ! function_exists('iacademy_mikado_load_search_template') ) {
    /**
     * Loads search HTML based on search type option.
     */
    function iacademy_mikado_load_search_template() {
        iacademy_mikado_get_module_template_part('templates/types/slide-from-header-bottom', 'search');
    }
}