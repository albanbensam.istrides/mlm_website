<?php

if ( ! function_exists('iacademy_mikado_search_options_map') ) {

	function iacademy_mikado_search_options_map() {

		iacademy_mikado_add_admin_page(
			array(
				'slug' => '_search_page',
				'title' => esc_html__('Search', 'iacademy'),
				'icon' => 'fa fa-search'
			)
		);

		$search_page_panel = iacademy_mikado_add_admin_panel(
			array(
				'title' => esc_html__('Search Page', 'iacademy'),
				'name' => 'search_template',
				'page' => '_search_page'
			)
		);

        iacademy_mikado_add_admin_field(array(
            'name'        => 'search_page_layout',
            'type'        => 'select',
            'label'       => esc_html__('Layout', 'iacademy'),
            'default_value' => 'in-grid',
            'description' => esc_html__('Set layout. Default is in grid.', 'iacademy'),
            'parent'      => $search_page_panel,
            'options'     => array(
                'in-grid'    => esc_html__('In Grid', 'iacademy'),
                'full-width' => esc_html__('Full Width', 'iacademy')
            )
        ));

		iacademy_mikado_add_admin_field(array(
			'name'        => 'search_page_sidebar_layout',
			'type'        => 'select',
			'label'       => esc_html__('Sidebar Layout', 'iacademy'),
            'description' 	=> esc_html__("Choose a sidebar layout for search page", 'iacademy'),
            'default_value' => 'no-sidebar',
            'options'       => array(
                'no-sidebar'        => esc_html__('No Sidebar', 'iacademy'),
                'sidebar-33-right'	=> esc_html__('Sidebar 1/3 Right', 'iacademy'),
                'sidebar-25-right' 	=> esc_html__('Sidebar 1/4 Right', 'iacademy'),
                'sidebar-33-left' 	=> esc_html__('Sidebar 1/3 Left', 'iacademy'),
                'sidebar-25-left' 	=> esc_html__('Sidebar 1/4 Left', 'iacademy')
            ),
			'parent'      => $search_page_panel
		));

        $iacademy_custom_sidebars = iacademy_mikado_get_custom_sidebars();
        if(count($iacademy_custom_sidebars) > 0) {
            iacademy_mikado_add_admin_field(array(
                'name' => 'search_custom_sidebar_area',
                'type' => 'selectblank',
                'label' => esc_html__('Sidebar to Display', 'iacademy'),
                'description' => esc_html__('Choose a sidebar to display on search page. Default sidebar is "Sidebar"', 'iacademy'),
                'parent' => $search_page_panel,
                'options' => $iacademy_custom_sidebars,
				'args' => array(
					'select2' => true
				)
            ));
        }

		$search_panel = iacademy_mikado_add_admin_panel(
			array(
				'title' => esc_html__('Search', 'iacademy'),
				'name' => 'search',
				'page' => '_search_page'
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'select',
				'name'			=> 'search_icon_pack',
				'default_value'	=> 'font_elegant',
				'label'			=> esc_html__('Search Icon Pack', 'iacademy'),
				'description'	=> esc_html__('Choose icon pack for search icon', 'iacademy'),
				'options'		=> iacademy_mikado_icon_collections()->getIconCollectionsExclude(array('linea_icons'))
			)
		);

        iacademy_mikado_add_admin_field(
            array(
                'parent'		=> $search_panel,
                'type'			=> 'yesno',
                'name'			=> 'search_in_grid',
                'default_value'	=> 'yes',
                'label'			=> esc_html__('Enable Grid Layout', 'iacademy'),
                'description'	=> esc_html__('Set search area to be in grid. (Applied for Search covers header and Slide from Window Top types.', 'iacademy'),
            )
        );
		
		iacademy_mikado_add_admin_section_title(
			array(
				'parent' 	=> $search_panel,
				'name'		=> 'initial_header_icon_title',
				'title'		=> esc_html__('Initial Search Icon in Header', 'iacademy')
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'text',
				'name'			=> 'header_search_icon_size',
				'default_value'	=> '',
				'label'			=> esc_html__('Icon Size', 'iacademy'),
				'description'	=> esc_html__('Set size for icon', 'iacademy'),
				'args'			=> array(
					'col_width' => 3,
					'suffix'	=> 'px'
				)
			)
		);
		
		$search_icon_color_group = iacademy_mikado_add_admin_group(
			array(
				'parent'	=> $search_panel,
				'title'		=> esc_html__('Icon Colors', 'iacademy'),
				'description' => esc_html__('Define color style for icon', 'iacademy'),
				'name'		=> 'search_icon_color_group'
			)
		);
		
		$search_icon_color_row = iacademy_mikado_add_admin_row(
			array(
				'parent'	=> $search_icon_color_group,
				'name'		=> 'search_icon_color_row'
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent'	=> $search_icon_color_row,
				'type'		=> 'colorsimple',
				'name'		=> 'header_search_icon_color',
				'label'		=> esc_html__('Color', 'iacademy')
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent' => $search_icon_color_row,
				'type'		=> 'colorsimple',
				'name'		=> 'header_search_icon_hover_color',
				'label'		=> esc_html__('Hover Color', 'iacademy')
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent'		=> $search_panel,
				'type'			=> 'yesno',
				'name'			=> 'enable_search_icon_text',
				'default_value'	=> 'no',
				'label'			=> esc_html__('Enable Search Icon Text', 'iacademy'),
				'description'	=> esc_html__("Enable this option to show 'Search' text next to search icon in header", 'iacademy'),
				'args'			=> array(
					'dependence' => true,
					'dependence_hide_on_yes' => '',
					'dependence_show_on_yes' => '#mkdf_enable_search_icon_text_container'
				)
			)
		);
		
		$enable_search_icon_text_container = iacademy_mikado_add_admin_container(
			array(
				'parent'			=> $search_panel,
				'name'				=> 'enable_search_icon_text_container',
				'hidden_property'	=> 'enable_search_icon_text',
				'hidden_value'		=> 'no'
			)
		);
		
		$enable_search_icon_text_group = iacademy_mikado_add_admin_group(
			array(
				'parent'	=> $enable_search_icon_text_container,
				'title'		=> esc_html__('Search Icon Text', 'iacademy'),
				'name'		=> 'enable_search_icon_text_group',
				'description'	=> esc_html__('Define style for search icon text', 'iacademy')
			)
		);
		
		$enable_search_icon_text_row = iacademy_mikado_add_admin_row(
			array(
				'parent'	=> $enable_search_icon_text_group,
				'name'		=> 'enable_search_icon_text_row'
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_text_color',
				'label'			=> esc_html__('Text Color', 'iacademy')
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'colorsimple',
				'name'			=> 'search_icon_text_color_hover',
				'label'			=> esc_html__('Text Hover Color', 'iacademy')
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_icon_text_font_size',
				'label'			=> esc_html__('Font Size', 'iacademy'),
				'default_value'	=> '',
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row,
				'type'			=> 'textsimple',
				'name'			=> 'search_icon_text_line_height',
				'label'			=> esc_html__('Line Height', 'iacademy'),
				'default_value'	=> '',
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);
		
		$enable_search_icon_text_row2 = iacademy_mikado_add_admin_row(
			array(
				'parent'	=> $enable_search_icon_text_group,
				'name'		=> 'enable_search_icon_text_row2',
				'next'		=> true
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_icon_text_text_transform',
				'label'			=> esc_html__('Text Transform', 'iacademy'),
				'default_value'	=> '',
				'options'		=> iacademy_mikado_get_text_transform_array()
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'fontsimple',
				'name'			=> 'search_icon_text_google_fonts',
				'label'			=> esc_html__('Font Family', 'iacademy'),
				'default_value'	=> '-1',
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_icon_text_font_style',
				'label'			=> esc_html__('Font Style', 'iacademy'),
				'default_value'	=> '',
				'options'		=> iacademy_mikado_get_font_style_array(),
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row2,
				'type'			=> 'selectblanksimple',
				'name'			=> 'search_icon_text_font_weight',
				'label'			=> esc_html__('Font Weight', 'iacademy'),
				'default_value'	=> '',
				'options'		=> iacademy_mikado_get_font_weight_array(),
			)
		);
		
		$enable_search_icon_text_row3 = iacademy_mikado_add_admin_row(
			array(
				'parent'	=> $enable_search_icon_text_group,
				'name'		=> 'enable_search_icon_text_row3',
				'next'		=> true
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent'		=> $enable_search_icon_text_row3,
				'type'			=> 'textsimple',
				'name'			=> 'search_icon_text_letter_spacing',
				'label'			=> esc_html__('Letter Spacing', 'iacademy'),
				'default_value'	=> '',
				'args'			=> array(
					'suffix'	=> 'px'
				)
			)
		);
	}

	add_action('iacademy_mikado_options_map', 'iacademy_mikado_search_options_map', 5);
}