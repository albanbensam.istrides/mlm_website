<?php

if(!function_exists('iacademy_mikado_register_search_opener_widget')) {
	/**
	 * Function that register search opener widget
	 */
	function iacademy_mikado_register_search_opener_widget($widgets) {
		$widgets[] = 'IacademyMikadoSearchOpener';
		
		return $widgets;
	}
	
	add_filter('iacademy_mikado_register_widgets', 'iacademy_mikado_register_search_opener_widget');
}