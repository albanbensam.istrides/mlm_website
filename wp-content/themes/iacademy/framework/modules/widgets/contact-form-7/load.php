<?php

if(iacademy_mikado_contact_form_7_installed()) {
	include_once MIKADO_FRAMEWORK_MODULES_ROOT_DIR . '/widgets/contact-form-7/contact-form-7.php';
	add_filter('iacademy_mikado_register_widgets', 'iacademy_mikado_register_cf7_widget');
}

if(!function_exists('iacademy_mikado_register_cf7_widget')) {
	/**
	 * Function that register cf7 widget
	 */
	function iacademy_mikado_register_cf7_widget($widgets) {
		$widgets[] = 'IacademyMikadoContactForm7Widget';
		
		return $widgets;
	}
}