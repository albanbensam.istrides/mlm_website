<?php

if(!function_exists('iacademy_mikado_register_sidearea_opener_widget')) {
	/**
	 * Function that register sidearea opener widget
	 */
	function iacademy_mikado_register_sidearea_opener_widget($widgets) {
		$widgets[] = 'IacademyMikadoSideAreaOpener';
		
		return $widgets;
	}
	
	add_filter('iacademy_mikado_register_widgets', 'iacademy_mikado_register_sidearea_opener_widget');
}