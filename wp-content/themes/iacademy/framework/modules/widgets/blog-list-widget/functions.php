<?php

if(!function_exists('iacademy_mikado_register_blog_list_widget')) {
	/**
	 * Function that register blog list widget
	 */
	function iacademy_mikado_register_blog_list_widget($widgets) {
		$widgets[] = 'IacademyMikadoBlogListWidget';
		
		return $widgets;
	}
	
	add_filter('iacademy_mikado_register_widgets', 'iacademy_mikado_register_blog_list_widget');
}