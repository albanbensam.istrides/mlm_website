<?php

if(!function_exists('iacademy_mikado_register_icon_widget')) {
	/**
	 * Function that register icon widget
	 */
	function iacademy_mikado_register_icon_widget($widgets) {
		$widgets[] = 'IacademyMikadoIconWidget';
		
		return $widgets;
	}
	
	add_filter('iacademy_mikado_register_widgets', 'iacademy_mikado_register_icon_widget');
}