<?php

if(!function_exists('iacademy_mikado_register_raw_html_widget')) {
	/**
	 * Function that register raw html widget
	 */
	function iacademy_mikado_register_raw_html_widget($widgets) {
		$widgets[] = 'IacademyMikadoRawHTMLWidget';
		
		return $widgets;
	}
	
	add_filter('iacademy_mikado_register_widgets', 'iacademy_mikado_register_raw_html_widget');
}