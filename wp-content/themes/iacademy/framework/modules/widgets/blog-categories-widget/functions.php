<?php

if(!function_exists('iacademy_mikado_register_blog_categories_widget')) {
	/**
	 * Function that register blog list widget
	 */
	function iacademy_mikado_register_blog_categories_widget($widgets) {
		$widgets[] = 'IacademyMikadoBlogCategoriesWidget';
		
		return $widgets;
	}
	
	add_filter('iacademy_mikado_register_widgets', 'iacademy_mikado_register_blog_categories_widget');
}