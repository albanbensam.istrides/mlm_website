<?php

if(!function_exists('iacademy_mikado_register_image_widget')) {
	/**
	 * Function that register image widget
	 */
	function iacademy_mikado_register_image_widget($widgets) {
		$widgets[] = 'IacademyMikadoImageWidget';
		
		return $widgets;
	}
	
	add_filter('iacademy_mikado_register_widgets', 'iacademy_mikado_register_image_widget');
}