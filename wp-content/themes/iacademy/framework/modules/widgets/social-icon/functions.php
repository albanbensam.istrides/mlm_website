<?php

if(!function_exists('iacademy_mikado_register_social_icon_widget')) {
	/**
	 * Function that register social icon widget
	 */
	function iacademy_mikado_register_social_icon_widget($widgets) {
		$widgets[] = 'IacademyMikadoSocialIconWidget';
		
		return $widgets;
	}
	
	add_filter('iacademy_mikado_register_widgets', 'iacademy_mikado_register_social_icon_widget');
}