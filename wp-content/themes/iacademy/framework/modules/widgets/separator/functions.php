<?php

if(!function_exists('iacademy_mikado_register_separator_widget')) {
	/**
	 * Function that register separator widget
	 */
	function iacademy_mikado_register_separator_widget($widgets) {
		$widgets[] = 'IacademyMikadoSeparatorWidget';
		
		return $widgets;
	}
	
	add_filter('iacademy_mikado_register_widgets', 'iacademy_mikado_register_separator_widget');
}