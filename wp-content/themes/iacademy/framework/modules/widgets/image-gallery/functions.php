<?php

if(!function_exists('iacademy_mikado_register_image_gallery_widget')) {
	/**
	 * Function that register image gallery widget
	 */
	function iacademy_mikado_register_image_gallery_widget($widgets) {
		$widgets[] = 'IacademyMikadoImageGalleryWidget';
		
		return $widgets;
	}
	
	add_filter('iacademy_mikado_register_widgets', 'iacademy_mikado_register_image_gallery_widget');
}