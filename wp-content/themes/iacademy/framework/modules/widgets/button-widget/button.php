<?php

class IacademyMikadoButtonWidget extends IacademyMikadoWidget {
	public function __construct() {
		parent::__construct(
			'mkdf_button_widget',
			esc_html__('Mikado Button Widget', 'iacademy'),
			array( 'description' => esc_html__( 'Add button element to widget areas', 'iacademy'))
		);

		$this->setParams();
	}

	/**
	 * Sets widget options
	 */
	protected function setParams() {
		$this->params = array(
			array(
				'type'    => 'dropdown',
				'name'    => 'type',
				'title'   => esc_html__( 'Type', 'iacademy' ),
				'options' => array(
					'solid'   => esc_html__( 'Solid', 'iacademy' ),
					'outline' => esc_html__( 'Outline', 'iacademy' ),
					'simple'  => esc_html__( 'Simple', 'iacademy' )
				)
			),
			array(
				'type'        => 'dropdown',
				'name'        => 'size',
				'title'       => esc_html__( 'Size', 'iacademy' ),
				'options'     => array(
					'small'  => esc_html__( 'Small', 'iacademy' ),
					'medium' => esc_html__( 'Medium', 'iacademy' ),
					'large'  => esc_html__( 'Large', 'iacademy' ),
					'huge'   => esc_html__( 'Huge', 'iacademy' )
				),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'iacademy' )
			),
			array(
				'type'    => 'textfield',
				'name'    => 'text',
				'title'   => esc_html__( 'Text', 'iacademy' ),
				'default' => esc_html__( 'Button Text', 'iacademy' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'link',
				'title' => esc_html__( 'Link', 'iacademy' )
			),
			array(
				'type'    => 'dropdown',
				'name'    => 'target',
				'title'   => esc_html__( 'Link Target', 'iacademy' ),
				'options' => iacademy_mikado_get_link_target_array()
			),
			array(
				'type'  => 'textfield',
				'name'  => 'color',
				'title' => esc_html__( 'Color', 'iacademy' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'hover_color',
				'title' => esc_html__( 'Hover Color', 'iacademy' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'background_color',
				'title'       => esc_html__( 'Background Color', 'iacademy' ),
				'description' => esc_html__( 'This option is only available for solid button type', 'iacademy' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'hover_background_color',
				'title'       => esc_html__( 'Hover Background Color', 'iacademy' ),
				'description' => esc_html__( 'This option is only available for solid button type', 'iacademy' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'border_color',
				'title'       => esc_html__( 'Border Color', 'iacademy' ),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'iacademy' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'hover_border_color',
				'title'       => esc_html__( 'Hover Border Color', 'iacademy' ),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'iacademy' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'margin',
				'title'       => esc_html__( 'Margin', 'iacademy' ),
				'description' => esc_html__( 'Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'iacademy' )
			)
		);
	}

	/**
	 * Generates widget's HTML
	 *
	 * @param array $args args from widget area
	 * @param array $instance widget's options
	 */
	public function widget($args, $instance) {
		$params = '';

		if (!is_array($instance)) { $instance = array(); }

		// Filter out all empty params
		$instance = array_filter($instance, function($array_value) { return trim($array_value) != ''; });

		// Default values
		if (!isset($instance['text'])) { $instance['text'] = 'Button Text'; }

		// Generate shortcode params
		foreach($instance as $key => $value) {
			$params .= " $key='$value' ";
		}

		echo '<div class="widget mkdf-button-widget">';
			echo do_shortcode("[mkdf_button $params]"); // XSS OK
		echo '</div>';
	}
}