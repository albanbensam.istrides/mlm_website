<?php

if(!function_exists('iacademy_mikado_register_button_widget')) {
	/**
	 * Function that register button widget
	 */
	function iacademy_mikado_register_button_widget($widgets) {
		$widgets[] = 'IacademyMikadoButtonWidget';
		
		return $widgets;
	}
	
	add_filter('iacademy_mikado_register_widgets', 'iacademy_mikado_register_button_widget');
}