<?php

if ( ! function_exists('iacademy_mikado_like') ) {
	/**
	 * Returns IacademyMikadoLike instance
	 *
	 * @return IacademyMikadoLike
	 */
	function iacademy_mikado_like() {
		return IacademyMikadoLike::get_instance();
	}
}

function iacademy_mikado_get_like() {

	echo wp_kses(iacademy_mikado_like()->add_like(), array(
		'span' => array(
			'class' => true,
			'aria-hidden' => true,
			'style' => true,
			'id' => true
		),
		'i' => array(
			'class' => true,
			'style' => true,
			'id' => true
		),
		'a' => array(
			'href' => true,
			'class' => true,
			'id' => true,
			'title' => true,
			'style' => true
		)
	));
}