<?php

if ( ! function_exists('iacademy_mikado_sidearea_options_map') ) {

	function iacademy_mikado_sidearea_options_map() {

		iacademy_mikado_add_admin_page(
			array(
				'slug' => '_side_area_page',
				'title' => esc_html__('Side Area', 'iacademy'),
				'icon' => 'fa fa-indent'
			)
		);

		$side_area_panel = iacademy_mikado_add_admin_panel(
			array(
				'title' => esc_html__('Side Area', 'iacademy'),
				'name' => 'side_area',
				'page' => '_side_area_page'
			)
		);

		$side_area_icon_style_group = iacademy_mikado_add_admin_group(
			array(
				'parent' => $side_area_panel,
				'name' => 'side_area_icon_style_group',
				'title' => esc_html__('Side Area Icon Style', 'iacademy'),
				'description' => esc_html__('Define styles for Side Area icon', 'iacademy')
			)
		);

		$side_area_icon_style_row1 = iacademy_mikado_add_admin_row(
			array(
				'parent'	=> $side_area_icon_style_group,
				'name'		=> 'side_area_icon_style_row1'
			)
		);

		iacademy_mikado_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row1,
				'type' => 'colorsimple',
				'name' => 'side_area_icon_color',
				'label' => esc_html__('Color', 'iacademy')
			)
		);

		iacademy_mikado_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row1,
				'type' => 'colorsimple',
				'name' => 'side_area_icon_hover_color',
				'label' => esc_html__('Hover Color', 'iacademy')
			)
		);

		$side_area_icon_style_row2 = iacademy_mikado_add_admin_row(
			array(
				'parent'	=> $side_area_icon_style_group,
				'name'		=> 'side_area_icon_style_row2',
				'next'		=> true
			)
		);

		iacademy_mikado_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row2,
				'type' => 'colorsimple',
				'name' => 'side_area_close_icon_color',
				'label' => esc_html__('Close Icon Color', 'iacademy')
			)
		);

		iacademy_mikado_add_admin_field(
			array(
				'parent' => $side_area_icon_style_row2,
				'type' => 'colorsimple',
				'name' => 'side_area_close_icon_hover_color',
				'label' => esc_html__('Close Icon Hover Color', 'iacademy')
			)
		);

		iacademy_mikado_add_admin_field(
			array(
				'parent' => $side_area_panel,
				'type' => 'text',
				'name' => 'side_area_width',
				'default_value' => '',
				'label' => esc_html__('Side Area Width', 'iacademy'),
				'description' => esc_html__('Enter a width for Side Area', 'iacademy'),
				'args' => array(
					'col_width' => 3,
					'suffix' => 'px'
				)
			)
		);

		iacademy_mikado_add_admin_field(
			array(
				'parent' => $side_area_panel,
				'type' => 'color',
				'name' => 'side_area_background_color',
				'label' => esc_html__('Background Color', 'iacademy'),
				'description' => esc_html__('Choose a background color for Side Area', 'iacademy')
			)
		);
		
		iacademy_mikado_add_admin_field(
			array(
				'parent' => $side_area_panel,
				'type' => 'text',
				'name' => 'side_area_padding',
				'label' => esc_html__('Padding', 'iacademy'),
				'description' => esc_html__('Define padding for Side Area in format top right bottom left', 'iacademy'),
				'args' => array(
					'col_width' => 3
				)
			)
		);

		iacademy_mikado_add_admin_field(
			array(
				'parent' => $side_area_panel,
				'type' => 'selectblank',
				'name' => 'side_area_aligment',
				'default_value' => '',
				'label' => esc_html__('Text Alignment', 'iacademy'),
				'description' => esc_html__('Choose text alignment for side area', 'iacademy'),
				'options' => array(
					'' => esc_html__('Default', 'iacademy'),
					'left' => esc_html__('Left', 'iacademy'),
					'center' => esc_html__('Center', 'iacademy'),
					'right' => esc_html__('Right', 'iacademy')
				)
			)
		);
	}

	add_action('iacademy_mikado_options_map', 'iacademy_mikado_sidearea_options_map', 4);
}