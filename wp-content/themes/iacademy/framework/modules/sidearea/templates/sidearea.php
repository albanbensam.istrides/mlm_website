<section class="mkdf-side-menu">
	<div class="mkdf-close-side-menu-holder">
		<a class="mkdf-close-side-menu" href="#" target="_self">
			<?php echo iacademy_mikado_icon_collections()->renderIcon('icon_close', 'font_elegant'); ?>
		</a>
	</div>
	<div class="mkdf-side-area-holders">
		<div class="mkdf-side-menu-top">
			<?php if(is_active_sidebar('sidearea')) {
				dynamic_sidebar('sidearea');
			} ?>
		</div>
		<?php if(is_active_sidebar('sidearea-bottom')) { ?>
			<div class="mkdf-side-menu-bottom">
				<?php dynamic_sidebar('sidearea-bottom'); ?>
			</div>
		<?php } ?>
	</div>
</section>