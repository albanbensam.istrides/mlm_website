<?php /* Template Name: User Page IST */  ?>
<?php


$mkdf_sidebar_layout  = iacademy_mikado_sidebar_layout();

$site_url=do_shortcode('[site_url]');
$ret_data= do_shortcode('[my_page_call]');
$subscripe_plan_array=json_decode($ret_data,true);

get_header();
//iacademy_mikado_get_title();
get_template_part('slider');

//foreach ($_SESSION as $key => $value) {
//	echo $key."=".$value."<br>";
//}
//Woocommerce content
?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="/wp-content/plugins/wp-ist-system/css/bootstrap4.css"  >

<style>
  .bg-1{background-color:#4682b4;color:#fff;}
 
  .text-left{text-align:left;}
.project  h6.exp {
    font-style: normal;
    color: #6d6d6d!important;
	font-weight:500!important;
	margin-bottom:2px;
}

</style>

<div class="mkdf-title-holder mkdf-standard-with-breadcrumbs-type mkdf-has-bg-image  " style="height: 150px; background-color: rgb(255, 255, 255); background-image: url(&quot;/wp-content/uploads/2017/05/pages-title-parallax.jpg&quot;); background-position: center 0px;" data-height="250">
  <div class="mkdf-title-image">
    <img itemprop="image" src="/wp-content/uploads/2017/05/pages-title-parallax.jpg" alt="a">
  </div>
  <div class="mkdf-title-wrapper" style="height: 150px"">
    <div class="mkdf-title-inner">
      <div class="mkdf-grid">
        <div class="mkdf-title-info">
          <h2 class="mkdf-page-title entry-title">My Account</h2>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="mkdf-con tainer">

	<div class="mkdf-container-inner clearfix "  >
		<div class="mkdf-grid-row">





      <?php // include('navigation-cus.php');  ?>
      <div>
        <?php 
        $subscripe_id='';
        $user_id='';
        $subscription_master_id='-';
        $commision_data_structure_id='';
        $registeration_date='';
        $activation_date='';
        $elimination_date='';
        $closing_date='';
        $rejoin_date='';
        $rejected_date='';
        $max_user_count='';
        $total_approved_user='';
        $pending_users_count='';
        $referal_code='-';
        $refered_by_code='';
        $refered_by_user='-';
        $current_status='';
        $reason='';
        $random_subscribe_no="-";
        $subscription_master_name='N0';
        $payment_status='';
				//print_r($subscripe_plan_array);

        $error=false;
        if(isset($subscripe_plan_array)){ foreach ($subscripe_plan_array as $key => $value) {
         $$key=$value;
         ?>
        
         <?PHP
       } } 
       if(isset($payment_status) && ($subscripe_plan_array['payment_status']=='PAYMENT_PENDING' || $subscripe_plan_array['payment_status']=='PAYMENT_FAILURE')){
          $error=true;
          $current_status='Payment Cancelled / Failed';
          $tit_string= 'Registered On';
          $referal_code= 'NA';
        }else{
          $tit_string= 'Activated On';
        }

       ?>
     </div>
   </div>





<style>
 .f-black{color:#000!important;}
</style>


   <div class="bg-l ight py-5 service-1">
    <div class="con tainer">


      <!-- Row  -->

      <div class=" card">
        <a href="/subscription-details">
          <div class="card-header bg-1">Subscriptions </div>
        </a>
        <div class="card-body pt-4 project">
          <div class="ses-border">
            <div id="ses_d_34" class="row">

             <div class="col-sm-6 col-xs-12 d-flex">
              <div class="project-title d-flex align-items-center">
                <div class="text">		
				  <h6 class="exp">Subscription ID (<span style="color:#000;"><?php echo $current_status; ?></span>)</h6>
                  <h3 class="h4">   <?php if(!empty($random_subscribe_no)){ echo $random_subscribe_no; }else { echo "-"; } ?> <!-- <span class="cls-board">(<?php // echo $current_status; ?>)</span> --></h3>
                  
				  <h6 class="exp">Your Referal Code</h6>
				   
                  <h6 class="font-weight-bold   mb1-01"><span id="textToCopy" style="color:#000!important;"> <?php echo $referal_code; ?></span> 
			     <span   class="btn btn-outline-default btn-sm" style="padding:0;" id="copyButton" title="copy code"><i class="fa fa-copy"></i></span> 
                  <span id="copyResult"></span>
			 </h6>
                  
                   <?php // echo $current_status; ?>
				    <h6 class="exp">Plan</h6>
                  
                       <h6 class="exp" style="color:#000!important;">   <?php echo $subscription_master_name; ?></h6>
                                    
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-xs-12 resp-align">
              <h6 class="exp"><?php echo $tit_string;  ?></h6>
              <h6 class="exp-on"><?php if($activation_date!=""){ echo date('d-M-Y ', strtotime($activation_date)); }else { echo "-"; } ?></h6>
              <h6 class="exp">Refered by</h6>
			  
              <h6><span class="f-normal"><?php if(!empty($refered_by_user)){ echo $refered_by_user; }else { echo "-"; } ?></span> </h6>
              <div>
              <?php if($error){ ?>
                 <a  href="javascript:voide(0);" disabled="disabled" class="btn btn-info btn-sm" id="reg_form_w" data-id="34" data-limit="1" >View Details</a>
              <?php }else{ ?>  
                <a   href="/subscription-details" class="btn btn-info btn-sm" id="reg_form_w" data-id="34" data-limit="1" >View Details</a>
                <?php } ?>
              </div>

           </div>

         </div>
       </div>

     </div>
   </div>








</div>



</div>








</div>

</div>

  <script>
   const answer = document.getElementById("copyResult");
const copy   = document.getElementById("copyButton");
const selection = window.getSelection();
const range = document.createRange();
const textToCopy = document.getElementById("textToCopy")

copy.addEventListener('click', function(e) {
    range.selectNodeContents(textToCopy);
    selection.removeAllRanges();
    selection.addRange(range);
    const successful = document.execCommand('copy');
    if(successful){
      answer.innerHTML = 'Copied!';
	  
    } else {
      answer.innerHTML = 'Unable to copy!';  
    }
	
    window.getSelection().removeAllRanges();
	
	 
	jQuery("#copyResult").fadeIn("3000");
    setTimeout(function(){ jQuery("#copyResult").fadeOut("slow"); }, 3000);
});
 
	
	
	
	
	 
 </script>


<?php  echo do_shortcode('[profile_form_call]'); ?>
<?php get_footer(); ?>