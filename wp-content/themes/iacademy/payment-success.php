<?php /* Template Name: Payment success */  ?>
<?php


$mkdf_sidebar_layout  = iacademy_mikado_sidebar_layout();

$site_url=do_shortcode('[site_url]');
$ret_data= do_shortcode('[my_page_call]');
$subscripe_plan_array=json_decode($ret_data,true);

$ret_pay= do_shortcode('[ist_get_user_pay_info]');
$ret_pay_array=json_decode($ret_pay,true);
$razorpay_payment_id="-";
if(isset($ret_pay_array)){
	foreach ($ret_pay_array as $key => $value) {
		$$key = $value;
	}
}

get_header();
//iacademy_mikado_get_title();
get_template_part('slider');

//foreach ($_SESSION as $key => $value) {
//	echo $key."=".$value."<br>";
//}
//Woocommerce content
?>
 

<div class="mkdf-title-holder mkdf-standard-with-breadcrumbs-type mkdf-has-bg-image  " style="height: 250px; background-color: rgb(255, 255, 255); background-image: url(&quot;/wp-content/uploads/2017/05/pages-title-parallax.jpg&quot;); background-position: center 0px;" data-height="250">
  <div class="mkdf-title-image">
    <img itemprop="image" src="/wp-content/uploads/2017/05/pages-title-parallax.jpg" alt="a">
  </div>
  <div class="mkdf-title-wrapper" style="height: 250px">
    <div class="mkdf-title-inner">
      <div class="mkdf-grid">
        <div class="mkdf-title-info">
          <h2 class="mkdf-page-title entry-title">Payment Status</h2>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="mkdf-container" style="padding-bottom:150px;">

	<div class="mkdf-container-inner clearfix " style="padding-top:50px;">
		<div class="mkdf-grid-row">
		
		   <div class="mkdf-grid-col-3"> </div>
		   <div class="mkdf-grid-col-6" style="text-align:center;">
		    <h4><i class="fa fa-check payment-success"></i></h4>
		    <h3>Your payment is being processed successfully. Please find transaction id for your reference <span class="fa  payment-warning"> <?php echo $razorpay_payment_id; ?></span>. To goto your account, please click the button below.</h3>
		    <p>
		   <a href="user-dashboard"><button type="button" class="mkdf-btn mkdf-btn-medium mkdf-btn-solid mkdf-btn-custom-hover-bg">Go To My Account</button></a>
		   </p>
		   </div>
				 
		</div>
 
	 
</div>
	
	</div>
	
 
 
 			
<?php // echo do_shortcode('[profile_form_call]'); ?>
<?php get_footer(); ?>