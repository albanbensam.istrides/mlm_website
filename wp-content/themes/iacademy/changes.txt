1.5
- Added WooCommerce 3.9.0 compatibility
- Added The Events Calendar 5.0.0 compatibility
- Updated WPBakery Page Builder to 6.1
- Updated Revolution Slider to 6.1.7
- Updated Mikado Membership to 1.1
- Updated Mikado Core to 1.3
- Updated Mikado Instagram Feed plugin to 1.0.2
- Updated Mikado Twitter Feed plugin to 1.0.2
- Updated Mikado LMS plugin to 1.1
- Updated iAacademy child theme to 1.0.1
- Updated facebook login app to 5.0 version
- Fixed issue with course archive pages
- Fixed theme options import issue
- Fixed Twitter share issue
- Fixed OG description rendering for pages
- Fixed Blog List pagination issue
- Fixed issue when all the courses are displayed in instructor profile
- Fixed issue with plain permalinks
- Improved translation for Mikado plugins
- Improved page transitions
- Improved Fullscreen menu
- Improved single blog post navigation
- Removed unused file instagram-redirect.php from Mikado Instagram Feed plugin
- Removed unused file twitter-redirect.php from Mikado Twitter Feed plugin

1.4
- Added WooCommerce 3.6.1 compatibility
- Updated Mikado Core to 1.2
- Improved import functionality
- Improved theme security

1.3
- Added WooCommerce 3.5.7 compatibility
- Updated WPBakery Page Builder to 5.7
- Updated Revolution Slider to 5.4.8.3
- Updated Core plugin to 1.1
- Updated Instagram plugin to 1.0.1
- Updated Twitter plugin to 1.0.1
- Updated Membership plugin to 1.0.1
- Updated LMS plugin to 1.0.1
- Updated pot files
- Updated Child Theme
- Improved framework files

1.2
- Added WooCommerce 3.3.5 compatibility
- Added Envato Market plugin as required
- Updated Visual Composer to 5.4.7
- Updated Revolution Slider to 5.4.7.3
- Updated Mikado Core plugin to 1.0.1
- Updated Mikado LMS plugin to 1.0.1
- Fixed deprecated create_function
- Improved menu import functionality

1.1
- Updated Visual Composer to 5.2.1
- Set The Event Calendar as recommended plugin
- Fixed potential security issue when saving theme options

1.0.1
- Fixed bug with 404 page not loading scripts